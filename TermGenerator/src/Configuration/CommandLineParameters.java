package Configuration;

import org.kohsuke.args4j.Option;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 20/09/13
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */
public class CommandLineParameters {


    @Option(name="-user",usage="username for accessing Mysql")
    private String user;

    @Option(name="-pwd",usage="password for accessing Mysql")
    private String password;

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "user: "+user+"\t"+"password: "+password+"\t";    //To change body of overridden methods use File | Settings | File Templates.
    }
}
