package databaseMapping;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 25/09/13
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */
public class SubCategory extends Category {
    String subName;

    public SubCategory(String name, String subName, int tid) {
        super(name, tid);
        this.subName = subName;
    }

    public String getSubName() {
        return subName;
    }

    public String toString() {
        return getName() + "\t" + getSubName();
    }
}
