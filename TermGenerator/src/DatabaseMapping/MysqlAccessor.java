package databaseMapping;

import Configuration.ClassifierConfiguration;
import NLPUtils.Utils;
import org.apache.commons.configuration.ConfigurationException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 30/06/13
 * Time: 20:11
 * To change this template use File | Settings | File Templates.
 */
public class MysqlAccessor {
    private static String URL;
    private static String USER = null;
    private static String PASSWORD = null;
    private static String TERM_DB = null;
    private static String TERM_TABLE = null;
    private static String TERM_HIERARCHY_DB = null;
    private static String TERM_HIERARCHY_TABLE = null;
    private static String TERM_TABLE_TID = null;
    private static String TERM_TABLE_NAME = null;
    private static String TERM_HIERARCHY_DB_TID = null;
    private static String TERM_HIERARCHY_DB_PARENT = null;
    private static String TAXONOMY_LANGUAGE_DB = null;
    private static String TAXONOMY_LANGAUE_TABLE = null;
    private static String TERM_TABLE_VID = null;
    private static MysqlAccessor mysqlAccessor = null;
    private ArrayList<CategoryHierarchy> categoryHierarchyArrayList = null;
    private ArrayList<Category> taxonomyCategoryArrayList = null;
    private ArrayList<CategoryDictionary> categoryDictionaryList = null;

    private MysqlAccessor() {
        ClassifierConfiguration configuration = ClassifierConfiguration.getClassiferConfigurationInstance();
        System.out.println("Loading  database configuration FROM " + ClassifierConfiguration.CONFIG_PATH);
        TERM_DB = configuration.getTermDB();
        TERM_TABLE = configuration.getTermTable();
        TERM_HIERARCHY_DB = configuration.getTermHierarchyDB();
        TERM_HIERARCHY_TABLE = configuration.getTermHierarchyTable();
        TERM_TABLE_TID = configuration.getTermDatatid();
        TERM_TABLE_NAME = configuration.getTermDataName();
        URL = configuration.getUrl();
        TERM_HIERARCHY_DB_TID = configuration.getTermHierarchyDBTid();
        TERM_HIERARCHY_DB_PARENT = configuration.getTermHierarchyDBParent();
        TAXONOMY_LANGUAGE_DB = configuration.getTaxonomyLanguageDB();
        TAXONOMY_LANGAUE_TABLE = configuration.getTaxonomyLanguageTable();
        TERM_TABLE_VID = configuration.getTermDataVid();
        try {
            checkConfiguration();
        } catch (ConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        }
    }

    public static MysqlAccessor getMysqlAccessorSingleton() {
        if (mysqlAccessor == null) mysqlAccessor = new MysqlAccessor();
        return mysqlAccessor;
    }

    private static void close(ResultSet resultSet, Statement statement, Connection connect) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
        }
    }

    public void setPASSWORD(String PASSWORD) {
        MysqlAccessor.PASSWORD = PASSWORD;
    }

    public void setUSER(String USER) {
        MysqlAccessor.USER = USER;
    }

    public void checkConfiguration() throws ConfigurationException {
        if (TERM_DB.isEmpty())
            throw new ConfigurationException("term.db not set in eclap.termexpansion.properties file");
        if (TERM_TABLE.isEmpty())
            throw new ConfigurationException("term.table not set in eclap.termexpansion.properties file");
        if (TERM_HIERARCHY_DB.isEmpty())
            throw new ConfigurationException("term.hierarchy.db not set in eclap.termexpansion.properties file");
        if (TERM_HIERARCHY_TABLE.isEmpty())
            throw new ConfigurationException("term.hierarchy.table not set in eclap.termexpansion.properties file");
        if (URL.isEmpty()) throw new ConfigurationException("url  not set in eclap.termexpansion.properties file");
        if (TERM_TABLE_TID.isEmpty())
            throw new ConfigurationException("term.table.tid  not set in eclap.termexpansion.properties file");
        if (TERM_TABLE_NAME.isEmpty())
            throw new ConfigurationException("term.table.name  not set in eclap.termexpansion.properties file");
        if (TAXONOMY_LANGUAGE_DB.isEmpty())
            throw new ConfigurationException("taxonomy.language.db not set in eclap.termexpansion.properties file");
        if (TAXONOMY_LANGAUE_TABLE.isEmpty())
            throw new ConfigurationException("taxonomy.language.table not set in eclap.termexpansion.properties file");
        if (TERM_TABLE_VID.isEmpty())
            throw new ConfigurationException("term.table.vid not set in eclap.termexpansion.properties file");
    }

    public String getTraductionForTaxonomyTerm(String term, ClassifierLanguage language) {
        String traduction = null;
        ArrayList<CategoryDictionary> dictionaryArrayList = getCategoryDictionaryList();
        for (CategoryDictionary dictionary : dictionaryArrayList) {
            if (dictionary.contains(term)) traduction = dictionary.getTraduction(language.getLanguage());
        }
        return Utils.deAccentuate(traduction);
    }

    private void getTaxonomyCategories() {
        ArrayList<Category> taxonomycategories = new ArrayList<Category>();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + TERM_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            resultSet = statement
                    .executeQuery("SELECT " + TERM_TABLE_TID + "," + TERM_TABLE_NAME + " from " + TERM_DB + "." + TERM_TABLE + " where " + TERM_TABLE_VID + " =" + 5);
            while (resultSet.next()) {
                int tid = resultSet.getInt(1);
                String category_name = resultSet.getString(2).toLowerCase().trim();
                taxonomycategories.add(new Category(category_name.trim().toLowerCase(), tid));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);

        } finally {
            close(resultSet, statement, connect);
        }
        taxonomyCategoryArrayList = taxonomycategories;
    }

    public Category getCategoryFromName(String categoryName) {
        Category category = null;
        ArrayList<Category> taxonomyCategories = getTaxonomyCategoryArrayList();
        for (Category cat : taxonomyCategories) {
            if (cat.getName().equals(Utils.deAccentuate(categoryName))) {
                category = cat;
                break;
            }
        }
        return category;
    }

    public Category getCategoryfromTid(int tid) {
        Category category = null;
        ArrayList<Category> taxonomyCategories = getTaxonomyCategoryArrayList();
        for (Category cat : taxonomyCategories) {
            if (cat.getTid() == tid) {
                category = cat;
                break;
            }
        }
        return category;
    }

    public ArrayList<Category> getTaxonomyCategoryArrayList() {
        if (taxonomyCategoryArrayList == null) getTaxonomyCategories();
        return taxonomyCategoryArrayList;
    }

    private void verifyLanguageList() {
        ArrayList<String> languages = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList());
        for (String lang : languages) {
            ClassifierLanguage classifierConfiguration = new ClassifierLanguage(lang);
        }
    }

    private void getTaxonomyCategoriesTraduction() {
        ArrayList<CategoryDictionary> categoryDictionaries = new ArrayList<CategoryDictionary>();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + TAXONOMY_LANGUAGE_DB + "?useUnicode=true&characterEncoding=UTF-8&"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            verifyLanguageList();
            ArrayList<String> languages = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList());
            resultSet = statement.executeQuery("SELECT *" + " FROM " + TAXONOMY_LANGUAGE_DB + "." + TAXONOMY_LANGAUE_TABLE);
            while (resultSet.next()) {
                CategoryDictionary categoryDictionary = new CategoryDictionary();
                String englishTrad = resultSet.getString(ClassifierLanguage.english);
                assert englishTrad != null && englishTrad.isEmpty() == false;
                if (getCategoryFromName(englishTrad.trim().toLowerCase()) != null)
                {
                    for (String lang : languages) {
                        categoryDictionary.addEntry(resultSet.getString(lang).toLowerCase(), lang);
                    }
                    categoryDictionaries.add(categoryDictionary);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
        categoryDictionaryList = categoryDictionaries;
    }

    private void getCategorieHierarchy() {
        ArrayList<CategoryHierarchy> categoryHierarchy = new ArrayList<CategoryHierarchy>();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + TERM_HIERARCHY_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            resultSet = statement
                    .executeQuery("SELECT " + TERM_HIERARCHY_TABLE + "." + TERM_HIERARCHY_DB_TID + "," + TERM_HIERARCHY_DB_PARENT + " from " + TERM_HIERARCHY_DB + "." + TERM_HIERARCHY_TABLE + " join " + TERM_DB + "." + TERM_TABLE + " WHERE " + TERM_TABLE + "." + TERM_TABLE_TID + " = " + TERM_HIERARCHY_TABLE + "." + TERM_HIERARCHY_DB_TID + " AND " + TERM_TABLE + "." + TERM_TABLE_VID + " = 5");
            while (resultSet.next()) {
                int tid = resultSet.getInt(1);
                int parentTid = resultSet.getInt(2);
                assert getCategoryfromTid(tid) != null;
                if (parentTid != 0) {
                    assert getCategoryfromTid(parentTid) != null;
                    categoryHierarchy.add(new CategoryHierarchy(getCategoryfromTid(tid), getCategoryfromTid(parentTid)));
                } else categoryHierarchy.add(new CategoryHierarchy(getCategoryfromTid(tid), null));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
        categoryHierarchyArrayList = categoryHierarchy;
    }

    public Category getTopLevelCategory(Category category) throws Exception {
        Category ancestor = category;
        while (getParentFromName(ancestor.getName()) != null) {
            ancestor = getParentFromName(ancestor.getName());
        }
        return ancestor;
    }

    /*
    this method get you the direct up level category but not the top level one
    used toplevelcategory() instead
     */
    public Category getParentFromName(String categoryName) throws Exception {
        Category parent = null;
        Boolean found = false;
        for (CategoryHierarchy hierarchy : getCategoriesHierarchyArrayList()) {
            if (hierarchy.getSon().getName().equals(categoryName)) {
                parent = hierarchy.getParent();
                found = true;
                break;
            }
        }
        if (!found) throw new Exception(" name: " + categoryName + " was not founded in categoryHierarchy");
        return parent;
    }

    /*
   this method get you the direct up level category but not the top level one
   used toplevelcategory() instead
    */
    public Category getParentFromId(int tid) throws Exception {
        Category parent = null;
        Boolean found = false;
        for (CategoryHierarchy hierarchy : getCategoriesHierarchyArrayList()) {
            if (hierarchy.getSon().getTid() == tid) {
                parent = hierarchy.getParent();
                found = true;
                break;
            }
        }
        if (!found) throw new Exception(" tid: " + tid + " was not founded in categoryHierarchy");
        return parent;
    }

    private ArrayList<CategoryHierarchy> getCategoriesHierarchyArrayList() {
        if (categoryHierarchyArrayList == null) getCategorieHierarchy();
        return categoryHierarchyArrayList;
    }

    private ArrayList<CategoryDictionary> getCategoryDictionaryList() {
        if (categoryDictionaryList == null) getTaxonomyCategoriesTraduction();
        return categoryDictionaryList;
    }
}
