package databaseMapping;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 23/09/13
 * Time: 17:56
 * To change this template use File | Settings | File Templates.
 */
public class DictionaryEntry {
    private String entry;
    private ClassifierLanguage language;

    public DictionaryEntry(String entry, String lang) {
        language = new ClassifierLanguage(lang);
        this.entry = entry;
    }

    public String getEntry() {
        return entry;
    }

    public ClassifierLanguage getLanguage() {
        return language;
    }
}
