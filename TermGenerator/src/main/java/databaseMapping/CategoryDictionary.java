package databaseMapping;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 */
public class CategoryDictionary {
    private ArrayList<DictionaryEntry> dictionaryEntries = null;
    private static Logger logger = LogManager.getLogger();
    public CategoryDictionary() {
        dictionaryEntries = new ArrayList<DictionaryEntry>();
    }

    public boolean contains(String name) {
        boolean found = false;
        for (DictionaryEntry entry : dictionaryEntries) {
            if (entry.getEntry().equals(name)) found = true;
        }
        return found;
    }

    public void addEntry(String entry, String lang) {
        dictionaryEntries.add(new DictionaryEntry(entry, lang));
    }

    public String getTraduction(String language) {
        String traduction = null;
        boolean found = false;
        EclapLanguage eclapLanguage = new EclapLanguage(language);
        for (DictionaryEntry entry : dictionaryEntries) {
            if (eclapLanguage.getLanguage().equals(entry.getLanguage().getLanguage()))
                traduction = entry.getEntry();
            found = true;
        }
        if (found == false)
            logger.error("the " + eclapLanguage.getLanguage() + " traduction for " + getTraduction(EclapLanguage.english) + " was not found");
        return traduction;
    }

}
