package databaseMapping;

import NLPUtils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 02/07/13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */

public class Category {
    private static Logger logger = LogManager.getLogger();
    private String name;
    private int tid;

    public Category(String name, int tid) {
        this.name = Utils.deAccentuate(name);
        this.tid = tid;
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return getName();
    }


}
