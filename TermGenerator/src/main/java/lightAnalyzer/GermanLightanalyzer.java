package lightAnalyzer;

import NLPUtils.Utils;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.de.GermanNormalizationFilter;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.StopwordAnalyzerBase;
import org.apache.lucene.analysis.util.WordlistLoader;
import org.apache.lucene.util.IOUtils;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.io.Reader;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 30/09/13
 * Time: 13:55
 * To change this template use File | Settings | File Templates.
 */
public class GermanLightanalyzer extends StopwordAnalyzerBase {
    /**
     * @deprecated in 3.1, remove in Lucene 5.0 (index bw compat)
     */
    @Deprecated
    private final static String[] GERMAN_STOP_WORDS = {
            "einer", "eine", "eines", "einem", "einen",
            "der", "die", "das", "dass", "daß",
            "du", "er", "sie", "es",
            "was", "wer", "wie", "wir",
            "und", "oder", "ohne", "mit",
            "am", "im", "in", "aus", "auf",
            "ist", "sein", "war", "wird",
            "ihr", "ihre", "ihres",
            "als", "für", "von", "mit",
            "dich", "dir", "mich", "mir",
            "mein", "sein", "kein",
            "durch", "wegen", "wird"
    };

    /**
     * File containing default German stopwords.
     */
    public final static String DEFAULT_STOPWORD_FILE = "german_stop.txt";

    /**
     * Returns a set of default German-stopwords
     *
     * @return a set of default German-stopwords
     */
    public static final CharArraySet getDefaultStopSet() {
        return new CharArraySet(Version.LUCENE_CURRENT, Utils.createStopWordList(DefaultSetHolder.DEFAULT_SET), true);
    }

    private static class DefaultSetHolder {
        private static final CharArraySet DEFAULT_SET;

        static {
            try {
                DEFAULT_SET = WordlistLoader.getSnowballWordSet(IOUtils.getDecodingReader(SnowballFilter.class,
                        DEFAULT_STOPWORD_FILE, IOUtils.CHARSET_UTF_8), Version.LUCENE_CURRENT);
            } catch (IOException ex) {
                // default set should always be present as it is part of the
                // distribution (JAR)
                throw new RuntimeException("Unable to load default stopword set");
            }
        }
    }


    public GermanLightanalyzer(Version matchVersion) {
        super(matchVersion, DefaultSetHolder.DEFAULT_SET);
    }

    /**
     * Creates
     * {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
     * used to tokenize all the text in the provided {@link java.io.Reader}.
     *
     * @return {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
     *         built from a {@link org.apache.lucene.analysis.standard.StandardTokenizer} filtered with
     *         {@link org.apache.lucene.analysis.standard.StandardFilter}, {@link org.apache.lucene.analysis.core.LowerCaseFilter}, {@link org.apache.lucene.analysis.core.StopFilter}
     *         , {@link org.apache.lucene.analysis.miscellaneous.SetKeywordMarkerFilter} if a stem exclusion set is
     *         provided, {@link org.apache.lucene.analysis.de.GermanNormalizationFilter} and {@link org.apache.lucene.analysis.de.GermanLightStemFilter}
     */
    @Override
    protected TokenStreamComponents createComponents(String fieldName,
                                                     Reader reader) {
        final Tokenizer source = new WhitespaceTokenizer(matchVersion, reader);
        TokenStream result = new StandardFilter(matchVersion, source);
        result = new LowerCaseFilter(matchVersion, result);
        result = new StopFilter(matchVersion, result, getDefaultStopSet());
        result = new GermanNormalizationFilter(result);
        return new TokenStreamComponents(source, result);
    }
}
