import databaseMapping.Category;
import databaseMapping.EclapLanguage;
import databaseMapping.MysqlAccessor;
import NLPUtils.Utils;
import configuration.ClassifierConfiguration;
import configuration.CommandLineParameters;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 25/09/13
 * Time: 16:42
 * To change this template use File | Settings | File Templates.
 */

public class TermGenerate {
    private static Logger logger = LogManager.getLogger();
    private static ExecutorService pool = Executors.newFixedThreadPool(10);
    private static ArrayList<Category> categoryGenerated = new ArrayList<Category>();

    public static void main(String[] args) {
        Configurator.initialize(null, null, ClassifierConfiguration.CONFIG_FOLDER + "/log4j2.xml");
     //   args = new String[4];
     //   args[0] = "-u";
     //   args[1] = "root";
     //   args[2] = "-p";
     //   args[3] = "";
        CommandLineParameters parameters = new CommandLineParameters();
        CmdLineParser cmdLineParser = new CmdLineParser(parameters);
        try {
            cmdLineParser.parseArgument(args);
            ClassifierConfiguration.getClassiferConfigurationInstance().checkConfiguration();
        } catch (CmdLineException e) {
            logger.catching(e);
            cmdLineParser.printUsage(System.err);
            return;
        } catch (org.apache.commons.configuration.ConfigurationException e) {
            e.printStackTrace();
            logger.catching(e);
            return;//To change body of catch statement use File | Settings | File Templates.
        }
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(parameters.getUser());
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(parameters.getPassword());
        ArrayList<Category> taxonomyCategoryArrayList = MysqlAccessor.getMysqlAccessorSingleton().getTaxonomyCategoryArrayList();
        List<String> languages = ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList();
        for (String lang : languages)
        {
            categoryGenerated.clear();
            EclapLanguage eclapLanguage = new EclapLanguage(lang);
            CountDownLatch countDownLatch = new CountDownLatch(taxonomyCategoryArrayList.size());
            for (Category category : taxonomyCategoryArrayList) {
                pool.execute(generateTask(category,eclapLanguage,countDownLatch));
            }
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                logger.catching(e);
                e.printStackTrace();
                return;
            }
            ArrayList<String> catLines = new ArrayList<String>();
            for (Category category : categoryGenerated) {
                logger.debug("tags: {}",category.toString());
                catLines.add(category.toString());
            }
            logger.debug("trying to create the file");
            File resourcesFile = null;
            resourcesFile = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getConfigPath(),ClassifierConfiguration.getClassiferConfigurationInstance().getDocuments());
            resourcesFile.mkdirs();
            Utils.writeLargerTextFile(resourcesFile,lang, catLines);
        //      Utils.writeLargerTextFile(resourcesFile,lang,catLines);
            logger.debug("Document in " + eclapLanguage.getLanguage() + " created!");
        }
        pool.shutdown();
    }
    private static Runnable generateTask(final Category cat ,final  EclapLanguage eclapLanguage,final CountDownLatch countDownLatch)
    {
        return new Runnable() {
            private String traduction_name;
            @Override
            public void run() {
                try {
                    traduction_name = MysqlAccessor.getMysqlAccessorSingleton().getTraductionForTaxonomyTerm(cat.getName(), eclapLanguage);
                    ArrayList<String> list = Utils.tokenizeString(Utils.getCustomAnalyser4Language(eclapLanguage), traduction_name);
                    if (list.isEmpty()){
                        throw logger.throwing(new Exception(
                                "somethin went wrong tokenizing " + cat.getName() + "\t" + "language: " + eclapLanguage.getLanguage() + "... probably category term is a stopworld as well"));
                    }
                    else if (list.size() == 1){
                        addCategoryGenerated(traduction_name, cat.getTid());
                    }
                    else
                    {
                        //addCategoryGenerated(StringUtils.join(list," "),cat.getTid());
                        addCategoryGenerated(traduction_name, cat.getTid());

                    }
                }
                catch (Exception e) {
                    logger.catching(Level.WARN,e);
                    logger.warn(e.getMessage());
                    return;
                }finally {
                    countDownLatch.countDown();
                }
            }
        };
    }
    private synchronized static void addCategoryGenerated(String name ,int tid)
    {
        categoryGenerated.add(new Category(name, tid));
    }

}
