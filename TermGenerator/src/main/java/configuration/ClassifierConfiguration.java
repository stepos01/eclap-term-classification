package configuration;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Path;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 16/07/13
 * Time: 07:59
 * To change this template use File | Settings | File Templates.
 */
public class ClassifierConfiguration {
    private static Logger logger = LogManager.getLogger();
    private static final ClassifierConfiguration instance = new ClassifierConfiguration();
    private org.apache.commons.configuration.PropertiesConfiguration config = null;
    public static final String CONFIG_FOLDER = "TermGenerator/config";
    public static final String CONFIG_PATH = "/eclap.generator.properties";

    private ClassifierConfiguration() {
        File configFile = new File(getConfigPath(),"config/eclap.generator.properties");

    //    File configFile = new File(CONFIG_FOLDER,CONFIG_PATH);
        boolean bDone = false;
        if (configFile.exists()) {
            logger.debug("Loading " + configFile + " FROM " + configFile.getAbsolutePath());
            try {
                this.config = new PropertiesConfiguration(configFile);
                bDone = true;
            } catch (ConfigurationException ce) {
                logger.catching(ce);
                ce.printStackTrace();
            }
        }
        if (!bDone) {
            try {
                throw new ConfigurationException("the following configuration file  'eclap.generator.properties is' not found");
            } catch (ConfigurationException e) {
                logger.catching(e);
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void checkConfiguration() throws ConfigurationException {
        if (getLangagueList().isEmpty())
            logger.throwing( new ConfigurationException("languageList not set in eclap.generator.properties file"));
        if (getDocuments().isEmpty())
            logger.throwing( new ConfigurationException("output documents not set in eclap.generator.properties file"));
    }

    public static ClassifierConfiguration getClassiferConfigurationInstance() {
        return instance;
    }

    public String getConfigurationParameter(String parameter) {
        if (config == null) {
            logger.throwing(new AssertionError("the following configuration file  'eclap.generator.properties is' not found"));
        }
        return config.getString(parameter);
    }

    public String getDocuments() {
        return config.getString("documents");
    }

    public String getTermHierarchyDBTid() {
        return config.getString("term.hierarchy.db.tid");
    }

    public String getTermHierarchyDBParent() {
        return config.getString("term.hierarchy.db.parent");
    }

    public String getUrl() {
        return config.getString("db.url");
    }

    public String getTermDatatid() {
        return config.getString("term.table.tid");
    }

    public String getTermDataName() {
        return config.getString("term.table.name");
    }

    public String getTermDataVid() {
        return config.getString("term.table.vid");
    }

    public String getTermDB() {
        return config.getString("term.db");
    }

    public String getTermTable() {
        return config.getString("term.table");
    }

    public String getTermHierarchyDB() {
        return config.getString("term.hierarchy.db");
    }

    public String getTermHierarchyTable() {
        return config.getString("term.hierarchy.table");
    }

    public String getTaxonomyLanguageDB() {
        return config.getString("taxonomy.language.db");
    }

    public String getTaxonomyLanguageTable() {
        return config.getString("taxonomy.language.table");
    }

    public List getLangagueList() {
        return config.getList("taxonomy.language.table.languages");
    }

    public List getStopWordExclusion() {
        return config.getList("stop.word.exclusion");
    }
    public String getConfigPath()
    {
        String decodedPath = null;

        try {
            String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();

            String jarpath = URLDecoder.decode(path, "UTF-8");
            File jarfile = new File(jarpath);
            decodedPath = jarfile.getParent();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return decodedPath;
    }

}
