import Configuration.ClassifierConfiguration;
import Configuration.CommandLineParameters;
import databaseMapping.Category;
import databaseMapping.ClassifierLanguage;
import databaseMapping.MysqlAccessor;
import databaseMapping.SubCategory;
import NLPUtils.Utils;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 25/09/13
 * Time: 16:42
 * To change this template use File | Settings | File Templates.
 */
public class TermGenerate {
    public static void main(String[] args) {
        args = new String[4];
        args[0] = "-user";
        args[1] = "root";
        args[2] = "-pwd";
        args[3] = "";
        CommandLineParameters parameters = new CommandLineParameters();
        CmdLineParser cmdLineParser = new CmdLineParser(parameters);
        try {
            cmdLineParser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java -jar Classifier.jar [options... ] arguments...");
            cmdLineParser.printUsage(System.err);
            return;
        }
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(parameters.getUser());
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(parameters.getPassword());
        ArrayList<Category> taxonomyCategoryArrayList = MysqlAccessor.getMysqlAccessorSingleton().getTaxonomyCategoryArrayList();
        List<String> languages = ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList();
        for (String lang : languages) {
            ArrayList<Category> categoryGenerated = new ArrayList<Category>();
            ClassifierLanguage classifierLanguage = new ClassifierLanguage(lang);
            for (Category category : taxonomyCategoryArrayList) {
                String name = category.getName();
                name = MysqlAccessor.getMysqlAccessorSingleton().getTraductionForTaxonomyTerm(name, classifierLanguage);
                ArrayList<String> list = Utils.tokenizeString(Utils.getCustomAnalyser4Language(classifierLanguage), name);
                if (list.isEmpty()) try {
                    throw new Exception("somethin went wrong tokenizing " + name + "\t" + "language: " + lang + "... probably category term is a stopworld as well");
                } catch (Exception e) {
                    e.printStackTrace();
                    categoryGenerated.add(new Category(name, category.getTid()));
                }
                else {
                    if (list.size() == 1)
                        categoryGenerated.add(new Category(name, category.getTid()));
                    else {
                        categoryGenerated.add(new Category(Utils.encodeCategoryName(name), category.getTid()));
                        for (String part : list)
                            categoryGenerated.add(new SubCategory(StringUtils.join(list, " "), part, category.getTid()));
                    }
                }
            }
            ArrayList<String> catLines = new ArrayList<String>();
            ArrayList<String> subLines = new ArrayList<String>();
            for (Category category : categoryGenerated) {
                if (category instanceof SubCategory) {
                    subLines.add(((SubCategory) category).toString());
                } else {
                    catLines.add(category.toString());
                }
            }
            String file = Utils.createDocument(classifierLanguage.getLanguage());
            String tagPath = file + "/" + Utils.TAGS;
            String partPath = file + "/" + Utils.PART;
            Utils.writeLargerTextFile(partPath, subLines);
            Utils.writeLargerTextFile(tagPath, catLines);
            System.out.println("Document in " + classifierLanguage.getLanguage().toUpperCase() + " created!");
        }


    }
}
