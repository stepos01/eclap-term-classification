package NLPUtils; /**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/06/13
 * Time: 01:09
 * To change this template use File | Settings | File Templates.
 */

import Configuration.ClassifierConfiguration;
import databaseMapping.ClassifierLanguage;
import LightAnalyzer.*;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;


public class Utils {
    public static Charset ENCODING = StandardCharsets.UTF_8;
    public static final String PART = "PARTS";
    public static final String TAGS = "TAGS";

    public static void writeLargerTextFile(String aFileName, List<String> aLines) {
        try {
            Path path = Paths.get(aFileName);
            BufferedWriter writer = Files.newBufferedWriter(path, ENCODING);
            writer.write("");
            for (String line : aLines) {
                writer.write(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> tokenizeString(Analyzer analyzer, String string) {
        ArrayList<String> result = new ArrayList<String>();
        try {
            TokenStream stream = analyzer.tokenStream(null, new StringReader(string));
            stream.reset();
            while (stream.incrementToken()) {
                result.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
        } catch (IOException e) {
            // not thrown b/c we're using a string reader...
            throw new RuntimeException(e);
        }
        return result;
    }

    public static String encodeCategoryName(String categoryName) {
        return categoryName.replaceAll(" ", "_");
    }

    public static ArrayList<String> createStopWordList(CharArraySet set) {
        ArrayList<String> stopWords = new ArrayList<String>();
        List<String> stopWordExlusion = ClassifierConfiguration.getClassiferConfigurationInstance().getStopWordExclusion();
        Iterator iter = set.iterator();
        stopWords.add("&");
        while (iter.hasNext()) {
            char[] stopWord = (char[]) iter.next();
            String word = new String(stopWord);
            if (!stopWordExlusion.contains(word)) stopWords.add(word);
        }
        return stopWords;
    }

    public static Analyzer getCustomAnalyser4Language(ClassifierLanguage language) {
        Analyzer analyzer = null;
        if (language.getLanguage().equals(ClassifierLanguage.french))
            analyzer = new FrenchLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.english))
            analyzer = new EnglishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.italian))
            analyzer = new ItalianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.portuguese))
            analyzer = new PortugueseLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.polish))
            analyzer = new PolishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.dutch))
            analyzer = new DutchLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.spanish))
            analyzer = new SpanishLightAnalyser(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.hungarian))
            analyzer = new HungarianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.german))
            analyzer = new GermanLightanalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.danish))
            analyzer = new DanishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.catalan))
            analyzer = new CatalanLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.slovene))
            analyzer = new SlovenianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.greek))
            analyzer = new GreekLightAnalyser(Version.LUCENE_44);

        return analyzer;
    }

    public static String deAccentuate(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

    public static String createDocument(String path) {
        File file = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocuments(), path);
        if (!file.exists()) file.mkdirs();
       /* File parts = new File(file,PART);
        File tags = new File(file,TAGS);
        try {
            if (!parts.exists())parts.createNewFile();
            if (!tags.exists())tags.createNewFile();
        }catch (IOException e)
        {
            e.printStackTrace();
        }*/
        return file.getPath();
    }


}










