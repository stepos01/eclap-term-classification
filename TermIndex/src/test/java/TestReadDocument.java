import IO.DocumentMapper;
import IO.IndexesCreator;
import configuration.ClassifierConfiguration;
import databaseEntity.Txt_Category;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.core.config.Configurator;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 26/02/2014
 * Time: 12:09
 * To change this template use File | Settings | File Templates.
 */
public class TestReadDocument {
    public static void main(String[] args) {
        try {
            Configurator.initialize(null, null, ClassifierConfiguration.CONFIG_FOLDER + "log4j2.xml");
            ObjectArrayList<Txt_Category> it = DocumentMapper.loadDocument4Traduction("it");
            for (Txt_Category category : it) {
                System.out.println(category.toString());
            }
            IndexesCreator indexesCreator = new IndexesCreator();
            indexesCreator.createLuceneIndex(it,"it");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
