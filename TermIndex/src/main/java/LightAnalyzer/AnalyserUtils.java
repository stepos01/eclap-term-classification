package lightAnalyzer; /**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/06/13
 * Time: 01:09
 * To change this template use File | Settings | File Templates.
 */

import databaseEntity.EclapLanguage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.ca.CatalanAnalyzer;
import org.apache.lucene.analysis.da.DanishAnalyzer;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.analysis.el.GreekAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.es.SpanishAnalyzer;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.analysis.hu.HungarianAnalyzer;
import org.apache.lucene.analysis.it.ItalianAnalyzer;
import org.apache.lucene.analysis.nl.DutchAnalyzer;
import org.apache.lucene.analysis.pl.PolishAnalyzer;
import org.apache.lucene.analysis.pt.PortugueseAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;


public class AnalyserUtils {
    private static String[] SLOVENIAN_DEFAULT_STOPWORDS = {"h", "in", "je", "k", "onega", "onem", "onemu", "onim", "ono", "v", "&"};
    private static Logger logger = LogManager.getLogger();

    public static ArrayList<String> tokenizeString(Analyzer analyzer, String string) {
        ArrayList<String> result = new ArrayList<String>();
        try {
            TokenStream stream = analyzer.tokenStream(null, new StringReader(string));
            stream.reset();
            while (stream.incrementToken()) {
                result.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
        } catch (IOException e) {
            // not thrown b/c we're using a string reader...
            throw new RuntimeException(e);
        }
        return result;
    }


    public static Analyzer getLuceneAnalyser(EclapLanguage language) {
        Analyzer analyser = null;
        try {
            if (language.getLanguage().equals("fr")) analyser = new FrenchAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("en")) analyser = new EnglishAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("it")) analyser = new ItalianAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("de")) analyser = new GermanAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("da")) analyser = new DanishAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("nl")) analyser = new DutchAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("hu")) analyser = new HungarianAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("pt")) analyser = new PortugueseAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("es")) analyser = new SpanishAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("ca")) analyser = new CatalanAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("pl")) analyser = new PolishAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("el")) analyser = new GreekAnalyzer(Version.LUCENE_45);
            else if (language.getLanguage().equals("sl"))
                return new StandardAnalyzer(Version.LUCENE_45, new CharArraySet(Version.LUCENE_45, Arrays.asList(SLOVENIAN_DEFAULT_STOPWORDS), true));
            else
                throw new RuntimeException("Analyser for the required language : " + language.getLanguage() + "  not found");
        } catch (RuntimeException e) {
            logger.catching(e);
            System.exit(1);
        }
        return analyser;
    }


}











