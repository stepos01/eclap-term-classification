package configuration;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 16/07/13
 * Time: 07:59
 * To change this template use File | Settings | File Templates.
 */
public class ClassifierConfiguration {
    private static ClassifierConfiguration instance = null;
    private static Logger logger = LogManager.getLogger();
    private PropertiesConfiguration config = null;
    public static final String CONFIG_FOLDER = "TermIndex/config/";
    public static final String CONFIG_PATH = "/eclap.index.properties";

    private ClassifierConfiguration() {
       // File configFile = LoadConfigFile(CONFIG_PATH);
        File configFile = new File(getConfigPath(),"config/eclap.index.properties");
        boolean bDone = false;
        if (configFile.exists()) {
            logger.debug("Loading " + configFile + " FROM " + configFile.getPath());
            try {
                this.config = new PropertiesConfiguration(configFile);
                bDone = true;
            } catch (ConfigurationException ce) {
                ce.printStackTrace();
            }
        }
        if (!bDone) {
            try {
                throw logger.throwing(new ConfigurationException("the following configuration file  eclap.index.properties is not found"));
            } catch (ConfigurationException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public void checkConfiguration() throws ConfigurationException {
        //TODO
        if (getLuceneIndexes().isEmpty())
            throw new ConfigurationException("luceneindexes not set in eclap.index.properties file");
        if (getStopWords().isEmpty())
            throw new ConfigurationException("stopwords not set in eclap.index.properties file");
    }

    public static ClassifierConfiguration getClassiferConfigurationInstance() {
        if (instance == null) instance = new ClassifierConfiguration();
        return instance;
    }

    public String getConfigurationParameter(String parameter) {
        if (config == null) {
            throw new AssertionError("the following configuration file  eclap.index.properties is' not found");
        }
        return config.getString(parameter);
    }




    public String getLuceneIndexes() {
        return config.getString("lucene.indexes");
    }

    public String getStopWords() {
        return config.getString("stopwords");
    }

    public List getLangagueList() {
        return config.getList("taxonomy.language.table.languages");
    }
    private File LoadConfigFile(String path)
    {

        return new File(getClass().getResource(path).getPath());

    }
    public String getDocumentSRC() {
        return config.getString("document.src");
    }
    public String getConfigPath()
    {
        String decodedPath = null;

        try {
            String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();

            String jarpath = URLDecoder.decode(path, "UTF-8");
            File jarfile = new File(jarpath);
            decodedPath = jarfile.getParent();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return decodedPath;
    }



}
