import IO.DocumentMapper;
import IO.IndexesCreator;
import configuration.ClassifierConfiguration;
import configuration.CommandLineParameters;
import databaseEntity.Txt_Category;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 25/02/2014
 * Time: 17:25
 * To change this template use File | Settings | File Templates.
 */
public class indexText {
    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        Configurator.initialize(null, null,new File(ClassifierConfiguration.getClassiferConfigurationInstance().getConfigPath(),"config/log4j2.xml").getAbsolutePath());


        logger.debug("Entering eclap index application");
        CommandLineParameters parameters = new CommandLineParameters();
        CmdLineParser cmdLineParser = new CmdLineParser(parameters);
        try {
            cmdLineParser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java -jar Classifier.jar [options... ] arguments...");
            cmdLineParser.printUsage(System.err);
            return;
        }
        ExecutorService pool = Executors.newFixedThreadPool(5);
        File src = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocumentSRC());
        List<String> languages = ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList();
        CountDownLatch countDownLatch = new CountDownLatch(languages.size());
        for (String lang : languages) {
            File document_lang = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocumentSRC(), lang);
            if (document_lang.exists())
                pool.execute(generateWriterThread(lang, countDownLatch));
            else{
                logger.error("no tag file found for language {}", lang);
                System.exit(-1);
                }
        }
        pool.shutdown();
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            logger.catching(e);
        }

    }

    private static Runnable generateWriterThread(final String lang, final CountDownLatch countDownLatch) {
        return new Runnable() {
            public void run() {
                try {
                    logger.debug("loading tags for language {}", lang);
                    ObjectArrayList<Txt_Category> txt_categories = DocumentMapper.loadDocument4Traduction(lang);
                    IndexesCreator indexesCreator = new IndexesCreator();
                    indexesCreator.createLuceneIndex(txt_categories,lang);
                } catch (IOException e) {
                    logger.catching(e);
                    Thread.currentThread().interrupt();
                    logger.error("failed to create lucene index for language {}", lang);
                } finally {
                    countDownLatch.countDown();
                    logger.debug("successfully created lucene index for language {}", lang);
                }
            }
        };
    }
}
