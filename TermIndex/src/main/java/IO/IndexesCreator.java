package IO;

import databaseEntity.CategoryTags;
import lightAnalyzer.AnalyserUtils;
import configuration.ClassifierConfiguration;
import databaseEntity.EclapLanguage;
import databaseEntity.Txt_Category;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 13/07/13
 * Time: 14:05
 * To change this template use File | Settings | File Templates.
 */

public class IndexesCreator {
    private static Logger logger = LogManager.getLogger();
    private IndexWriter writer;
    private Directory directory;

    public enum fieldUsed {
        tags,
        category_title,
        tokenized_title,
        tagCount,
    }

    public IndexesCreator() {
    }

    public void createLuceneIndex(ObjectArrayList<Txt_Category> txt_categories,String lang) {
        try {
            File luceneIndex = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getConfigPath(),ClassifierConfiguration.getClassiferConfigurationInstance().getLuceneIndexes() + lang);
            if (!luceneIndex.exists()) luceneIndex.mkdirs();
            else deletePreviousLuceneIndex(luceneIndex.getPath());
            directory = FSDirectory.open(luceneIndex);
            Map<String, Analyzer> analyzerPerField = new HashMap<String, Analyzer>();
            analyzerPerField.put(fieldUsed.tagCount.name(), new KeywordAnalyzer());
            analyzerPerField.put(fieldUsed.category_title.name(),new KeywordAnalyzer());
            PerFieldAnalyzerWrapper wrapper = new PerFieldAnalyzerWrapper(AnalyserUtils.getLuceneAnalyser(new EclapLanguage(lang)), analyzerPerField);
            IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_45, wrapper);
            writer = new IndexWriter(directory, config);
            addCategories2Index(txt_categories);

        } catch (IOException e) {
            logger.catching(e);
            logger.debug("could not open lucene index ");
            System.exit(-1);
        } finally {
            try {
                writer.close();
                directory.close();
            } catch (IOException e) {
                logger.catching(e);
                logger.error("Failed to close lucene doc");
                System.exit(-1);
            }
        }

    }


    private void deletePreviousLuceneIndex(String path) {
        for (File f : new File(path).listFiles()) {
            if (f.isDirectory() && !f.isHidden()) deletePreviousLuceneIndex(f.getPath());
            else f.delete();
        }
        logger.debug("rewriting index in {}", path);


    }

    private void addCategories2Index(ObjectArrayList<Txt_Category> documentsCategories) {
        for (Txt_Category documentCategory : documentsCategories) {
            String categoryName = documentCategory.getName();
            addDoc(categoryName, documentCategory.getCategoryTags());

        }
    }

    private void addDoc(String title, CategoryTags categoryTags) {
        try {
            ObjectArrayList<Document> documents = new ObjectArrayList<Document>();
            if (categoryTags!=null&&!categoryTags.isEmpty())
            {
                for (Map.Entry<String, Integer> entry : categoryTags.getTags().entrySet()) {
                    Document tagDoc = new Document();
                    tagDoc.add(new TextField(fieldUsed.tags.name(), entry.getKey(), Field.Store.YES));
                    tagDoc.add(new IntField(fieldUsed.tagCount.name(), entry.getValue(), Field.Store.YES));
                    tagDoc.add(new StringField(fieldUsed.category_title.name(),title, Field.Store.YES));
                    documents.add(tagDoc);
                }
            }

            Document titleDoc = new Document();
            titleDoc.add(new StringField(fieldUsed.category_title.name(), title, Field.Store.YES));
            titleDoc.add(new TextField(fieldUsed.tokenized_title.name(), title, Field.Store.YES));
            documents.add(titleDoc);
            writer.addDocuments(documents);
        } catch (IOException e) {
            logger.catching(e);
            logger.error("Failed to write in lucene doc");
            System.exit(-1);
        }

    }

}
