package IO;

import configuration.ClassifierConfiguration;
import databaseEntity.CategoryTags;
import databaseEntity.EclapLanguage;
import databaseEntity.Txt_Category;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/02/2014
 * Time: 13:53
 * To change this template use File | Settings | File Templates.
 */

public class DocumentMapper {

    public static Charset ENCODING = StandardCharsets.UTF_8;
    private static Logger logger = LogManager.getLogger();

    public static ObjectArrayList<Txt_Category> loadDocument4Traduction(String lang) throws IOException {
        File dest = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocumentSRC(),lang);
        ObjectArrayList<Txt_Category> categories = new ObjectArrayList<Txt_Category>();
        if (!dest.exists()) {
            throw logger.throwing(Level.ERROR, new IOException("error using file: " + dest.getPath()));
        } else {
            ObjectArrayList<String> list = loadBigFile(dest.getPath());
            for (String line : list) {
                categories.add(parseTextLine(line, lang));
            }
            return categories;
        }
    }

    public static Txt_Category parseTextLine(String line, String lang) {
        CategoryTags categoryTags = new CategoryTags(new EclapLanguage(lang));
        String[] block = line.split("\t");
        String name = block[0];
        if (block.length == 1) return new Txt_Category(name);
        String[] tags = block[1].split("\\|");
        for (String tag : tags) {
            try {
                String tagPart1 = tag.split(",")[0];
                tagPart1 = tagPart1.replace("{", "").trim();
                String tagPart2 = tag.split(",")[1];
                tagPart2 = tagPart2.replace("}", "").trim();
                Integer count = Integer.valueOf(tagPart2);
                for (int i = 0; i < count; i++) {
                    categoryTags.addTag(tagPart1);
                }
            }   catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        return new Txt_Category(name, categoryTags);
    }

    public static ObjectArrayList<String> loadBigFile(String path) {
        ObjectArrayList<String> objectArrayList = new ObjectArrayList<String>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            while (line != null) {
                objectArrayList.add(line);
                line = br.readLine();
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
        return objectArrayList;
    }

}
