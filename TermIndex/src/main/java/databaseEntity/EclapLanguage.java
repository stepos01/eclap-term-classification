package databaseEntity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 14/09/13
 * Time: 22:22
 * To change this template use File | Settings | File Templates.
 */
public class EclapLanguage {
    private static Logger logger = LogManager.getLogger();
    public final static int LANGUAGE_COUNT = 13;
    public final static String italian = "it";
    public final static String italian_alternative = "ita";
    public final static String french = "fr";
    public final static String catalan = "ca";
    public final static String english = "en";
    public final static String hungarian = "hu";
    public final static String polish = "pl";
    public final static String dutch = "nl";
    public final static String spanish = "es";
    public final static String portuguese = "pt";
    public final static String greek = "el";
    public final static String danish = "da";
    public final static String german = "de";
    public final static String slovene = "sl";
    public final static String slovene_alternative = "slo";
    private String language;

    public EclapLanguage(String language) {
        this.language = convert(language);
    }

    public static String convert(String language) {
        String result = null;
        try {
            if (language.equals(italian)) result = italian;
            else if (language.equals(italian_alternative)) result = italian;
            else if (language.equals(french)) result = french;
            else if (language.equals(catalan)) result = catalan;
            else if (language.equals(english)) result = english;
            else if (language.equals(hungarian)) result = hungarian;
            else if (language.equals(polish)) result = polish;
            else if (language.equals(dutch)) result = dutch;
            else if (language.equals(spanish)) result = spanish;
            else if (language.equals(portuguese)) result = portuguese;
            else if (language.equals(greek)) result = greek;
            else if (language.equals(danish)) result = danish;
            else if (language.equals(german)) result = german;
            else if (language.equals(slovene)) result = slovene;
            else if (language.equals(slovene_alternative)) return slovene;
            else throw new ParseException("language not found: " + language, 0);
        } catch (ParseException e) {
            logger.catching(e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return result;
    }

    public String getLanguage() {
        return language;
    }

    public String toString() {
        return "defLanguage: " + getLanguage();
    }

    public boolean equals(Object eclapLanguage) {
        if (this.language.equals(((EclapLanguage) eclapLanguage).getLanguage())) return true;
        else return false;
    }


}
