package databaseEntity;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.commons.lang.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/09/13
 * Time: 17:04
 * To change this template use File | Settings | File Templates.
 */
public class CategoryTags {
    private EclapLanguage language;
    private Object2IntOpenHashMap<String> tags = new Object2IntOpenHashMap<String>();

    public CategoryTags(EclapLanguage language) {
        this.language = language;
    }

    public EclapLanguage getLanguage() {
        return language;
    }

    public Object2IntOpenHashMap<String> getTags() {
        return tags;
    }

    public void addTag(String _tag) {
        String tag = _tag.toLowerCase().trim();
        assert tag != null && !tag.isEmpty();
        if (tags.containsKey(tag)) {
            tags.addTo(tag, 1);
        } else tags.put(tag, 1);
    }
    public boolean isEmpty()
    {
        return tags.isEmpty();
    }
    public String toString() {
        ObjectArrayList<String> list = new ObjectArrayList<String>();
        for (Object2IntMap.Entry<String> entry : tags.object2IntEntrySet()) {
            list.add("{" + entry.getKey() + "," + entry.getIntValue() + "}");
        }
        return StringUtils.join(list, "|");
    }
}
