import IO.IndexesCreator;
import configuration.ClassifierConfiguration;
import databaseEntity.EclapLanguage;
import databaseEntity.Txt_Category;
import databaseMapping.MysqlAccessor;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/06/2014
 * Time: 14:05
 * To change this template use File | Settings | File Templates.
 */
public class AnalyseAccuracy_wiki {
    public static void main(String[]args)
    {
        File expansionfolder = new File(Utils.termexpansion_resource_folder);
        for (File f : expansionfolder.listFiles())
        {
            if (!f.isHidden()){
            generate_index_base_on_wikisoglia(f.getPath());
            calcul_pre_rec_base_on_wikisoglia(String.valueOf(f.getName().charAt(f.getName().length() - 1)));
                String indexpath = ClassifierConfiguration.getClassiferConfigurationInstance().getLuceneIndexes()+"en";
            }
        }
    }

    public static void  generate_index_base_on_wikisoglia(String indexsourcepath)  {
        try {
            File indextxtfile = new File(indexsourcepath);
            if (!indextxtfile.exists()) throw new FileNotFoundException();
            ObjectArrayList<Txt_Category> categories = Utils.loadDocument(indexsourcepath);
            IndexesCreator indexesCreator = new IndexesCreator();
            indexesCreator.createLuceneIndex(categories, EclapLanguage.english);
        }catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }catch (IOException e)
        {
            e.printStackTrace();
        }

    }
    public static void calcul_pre_rec_base_on_wikisoglia(String soglia)
    {
        ArrayList<GroundTruthEntity> list = null;
        ArrayList<String> precisionLines = new ArrayList<String>();
        ArrayList<String> recallLines = new ArrayList<String>();
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(Utils.PASSWORD);
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(Utils.USER);
        try
        {
            list  = Utils.readGroundtruthFile();
            list = Utils.removeNonEnglishEntries(list);

            ClassifierConfiguration.getClassiferConfigurationInstance().setAccuracytAccuracy(0.00f);
            Utils.deleteLinesTable();
            float precisionList = 0;
            float recallList = 0;

            for (GroundTruthEntity entity : list)
            {
                Float[] floats = Utils.calculatePrecision_Recall(entity);
                precisionList=precisionList+floats[0];
                recallList=recallList+floats[1];
            }
            String precisionLine = "precision: " +"\t"+precisionList/list.size();
            String recallLine = "recall:" + "\t"+recallList/list.size();
            precisionLines.add(precisionLine.replace(".",","));
            recallLines.add(recallLine.replace(".",","));


        }catch (Exception e)
        {
            e.printStackTrace();
        }
        System.out.println("soglia:" +"\t"+soglia);
        System.out.println(precisionLines);
        System.out.println(recallLines);



    }
}