import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 28/05/2014
 * Time: 20:31
 * To change this template use File | Settings | File Templates.
 */
public class GroundTruthEntity {
    private String axoid;
    private ArrayList<Category> categories = new ArrayList<Category>();

    public String getAxoid() {
        return axoid;
    }

    public void setAxoid(String axoid) {
        this.axoid = axoid;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void addCategories(Category category) {
        this.categories.add(category);
    }
}
