import IO.DocumentMapper;
import configuration.ClassifierConfiguration;
import databaseEntity.EclapLanguage;
import databaseEntity.Txt_Category;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.Level;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/06/2014
 * Time: 11:11
 * To change this template use File | Settings | File Templates.
 */
public class termSize_soglia_filtering {
    public static void main(String[]args)
    {
        ArrayList<String> means = new ArrayList<String>();

        try {
        File expansionfolder = new File(Utils.termexpansion_resource_folder);
        for (File f: expansionfolder.listFiles())
        {
            if (!f.isHidden())
            {
                ObjectArrayList<Txt_Category> categories = Utils.loadDocument(f.getPath());
                float mean = 0f;
                for (Txt_Category category : categories)
                {

                    if (category.getCategoryTags()!=null)mean = mean + category.getCategoryTags().getTags().size();
                }
                mean = mean /categories.size();
                means.add(f.getPath().charAt(f.getPath().length()-1)+"\t"+String.valueOf(mean).replace(".",","));
            }

        }
        File f = new File(Utils.groundtruth_resourcefolter);
            if (!f.exists())f.mkdirs();
            File f1 = new File(f,"mean_filtering.txt");
            if (!f1.exists())f1.createNewFile();
            Utils.writeLargerTextFile(f1.getPath(),means);

        }catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
