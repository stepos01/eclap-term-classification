/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 02/07/13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */

public class Category {
    private String name;
    private int tid;






    public Category(int tid) {
        this.tid = tid;
    }

    public Category(String name, int tid) {
        this.tid = tid;
        this.name = convertCategoryName(name);
    }

    public Category(String name) {
        this.name = convertCategoryName(name);
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public String getName() {
        return name;
    }


    public static String convertCategoryName(String categoryName) {
        return Utils.deAccentuate(categoryName).trim().toLowerCase();
    }


}
