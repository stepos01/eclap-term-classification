import configuration.ClassifierConfiguration;
import databaseMapping.MysqlAccessor;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 19/06/2014
 * Time: 14:38
 * To change this template use File | Settings | File Templates.
 */
public class AnalyseAccuracy {

    public static void main(String[]args)
    {
        calcul_pre_rec_baseon_taxononomysoglia();

    }

    public static Callable<Float[]> generateCallable(final GroundTruthEntity entity,final CountDownLatch latch,final int index)
    {
        return new Callable<Float[]>() {
            public Float[] call() throws Exception {
                try {
                    return  Utils.calculatePrecision_Recall(entity);
                }finally {
                    latch.countDown();
                    //  System.out.println("Done with entity: "+index+"...");

                }
            }
        };
    }

    public static void calcul_pre_rec_baseon_taxononomysoglia()
    {
        ArrayList<GroundTruthEntity> list = null;
        ArrayList<String> precisionLines = new ArrayList<String>();
        ArrayList<String> recallLines = new ArrayList<String>();
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(Utils.PASSWORD);
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(Utils.USER);
        try
        {
            list  = Utils.readGroundtruthFile();

            for (float i = 0.00f;i<0.1f;i=i+0.01f)
            {
                ClassifierConfiguration.getClassiferConfigurationInstance().setAccuracytAccuracy(i);
                Utils.deleteLinesTable();
                float precisionList = 0;
                float recallList = 0;

                System.out.println("\n soglia: "+i);

                for (GroundTruthEntity entity : list)
                {
                    Float[] floats = Utils.calculatePrecision_Recall(entity);
                    precisionList=precisionList+floats[0];
                    recallList=recallList+floats[1];
                }
                String precisionLine = i +"\t"+precisionList/list.size();
                String recallLine = i + "\t"+recallList/list.size();
                precisionLines.add(precisionLine.replace(".",","));
                recallLines.add(recallLine.replace(".",","));
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }

        Utils.writeLargerTextFile(Utils.path2,precisionLines);
        Utils.writeLargerTextFile(Utils.path3,recallLines);

    }
}
