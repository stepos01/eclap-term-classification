import IO.DocumentMapper;
import databaseEntity.Txt_Category;
import databaseMapping.Dcmi;
import databaseMapping.EclapLanguage;
import databaseMapping.MysqlAccessor;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import searchUtils.Classifier.ClassifierQueue;
import searchUtils.Classifier.ClassifierWriter;
import searchUtils.Classifier.SearchThread;
import searchUtils.MyLatch;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.text.Normalizer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 27/05/2014
 * Time: 18:26
 * To change this template use File | Settings | File Templates.
 */
public class Utils {
    public static final String path = "/Users/stephanekamga/Documents/DocumentCreator/GroundTruth/src/main/resources/groundthruth.txt";
    public static final String groundtruth_data = "/Users/stephanekamga/Documents/DocumentCreator/GroundTruth/src/main/resources/groundthruth_data.txt";
    public static final String termexpansion_resource_folder = "resources/TermExpansion/";
    public static final String path2 = "/Users/stephanekamga/Documents/DocumentCreator/GroundTruth/src/main/resources/pre.txt";
    public static final String path3 = "/Users/stephanekamga/Documents/DocumentCreator/GroundTruth/src/main/resources/rec.txt";
    public static final String groundtruth_resourcefolter = "resources/GroundTruth/";
    public static final String DBURL = "localhost";
    public static final String AXDBV4_DB_NAME = "axdbv4";
    public static final String DRUPAL_DB_NAME = "drupal";
    public static  final String TERM_DATA_DB_TABLE = "term_data" ;
    public static final String DCMI_DB_TABLE = "dcmi";
    public static final String CACHE_INDEX_DB_TABLE = "cache_index";
    public static final String TAXONOMY_LINK_DB_TABLE = "taxonomy_link";
    public static final String NAME = "name";
    public static final String VID = "vid";
    public static final String TID = "tid";

    public static final String USER = "root";
    public static final String PASSWORD = "";
    public static final String AXOID = "AXOID";




    public static String deAccentuate(String str) {
        if (str == null) {
            throw new AssertionError("accentremover String to should not be null ");
        }
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

    public static ArrayList<GroundTruthEntity>readGroundtruthFile() throws ParseException {
          ArrayList<GroundTruthEntity> list = new ArrayList<GroundTruthEntity>();
          ArrayList<String> axoids = new ArrayList<String>();
        ArrayList<String> delete = new ArrayList<String>();

        ArrayList<String> lines = loadBigFile(path);
        for (String s : lines)
        {
            GroundTruthEntity entity = check(s) ;
           if (entity != null)
           {
                if (!axoids.contains(entity.getAxoid()))   {
                    list.add(entity);
                    axoids.add(entity.getAxoid());   }
               else {
                    delete.add(entity.getAxoid());
                }
           }else
           {
             //  throw new ParseException(s,0);
           }
        }
        if (!delete.isEmpty())System.err.println("delete the following axoids: "+delete.toString());
        return  list;
    }
    public static GroundTruthEntity  check(String target)
    {
        String[] split = target.split("\t");
        String axoid = split[0];
        GroundTruthEntity entity = new GroundTruthEntity();
       if (!doAxoidExists(axoid))return null;
        else entity.setAxoid(axoid);
        for (int i= 1;i< split.length;i++)
        {
            int id =  doCategoryExist(split[i]);
            if (id == 0)return null;
            else entity.addCategories(new Category(split[i],id));
        }
        return entity;
    }

    public static ArrayList<String> loadBigFile(String path) {
        ArrayList<String> list = new ArrayList<String>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            while (line != null) {
                list.add(line);
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    private static boolean doAxoidExists(String target) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        boolean result = false;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + AXDBV4_DB_NAME + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT count(*) from " + DCMI_DB_TABLE + " WHERE " + AXOID + " = '" + target + "'");
            while (resultSet.next()) {
                int count = resultSet.getInt(1);
                result = count > 0;
            }
            connection.close();
            statement.close();
            resultSet.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return  result;
    }

    public static int doCategoryExist(String target)
    {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        int result = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + DRUPAL_DB_NAME + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * from " + TERM_DATA_DB_TABLE + " WHERE " + NAME + " = '" + target + "' AND "+VID+" = 5");
            while (resultSet.next()) {
                result = resultSet.getInt(1);
            }
            connection.close();
            statement.close();
            resultSet.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return  result;
    }
    public static ArrayList<Integer> getClassifierResultForAxoid(String axoid)
    {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ArrayList<Integer> result = new ArrayList<Integer>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + CACHE_INDEX_DB_TABLE + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connection.prepareStatement("SELECT "+TID+" from " + TAXONOMY_LINK_DB_TABLE + " WHERE " + AXOID+" =  ? ");
            statement.setString(1,axoid);
            resultSet = statement.executeQuery();
            while (resultSet.next()&&resultSet.getInt(1)!= 0) {
               result.add(resultSet.getInt(1));
            }
            connection.close();
            statement.close();
            resultSet.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return  result;

    }
    public  static void classifyThisAxoids(String axoid,final CountDownLatch latch)
    {
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(PASSWORD);
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(USER);
        Dcmi target = null;
            ObjectBigArrayBigList<Dcmi> dcmifromDB = MysqlAccessor.getMysqlAccessorSingleton().getDcmiForAxoid(axoid);
            assert dcmifromDB.size64() == 1;
        target = dcmifromDB.get(0);
        final ClassifierQueue classifierQueue = new ClassifierQueue(1);
        final ClassifierWriter classifierWriter = new ClassifierWriter(classifierQueue);
        classifierWriter.start();
        ExecutorService pool = Executors.newFixedThreadPool(1);
        MyLatch myLatch = new MyLatch(1);
        pool.execute(new SearchThread(target,classifierQueue,myLatch));
        pool.shutdown();
        try {
            synchronized (myLatch)
            {
                while (!myLatch.isDone())
                {
                    myLatch.wait();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assert myLatch.isDone();
        synchronized (latch)
        {
            latch.countDown();
        }
        classifierQueue.setKeepProducing(false);
    }

    public static ArrayList<Integer> getTIDS(GroundTruthEntity entity)
    {
        ArrayList<Integer> result = new ArrayList<Integer>();
        for (Category category: entity.getCategories())
        {
             result.add(category.getTid());
        }
        return result;
    }
    public static void deleteLinesTable()
    {
        Connection connection = null;
        Statement statement = null;
        ArrayList<Integer> result = new ArrayList<Integer>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + CACHE_INDEX_DB_TABLE + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connection.createStatement();
            statement.executeUpdate("DELETE  from " + TAXONOMY_LINK_DB_TABLE );

            connection.close();
            statement.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public static void writeLargerTextFile(String aFileName, List<String> aLines) {
        try {
            PrintWriter printWriter = new PrintWriter(new File(aFileName));
            printWriter.print("");
            printWriter.close();
            Path path = Paths.get(aFileName);
            BufferedWriter writer = Files.newBufferedWriter(path, Charset.defaultCharset());
            writer.write("");
            for (String line : aLines) {
                writer.write(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static ArrayList<GroundTruthEntity> removeNonEnglishEntries(ArrayList<GroundTruthEntity> list) {

        ArrayList<GroundTruthEntity> result = new ArrayList<GroundTruthEntity>();
        for (GroundTruthEntity entity : list)
        {
            try {
                String axoid = entity.getAxoid();
                String deflanguage = MysqlAccessor.getMysqlAccessorSingleton().getDefLanguage(axoid);
                String eclaplanguage = EclapLanguage.convert(deflanguage);
                if (eclaplanguage.equals(EclapLanguage.english))result.add(entity);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return result;
    }
    public static Float[] calculatePrecision_Recall(GroundTruthEntity entity) throws Exception {
        String axoid = entity.getAxoid();
        ArrayList<Integer> retrieved = Utils.getClassifierResultForAxoid(axoid);
        if (retrieved.isEmpty())
        {
            CountDownLatch keepgoing = new CountDownLatch(1);
            Utils.classifyThisAxoids(axoid,keepgoing);
            try {
                synchronized (keepgoing)
                {
                    while (keepgoing.getCount()!=0) keepgoing.await();
                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }else {
            throw new Exception("used a previously created set of data: should not happen");
        }
        //     System.out.println();
        ArrayList<Integer> relevants = Utils.getTIDS(entity);
        retrieved = Utils.getClassifierResultForAxoid(axoid);
        float retrievedNrelevant = 0;
        for (int i : retrieved)
        {
            if (relevants.contains(i))retrievedNrelevant++;
        }
        Float[] pre_re = new Float[2];
        if (retrieved.size()>0)pre_re[0] = retrievedNrelevant /(float)retrieved.size();else pre_re[0] = 0f;
        pre_re[1] = retrievedNrelevant /(float)relevants.size();
        //     System.out.println("axoid"+": \t"+axoid+"\t relevants: "+relevants.size()+"\t"+"retrieved: "+retrieved.size()+"\t precision: "+pre_re[0]+"\t"+"recall"+pre_re[1]);
        return pre_re;
    }
    public static ObjectArrayList<Txt_Category> loadDocument(String path) throws IOException {
        ObjectArrayList<Txt_Category> categories = new ObjectArrayList<Txt_Category>();
        File f = new File(path);
        if (!f.exists()) {
            throw new IOException("error using file: " + f.getPath());
        } else {
            ObjectArrayList<String> list = DocumentMapper.loadBigFile(f.getPath());
            for (String line : list) {
                categories.add(DocumentMapper.parseTextLine(line, databaseEntity.EclapLanguage.english));
            }
            return categories;
        }
    }
        public static void deleteFolder(String path)
        {
            File f = new File(path);
            if (!f.isDirectory()) f.delete();
            else {
                for (File son : f.listFiles())
                {
                    deleteFolder(son.getPath());
                }
                f.delete();
            }
        }


}
