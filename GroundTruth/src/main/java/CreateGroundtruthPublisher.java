import configuration.ClassifierConfiguration;
import databaseEntity.EclapLanguage;
import databaseMapping.Dcmi;
import databaseMapping.MysqlAccessor;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 22/06/2014
 * Time: 14:49
 * To change this template use File | Settings | File Templates.
 */
public class CreateGroundtruthPublisher {

    public static void main(String[]args)
    {
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(Utils.USER);
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(Utils.PASSWORD);
        creategroudtruthForpublisher();
    }
    public static void creategroudtruthForpublisher()
    {

        List<String> publisherList = ClassifierConfiguration.getClassiferConfigurationInstance().getPublisherList();
        ExecutorService pool = Executors.newFixedThreadPool(publisherList.size());

        for (String publisher: publisherList)
        {
            pool.submit(generatepublishergroundturth(publisher));
        }
        pool.shutdown();

    }

    private static Runnable generatepublishergroundturth(final String publisher)
    {
        return new Runnable() {
            public void run() {
                ArrayList<String> lines = new ArrayList<String>();
                ObjectArrayList<String> axoid4Publisher = MysqlAccessor.getMysqlAccessorSingleton().getAxoid4Publisher(publisher);

                for (String axoid : axoid4Publisher)
                {
                    String line = axoid+"\n";
                    ObjectBigArrayBigList<Dcmi> dcmiForAxoid = MysqlAccessor.getMysqlAccessorSingleton().getDcmiForAxoid(axoid);
                    assert dcmiForAxoid.size64() == 1;
                    Dcmi dcmi = dcmiForAxoid.get(0);
                    Map.Entry<String, ArrayList<String>> contentsForLanguage1 = dcmi.getContentsForLanguage(EclapLanguage.english);
                    Map.Entry<String, ArrayList<String>> contentsForLanguage2 = dcmi.getContentsForLanguage(EclapLanguage.italian);
                    Map.Entry<String, ArrayList<String>> contentsForLanguage3 = dcmi.getContentsForLanguage(EclapLanguage.french);
                    if (contentsForLanguage1!= null){
                        line = line.concat(EclapLanguage.english).concat(":\t");
                        line = line.concat(StringUtils.join(contentsForLanguage1.getValue(),"")).concat("\n");
                    }
                    if (contentsForLanguage2!= null){
                        line = line.concat(EclapLanguage.italian).concat(":\t");
                        line = line.concat(StringUtils.join(contentsForLanguage2.getValue(),"")).concat("\n");
                    }
                    if (contentsForLanguage3!= null){
                        line = line.concat(EclapLanguage.french).concat(":\t");
                        line = line.concat(StringUtils.join(contentsForLanguage3.getValue(),"")).concat("\n");
                    }
                    lines.add(line);
                }
                try {
                    File file = new File(Utils.groundtruth_resourcefolter,"groundtruth_publisher_"+publisher+".txt");
                    if (!file.exists())file.createNewFile();
                    Utils.writeLargerTextFile(file.getPath(),lines);
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

            }
        };
    }

}
