import configuration.ClassifierConfiguration;
import databaseMapping.Dcmi;
import databaseMapping.MysqlAccessor;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import searchUtils.MyLatch;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 22/06/2014
 * Time: 14:45
 * To change this template use File | Settings | File Templates.
 */
public class AnalysePublisher {
    public static void main(String[]args)
    {
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(Utils.USER);
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(Utils.PASSWORD);

            List<String> publisherList = ClassifierConfiguration.getClassiferConfigurationInstance().getPublisherList();
            for (String publisher : publisherList)
            {
                ArrayList<String>lines = new ArrayList<String>();
                for (float f = 0f;f<0.2f;f=f+0.01f)
                {
                    ClassifierConfiguration.getClassiferConfigurationInstance().setAccuracytAccuracy(f);
                    Utils.deleteLinesTable();

                    ObjectArrayList<String> axoid4Publisher = MysqlAccessor.getMysqlAccessorSingleton().getAxoid4Publisher(publisher);
                    CountDownLatch latch = new CountDownLatch(axoid4Publisher.size());
                    for (String axoid : axoid4Publisher)
                    {
                        Utils.classifyThisAxoids(axoid,latch);

                    }
                    float count = 0f;
                    for (String axoid : axoid4Publisher)
                    {
                        count = count +  Utils.getClassifierResultForAxoid(axoid).size();
                    }
                    count = count/axoid4Publisher.size();
                   lines.add(f + "\t" + count);
                    try {
                        File publisher_file = new File(Utils.groundtruth_resourcefolter,publisher);
                        publisher_file.createNewFile();
                        Utils.writeLargerTextFile(publisher_file.getPath(),lines);
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

            }


    }


}
