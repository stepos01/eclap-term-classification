import databaseMapping.Dcmi;
import databaseMapping.EclapLanguage;
import databaseMapping.MysqlAccessor;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 04/06/2014
 * Time: 17:29
 * To change this template use File | Settings | File Templates.
 */
public class CreateGroundtruth {
    public static void main(String[]args)
    {
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(Utils.USER);
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(Utils.PASSWORD);
        int i = 10;
        ObjectBigArrayBigList<Dcmi> dcmifromDB = MysqlAccessor.getMysqlAccessorSingleton().getDcmifromDB(1000);
        ArrayList<String>lines = new ArrayList<String>();
        for (int j= 0;j<1000;j++)
        {
            int rand = (int) (Math.random()*dcmifromDB.size64());
            Map.Entry<String, ArrayList<String>> contentsForLanguage = dcmifromDB.get(rand).getContentsForLanguage(EclapLanguage.english);
            if (contentsForLanguage != null&&contentsForLanguage.getValue().size()>1)
            {
                i--;
                String axoid = dcmifromDB.get(rand).getAxoid();
                String text = StringUtils.join(contentsForLanguage.getValue()," ");
                lines.add("axoid: "+axoid);
                lines.add("text: "+text);
                lines.add("-----------------------------------------------------------");

                if (i == 0)break;

            }
        }
        Utils.writeLargerTextFile(Utils.groundtruth_data,lines);
    }
}
