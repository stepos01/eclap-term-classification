import configuration.ClassifierConfiguration;
import databaseMapping.MysqlAccessor;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.concurrent.*;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 29/05/2014
 * Time: 04:33
 * To change this template use File | Settings | File Templates.
 */
public class Starter {
    public static void main(String[]args)
    {

        ArrayList<GroundTruthEntity> list = null;
        ArrayList<String> precisionLines = new ArrayList<String>();
        ArrayList<String> recallLines = new ArrayList<String>();
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(Utils.PASSWORD);
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(Utils.USER);

        try {
            list  = Utils.readGroundtruthFile();
     //       list = Utils.removeNonEnglishEntries(list);
            for (int j = 0; j < 4 ; j ++)
            {
                String lucenpath = "resources/TermIndex/en_soglia_"+j;
                ClassifierConfiguration.getClassiferConfigurationInstance().setLunceIndexes(lucenpath);
                Utils.deleteLinesTable();

                float meanP = 0;
                float meanR = 0;
                ExecutorService pool = Executors.newFixedThreadPool(5);
                CountDownLatch latch = new CountDownLatch(list.size());
                ArrayList<Future<Float[]>> futures = new ArrayList<Future<Float[]>>();
                for (GroundTruthEntity entity : list)
                {
                  //  System.out.println("Analysing entry "+list.indexOf(entity)+"...");
                    futures.add(pool.submit(generateCallable(entity, latch,list.indexOf(entity))));
                }
                pool.shutdown();
                try {
                    synchronized (latch)
                    {
                        while (latch.getCount()!=0)latch.await();
                    }
                    for (Future<Float[]> future : futures)
                    {
                        Float[] pre_rec = future.get();
                        meanP = meanP + pre_rec[0];
                        meanR = meanR + pre_rec[1];
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.exit(-1);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                System.out.println("Precisionmean: "+meanP/list.size());
                System.out.println("Recallmean: " + meanR / list.size() + "\n");
                precisionLines.add(j+"\t"+meanP/list.size());
                recallLines.add(j + "\t" + meanR / list.size());
            }

        } catch (ParseException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        Utils.writeLargerTextFile(Utils.path2,precisionLines);
        Utils.writeLargerTextFile(Utils.path3,recallLines);

    }
    public static Float[] calculatePrecision_Recall(GroundTruthEntity entity) throws Exception {
        String axoid = entity.getAxoid();
        ArrayList<Integer> retrieved = Utils.getClassifierResultForAxoid(axoid);
        if (retrieved.isEmpty())
        {
            CountDownLatch keepgoing = new CountDownLatch(1);
            Utils.classifyThisAxoids(axoid,keepgoing);
            try {
                synchronized (keepgoing)
                {
                    while (keepgoing.getCount()!=0) keepgoing.await();
                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }else {
            throw new Exception("used a previously created set of data: should not happen");
        }
        System.out.println();
        ArrayList<Integer> relevants = Utils.getTIDS(entity);
        retrieved = Utils.getClassifierResultForAxoid(axoid);
        float retrievedNrelevant = 0;
        for (int i : retrieved)
        {
            if (relevants.contains(i))retrievedNrelevant++;
        }
        Float[] pre_re = new Float[2];
        if (retrieved.size()>0)pre_re[0] = retrievedNrelevant /(float)retrieved.size();else pre_re[0] = (float)0;
        pre_re[1] = retrievedNrelevant /(float)relevants.size();
        System.out.println("axoid"+": \t"+axoid+"\t relevants: "+relevants.size()+"\t"+"retrieved: "+retrieved.size()+"\t precision: "+pre_re[0]+"\t"+"recall"+pre_re[1]);
        return pre_re;
    }
    public static Callable<Float[]> generateCallable(final GroundTruthEntity entity,final CountDownLatch latch,final int index)
    {
        return new Callable<Float[]>() {
            public Float[] call() throws Exception {
                try {
                    return  calculatePrecision_Recall(entity);
                }finally {
                    latch.countDown();
                  //  System.out.println("Done with entity: "+index+"...");

                }
            }
        };
    }





}
