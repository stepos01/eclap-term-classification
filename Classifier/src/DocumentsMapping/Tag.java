package DocumentsMapping;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 04/10/13
 * Time: 21:08
 * To change this template use File | Settings | File Templates.
 */
public class Tag {
    private String lemma;
    private int count;

    public Tag(int count, String lemma) {
        this.count = count;
        this.lemma = lemma;
    }

    public Tag(String lemma) {
        this.lemma = lemma;
    }

    public int getCount() {
        return count;
    }

    public String getLemma() {
        return lemma;
    }

    public void inc() {
        this.count += 1;
    }

    public String toString() {
        return "(" + lemma + "," + count + ")";
    }
}
