package DocumentsMapping;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 02/07/13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */

public class DocumentCategory {
    private String name;
    private CategoryTags categoryTags;

    public DocumentCategory(String name, CategoryTags categoryTags) {
        this.name = name;
        this.categoryTags = categoryTags;

    }

    public void setCategoryTags(CategoryTags categoryTags) {
        this.categoryTags = categoryTags;
    }


    public String getName() {
        return name;
    }

    public CategoryTags getCategoryTags() {
        return categoryTags;
    }

    public String toString() {
        return getName().replaceAll(" ", "_") + "\t" + categoryTags.toString();
    }


}
