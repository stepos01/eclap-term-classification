package DocumentsMapping;

import Configuration.ClassifierConfiguration;
import NLPUtils.ClassifierLanguage;
import NLPUtils.Utils;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;

import java.io.File;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 05/10/13
 * Time: 15:09
 * To change this template use File | Settings | File Templates.
 */
public class DocumentMapper {
    private ArrayList<DocumentCategory> categories;
    private ArrayList<DocumentCategory> subcategories;
    private static DocumentMapper ourInstance = null;


    private DocumentMapper() {
    }

    public static DocumentMapper getInstance() {
        if (ourInstance == null) ourInstance = new DocumentMapper();
        return ourInstance;
    }

    private void loadDocuments(ClassifierLanguage language) {
        categories = new ArrayList<DocumentCategory>();
        subcategories = new ArrayList<DocumentCategory>();
        File file = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocuments() + "/" + language.getLanguage());
        if (file.exists() && file.isDirectory() && !file.isHidden()) {
            for (File doc : file.listFiles()) {
                if (doc.canRead() && !doc.isHidden()) {
                    ObjectBigArrayBigList<String> documents = Utils.loadBigFile(doc.getPath());
                    for (String line : documents) {
                        DocumentCategory documentCategory = parseDocumentsLine(line, file.getName(), doc.getName().equals(Utils.PARTS));
                        if (documentCategory instanceof SubDocumentCategory) subcategories.add(documentCategory);
                        else categories.add(documentCategory);
                    }
                }
            }
        }
    }

    private DocumentCategory parseDocumentsLine(String line, String language, boolean isPart) {
        ArrayList<Tag> tags = new ArrayList<Tag>();
        String[] blocks = line.split("\t");
        DocumentCategory documentCategory = null;
        if (!isPart) {
            String name = blocks[0];
            String[] tagsString = null;
            if (blocks.length > 1) {
                tagsString = blocks[1].split(";");
                for (String t : tagsString) {
                    tags.add(document2Tag(t));
                }
            }
            documentCategory = new DocumentCategory(name, new CategoryTags(new ClassifierLanguage(language), tags));
        } else {
            String name = blocks[0];
            String partName = blocks[1];
            String[] tagsString = null;
            if (blocks.length > 2) {
                tagsString = blocks[2].split(";");
                for (String t : tagsString) {
                    tags.add(document2Tag(t));
                }
            }
            documentCategory = new SubDocumentCategory(name, partName, new CategoryTags(new ClassifierLanguage(language), tags));
        }
        return documentCategory;
    }

    public ArrayList<DocumentCategory> getCategories(ClassifierLanguage language) {
        if (categories == null || categories.isEmpty() || !categories.get(0).getCategoryTags().getLanguage().equals(language))
            loadDocuments(language);
        return categories;

    }


    public ArrayList<DocumentCategory> getSubcategories(ClassifierLanguage language) {
        if (subcategories == null || subcategories.isEmpty() || !subcategories.get(0).getCategoryTags().getLanguage().equals(language))
            loadDocuments(language);
        return subcategories;
    }

    private Tag document2Tag(String s) {
        String t = s.replace("(", "").trim();
        t = t.replace(")", "").trim();
        int last = t.lastIndexOf(",");
        Tag tag = new Tag(Integer.valueOf(t.substring(last + 1)), t.substring(0, last));
        return tag;
    }
}
