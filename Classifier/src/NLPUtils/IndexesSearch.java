package NLPUtils;

import Configuration.ClassifierConfiguration;
import DocumentsMapping.Tag;
import NLPUtils.SearchHit.CategoryNameHit;
import NLPUtils.SearchHit.CategorySubNameHit;
import NLPUtils.SearchHit.CategoryTagHit;
import NLPUtils.SearchHit.ClassifierHit;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.apache.lucene.search.grouping.TopGroups;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleFragmenter;
import org.apache.lucene.search.highlight.TokenSources;
import org.apache.lucene.search.join.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 10/10/13
 * Time: 15:46
 * To change this template use File | Settings | File Templates.
 */
public class IndexesSearch {
    private static IndexesSearch instance = null;
    private Filter parentsFilter = null;

    public static IndexesSearch getInstance() {
        if (instance == null) instance = new IndexesSearch();
        return instance;
    }

    private IndexesSearch() {
        parentsFilter = new FixedBitSetCachingWrapperFilter(new
                QueryWrapperFilter(new TermQuery(new Term(IndexesCreator.fieldUsed.type.toString(), IndexesCreator.FIELD_TYPE_TITLE))));
    }

    public ArrayList<ClassifierHit> search(String text, String lang) {
        ArrayList<String> lemmas4Tags = null;
        ArrayList<String> lemmas4Title = null;
        ArrayList<ClassifierHit> hits = new ArrayList<ClassifierHit>();
        try {
            ArrayList<String> lemma4Search = new ArrayList<String>();
            lemmas4Title = Utils.tokenizeString(Utils.getLightAnalyser(new ClassifierLanguage(lang)), text);
            for (int i = 0; i < lemmas4Title.size(); i++) {
                lemma4Search.add(lemmas4Title.get(i));
                if (lemma4Search.size() > (BooleanQuery.getMaxClauseCount() / 2) - 1) {
                    ArrayList<ClassifierHit> results = searchTitlesInIndex(lemma4Search, lang);
                    if (results != null) hits.addAll(results);
                    lemma4Search.clear();
                }
            }
            ArrayList<ClassifierHit> results = searchTitlesInIndex(lemma4Search, lang);
            if (results != null) hits.addAll(results);
            lemma4Search = new ArrayList<String>();
            lemmas4Tags = Utils.tokenizeString(Utils.getLuceneAnalyser(new ClassifierLanguage(lang)), text);
            for (String lemma : lemmas4Tags) {
                lemma4Search.add(lemma);
                if (lemma4Search.size() > BooleanQuery.getMaxClauseCount() - 4) {
                    results = searchTagsInIndex(lemma4Search, lang);
                    if (results != null) hits.addAll(results);
                    lemma4Search.clear();
                }
            }
            results = searchTagsInIndex(lemma4Search, lang);
            if (results != null) hits.addAll(results);

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        if (hits.isEmpty()) hits = null;
        return hits;
    }

    private ArrayList<ClassifierHit> searchTitlesInIndex(ArrayList<String> tokens, String lang) {
        ArrayList<ClassifierHit> classifierHits = new ArrayList<ClassifierHit>();
        try {
            File luceneIndex = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getLuceneIndexes() + lang);
            Directory directory = FSDirectory.open(luceneIndex);
            DirectoryReader ireader = DirectoryReader.open(directory);
            IndexSearcher isearcher = new IndexSearcher(ireader);
            BooleanQuery query = new BooleanQuery();
            for (String token : tokens) {
                query.add(new TermQuery(new Term(IndexesCreator.fieldUsed.title.toString(), token)), BooleanClause.Occur.SHOULD);
                query.add(new TermQuery(new Term(IndexesCreator.fieldUsed.partTitle.toString(), token)), BooleanClause.Occur.SHOULD);
            }
            ScoreDoc[] hits = isearcher.search(query, null, 1000).scoreDocs;
            if (hits == null) return null;
//            System.out.println("Found " + hits.length + " hits on title.");
            for (int i = 0; i < hits.length; i++) {
                int docId = hits[i].doc;
                Document d = isearcher.doc(docId);
                String partTitle = d.get(IndexesCreator.fieldUsed.partTitle.toString());
                String title = d.get(IndexesCreator.fieldUsed.title.toString());
//                System.out.println(i + 1 + "- the score is: " + hits[i].score + "\n"
//                        + "the title is : " + title + "\n"
//                        + "the partTitle is : " + partTitle + "\n");
                if (partTitle == null) classifierHits.add(new CategoryNameHit(title));
                else
                    classifierHits.add(new CategorySubNameHit(title, partTitle).setSubNames(getPartitles(title, lang)));
            }
            ireader.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return classifierHits;
    }

    private ArrayList<ClassifierHit> searchTagsInIndex(ArrayList<String> tokens, String lang) {
        ArrayList<ClassifierHit> classifierHits = new ArrayList<ClassifierHit>();
        try {
            parentsFilter = new FixedBitSetCachingWrapperFilter(new
                    QueryWrapperFilter(new TermQuery(new Term(IndexesCreator.fieldUsed.type.toString(), IndexesCreator.FIELD_TYPE_TITLE))));
            File luceneIndex = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getLuceneIndexes() + lang);
            Directory directory = FSDirectory.open(luceneIndex);
            DirectoryReader ireader = DirectoryReader.open(directory);
            IndexSearcher isearcher = new IndexSearcher(ireader);
            BooleanQuery tagsQuery = new BooleanQuery();
            for (String token : tokens) {
                tagsQuery.add(new TermQuery(new Term(IndexesCreator.fieldUsed.tags.toString(), token)), BooleanClause.Occur.SHOULD);
            }
            BooleanQuery booleanClauses = new BooleanQuery();
            ToParentBlockJoinQuery toParentBlockJoinQuery = new ToParentBlockJoinQuery(tagsQuery, parentsFilter, ScoreMode.None);
            ToParentBlockJoinCollector toParentBlockJoinCollector = new ToParentBlockJoinCollector(Sort.INDEXORDER, 1000, false, false);
            booleanClauses.add(toParentBlockJoinQuery, BooleanClause.Occur.MUST);
            TermQuery typeQuery = new TermQuery(new Term(IndexesCreator.fieldUsed.type.toString(), IndexesCreator.FIELD_TYPE_TITLE));
            booleanClauses.add(typeQuery, BooleanClause.Occur.MUST);
            isearcher.search(booleanClauses, toParentBlockJoinCollector);
            TopGroups<Integer> topGroupsWithAllChildDocs = toParentBlockJoinCollector.getTopGroupsWithAllChildDocs(toParentBlockJoinQuery, Sort.INDEXORDER, 0, 0, false);
            if (topGroupsWithAllChildDocs == null) return null;
//            System.out.println("Found " + topGroupsWithAllChildDocs.totalGroupedHitCount + " hits.");
            for (int i = 0; i < topGroupsWithAllChildDocs.groups.length; i++) {
                int parentDocId = topGroupsWithAllChildDocs.groups[i].groupValue;
                Document parentDoc = isearcher.doc(parentDocId);
                String partTitle = parentDoc.get(IndexesCreator.fieldUsed.partTitle.toString());
                String title = parentDoc.get(IndexesCreator.fieldUsed.title.toString());
//                System.out.println("**************************" + String.valueOf(i + 1) + "- GROUP " + "**********************" + "\n"
//                        + "the group title is: " + title + "\n"
//                        + "the group partitle is: " + partTitle + "\n"
//                );
                Document childDocument = null;
                for (int j = 0; j < topGroupsWithAllChildDocs.groups[i].scoreDocs.length; j++) {
                    int childId = topGroupsWithAllChildDocs.groups[i].scoreDocs[j].doc;
                    childDocument = isearcher.doc(childId);
                    int count = childDocument.getField(IndexesCreator.fieldUsed.tagCount.name()).numericValue().intValue();
                    ArrayList<String> tokenizedTag = Utils.tokenizeString(Utils.getLightAnalyser(new ClassifierLanguage(lang)), childDocument.get(IndexesCreator.fieldUsed.tags.toString()));
                    String text = childDocument.get(IndexesCreator.fieldUsed.tags.toString());
                    Highlighter highlighter = new Highlighter(new QueryScorer(tagsQuery));
                    highlighter.setTextFragmenter(new SimpleFragmenter());
                    TokenStream tokenStream = TokenSources.getAnyTokenStream(isearcher.getIndexReader(), childId, IndexesCreator.fieldUsed.tags.toString(), Utils.getLuceneAnalyser(new ClassifierLanguage(lang)));
                    String tag = highlighter.getBestFragment(tokenStream, text);
                    ArrayList<String> tags = new ArrayList<String>();
                    while (tag.contains("<")) {
                        int start = tag.indexOf(">");
                        int end = tag.indexOf("<", start);
                        tags.add(tag.substring(start + 1, end));
                        tag = tag.replaceFirst("<B>", "");
                        tag = tag.replaceFirst("</B>", "");
                    }
//                    System.out.println("hits on tags:");
//                    System.out.println(j + 1 + "- "
//                            + "the full tag is : " + childDocument.get(IndexesCreator.fieldUsed.tags.toString()) + "\n"
//                            + "the exact tags are : " + tags + "\n"
//                            + "the tag count is: " + count);
                    if (partTitle == null) {
                        if (tokenizedTag.size() > 1) {
                            for (String cutTag : tags) {
                                classifierHits.add(new CategoryTagHit(title, cutTag, count).setSubTags(tokenizedTag).setOtherTags(getOtherTags(title, lang)));
                            }
                        } else if (tokenizedTag.size() == 1)
                            classifierHits.add(new CategoryTagHit(title, tags.get(0), count).setOtherTags(getOtherTags(title, lang)));
                    } else {
                        if (tokenizedTag.size() > 1) {
                            for (String cutTag : tags) {
                                classifierHits.add(new CategoryTagHit(title, cutTag, count)
                                        .setSubName(partTitle)
                                        .setSubTags(tokenizedTag)
                                        .setSubNames(getPartitles(title, lang))
                                        .setOtherTags(getOtherTags(title, lang)));
                            }
                        } else if (tokenizedTag.size() == 1) {
                            classifierHits.add(new CategoryTagHit(title, tags.get(0), count)
                                    .setSubName(partTitle)
                                    .setSubNames(getPartitles(title, lang))
                                    .setOtherTags(getOtherTags(title, lang)));
                        }
                    }

                }
            }
            ireader.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return classifierHits;
    }

    public ArrayList<String> getPartitles(String title, String lang) {
        ArrayList<String> partitles = new ArrayList<String>();
        try {
            File luceneIndex = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getLuceneIndexes() + lang);
            Directory directory = FSDirectory.open(luceneIndex);
            DirectoryReader ireader = DirectoryReader.open(directory);
            IndexSearcher isearcher = new IndexSearcher(ireader);
            TermQuery query = new TermQuery(new Term(IndexesCreator.fieldUsed.title.name(), title));
            ScoreDoc[] hits = isearcher.search(query, null, 1000).scoreDocs;
            for (ScoreDoc hit : hits) {
                int docId = hit.doc;
                Document doc = isearcher.doc(docId);
                partitles.add(doc.get(IndexesCreator.fieldUsed.partTitle.name()));
            }
            assert partitles.size() > 0;
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);

        }
        return partitles;
    }

    private ArrayList<Tag> getOtherTags(String title, String lang) {
        ArrayList<Tag> tags = new ArrayList<Tag>();
        try {
            File luceneIndex = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getLuceneIndexes() + lang);
            Directory directory = FSDirectory.open(luceneIndex);
            DirectoryReader ireader = DirectoryReader.open(directory);
            IndexSearcher isearcher = new IndexSearcher(ireader);
            TermQuery parentQuery = new TermQuery(new Term(IndexesCreator.fieldUsed.title.name(), title));
            ToChildBlockJoinQuery toChildBlockJoinQuery = new ToChildBlockJoinQuery(parentQuery, parentsFilter, true);
            ScoreDoc[] hits = isearcher.search(toChildBlockJoinQuery, 1000).scoreDocs;
            for (ScoreDoc hit : hits) {
                int docId = hit.doc;
                Document doc = isearcher.doc(docId);
                String tag = doc.get(IndexesCreator.fieldUsed.tags.name());
                int count = doc.getField(IndexesCreator.fieldUsed.tagCount.name()).numericValue().intValue();

                tags.add(new Tag(count, tag));
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);

        }
        return tags;
    }
}
