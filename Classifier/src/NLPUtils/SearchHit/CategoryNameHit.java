package NLPUtils.SearchHit;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 09/10/13
 * Time: 00:38
 * To change this template use File | Settings | File Templates.
 */
public class CategoryNameHit extends ClassifierHit {

    public CategoryNameHit(String name) {
        setName(name);

    }

    @Override
    public boolean equals(Object hit) {
        boolean isequal = false;
        if (hit != null && hit instanceof CategoryNameHit) {
            if (((CategoryNameHit) hit).getName().equals(getName()))
                isequal = true;
        }
        return isequal;

    }
}
