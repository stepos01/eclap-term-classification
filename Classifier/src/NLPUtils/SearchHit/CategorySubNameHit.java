package NLPUtils.SearchHit;

import NLPUtils.Utils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 09/10/13
 * Time: 00:46
 * To change this template use File | Settings | File Templates.
 */
public class CategorySubNameHit extends CategoryNameHit {
    private String subName;
    private ArrayList<String> subNames;

    public CategorySubNameHit(String name, String subname) {
        super(name);
        this.subName = subname;
    }


    public boolean isSubnameHit(ClassifierHit hit) {
        Boolean isSubname = false;
        if (super.equals(hit)) {
            if (hit instanceof CategorySubNameHit && Utils.compareArraysOfString(getSubNames(), ((CategorySubNameHit) hit).getSubNames()))
                if (!getSubName().equals(((CategorySubNameHit) hit).getSubName()))
                    isSubname = true;
        }
        return isSubname;
    }

    public String getSubName() {
        return subName;
    }


    public ArrayList<String> getSubNames() {
        return subNames;
    }

    public CategorySubNameHit setSubNames(ArrayList<String> subNames) {
        this.subNames = subNames;
        return this;
    }

    @Override
    public boolean equals(Object hit) {
        Boolean isEqual = false;
        if (super.equals(hit)) {
            if (hit instanceof CategorySubNameHit && ((CategorySubNameHit) hit).getSubName().equals(getSubName()))
                isEqual = true;
        }
        return isEqual;
    }
}
