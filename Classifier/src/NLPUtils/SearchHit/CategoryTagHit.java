package NLPUtils.SearchHit;

import DocumentsMapping.Tag;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;

import static NLPUtils.Utils.compareArraysOfString;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 14/10/13
 * Time: 11:09
 * To change this template use File | Settings | File Templates.
 */
public class CategoryTagHit extends ClassifierHit {
    private String tag;
    private String subName;
    private int count;
    private ArrayList<Tag> otherTags;
    private ArrayList<String> subNames;
    private ArrayList<String> subTags;

    public CategoryTagHit(String name, String tag, int count) {
        setName(name);
        this.tag = tag;
        this.count = count;
    }

    public String getTag() {
        return tag;
    }

    public int getCount() {
        return count;
    }

    @Override
    public boolean equals(Object hit) {
        boolean isEquals = false;
        if (hit != null && hit instanceof CategoryTagHit) {
            if (((CategoryTagHit) hit).getName().equals(getName()))
                if (getTag().equals(((CategoryTagHit) hit).getTag()))
                    if (StringUtils.equals(subName, ((CategoryTagHit) hit).getSubName()))
                        if (compareArraysOfString(subTags, ((CategoryTagHit) hit).getSubTags()))
                            isEquals = true;

        }
        return isEquals;
    }

    public boolean isSubnames(ClassifierHit hit) {
        boolean isSubname = false;
        if (hit != null && hit instanceof CategoryTagHit) {
            if (getSubName() != null && ((CategoryTagHit) hit).getSubName() != null)
                if (getName().equals(hit.getName()) && compareArraysOfString(getSubNames(), ((CategoryTagHit) hit).getSubNames()))
                    if (!getSubName().equals(((CategoryTagHit) hit).getSubName()))
                        isSubname = true;
        }
        return isSubname;
    }

    public boolean isPartTags(ClassifierHit hit) {
        boolean isPartTag = false;
        if (hit != null && hit instanceof CategoryTagHit) {
            if ((hit).getName().equals(getName()))
                if (StringUtils.equals(subName, ((CategoryTagHit) hit).getSubName()))
                    if (compareArraysOfString(getSubTags(), ((CategoryTagHit) hit).getSubTags()))
                        isPartTag = true;
        }
        return isPartTag;
    }

    public String getSubName() {
        return subName;
    }

    public CategoryTagHit setSubName(String subName) {
        this.subName = subName;
        return this;
    }

    public ArrayList<String> getSubNames() {
        return subNames;
    }

    public CategoryTagHit setSubNames(ArrayList<String> subNames) {
        this.subNames = subNames;
        return this;
    }

    public ArrayList<String> getSubTags() {
        return subTags;
    }

    public CategoryTagHit setSubTags(ArrayList<String> subTags) {
        this.subTags = subTags;
        return this;
    }

    public CategoryTagHit setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotal_tags_count() {
        int count = 0;
        assert getOtherTags() != null;
        for (Tag t : getOtherTags()) {
            count += t.getCount();
        }
        return count;
    }

    public ArrayList<Tag> getOtherTags() {
        return otherTags;
    }

    public CategoryTagHit setOtherTags(ArrayList<Tag> otherTags) {
        this.otherTags = otherTags;
        return this;
    }
}
