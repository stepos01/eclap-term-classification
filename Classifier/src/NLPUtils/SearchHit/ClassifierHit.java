package NLPUtils.SearchHit;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 09/10/13
 * Time: 00:33
 * To change this template use File | Settings | File Templates.
 */
public abstract class ClassifierHit {
    private String name;

    public abstract boolean equals(Object hit);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
