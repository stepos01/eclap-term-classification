package NLPUtils; /**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/06/13
 * Time: 01:09
 * To change this template use File | Settings | File Templates.
 */

import Configuration.ClassifierConfiguration;
import LightAnalyzer.*;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.ca.CatalanAnalyzer;
import org.apache.lucene.analysis.da.DanishAnalyzer;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.analysis.el.GreekAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.es.SpanishAnalyzer;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.analysis.hu.HungarianAnalyzer;
import org.apache.lucene.analysis.it.ItalianAnalyzer;
import org.apache.lucene.analysis.nl.DutchAnalyzer;
import org.apache.lucene.analysis.pl.PolishAnalyzer;
import org.apache.lucene.analysis.pt.PortugueseAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;


public class Utils {
    public static Charset ENCODING = StandardCharsets.UTF_8;
    public static final String PARTS = "PARTS";
    public static final String TAGS = "TAGS";
    private final static String[] SLOVENIAN_DEFAULT_STOPWORDS = {"h", "in", "je", "k", "onega", "onem", "onemu", "onim", "ono", "v", "&"};


    public static void writeLargerTextFile(String aFileName, List<String> aLines) {
        try {
            Path path = Paths.get(aFileName);
            BufferedWriter writer = Files.newBufferedWriter(path, ENCODING);
            writer.write("");
            for (String line : aLines) {
                writer.write(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> tokenizeString(Analyzer analyzer, String string) {
        ArrayList<String> result = new ArrayList<String>();
        try {
            TokenStream stream = analyzer.tokenStream(null, new StringReader(string));
            stream.reset();
            while (stream.incrementToken()) {
                result.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
        } catch (IOException e) {
            // not thrown b/c we're using a string reader...
            throw new RuntimeException(e);
        }
        return result;
    }


    public static ObjectBigArrayBigList<String> loadBigFile(String path) {
        ObjectBigArrayBigList<String> objectBigArrayBigList = new ObjectBigArrayBigList<String>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            while (line != null) {
                objectBigArrayBigList.add(line);
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return objectBigArrayBigList;
    }

    public static String formatParameter(String s) {
        return "'" + s + "'";
    }

    public static String deAccentuate(String str) {
        if (str == null) {
            throw new AssertionError("accentremover String to should not be null ");
        }
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

    public static Analyzer getLuceneAnalyser(ClassifierLanguage language) throws Exception {
        if (language.getLanguage().equals("fr")) return new FrenchAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("en")) return new EnglishAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("it")) return new ItalianAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("de")) return new GermanAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("da")) return new DanishAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("nl")) return new DutchAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("hu")) return new HungarianAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("pt")) return new PortugueseAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("es")) return new SpanishAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("ca")) return new CatalanAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("pl")) return new PolishAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("el")) return new GreekAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("sl"))
            return new StandardAnalyzer(Version.LUCENE_45, new CharArraySet(Version.LUCENE_45, Arrays.asList(SLOVENIAN_DEFAULT_STOPWORDS), true));
        else throw new Exception("Analyser for the required language : " + language.getLanguage() + "  not found");
    }

    public static Analyzer getLightAnalyser(ClassifierLanguage language) throws Exception {
        if (language.getLanguage().equals("fr")) return new FrenchLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("en")) return new EnglishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("it")) return new ItalianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("de")) return new GermanLightanalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("da")) return new DanishLightAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("nl")) return new DutchLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("hu")) return new HungarianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("pt")) return new PortugueseLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("es")) return new SpanishLightAnalyser(Version.LUCENE_44);
        else if (language.getLanguage().equals("ca")) return new CatalanLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("pl")) return new PolishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("el")) return new GreekLightAnalyser(Version.LUCENE_44);
        else if (language.getLanguage().equals("sl")) return new SlovenianLightAnalyzer(Version.LUCENE_44);
        else
            throw new Exception("light Analyser for the required language : " + language.getLanguage() + "  not found");
    }

    public static ArrayList<String> createStopWordList(CharArraySet set) {
        ArrayList<String> stopWords = new ArrayList<String>();
        List<String> stopWordExlusion = ClassifierConfiguration.getClassiferConfigurationInstance().getStopWordExclusion();
        Iterator iter = set.iterator();
        stopWords.add("&");
        while (iter.hasNext()) {
            char[] stopWord = (char[]) iter.next();
            String word = new String(stopWord);
            if (!stopWordExlusion.contains(word)) stopWords.add(word);
        }
        return stopWords;
    }

    public static boolean compareArraysOfString(ArrayList<String> a, ArrayList<String> b) {
        boolean equals = false;
        if (a == null && b == null) equals = true;
        else if (a != null && b != null) {
            if (a.size() == b.size()) {
                equals = true;
                for (int i = 0; i < a.size(); i++) {
                    if (!a.get(i).equals(b.get(i))) {
                        equals = false;
                        break;
                    }
                }
            }
        } else equals = false;
        return equals;

    }
    public static String removeDoubleSpace(String s) {
        return s.replaceAll(" +", " ").trim();
    }
}










