package NLPUtils;

import NLPUtils.SearchHit.CategoryNameHit;
import NLPUtils.SearchHit.CategorySubNameHit;
import NLPUtils.SearchHit.CategoryTagHit;
import NLPUtils.SearchHit.ClassifierHit;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 13/10/13
 * Time: 15:38
 * To change this template use File | Settings | File Templates.
 */
public class HitsAggregate {
    private static HitsAggregate instance = null;

    private HitsAggregate() {
    }

    public static ArrayList<ClassifierHit> ordinateHits(ArrayList<ClassifierHit> hits) {
        ArrayList<ClassifierHit> orderedHits = new ArrayList<ClassifierHit>();
        ArrayList<ClassifierHit> tempHits = new ArrayList<ClassifierHit>(hits);
        ArrayList<ClassifierHit> tempTagsHit = new ArrayList<ClassifierHit>();
        while (!tempHits.isEmpty()) {
            ClassifierHit hit = tempHits.get(0);
            if (hit instanceof CategorySubNameHit) {
                int count = ((CategorySubNameHit) hit).getSubNames().size() - 1;
                Iterator<ClassifierHit> it = tempHits.iterator();
                while (it.hasNext()) {
                    ClassifierHit newHit = it.next();
                    if (((CategorySubNameHit) hit).isSubnameHit(newHit)) {
                        count--;
                        it.remove();
                        if (count == 0) break;
                    }
                }
                tempHits.remove(hit);
                if (count == 0) orderedHits.add(hit);
            } else if (hit instanceof CategoryNameHit) {
                orderedHits.add(hit);
                tempHits.remove(hit);
            } else if (hit instanceof CategoryTagHit) {
                tempTagsHit.add(hit);
                tempHits.remove(hit);
            }
        }
        if (!tempTagsHit.isEmpty()) orderedHits.addAll(ordinateTagHits(tempTagsHit));
        if (orderedHits.isEmpty()) orderedHits = null;
        return orderedHits;
    }

    private static ArrayList<ClassifierHit> ordinateTagHits(ArrayList<ClassifierHit> hitzs) {
        ArrayList<ClassifierHit> temp = new ArrayList<ClassifierHit>(hitzs);
        ArrayList<ClassifierHit> orderedHits = new ArrayList<ClassifierHit>(temp.size());
        ArrayList<ClassifierHit> fullTags = new ArrayList<ClassifierHit>(temp.size());
        while (!temp.isEmpty()) {
            CategoryTagHit target = (CategoryTagHit) temp.get(0);
            if (target.getSubTags() == null) {
                fullTags.add(target);
                temp.remove(target);
            } else {
                int count = target.getSubTags().size();
                Iterator<ClassifierHit> it = temp.iterator();
                while (it.hasNext()) {
                    ClassifierHit hit = it.next();
                    if (target.isPartTags(hit)) {
                        count--;
                        it.remove();
                        if (count == 0) break;

                    }
                }
                temp.remove(target);
                if (count == 0) fullTags.add(target.setTag(StringUtils.join(target.getSubTags(), " ")));
            }
        }
        fullTags.trimToSize();
        while (!fullTags.isEmpty()) {
            CategoryTagHit target = (CategoryTagHit) fullTags.get(0);
            if (target.getSubNames() == null) {
                orderedHits.add(target);
                fullTags.remove(target);
            } else {
                int count = target.getSubNames().size() - 1;
                Iterator<ClassifierHit> it = fullTags.iterator();
                while (it.hasNext()) {
                    ClassifierHit hit = it.next();
                    if ((target).isSubnames(hit)) {
                        count--;
                        it.remove();
                        target.setCount(Math.min(target.getCount(), ((CategoryTagHit) hit).getCount()));
                        if (count == 0) break;
                    }
                }
                fullTags.remove(target);
                if (count == 0) orderedHits.add(target.setSubName(StringUtils.join(target.getSubNames(), " ")));
            }

        }
        return orderedHits;
    }
}
