package NLPUtils;

import Configuration.ClassifierConfiguration;
import DatabaseMapping.Category;
import DatabaseMapping.Dcmi;
import DatabaseMapping.MysqlAccessor;
import DocumentsMapping.DocumentCategory;
import DocumentsMapping.DocumentMapper;
import NLPUtils.SearchHit.CategoryNameHit;
import NLPUtils.SearchHit.ClassifierHit;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 10/10/13
 * Time: 16:19
 * To change this template use File | Settings | File Templates.
 */
public class TestIndexesSearch {

    public static void main(String[] args) {
        MysqlAccessor.getMysqlAccessorSingleton().setUSER("root");
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD("");
        testAccuracy();
    }

    private static void testDocument2db() {
        List<String> langagueList = ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList();
        for (String l : langagueList) {
            ArrayList<ClassifierHit> classifierHits = new ArrayList<ClassifierHit>();
            System.out.println("workin in ..." + l);
            ArrayList<DocumentCategory> categories = DocumentMapper.getInstance().getCategories(new ClassifierLanguage(l));
            for (DocumentCategory documentCategory : categories) {
                classifierHits.add(new CategoryNameHit(documentCategory.getName()));
            }
            for (ClassifierHit classifierHit : classifierHits) {
                String categoryName = ModelScorer.getInstance().transformDocumentCat2dbCat(classifierHit, new ClassifierLanguage(l));
                if (categoryName == null) System.err.println("something went wrong...");
            }
        }

    }

    private static void testAccuracy() {
        System.out.println("STARTING TEST");
        long compute = 10;
        try {
            ObjectBigArrayBigList<Dcmi> dcmifromDB = MysqlAccessor.getMysqlAccessorSingleton().getDcmifromDB();
            for (long j = 0; j < compute; j++) {
                int i = (int) (Math.random() * (dcmifromDB.size64()));
                while (!dcmifromDB.get(i).getDefLanguage().equals(new ClassifierLanguage(ClassifierLanguage.french))
                        && !dcmifromDB.get(i).getDefLanguage().equals(new ClassifierLanguage(ClassifierLanguage.italian))
                        && !dcmifromDB.get(i).getDefLanguage().equals(new ClassifierLanguage(ClassifierLanguage.english))) {
                    i = (int) (Math.random() * (dcmifromDB.size64()));
                }
                String text = dcmifromDB.get(i).getPriorityLanguageContent().toString();
                System.out.println("text:" + text + "\t" + "language:" + dcmifromDB.get(i).getDefLanguage());
                ClassifierLanguage language = dcmifromDB.get(i).getPriorityLanguageContent().getClassifierLanguage();
                int tokenNumber = Utils.tokenizeString(Utils.getLightAnalyser(language), text).size();
                ArrayList<ClassifierHit> hits = IndexesSearch.getInstance().search(text, language.getLanguage());
                ArrayList<ClassifierHit> agreggateHits = null;
                if (hits != null) agreggateHits = HitsAggregate.ordinateHits(hits);
                ArrayList<Category> categories = null;
                if (agreggateHits != null)
                    categories = ModelScorer.getInstance().elaborateResults(agreggateHits, tokenNumber, language);
                if (categories != null) {
                    System.out.println("winning categories");
                    for (Category cat : categories) {
                        System.out.println(cat.toString());
                    }
                    ClassifierWriter.getInstance().createTaxonomyLinks(dcmifromDB.get(i).getAxoid(), categories);
                } else System.out.println("no winner categories for this axoid!!");

            }
            ClassifierWriter.getInstance().updateTaxonomyDB();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }


    }

    private static void searchINdex() {

        {
            ArrayList<ClassifierHit> results = IndexesSearch.getInstance().search("theatre cinema film action rama yade theatre operations", "en");
            ArrayList<ClassifierHit> classifierHits = HitsAggregate.ordinateHits(results);
            System.out.println("è la morte");
        }
    }

    public static void createTestIndex() {
        Directory directory = null;
        IndexWriter writer = null;
        try {
            File luceneIndex = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getLuceneIndexes() + "en");
            ArrayList<DocumentCategory> categories = DocumentMapper.getInstance().getCategories(new ClassifierLanguage("en"));
            IndexWriterConfig config = null;
            directory = FSDirectory.open(luceneIndex);
            Map<String, Analyzer> analyzerPerField = new HashMap<String, Analyzer>();
            analyzerPerField.put(IndexesCreator.fieldUsed.title.toString(), new KeywordAnalyzer());
            PerFieldAnalyzerWrapper wrapper = new PerFieldAnalyzerWrapper(Utils.getLuceneAnalyser(new ClassifierLanguage("en")), analyzerPerField);
            config = new IndexWriterConfig(Version.LUCENE_45, wrapper);
            writer = new IndexWriter(directory, config);
            for (DocumentCategory category : categories) {
                Document d = new Document();
                d.add(new StringField(IndexesCreator.fieldUsed.title.toString(), category.getName(), Field.Store.YES));
                writer.addDocument(d);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            try {
                writer.close();
                directory.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
