package NLPUtils;

import Configuration.ClassifierConfiguration;
import DatabaseMapping.Category;
import DatabaseMapping.MysqlAccessor;
import DatabaseMapping.TaxonomyLink;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 20/09/13
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */
public class ClassifierWriter {

    ArrayList<TaxonomyLink> taxonomyLinks = null;
    private static ClassifierWriter instance = new ClassifierWriter();

    private ClassifierWriter() {
        taxonomyLinks = new ArrayList<TaxonomyLink>();
    }

    public static ClassifierWriter getInstance() {
        return instance;
    }

    public void createTaxonomyLinks(String axoid, ArrayList<Category> categories) {
        for (Category category : categories) {
            TaxonomyLink link = new TaxonomyLink(axoid, category.getTid());
            taxonomyLinks.add(link);
        }
        if (taxonomyLinks.size() >= 100) {
            updateTaxonomyDB();
            taxonomyLinks.clear();
        }
    }

    public void updateTaxonomyDB() {
        if (taxonomyLinks.isEmpty()) return;
        try {
            writeOnTaxonomyLinkUpdate(taxonomyLinks);
            MysqlAccessor.writeTaxonomyLinkOnDatabaseAlternate();
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private static void writeOnTaxonomyLinkUpdate(ArrayList<TaxonomyLink> taxonomyLinkArrayList) {
        File taxonomyLinkUpadate = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getTaxonomyLinkUpdate());
        List<String> lines = new ArrayList<String>();
        try {
            taxonomyLinkUpadate.createNewFile();
            for (TaxonomyLink link : taxonomyLinkArrayList) {
                String line = link.toString();
                lines.add(line);
            }
            Utils.writeLargerTextFile(taxonomyLinkUpadate.getPath(), lines);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public ArrayList<TaxonomyLink> getTaxonomyLinks() {
        return taxonomyLinks;
    }

    public ArrayList<TaxonomyLink> getTaxonomyLink(String axoid) {
        ArrayList<TaxonomyLink> links = new ArrayList<TaxonomyLink>();
        for (TaxonomyLink taxonomyLink : taxonomyLinks) {
            if (taxonomyLink.getAxoid().equals(axoid)) {
                links.add(taxonomyLink);
            }
        }
        return links;
    }
}
