package NLPUtils;

import Configuration.ClassifierConfiguration;
import DocumentsMapping.DocumentCategory;
import DocumentsMapping.DocumentMapper;
import DocumentsMapping.SubDocumentCategory;
import DocumentsMapping.Tag;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 13/07/13
 * Time: 14:05
 * To change this template use File | Settings | File Templates.
 */

public class IndexesCreator {
    public final static String FIELD_TYPE_TITLE = "type";
    private IndexWriter writer;
    private Directory directory;

    public enum fieldUsed {
        tags,
        title,
        partTitle,
        tagCount,
        type
    }

    public IndexesCreator() throws IOException {
    }

    public void createLuceneIndexes() {
        File documentFolder = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocuments());
        if (documentFolder.exists() && documentFolder.isDirectory() && !documentFolder.isHidden()) {
            for (File documentLang : documentFolder.listFiles()) {
                if (documentLang.isDirectory() && !documentLang.isHidden()) {
                    try {
                        String language = documentLang.getName();
                        File luceneIndex = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getLuceneIndexes() + language);
                        if (!luceneIndex.exists()) luceneIndex.mkdirs();
                        else deletePreviousLuceneIndex(luceneIndex.getPath());
                        directory = FSDirectory.open(luceneIndex);
                        Map<String, Analyzer> analyzerPerField = new HashMap<String, Analyzer>();
                        analyzerPerField.put(fieldUsed.title.toString(), new KeywordAnalyzer());
                        analyzerPerField.put(fieldUsed.partTitle.toString(), new KeywordAnalyzer());
                        analyzerPerField.put(fieldUsed.type.toString(), new KeywordAnalyzer());
                        analyzerPerField.put(fieldUsed.tagCount.name(), new KeywordAnalyzer());
                        PerFieldAnalyzerWrapper wrapper = new PerFieldAnalyzerWrapper(Utils.getLuceneAnalyser(new ClassifierLanguage(language)), analyzerPerField);
                        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_45, wrapper);
                        writer = new IndexWriter(directory, config);
                        addCategories2Index(DocumentMapper.getInstance().getCategories(new ClassifierLanguage(language)));
                        addCategories2Index(DocumentMapper.getInstance().getSubcategories(new ClassifierLanguage(language)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            writer.close();
                            directory.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                System.out.println(documentLang.getName() + " index created!");
            }
        }
    }

    private void deletePreviousLuceneIndex(String path) {
        for (File f : new File(path).listFiles()) {
            if (f.isDirectory() && !f.isHidden()) deletePreviousLuceneIndex(f.getPath());
            else f.delete();
        }


    }

    private void addCategories2Index(ArrayList<DocumentCategory> documentsCategories) {
        for (DocumentCategory documentCategory : documentsCategories) {
            String categoryName = documentCategory.getName();
            String categorySubName = null;
            if (documentCategory instanceof SubDocumentCategory)
                categorySubName = ((SubDocumentCategory) documentCategory).getSubName();
            addDoc(categoryName, categorySubName, documentCategory.getCategoryTags().getTags());

        }
    }

    private void addDoc(String title, String partTitle, ArrayList<Tag> tags) {
        try {
            ArrayList<Document> documents = new ArrayList<Document>();
            //        StringField tagCountField = new StringField(fieldUsed.tagCount.toString(), "", Field.Store.YES);
            for (Tag tag : tags) {
                Document tagDoc = new Document();
                tagDoc.add(new TextField(fieldUsed.tags.toString(), tag.getLemma(), Field.Store.YES));
//                tagCountField.setStringValue(String.valueOf(tag.getCount()));
                tagDoc.add(new IntField(fieldUsed.tagCount.toString(), tag.getCount(), Field.Store.YES));
                documents.add(tagDoc);
            }
            Document titleDoc = new Document();
            titleDoc.add(new StringField(fieldUsed.title.toString(), title, Field.Store.YES));
            if (partTitle != null)
                titleDoc.add(new StringField(fieldUsed.partTitle.toString(), partTitle, Field.Store.YES));
            titleDoc.add(new StringField(fieldUsed.type.toString(), fieldUsed.type.toString(), Field.Store.YES));
            documents.add(titleDoc);
            writer.addDocuments(documents);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

}
