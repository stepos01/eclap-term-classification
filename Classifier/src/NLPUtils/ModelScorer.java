package NLPUtils;


import Configuration.ClassifierConfiguration;
import DatabaseMapping.Category;
import DatabaseMapping.MysqlAccessor;
import NLPUtils.SearchHit.CategoryNameHit;
import NLPUtils.SearchHit.CategorySubNameHit;
import NLPUtils.SearchHit.CategoryTagHit;
import NLPUtils.SearchHit.ClassifierHit;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 07/09/13
 * Time: 20:09
 * To change this template use File | Settings | File Templates.
 */


public class ModelScorer {
    private static float TAG_HIT_SCORE;
    private static float TITLE_HIT_SCORE;
    private static float SCORE_THRESHOLD;
    private static float ACCURACY_THRESHOLD;
    private final static float TITLE_ACCURACY = 1;


    private static ModelScorer instance = null;

    private ModelScorer() {
        TITLE_HIT_SCORE = ClassifierConfiguration.getClassiferConfigurationInstance().getTitleHitScore();
        TAG_HIT_SCORE = ClassifierConfiguration.getClassiferConfigurationInstance().getTagHitScore();
        SCORE_THRESHOLD = ClassifierConfiguration.getClassiferConfigurationInstance().getScoreThreshold();
        ACCURACY_THRESHOLD = ClassifierConfiguration.getClassiferConfigurationInstance().getAccuracyThreshold();

    }

    public static ModelScorer getInstance() {
        if (instance == null) instance = new ModelScorer();
        return instance;
    }

    public ArrayList<Category> elaborateResults(ArrayList<ClassifierHit> hits, int totalWordCount, ClassifierLanguage language) {
        ArrayList<Category> categories = new ArrayList<Category>();
        for (ClassifierHit hit : hits) {
            Iterator<Category> it = categories.iterator();
            Boolean found_duplicate = false;
            while (it.hasNext()) {
                Category cat = it.next();
                if (Category.convertCategoryName(transformDocumentCat2dbCat(hit, language)).equals(cat.getName())) {
                    float oldScore = cat.getScore();
                    cat.setScore(oldScore + generateScore(hit, totalWordCount));
                    float oldAccuracy = cat.getAccuracy();
                    cat.setAccuracy(oldAccuracy + generateAccuracy(hit));
                    found_duplicate = true;
                }
            }
            if (!found_duplicate) {
                Category category = new Category(transformDocumentCat2dbCat(hit, language));
                String traductedTerm = MysqlAccessor.getMysqlAccessorSingleton().getTraductionForTaxonomyTerm(category.getName(), new ClassifierLanguage(ClassifierLanguage.english));
                Category originalCat = MysqlAccessor.getMysqlAccessorSingleton().getCategoryFromName(traductedTerm);
                assert originalCat != null;
                category.setTid(originalCat.getTid());
                category.setScore(generateScore(hit, totalWordCount));
                category.setAccuracy(generateAccuracy(hit));
                categories.add(category);
            }
        }
        Collections.sort(categories, new Comparator<Category>() {
            @Override
            public int compare(Category o1, Category o2) {
                int result = 0;
                if (o1.getScore() > o2.getScore()) result = -1;
                else if (o1.getScore() == o2.getScore()) result = 0;
                else if (o1.getScore() < o2.getScore()) result = 1;
                return result;
            }
        });
        ArrayList<String> taxonomyExclude = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getTaxonomyExclude());
        Iterator<Category> it = categories.iterator();
        while (it.hasNext()) {
            Category cat = it.next();
            if (!taxonomyExclude.contains(cat.getName())) {
                if (cat.getScore() < SCORE_THRESHOLD || cat.getAccuracy() < ACCURACY_THRESHOLD) {
                    it.remove();
                }
            } else {
                it.remove();
                System.out.println(cat.getName() + " is  listed as an  excluded category  in the configuration file");
            }

        }
        if (categories.isEmpty()) categories = null;
        return categories;

    }

    private float generateAccuracy(ClassifierHit hits) {
        //TODO modify in case
        float accuracy;
        if (hits instanceof CategoryNameHit) {
            accuracy = TITLE_ACCURACY;
        } else {
            assert (hits instanceof CategoryTagHit);
            accuracy = (float) ((CategoryTagHit) hits).getCount() / (float) ((CategoryTagHit) hits).getTotal_tags_count();
        }
        return accuracy;
    }

    private float generateScore(ClassifierHit hits, int totalWordCount) {
        //TODO MODIFY IN CASE
        float score;
        if (hits instanceof CategoryNameHit) {
            //hits on the title ..higher score
            score = TITLE_HIT_SCORE;
        } else {
            assert (hits instanceof CategoryTagHit);
            score = ((CategoryTagHit) hits).getCount() * TAG_HIT_SCORE;
            score = score / totalWordCount;
        }
        return score;
    }

    public String transformDocumentCat2dbCat(ClassifierHit hit, ClassifierLanguage language) {
        boolean found = false;
        String categoryName = null;
        try {
            ArrayList<Category> categories = MysqlAccessor.getMysqlAccessorSingleton().getTaxonomyCategoryArrayList();

            for (Category category : categories) {

                String name = category.getName();
                name = MysqlAccessor.getMysqlAccessorSingleton().getTraductionForTaxonomyTerm(name, language);

                ArrayList<String> list = Utils.tokenizeString(Utils.getLightAnalyser(language), name);
                if (list == null || list.isEmpty()) {
                    //   System.err.println("somethin went wrong tokenizing "+name+"\t"+"language: "+language.getLanguage() +"... probably category term is a stopword as well... processing it anyway!!");
                } else {
                    if (hit instanceof CategorySubNameHit) {
                        name = list.size() > 1 ? StringUtils.join(list, " ") : list.get(0);
                    } else if (hit instanceof CategoryTagHit) {
                        if (((CategoryTagHit) hit).getSubName() == null) name = name.replace(" ", "_");
                        else name = list.size() > 1 ? StringUtils.join(list, " ") : list.get(0);
                    } else {
                        assert hit instanceof CategoryNameHit;
                        name = name.replace(" ", "_");
                    }
                }
                if (name.equals(hit.getName())) {
                    categoryName = MysqlAccessor.getMysqlAccessorSingleton().getTraductionForTaxonomyTerm(category.getName(), language);
                    found = true;
                    break;
                }
            }
            if (!found) {
                try {
                    throw new Exception("category from classifier hit not found in db: " + hit.getName());
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return categoryName;
    }
}
