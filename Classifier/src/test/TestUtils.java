package Test;

import Configuration.ClassifierConfiguration;
import DatabaseMapping.Category;
import NLPUtils.Utils;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;

import java.io.File;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 18/10/13
 * Time: 18:56
 * To change this template use File | Settings | File Templates.
 */
public class TestUtils {
    public static final String SCORE_PREFIX = "S_";
    public static final String ACCURACY_PREFIX = "A_";

    public static ArrayList<TestData> readTestFile(float score, float accuracy)
    {
        String filename = SCORE_PREFIX
                .concat(String.valueOf(score))
                .concat("_")
                .concat(ACCURACY_PREFIX)
                .concat(String.valueOf(accuracy));
        ArrayList<TestData>datas = new ArrayList<TestData>();
        File folder = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getTestFileOutput());
        if (folder.exists()&&folder.isDirectory())
        {
            for (File file : folder.listFiles())
            {
                if (file.getName().contains(filename))
                {
                    ObjectBigArrayBigList<String> lines = Utils.loadBigFile(file.getPath());
                    for (String line : lines)
                        datas.add(TestData.parse(line));

                }
            }
            if (datas.isEmpty())
            {
                datas = null;
                System.err.println("no  file found with the requested paramters: score = "+score+"& accuracy = "+accuracy);
            }
        }
        else System.err.println("folder: "+folder.getPath()+" not found");
        return datas;
    }
    public static float getRecall(ArrayList<TestData> datas)
    {    float recall = 0;
        for (TestData data : datas)
        {
            ArrayList<Category> manualCategories = data.getManualCategory();
            ArrayList<Category> automaticCategories = data.getAutomaticCategory();
            if (manualCategories == null || automaticCategories == null)
                continue;
            else {
                for (Category category : manualCategories)
                {
                    int tid = category.getTid();
                    for (Category automaticCat: automaticCategories)
                    {
                        if (tid == automaticCat.getTid()){
                            recall = recall + ((float) 1 / manualCategories.size());
                            break;
                        }
                    }
                }
            }
        }
        return recall / datas.size();
    }
    public static float getPrecision (ArrayList<TestData> datas)
    {
        float precision = 0;
        for (TestData data : datas)
        {
            ArrayList<Category> manualCategories = data.getManualCategory();
            ArrayList<Category> automaticCategories = data.getAutomaticCategory();
            if (manualCategories == null || automaticCategories == null)
                continue;
            else {
                for (Category automaticCat : automaticCategories)
                {
                    int tid = automaticCat.getTid();
                    for (Category manualCat : manualCategories)
                    {
                        if (tid == manualCat.getTid())
                        {
                            precision += ((float)1/automaticCategories.size());
                            break;
                        }
                    }
                }
            }
        }
        return precision/datas.size();
    }


}
