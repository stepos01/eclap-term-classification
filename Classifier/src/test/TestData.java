package Test;

import DatabaseMapping.Category;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 18/10/13
 * Time: 16:33
 * To change this template use File | Settings | File Templates.
 */
public class TestData {
    private String test;
    private String axoid;
    private ArrayList<Category> manualCategory;
    private ArrayList<Category> automaticCategory;

    public TestData(String test, String axoid) {
        this.test = test;
        this.axoid = axoid;
        manualCategory = new ArrayList<Category>();
        automaticCategory = new ArrayList<Category>();
    }

    public TestData(String axoid) {
        this(axoid, null);
    }

    public String getTest() {
        return test;
    }

    public ArrayList<Category> getManualCategory() {
        return manualCategory;
    }

    public String getAxoid() {
        return axoid;
    }

    public ArrayList<Category> getAutomaticCategory() {
        return automaticCategory;
    }

    public void addManualCategory(Category manual) {
        manualCategory.add(manual);
    }

    public void addAutomaticCategory(Category automatic) {
        automaticCategory.add(automatic);
    }

    public String toString() {
        return axoid + "\t" + getCategoryTids(manualCategory) + "\t" + getCategoryTids(automaticCategory);
    }

    private String getCategoryTids(ArrayList<Category> categories) {
        if (categories == null)
            return "[]";
        ArrayList<Integer> tids = new ArrayList<Integer>();
        for (Category c : categories) {
            tids.add(c.getTid());
        }
        return "[".concat(StringUtils.join(tids, ",")).concat("]");
    }

    public void setAutomaticCategory(ArrayList<Category> automaticCategory) {
        this.automaticCategory = automaticCategory;
    }

    public void setManualCategory(ArrayList<Category> manualCategory) {
        this.manualCategory = manualCategory;
    }

    public static TestData parse(String line) {
        String[] components = line.split("\t");
        assert components.length == 3;
        TestData t = new TestData(components[0]);
        if (components[2].substring(1, components[2].length() - 1).isEmpty())
            t.setAutomaticCategory(null);
        else {
            String[] automaticCategoriesString = components[2].substring(1, components[2].length() - 1).split(",");
            for (String a : automaticCategoriesString)
                t.addAutomaticCategory(new Category(Integer.valueOf(a).intValue()));
        }
        if (components[1].substring(1, components[1].length() - 1).isEmpty())
            t.setManualCategory(null);
        else {
            String[] manualCategoriesString = components[1].substring(1, components[1].length() - 1).split(",");
            for (String m : manualCategoriesString)
                t.addManualCategory(new Category(Integer.valueOf(m).intValue()));
        }
        return t;
    }
}
