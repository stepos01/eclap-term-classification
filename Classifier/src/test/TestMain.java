package Test;

import Configuration.ClassifierConfiguration;
import DatabaseMapping.Category;
import DatabaseMapping.Dcmi;
import DatabaseMapping.MysqlAccessor;
import NLPUtils.*;
import NLPUtils.SearchHit.ClassifierHit;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.commons.configuration.ConfigurationException;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 18/10/13
 * Time: 16:32
 * To change this template use File | Settings | File Templates.
 */
public class TestMain {
    public static void main(String[] args) {
        args = new String[6];
        args[0] = "-user";
        args[1] = "root";
        args[2] = "-pwd";
        args[3] = "";
        args[4] = "-nmb";
        args[5] = "10";
        CommandLineParameters4test parameters = loadArgs(args);
        try {
            MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(parameters.getPassword());
            MysqlAccessor.getMysqlAccessorSingleton().setUSER(parameters.getUser());
            ClassifierConfiguration.getClassiferConfigurationInstance().checkConfiguration();
        } catch (ConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        }
     //   createGroundTruth(parameters.getNumber());
        displayTestResult(ClassifierConfiguration.getClassiferConfigurationInstance().getScoreThreshold(),ClassifierConfiguration.getClassiferConfigurationInstance().getAccuracyThreshold());
    }
    private static void createGroundTruth(int nmb)
    {
        ArrayList<TestData> completeDatas = new ArrayList<TestData>();
        while (completeDatas.size()<nmb)
        {
            ObjectBigArrayBigList<Dcmi> dcmis = selectDcmi4Test(nmb-completeDatas.size());
            ArrayList<TestData> automaticDatas = automaticTest(dcmis);
            completeDatas.addAll(manualTest(automaticDatas));
            Iterator<TestData> it = completeDatas.iterator();
            while (it.hasNext())
            {
                TestData data = it.next();
                if (data.getManualCategory()==null)
                    it.remove();
            }

        }
        printTestData(completeDatas);
    }
    private static CommandLineParameters4test loadArgs(String[] args) {
        CommandLineParameters4test commandLineParameters4test = new CommandLineParameters4test();
        CmdLineParser cmdLineParser = new CmdLineParser(commandLineParameters4test);
        try {
            cmdLineParser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java -jar Classifier.jar [options... ] arguments...");
            cmdLineParser.printUsage(System.err);
            System.exit(-1);
        }
        return commandLineParameters4test;
    }
    private static void displayTestResult(float score,float accuracy)
    {
        ArrayList<TestData> testDatas = TestUtils.readTestFile(score, accuracy);
        float precision = testDatas==null?null: TestUtils.getPrecision(testDatas);
        float recall = testDatas == null?null:TestUtils.getRecall(testDatas);
        System.out.println("precision: "+precision+"\t"+"recall: "+recall);
    }
    private static String getInput(String content) {
        String terms = null;
        try {
            System.out.println("\n Text: " + Utils.removeDoubleSpace(content));
            System.out.print("\n Enter taxonomy category: ");
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            terms = bufferRead.readLine();

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return terms;
    }

    private static ObjectBigArrayBigList<Dcmi> selectDcmi4Test(int compute) {
        ObjectBigArrayBigList<Dcmi> dcmi4Test = new ObjectBigArrayBigList<Dcmi>();
        ObjectBigArrayBigList<Dcmi> dcmifromDB = MysqlAccessor.getMysqlAccessorSingleton().getDcmifromDB();
        for (int j = 0; j < compute; j++)
        {
            long i = (long) (Math.random() * dcmifromDB.size64());
            while (!dcmifromDB.get(i).getDefLanguage().equals(new ClassifierLanguage(ClassifierLanguage.french))
                   )
            {
                i = (long) (Math.random() * (dcmifromDB.size64()));
            }
            dcmi4Test.add(dcmifromDB.get(i));
            while (!dcmifromDB.get(i).getDefLanguage().equals(new ClassifierLanguage(ClassifierLanguage.italian)))
            {
                i = (long) (Math.random() * (dcmifromDB.size64()));
            }
            dcmi4Test.add(dcmifromDB.get(i));
            while (!dcmifromDB.get(i).getDefLanguage().equals(new ClassifierLanguage(ClassifierLanguage.english)))
            {
                i = (long) (Math.random() * (dcmifromDB.size64()));
            }
            dcmi4Test.add(dcmifromDB.get(i));
        }
        return dcmi4Test;
    }

    private static ArrayList<TestData> manualTest(ArrayList<TestData> datas) {
        System.out.println("starting manual test");
        ArrayList<TestData> automaticDatas = new ArrayList<TestData>(datas);
        for (int i = 0; i < datas.size(); i++) {
            boolean done = false;
            while (!done) {
                done = true;
                String text = datas.get(i).getTest();
                String s = getInput(text);
                if (s.isEmpty()) automaticDatas.get(i).setManualCategory(null);
                else {
                    String[] categoriesName = s.split(",");
                    ArrayList<Category> categories = new ArrayList<Category>();
                    for (String cat : categoriesName) {
                        Category categoryFromName = MysqlAccessor.getMysqlAccessorSingleton().getCategoryFromName(cat);
                        if (categoryFromName == null) {
                            done = false;
                            System.err.println("couldn't parse " + cat + "!!");
                            break;
                        } else {
                            categories.add(categoryFromName);
                        }
                    }
                    if (done) automaticDatas.get(i).setManualCategory(categories);
                }

            }
        }
        return automaticDatas;

    }

    private static ArrayList<TestData> automaticTest(ObjectBigArrayBigList<Dcmi> dcmis) {
        System.out.println("starting automatic test");
        ArrayList<TestData> testDatas = new ArrayList<TestData>();
        try {
            for (long j = 0; j < dcmis.size64(); j++) {
                String text = dcmis.get(j).getPriorityLanguageContent().toString();
                ClassifierLanguage language = dcmis.get(j).getPriorityLanguageContent().getClassifierLanguage();
                int tokenNumber = Utils.tokenizeString(Utils.getLightAnalyser(language), text).size();
                ArrayList<ClassifierHit> hits = IndexesSearch.getInstance().search(text, language.getLanguage());
                ArrayList<ClassifierHit> agreggateHits = null;
                if (hits != null) agreggateHits = HitsAggregate.ordinateHits(hits);
                ArrayList<Category> categories = null;
                if (agreggateHits != null)
                    categories = ModelScorer.getInstance().elaborateResults(agreggateHits, tokenNumber, language);
                TestData data = new TestData(text, dcmis.get(j).getAxoid());
                data.setAutomaticCategory(categories);
                testDatas.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return testDatas;
    }

    private static void printTestData(ArrayList<TestData> datas) {
        int i = 1;
        String fileName = TestUtils.SCORE_PREFIX
                .concat(String.valueOf(ClassifierConfiguration.getClassiferConfigurationInstance().getScoreThreshold()))
                .concat("_")
                .concat(TestUtils.ACCURACY_PREFIX)
                .concat(String.valueOf(ClassifierConfiguration.getClassiferConfigurationInstance().getAccuracyThreshold()))
                .concat("_")
                .concat(String.valueOf(i));
        File testDataFile = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getTestFileOutput(), fileName);
        while (testDataFile.exists()) {
            fileName = TestUtils.SCORE_PREFIX
                    .concat(String.valueOf(ClassifierConfiguration.getClassiferConfigurationInstance().getScoreThreshold()))
                    .concat("_")
                    .concat(TestUtils.ACCURACY_PREFIX)
                    .concat(String.valueOf(ClassifierConfiguration.getClassiferConfigurationInstance().getAccuracyThreshold()))
                    .concat("_")
                    .concat(String.valueOf(i + 1));
            testDataFile = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getTestFileOutput(), fileName);
        }
        assert !testDataFile.exists();
        try {
            testDataFile.createNewFile();
            ArrayList<String> lines = new ArrayList<String>();
            for (TestData data : datas) {
                String line = data.toString();
                lines.add(line);
            }
            Utils.writeLargerTextFile(testDataFile.getPath(), lines);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

}
