package LightAnalyzer;

import NLPUtils.Utils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.ElisionFilter;
import org.apache.lucene.analysis.util.StopwordAnalyzerBase;
import org.apache.lucene.analysis.util.WordlistLoader;
import org.apache.lucene.util.IOUtils;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 29/09/13
 * Time: 15:00
 * To change this template use File | Settings | File Templates.
 */
public class ItalianLightAnalyzer extends StopwordAnalyzerBase {
    /**
     * File containing default Italian stopwords.
     */
    public final static String DEFAULT_STOPWORD_FILE = "italian_stop.txt";

    private static final CharArraySet DEFAULT_ARTICLES = CharArraySet.unmodifiableSet(
            new CharArraySet(Version.LUCENE_CURRENT,
                    Arrays.asList(
                            "c", "l", "all", "dall", "dell", "nell", "sull", "coll", "pell",
                            "gl", "agl", "dagl", "degl", "negl", "sugl", "un", "m", "t", "s", "v", "d"
                    ), true));

    /**
     * Returns an unmodifiable instance of the default stop words set.
     *
     * @return default stop words set.
     */
    public static CharArraySet getDefaultStopSet() {
        return new CharArraySet(Version.LUCENE_CURRENT, Utils.createStopWordList(DefaultSetHolder.DEFAULT_STOP_SET), true);
    }

    /**
     * Atomically loads the DEFAULT_STOP_SET in a lazy fashion once the outer class
     * accesses the static final set the first time.;
     */
    private static class DefaultSetHolder {
        static final CharArraySet DEFAULT_STOP_SET;

        static {
            try {
                DEFAULT_STOP_SET = WordlistLoader.getSnowballWordSet(IOUtils.getDecodingReader(SnowballFilter.class,
                        DEFAULT_STOPWORD_FILE, IOUtils.CHARSET_UTF_8), Version.LUCENE_CURRENT);
            } catch (IOException ex) {
                // default set should always be present as it is part of the
                // distribution (JAR)
                throw new RuntimeException("Unable to load default stopword set");
            }
        }
    }

    /**
     * Builds an analyzer with the default stop words: {@link #DEFAULT_STOPWORD_FILE}.
     */
    public ItalianLightAnalyzer(Version matchVersion) {
        super(matchVersion, DefaultSetHolder.DEFAULT_STOP_SET);
    }


    /**
     * Creates a
     * {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
     * which tokenizes all the text in the provided {@link java.io.Reader}.
     *
     * @return A
     *         {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
     *         built from an {@link org.apache.lucene.analysis.standard.StandardTokenizer} filtered with
     *         {@link org.apache.lucene.analysis.standard.StandardFilter}, {@link org.apache.lucene.analysis.util.ElisionFilter}, {@link org.apache.lucene.analysis.core.LowerCaseFilter}, {@link org.apache.lucene.analysis.core.StopFilter}
     *         , {@link org.apache.lucene.analysis.miscellaneous.SetKeywordMarkerFilter} if a stem exclusion set is
     *         provided and {@link org.apache.lucene.analysis.it.ItalianLightStemFilter}.
     */
    @Override
    protected Analyzer.TokenStreamComponents createComponents(String fieldName,
                                                              Reader reader) {
        final Tokenizer source = new WhitespaceTokenizer(matchVersion, reader);
        TokenStream result = new StandardFilter(matchVersion, source);
        result = new ElisionFilter(result, DEFAULT_ARTICLES);
        result = new LowerCaseFilter(matchVersion, result);
        result = new StopFilter(matchVersion, result, getDefaultStopSet());
        return new Analyzer.TokenStreamComponents(source, result);
    }
}
