package DatabaseMapping;

import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 30/06/13
 * Time: 14:54
 * To change this template use File | Settings | File Templates.
 */
public class TaxonomyLink {

    private String axoid;
    private int tid;

    public TaxonomyLink() {
    }

    public TaxonomyLink(String axoid, int tid) {
        this.axoid = axoid;
        this.tid = tid;

    }

    public java.util.Date getTaxExtractionDate() {
        return new Timestamp(new java.util.Date().getTime());
    }


    public String getAxoid() {
        return axoid;
    }

    public void setAxoid(String axoid) {
        this.axoid = axoid;
    }

    public int getTid() {
        return tid;
    }

    private void setTid(int tid) {
        this.tid = tid;
    }

    public String toString() {
        String line = this.getAxoid() + "\t" + this.getTid() + "\t" + this.getTaxExtractionDate();
        return line.replaceAll("null", "\\\\N");
    }
}
