package DatabaseMapping;


import NLPUtils.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 02/07/13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */

public class Category {
    private String name;
    private int tid;
    private float score;
    private float accuracy;

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public Category(int tid) {
        this.tid = tid;
    }

    public Category(String name, int tid) {
        this.tid = tid;
        this.name = convertCategoryName(name);
    }

    public Category(String name) {
        this.name = convertCategoryName(name);
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return "name: " + getName() + "\t" + "score: " + getScore() + "\t" + "accuracy: " + getAccuracy();
    }

    public static String convertCategoryName(String categoryName) {
        return Utils.deAccentuate(categoryName).trim().toLowerCase();
    }


}
