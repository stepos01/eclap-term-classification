package DatabaseMapping;

import NLPUtils.ClassifierLanguage;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 14/07/13
 * Time: 13:08
 * To change this template use File | Settings | File Templates.
 */
public class LanguageContents {
    ClassifierLanguage classifierLanguage;
    ArrayList<String> content;

    public LanguageContents(String lang, String content) {
        this.classifierLanguage = new ClassifierLanguage(lang);
        this.content = new ArrayList<String>();
        this.content.add(content);
    }

    public void addContent(String content) {
        this.content.add(content);
    }

    public ArrayList<String> getContent() {
        return content;
    }

    public ClassifierLanguage getClassifierLanguage() {
        return classifierLanguage;
    }

    public String toString() {
        return StringUtils.join(content, " ");
    }

}
