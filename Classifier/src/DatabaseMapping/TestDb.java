package DatabaseMapping;

import NLPUtils.ClassifierLanguage;
import org.apache.commons.configuration.ConfigurationException;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 08/10/13
 * Time: 14:51
 * To change this template use File | Settings | File Templates.
 */
public class TestDb {
    public static void main(String[] args) {
        try {
            MysqlAccessor.getMysqlAccessorSingleton().setUSER("root");
            MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD("");
            MysqlAccessor.getMysqlAccessorSingleton().checkConfiguration();
        } catch (ConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static void testdatabase() {
        try {
            Category categoryFromName = MysqlAccessor.getMysqlAccessorSingleton().getCategoryFromName("Management and organisation");
            Category topLevelCategory = MysqlAccessor.getMysqlAccessorSingleton().getTopLevelCategory(categoryFromName);
            Category categoryFromName1 = MysqlAccessor.getMysqlAccessorSingleton().getCategoryFromName("lyric");
            Category topLevelCategory1 = MysqlAccessor.getMysqlAccessorSingleton().getTopLevelCategory(categoryFromName1);
            String fr = MysqlAccessor.getMysqlAccessorSingleton().getTraductionForTaxonomyTerm("Management and organisation", new ClassifierLanguage("fr"));
            System.out.println("done!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
