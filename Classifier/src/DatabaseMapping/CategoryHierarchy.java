package DatabaseMapping;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 18/09/13
 * Time: 20:28
 * To change this template use File | Settings | File Templates.
 */
public class CategoryHierarchy {
    private Category parent;
    private Category son;

    public CategoryHierarchy(Category son, Category parent) {
        this.son = son;
        this.parent = parent;
    }

    public Category getParent() {
        return parent;
    }

    public Category getSon() {
        return son;
    }
}
