package DatabaseMapping;

import NLPUtils.ClassifierLanguage;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 14/07/13
 * Time: 13:02
 * To change this template use File | Settings | File Templates.
 */
public class Dcmi {

    private String axoid;
    private ArrayList<LanguageContents> contentList;
    private ClassifierLanguage defLanguage;

    public Dcmi(String axoid, String defLanguage) {
        this.axoid = axoid;
        contentList = new ArrayList<LanguageContents>();
        this.defLanguage = (defLanguage == null ? null : new ClassifierLanguage(defLanguage));
    }

    public void addContent(String lang, String newContent) {
        try {
            LanguageContents oldContent = getContentsForLanguage(ClassifierLanguage.convert(lang));
            if (oldContent == null) {
                contentList.add(new LanguageContents(lang, newContent));
            } else {
                contentList.remove(oldContent);
                oldContent.addContent(newContent);
                contentList.add(oldContent);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public LanguageContents getPriorityLanguageContent() throws Exception {
        LanguageContents priorityContent;
        if (defLanguage != null) {
            priorityContent = getContentsForLanguage(defLanguage.getLanguage());
        } else {
            priorityContent = getLowPriority();
        }
        return priorityContent;
    }

    private LanguageContents getLowPriority() throws Exception {
        if (getContentsForLanguage(ClassifierLanguage.english) != null)
            return getContentsForLanguage(ClassifierLanguage.english);
        else if (getContentsForLanguage(ClassifierLanguage.italian) != null)
            return getContentsForLanguage(ClassifierLanguage.italian);
        else if (getContentsForLanguage(ClassifierLanguage.french) != null)
            return getContentsForLanguage(ClassifierLanguage.french);
        else if (getContentsForLanguage(ClassifierLanguage.catalan) != null)
            return getContentsForLanguage(ClassifierLanguage.catalan);
        else if (getContentsForLanguage(ClassifierLanguage.hungarian) != null)
            return getContentsForLanguage(ClassifierLanguage.hungarian);
        else if (getContentsForLanguage(ClassifierLanguage.polish) != null)
            return getContentsForLanguage(ClassifierLanguage.polish);
        else if (getContentsForLanguage(ClassifierLanguage.dutch) != null)
            return getContentsForLanguage(ClassifierLanguage.dutch);
        else if (getContentsForLanguage(ClassifierLanguage.spanish) != null)
            return getContentsForLanguage(ClassifierLanguage.spanish);
        else if (getContentsForLanguage(ClassifierLanguage.portuguese) != null)
            return getContentsForLanguage(ClassifierLanguage.portuguese);
        else if (getContentsForLanguage(ClassifierLanguage.greek) != null)
            return getContentsForLanguage(ClassifierLanguage.greek);
        else if (getContentsForLanguage(ClassifierLanguage.danish) != null)
            return getContentsForLanguage(ClassifierLanguage.danish);
        else if (getContentsForLanguage(ClassifierLanguage.slovene) != null)
            return getContentsForLanguage(ClassifierLanguage.slovene);
        else if (getContentsForLanguage(ClassifierLanguage.german) != null)
            return getContentsForLanguage(ClassifierLanguage.german);
        else throw new Exception("not lang available for this axoid: " + axoid);
    }

    public ArrayList<LanguageContents> getContentList() {
        return new ArrayList<LanguageContents>();
    }

    public String getAxoid() {
        return axoid;
    }

    private LanguageContents getContentsForLanguage(String language) {
        LanguageContents languageContents = null;
        ClassifierLanguage classifierLanguage = new ClassifierLanguage(language);
        for (LanguageContents content : contentList) {
            if (content.getClassifierLanguage().getLanguage().equals(classifierLanguage.getLanguage())) {
                languageContents = content;
                break;
            }
        }
        return languageContents;
    }

    public ClassifierLanguage getDefLanguage() {
        return defLanguage;
    }

    public String toString() {
        return "axoid: " + axoid + "\t" + defLanguage.toString() + "\t" + contentList.toString();


    }
}
