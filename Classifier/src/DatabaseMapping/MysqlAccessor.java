package DatabaseMapping;

import Configuration.ClassifierConfiguration;
import NLPUtils.ClassifierLanguage;
import NLPUtils.Utils;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 30/06/13
 * Time: 20:11
 * To change this template use File | Settings | File Templates.
 */
public class MysqlAccessor {
    private ArrayList<CategoryHierarchy> categoryHierarchyArrayList = null;
    private ArrayList<Category> taxonomyCategoryArrayList = null;
    private HashMap<String, String> defLanguagesHashMap = null;
    private HashMap<String, ArrayList<String>> optionalFieldHashMap = null;
    private ArrayList<CategoryDictionary> categoryDictionaryList = null;
    private static String URL;
    private static String USER = null;
    private static String PASSWORD = null;
    private static String AXOID_DB = null;
    private static String AXOID_TABLE = null;
    private static String TERM_DB = null;
    private static String TERM_TABLE = null;
    private static String TERM_HIERARCHY_DB = null;
    private static String TERM_HIERARCHY_TABLE = null;
    private static String TAXONOMY_LINK_DB = null;
    private static String TAXONOMY_LINK_TABLE = null;
    private static String TERM_TABLE_TID = null;
    private static String TERM_TABLE_NAME = null;
    private static String TAXONOMY_LINK_TABLE_TID = null;
    private static String TAXONOMY_LINK_TABLE_AXOID = null;
    private static String TAXONOMY_LINK_TABLE_TAXEXTRACTIONDATE = null;
    private static String AXOIDDB_AXOID = null;
    private static String AXOIDDB_TERMVALUE = null;
    private static String AXOIDDB_LANGUAGE = null;
    private static String AXOIDDB_TERMREFINEMENT = null;
    private static String TERM_HIERARCHY_DB_TID = null;
    private static String TERM_HIERARCHY_DB_PARENT = null;
    private static String TAXONOMY_LANGUAGE_DB = null;
    private static String TAXONOMY_LANGAUE_TABLE = null;
    private static String ROOT_OBJECT_INFO_DB = null;
    private static String ROOT_OBJECT_INFO_TABLE = null;
    private static String ROOT_OBJECT_INFO_AXOID = null;
    private static String ROOT_OBJECT_INFO_DEFLANGUAGE = null;
    private static String OPTIONAL_FIELD_DB = null;
    private static String OPTIONAL_FIELD_TABLE = null;
    private static String OPTIONAL_FIELD_AXOID = null;
    private static String OPTIONAL_FIELD_FIELDVALUE = null;
    private static String OPTIONAL_FIELD_FIELDNAME = null;
    private static String TERM_TABLE_VID = null;
    private static MysqlAccessor mysqlAccessor = null;

    private MysqlAccessor() {
        ClassifierConfiguration configuration = ClassifierConfiguration.getClassiferConfigurationInstance();
        System.out.println("Loading  database configuration FROM " + ClassifierConfiguration.CONFIG_PATH);
        AXOID_DB = configuration.getAxoidDB();
        AXOID_TABLE = configuration.getAxoidTable();
        TERM_DB = configuration.getTermDB();
        TERM_TABLE = configuration.getTermTable();
        TERM_HIERARCHY_DB = configuration.getTermHierarchyDB();
        TERM_HIERARCHY_TABLE = configuration.getTermHierarchyTable();
        TAXONOMY_LINK_DB = configuration.getTaxonomyLinkDB();
        TAXONOMY_LINK_TABLE = configuration.getTaxonomyLinkTable();
        TERM_TABLE_TID = configuration.getTermDatatid();
        TERM_TABLE_NAME = configuration.getTermDataName();
        URL = configuration.getUrl();
        TAXONOMY_LINK_TABLE_TID = configuration.getTaxonomyLinkTableTid();
        TAXONOMY_LINK_TABLE_AXOID = configuration.getTaxonomyLinkTableAxoid();
        TAXONOMY_LINK_TABLE_TAXEXTRACTIONDATE = configuration.getTaxonomyLinkTableTaxExtractionDate();
        AXOIDDB_AXOID = configuration.getAxoidDBAxoid();
        AXOIDDB_TERMVALUE = configuration.getAxoidDBTermValue();
        AXOIDDB_LANGUAGE = configuration.getAxoiDBLanguage();
        TERM_HIERARCHY_DB_TID = configuration.getTermHierarchyDBTid();
        TERM_HIERARCHY_DB_PARENT = configuration.getTermHierarchyDBParent();
        TAXONOMY_LANGUAGE_DB = configuration.getTaxonomyLanguageDB();
        TAXONOMY_LANGAUE_TABLE = configuration.getTaxonomyLanguageTable();
        ROOT_OBJECT_INFO_DB = configuration.getRootObjectInfoDB();
        ROOT_OBJECT_INFO_DEFLANGUAGE = configuration.getRootObjectInfoDefLanguage();
        ROOT_OBJECT_INFO_AXOID = configuration.getRootObjectAxoid();
        ROOT_OBJECT_INFO_TABLE = configuration.getRootObjectInfoTable();
        OPTIONAL_FIELD_AXOID = configuration.getOptionalFieldAxoid();
        OPTIONAL_FIELD_FIELDVALUE = configuration.getOptionalFieldFieldValue();
        OPTIONAL_FIELD_TABLE = configuration.getOptionalFieldTable();
        OPTIONAL_FIELD_DB = configuration.getOptionalFieldDB();
        OPTIONAL_FIELD_FIELDNAME = configuration.getOptionalFieldFieldName();
        AXOIDDB_TERMREFINEMENT = configuration.getAxoidDBTermrefinement();
        TERM_TABLE_VID = configuration.getTermDataVid();
        try {
            checkConfiguration();
        } catch (ConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        }
    }

    private static void close(ResultSet resultSet, Statement statement, Connection connect) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
        }
    }

    public void setPASSWORD(String PASSWORD) {
        MysqlAccessor.PASSWORD = PASSWORD;
    }

    public void setUSER(String USER) {
        MysqlAccessor.USER = USER;
    }

    public void checkConfiguration() throws ConfigurationException {
        verifyLanguageList();
        if (AXOID_DB.isEmpty())
            throw new ConfigurationException("axoid.db not set in eclap.classifier.properties file");
        if (AXOID_TABLE.isEmpty())
            throw new ConfigurationException("axoid.table not set in eclap.classifier.properties file");
        if (TERM_DB.isEmpty()) throw new ConfigurationException("term.db not set in eclap.classifier.properties file");
        if (TERM_TABLE.isEmpty())
            throw new ConfigurationException("term.table not set in eclap.classifier.properties file");
        if (TERM_HIERARCHY_DB.isEmpty())
            throw new ConfigurationException("term.hierarchy.db not set in eclap.classifier.properties file");
        if (TERM_HIERARCHY_TABLE.isEmpty())
            throw new ConfigurationException("term.hierarchy.table not set in eclap.classifier.properties file");
        if (TAXONOMY_LINK_DB.isEmpty())
            throw new ConfigurationException("taxonomy.link.db not set in eclap.classifier.properties file");
        if (TAXONOMY_LINK_TABLE.isEmpty())
            throw new ConfigurationException("taxonomy.link.table not set in eclap.classifier.properties file");
        if (URL.isEmpty()) throw new ConfigurationException("url  not set in eclap.classifier.properties file");
        if (TERM_TABLE_TID.isEmpty())
            throw new ConfigurationException("term.table.tid  not set in eclap.classifier.properties file");
        if (TERM_TABLE_NAME.isEmpty())
            throw new ConfigurationException("term.table.name  not set in eclap.classifier.properties file");
        if (TAXONOMY_LINK_TABLE_TID.isEmpty())
            throw new ConfigurationException("taxonomy.link.table.tid   not set in eclap.classifier.properties file");
        if (TAXONOMY_LINK_TABLE_AXOID.isEmpty())
            throw new ConfigurationException("taxonomy.link.table.axoid not set in eclap.classifier.properties file");
        if (TAXONOMY_LANGUAGE_DB.isEmpty())
            throw new ConfigurationException("taxonomy.language.db not set in eclap.classifier.properties file");
        if (TAXONOMY_LANGAUE_TABLE.isEmpty())
            throw new ConfigurationException("taxonomy.language.table not set in eclap.classifier.properties file");
        if (ROOT_OBJECT_INFO_TABLE.isEmpty())
            throw new ConfigurationException("axoid.rootobjectinfo.table not set in eclap.classifier.properties file");
        if (ROOT_OBJECT_INFO_DEFLANGUAGE.isEmpty())
            throw new ConfigurationException("axoid.rootobjectinfo.deflanguage not set in eclap.classifier.properties file");
        if (ROOT_OBJECT_INFO_AXOID.isEmpty())
            throw new ConfigurationException("axoid.rootobjectinfo.rootobjectaxoid not set in eclap.classifier.properties file");
        if (OPTIONAL_FIELD_AXOID.isEmpty())
            throw new ConfigurationException("axoid.optionalfield.axoid not set in eclap.classifier.properties file");
        if (OPTIONAL_FIELD_TABLE.isEmpty())
            throw new ConfigurationException("axoid.optionalfield.table not set in eclap.classifier.properties file");
        if (OPTIONAL_FIELD_FIELDVALUE.isEmpty())
            throw new ConfigurationException("axoid.optionalfield.fieldvalue not set in eclap.classifier.properties file");
        if (ROOT_OBJECT_INFO_DB.isEmpty())
            throw new ConfigurationException("axoid.rootobjectinfo.db not set in eclap.classifier.properties file");
        if (OPTIONAL_FIELD_DB.isEmpty())
            throw new ConfigurationException("axoid.optionalfield.db not set in eclap.classifier.properties file");
        if (OPTIONAL_FIELD_FIELDNAME.isEmpty())
            throw new ConfigurationException("axoid.optionalfield.fieldName not set in eclap.classifier.properties file");
        if (AXOIDDB_TERMREFINEMENT.isEmpty())
            throw new ConfigurationException("axoid.table.termrefinement not set in eclap.classifier.properties file");
        if (TERM_TABLE_VID.isEmpty())
            throw new ConfigurationException("term.table.vid not set in eclap.classifier.properties file");
    }

    public static MysqlAccessor getMysqlAccessorSingleton() {
        if (mysqlAccessor == null) mysqlAccessor = new MysqlAccessor();
        return mysqlAccessor;
    }

    private static void createTaxonomy_link() {
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + TAXONOMY_LINK_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            DatabaseMetaData DBmetadata = connect.getMetaData();
            resultSet = DBmetadata.getTables(null, null, TAXONOMY_LINK_TABLE, null);
            if (resultSet.next()) return;
            statement = connect.createStatement();
            statement.execute("CREATE  TABLE `" + TAXONOMY_LINK_DB + "`.`" + TAXONOMY_LINK_TABLE + "` (" +
                    "  `id` INT NOT NULL AUTO_INCREMENT ," +
                    TAXONOMY_LINK_TABLE_TID + " VARCHAR(60) NOT NULL ," +
                    TAXONOMY_LINK_TABLE_AXOID + "  VARCHAR(60) NOT NULL ," +
                    TAXONOMY_LINK_TABLE_TAXEXTRACTIONDATE + " DATETIME NULL ," +
                    "  PRIMARY KEY (`id`) ," +
                    "  UNIQUE INDEX `id_UNIQUE` (`id` ASC) );");
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
    }

    public String getTraductionForTaxonomyTerm(String term, ClassifierLanguage language) {
        String traduction = null;
        ArrayList<CategoryDictionary> dictionaryArrayList = getCategoryDictionaryList();
        for (int i = 0; i < dictionaryArrayList.size(); i++) {
            CategoryDictionary dictionary = dictionaryArrayList.get(i);
            if (dictionary.contains(Utils.deAccentuate(term))) {
                traduction = dictionary.getTraduction(language);
                break;
            }
        }
        if ((traduction == null)) {
            throw new AssertionError("term " + term + " traduction in " + language.getLanguage() + " was not found");
        }
        return traduction;
    }

    private void getTaxonomyCategories() {
        ArrayList<Category> taxonomycategories = new ArrayList<Category>();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + TERM_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            resultSet = statement
                    .executeQuery("SELECT " + TERM_TABLE_TID + "," + TERM_TABLE_NAME + " from " + TERM_DB + "." + TERM_TABLE + " where " + TERM_TABLE_VID + " =" + 5);
            while (resultSet.next()) {
                int tid = resultSet.getInt(1);
                String category_name = resultSet.getString(2);
                taxonomycategories.add(new Category(category_name, tid));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);

        } finally {
            close(resultSet, statement, connect);
        }
        taxonomyCategoryArrayList = taxonomycategories;
    }

    public int isAxoidClassifiedeInTaxonomyLink(String axoid) {
        ArrayList<Category> taxonomycategories = new ArrayList<Category>();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        int result = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + TAXONOMY_LINK_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            resultSet = statement
                    .executeQuery("SELECT " + TERM_HIERARCHY_DB_TID + " from " + TAXONOMY_LINK_DB + "." + TAXONOMY_LINK_TABLE + " where " + AXOIDDB_AXOID + " = " + Utils.formatParameter(axoid));
            if (resultSet.next()) {
                if (resultSet.getInt(TERM_HIERARCHY_DB_TID) == 0)         // null cell in resultset
                {
                    result = -1;
                } else
                    result = 1;
            } else
                result = 0;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);

        } finally {
            close(resultSet, statement, connect);
        }
        return result;
    }

    public Category getCategoryFromName(String categoryName) {
        Category category = null;
        ArrayList<Category> taxonomyCategories = getTaxonomyCategoryArrayList();
        for (Category cat : taxonomyCategories) {
            if (cat.getName().equals(Category.convertCategoryName(categoryName))) {
                category = cat;
                break;
            }
        }
        return category;
    }

    public Category getCategoryfromTid(int tid) {
        Category category = null;
        ArrayList<Category> taxonomyCategories = getTaxonomyCategoryArrayList();
        for (Category cat : taxonomyCategories) {
            if (cat.getTid() == tid) {
                category = cat;
                break;
            }
        }
        return category;
    }

    public ArrayList<Category> getTaxonomyCategoryArrayList() {
        if (taxonomyCategoryArrayList == null) getTaxonomyCategories();
        return taxonomyCategoryArrayList;
    }

    public ArrayList<Dcmi> getDcmibyPublisher(String publisher, int number) {
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<String> axoidOfPublisher = new ArrayList<String>();
        HashMap<String, Dcmi> publisherMap = new HashMap<String, Dcmi>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            resultSet = statement.executeQuery("SELECT " + AXOIDDB_AXOID + " FROM " + AXOID_DB + "." + AXOID_TABLE + " WHERE " + AXOIDDB_TERMVALUE + " = " + publisher);
            while (resultSet.next()) {
                axoidOfPublisher.add(resultSet.getString(1));
            }
            for (String axoid : axoidOfPublisher) {
                String query = "SELECT " + AXOIDDB_AXOID + "," + AXOIDDB_TERMVALUE + "," + AXOIDDB_LANGUAGE + " FROM " + AXOID_DB + "." + AXOID_TABLE + " where " + AXOIDDB_TERMVALUE + " > '' AND " + AXOIDDB_LANGUAGE + " > '' AND " + AXOIDDB_AXOID + " = " + axoid;
                ArrayList<String> parameters = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermNameIncluded());
                ArrayList<String> formatedParameters = new ArrayList<String>();
                for (String param : parameters) {
                    formatedParameters.add(Utils.formatParameter(param));
                }
                if (!parameters.isEmpty() && !parameters.get(0).isEmpty()) {
                    query = query.concat(" AND( " + AXOIDDB_TERMREFINEMENT + "=");
                    query = query.concat(StringUtils.join(formatedParameters, " OR " + AXOIDDB_TERMREFINEMENT + "="));
                    query = query.concat(")");
                }
                resultSet = statement.executeQuery(query);
                while (resultSet.next()) {
                    String termValue = resultSet.getString(AXOIDDB_TERMVALUE);
                    String lang = resultSet.getString(AXOIDDB_LANGUAGE);
                    if (publisherMap.containsKey(axoid)) {
                        publisherMap.get(axoid).addContent(lang, termValue);
                    } else {
                        String defLanguage = getDefLanguagesHashMap().get(axoid);
                        publisherMap.put(axoid, new Dcmi(axoid, defLanguage));
                        publisherMap.get(axoid).addContent(lang, termValue);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
        publisherMap = includeOptionalField(publisherMap);
        ArrayList<Dcmi> dcmisByPublisher = new ArrayList<Dcmi>();
        ArrayList<Integer> integers = new ArrayList<Integer>();
        if (publisherMap.values().size() > number) {
            for (int i = 0; i < number; i++) {
                int randomNumber = RandomUtils.nextInt(publisherMap.values().size());
                if (!integers.contains(randomNumber)) {
                    integers.add(randomNumber);
                    dcmisByPublisher.add(new ArrayList<Dcmi>(publisherMap.values()).get(randomNumber));
                }
            }
        }
        return dcmisByPublisher;

    }

    public ArrayList<String> getAxoid4Publisher(String publisher) {
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<String> axoidListForPublisher = new ArrayList<String>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            String query = "select " + AXOIDDB_AXOID + " FROM " + AXOID_DB + "." + AXOID_TABLE + " WHERE " + AXOIDDB_TERMREFINEMENT + " = " + Utils.formatParameter(ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermrefinement_publisher()) + " AND " + AXOIDDB_TERMVALUE + " = " + Utils.formatParameter(publisher);
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String axoid = resultSet.getString(AXOIDDB_AXOID);
                axoidListForPublisher.add(axoid);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return axoidListForPublisher;
    }

    public ObjectBigArrayBigList<Dcmi> getDcmifromDBForPublisher(String publisher) {
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        HashMap<String, Dcmi> dcmiHashMap = new HashMap<String, Dcmi>();
        ArrayList<String> axoidListForPublisher = new ArrayList<String>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            String query = "select " + AXOIDDB_AXOID + " FROM " + AXOID_DB + "." + AXOID_TABLE + " WHERE " + AXOIDDB_TERMREFINEMENT + " = " + Utils.formatParameter(ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermrefinement_publisher()) + " AND " + AXOIDDB_TERMVALUE + " = " + Utils.formatParameter(publisher);
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String axoid = resultSet.getString(AXOIDDB_AXOID);
                axoidListForPublisher.add(axoid);
            }
            for (int i = 0; i < axoidListForPublisher.size(); i++) {
                String axoid = axoidListForPublisher.get(i);
                query = "SELECT " + AXOIDDB_AXOID + "," + AXOIDDB_TERMVALUE + "," + AXOIDDB_LANGUAGE + " FROM " + AXOID_DB + "." + AXOID_TABLE + " where " + AXOIDDB_TERMVALUE + " > '' AND " + AXOIDDB_LANGUAGE + " > '' AND " + AXOIDDB_AXOID + " = " + Utils.formatParameter(axoid);
                ArrayList<String> parameters = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermNameIncluded());
                ArrayList<String> formatedParameters = new ArrayList<String>();
                for (String param : parameters) {
                    formatedParameters.add(Utils.formatParameter(param));
                }
                if (!parameters.isEmpty() && !parameters.get(0).isEmpty()) {
                    query = query.concat(" AND( " + AXOIDDB_TERMREFINEMENT + "=");
                    query = query.concat(StringUtils.join(formatedParameters, " OR " + AXOIDDB_TERMREFINEMENT + "="));
                    query = query.concat(")");
                }
                resultSet = statement
                        .executeQuery(query);
                while (resultSet.next()) {
                    String termValue = resultSet.getString(AXOIDDB_TERMVALUE);
                    String lang = resultSet.getString(AXOIDDB_LANGUAGE);
                    if (dcmiHashMap.containsKey(axoid)) {
                        dcmiHashMap.get(axoid).addContent(lang, termValue);
                    } else {
                        String defLanguage = getDefLanguagesHashMap().get(axoid);
                        dcmiHashMap.put(axoid, new Dcmi(axoid, defLanguage));
                        dcmiHashMap.get(axoid).addContent(lang, termValue);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
        dcmiHashMap = includeOptionalField(dcmiHashMap);
        return new ObjectBigArrayBigList<Dcmi>(dcmiHashMap.values().iterator());
    }

    public ObjectBigArrayBigList<Dcmi> getDcmifromDB() {
        HashSet<String> alreadyProcessed = loadDcmiAlreadyProcessed();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        HashMap<String, Dcmi> dcmiHashMap = new HashMap<String, Dcmi>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            String query = "SELECT " + AXOIDDB_AXOID + "," + AXOIDDB_TERMVALUE + "," + AXOIDDB_LANGUAGE + " FROM " + AXOID_DB + "." + AXOID_TABLE + " where " + AXOIDDB_TERMVALUE + " > '' AND " + AXOIDDB_LANGUAGE + " > '' ";
            ArrayList<String> parameters = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermNameIncluded());
            ArrayList<String> formatedParameters = new ArrayList<String>();
            for (String param : parameters) {
                formatedParameters.add(Utils.formatParameter(param));
            }
            if (!parameters.isEmpty() && !parameters.get(0).isEmpty()) {
                query = query.concat(" AND( " + AXOIDDB_TERMREFINEMENT + "=");
                query = query.concat(StringUtils.join(formatedParameters, " OR " + AXOIDDB_TERMREFINEMENT + "="));
                query = query.concat(")");
            }
//
//          resultSet = statement
//                    .executeQuery("SELECT "+AXOIDDBAXOID+","+AXOIDDB_TERMVALUE+","+AXOIDDBLANGUAGE+" FROM "+AXOID_DB+"."+AXOID_TABLE+" where termRefinement != 'language' and termRefinement !='contributor' and language IS NOT NULL and termRefinement !='coverage_spatial' and termRefinement !='creator' and termValue != '' and termRefinement !='provenance' and termRefinement !='format_extent' and termRefinement !='format_medium' and termRefinement!='description_tableOfContents' order by axoid and termRefinement != 'coverage' ");
            resultSet = statement
                    .executeQuery(query);
            while (resultSet.next()) {
                String axoid = resultSet.getString(AXOIDDB_AXOID);
                String termValue = resultSet.getString(AXOIDDB_TERMVALUE);
                String lang = resultSet.getString(AXOIDDB_LANGUAGE);
                if (!alreadyProcessed.contains(axoid)) {
                    if (dcmiHashMap.containsKey(axoid)) {
                        dcmiHashMap.get(axoid).addContent(lang, termValue);
                    } else {
                        String defLanguage = getDefLanguagesHashMap().get(axoid);
                        dcmiHashMap.put(axoid, new Dcmi(axoid, defLanguage));
                        dcmiHashMap.get(axoid).addContent(lang, termValue);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
        dcmiHashMap = includeOptionalField(dcmiHashMap);
        ObjectBigArrayBigList<Dcmi> list = new ObjectBigArrayBigList<Dcmi>();
        //   list.add(dcmiHashMap.get("urn:axmedis:00000:obj:eaadc534-338e-468f-a20b-101d69f00220"));
        return new ObjectBigArrayBigList<Dcmi>(dcmiHashMap.values().iterator());
    }

    private HashMap<String, Dcmi> includeOptionalField(HashMap<String, Dcmi> dcmiHashMap) {
        HashMap<String, Dcmi> hashMap = dcmiHashMap;
        for (Map.Entry<String, Dcmi> entry : hashMap.entrySet()) {
            String axoid = entry.getKey();
            if (getOptionalFieldHashMap().containsKey(axoid)) {
                String language = entry.getValue().getDefLanguage() == null ? ClassifierLanguage.english : entry.getValue().getDefLanguage().getLanguage();
                for (String value : getOptionalFieldHashMap().get(axoid)) {
                    entry.getValue().addContent(language, value);
                }
            }
        }
        return hashMap;
    }

    private void getDefLanguages() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        HashMap<String, String> defLanguage = new HashMap<String, String>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + ROOT_OBJECT_INFO_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            resultSet = statement.executeQuery("SELECT DISTINCT " + ROOT_OBJECT_INFO_AXOID + "," + ROOT_OBJECT_INFO_DEFLANGUAGE + " from " + ROOT_OBJECT_INFO_DB + "." + ROOT_OBJECT_INFO_TABLE + " WHERE " + ROOT_OBJECT_INFO_DEFLANGUAGE + " > '' ");
            while (resultSet.next()) {
                String axoid = resultSet.getString(1);
                String deflanguage = resultSet.getString(2);
                defLanguage.put(axoid, deflanguage);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        } finally {
            close(resultSet, statement, connection);
        }
        defLanguagesHashMap = defLanguage;
    }

    private void getOptionalFields() {
        HashMap<String, ArrayList<String>> optionalFields = new HashMap<String, ArrayList<String>>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<String> parameterList = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getOptionalFieldFieldNameIncluded());
        ArrayList<String> formatedParameterList = new ArrayList<String>();
        for (String param : parameterList) {
            formatedParameterList.add(Utils.formatParameter(param));
        }
        String query = "SELECT " + OPTIONAL_FIELD_AXOID + "," + OPTIONAL_FIELD_FIELDVALUE + " FROM " + OPTIONAL_FIELD_DB + "." + OPTIONAL_FIELD_TABLE;
        if (parameterList != null && !parameterList.isEmpty() && !parameterList.get(0).isEmpty()) {
            query = query.concat(" WHERE " + OPTIONAL_FIELD_FIELDNAME + " > ' ' AND (" + OPTIONAL_FIELD_FIELDNAME + " = ");
            query = query.concat(StringUtils.join(formatedParameterList, " or " + OPTIONAL_FIELD_FIELDNAME + " = "));
            query = query.concat(")");
        }
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String axoid = resultSet.getString(1);
                String value = resultSet.getString(2);
                if (optionalFields.containsKey(axoid)) {
                    ArrayList<String> oldValue = optionalFields.get(axoid);
                    oldValue.add(value);
                    optionalFields.remove(axoid);
                    optionalFields.put(axoid, oldValue);
                } else {
                    ArrayList<String> newValue = new ArrayList<String>();
                    newValue.add(value);
                    optionalFields.put(axoid, newValue);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connection);
        }
        optionalFieldHashMap = optionalFields;
    }

    private HashSet<String> loadDcmiAlreadyProcessed() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        HashSet<String> alreadyProcessed = new HashSet<String>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + TAXONOMY_LINK_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            DatabaseMetaData dbm = connection.getMetaData();
            ResultSet tables = dbm.getTables(null, null, TAXONOMY_LINK_TABLE, null);
            if (tables.next()) {
                statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                        ResultSet.CONCUR_READ_ONLY);
                statement.setFetchSize(Integer.MIN_VALUE);
                resultSet = statement
                        .executeQuery("SELECT " + TAXONOMY_LINK_TABLE_AXOID + " from " + TAXONOMY_LINK_DB + "." + TAXONOMY_LINK_TABLE);
                while (resultSet.next()) {
                    String axoid = resultSet.getString(TAXONOMY_LINK_TABLE_AXOID);
                    alreadyProcessed.add(axoid);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connection);
        }
        return alreadyProcessed;

    }

    public static void writeTaxonomyLinkOnDatabaseAlternate() throws FileNotFoundException {
        createTaxonomy_link();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        File file = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getTaxonomyLinkUpdate());
        try {
            if (!file.exists())
                file.createNewFile();
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + TAXONOMY_LINK_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement();
            resultSet = statement
                    .executeQuery("load data local INFILE '" + file.getPath() + "' replace into table " + TAXONOMY_LINK_DB + "." + TAXONOMY_LINK_TABLE + "(" + TAXONOMY_LINK_TABLE_AXOID + "," + TAXONOMY_LINK_TABLE_TID + "," + TAXONOMY_LINK_TABLE_TAXEXTRACTIONDATE + ")");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
    }

    private void verifyLanguageList() {
        try {
            ArrayList<String> languages = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList());
            for (String lang : languages) {
                ClassifierLanguage.convert(lang);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return;
        }
    }

    private void getTaxonomyCategoriesTraduction() {
        ArrayList<CategoryDictionary> categoryDictionaries = new ArrayList<CategoryDictionary>();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + TAXONOMY_LANGUAGE_DB + "?useUnicode=true&characterEncoding=UTF-8&"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            verifyLanguageList();
            ArrayList<String> languages = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList());
            resultSet = statement.executeQuery("SELECT *" + " FROM " + TAXONOMY_LANGUAGE_DB + "." + TAXONOMY_LANGAUE_TABLE);
            while (resultSet.next()) {
                CategoryDictionary categoryDictionary = new CategoryDictionary();
                String englishTrad = resultSet.getString(ClassifierLanguage.english);

                assert englishTrad != null && englishTrad.isEmpty() == false;
                if (getCategoryFromName(englishTrad.trim().toLowerCase()) != null) {
                    for (String lang : languages) {
                        categoryDictionary.addEntry(resultSet.getString(lang).toLowerCase(), lang);
                    }
                    categoryDictionaries.add(categoryDictionary);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
        categoryDictionaryList = categoryDictionaries;
    }


    private void getCategorieHierarchy() {
        ArrayList<CategoryHierarchy> categoryHierarchy = new ArrayList<CategoryHierarchy>();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + URL + "/" + TERM_HIERARCHY_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            resultSet = statement
                    .executeQuery("SELECT " + TERM_HIERARCHY_TABLE + "." + TERM_HIERARCHY_DB_TID + "," + TERM_HIERARCHY_TABLE + "." + TERM_HIERARCHY_DB_PARENT + " from " + TERM_HIERARCHY_DB + "." + TERM_HIERARCHY_TABLE + " join " + TERM_DB + "." + TERM_TABLE + " WHERE " + TERM_TABLE + "." + TERM_TABLE_TID + " = " + TERM_HIERARCHY_TABLE + "." + TERM_HIERARCHY_DB_TID + " AND " + TERM_TABLE + "." + TERM_TABLE_VID + " = 5");
            System.out.println("SELECT " + TERM_HIERARCHY_TABLE + "." + TERM_HIERARCHY_DB_TID + "," + TERM_HIERARCHY_TABLE + "." + TERM_HIERARCHY_DB_PARENT + " from " + TERM_HIERARCHY_DB + "." + TERM_HIERARCHY_TABLE + " join " + TERM_DB + "." + TERM_TABLE + " WHERE " + TERM_TABLE + "." + TERM_TABLE_TID + " = " + TERM_HIERARCHY_TABLE + "." + TERM_HIERARCHY_DB_TID + " AND " + TERM_TABLE + "." + TERM_TABLE_VID + " = 5");
            while (resultSet.next()) {
                int tid = resultSet.getInt(1);
                int parentTid = resultSet.getInt(2);
                assert getCategoryfromTid(tid) != null;
                if (parentTid != 0) {
                    assert getCategoryfromTid(parentTid) != null;
                    categoryHierarchy.add(new CategoryHierarchy(getCategoryfromTid(tid), getCategoryfromTid(parentTid)));
                } else categoryHierarchy.add(new CategoryHierarchy(getCategoryfromTid(tid), null));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
        categoryHierarchyArrayList = categoryHierarchy;
    }

    public Category getTopLevelCategory(Category category) throws Exception {
        Category ancestor = category;
        while (getParentFromName(ancestor.getName()) != null) {
            ancestor = getParentFromName(ancestor.getName());
        }
        return ancestor;
    }

    /*
    this method get you the direct up level category but not the top level one
    used toplevelcategory() instead
     */
    public Category getParentFromName(String categoryName) throws Exception {
        Category parent = null;
        Boolean found = false;
        for (CategoryHierarchy hierarchy : getCategoriesHierarchyArrayList()) {
            if (hierarchy.getSon().getName().equals(categoryName)) {
                parent = hierarchy.getParent();
                found = true;
                break;
            }
        }
        if (!found) throw new Exception(" name: " + categoryName + " was not founded in categoryHierarchy");
        return parent;
    }

    /*
   this method get you the direct up level category but not the top level one
   used toplevelcategory() instead
    */
    public Category getParentFromId(int tid) throws Exception {
        Category parent = null;
        Boolean found = false;
        for (CategoryHierarchy hierarchy : getCategoriesHierarchyArrayList()) {
            if (hierarchy.getSon().getTid() == tid) {
                parent = hierarchy.getParent();
                found = true;
                break;
            }
        }
        if (!found) throw new Exception(" tid: " + tid + " was not founded in categoryHierarchy");
        return parent;
    }


    private ArrayList<CategoryHierarchy> getCategoriesHierarchyArrayList() {
        if (categoryHierarchyArrayList == null) getCategorieHierarchy();
        return categoryHierarchyArrayList;
    }

    private HashMap<String, String> getDefLanguagesHashMap() {
        if (defLanguagesHashMap == null) getDefLanguages();
        return defLanguagesHashMap;
    }

    private HashMap<String, ArrayList<String>> getOptionalFieldHashMap() {
        if (optionalFieldHashMap == null) getOptionalFields();
        return optionalFieldHashMap;
    }

    private ArrayList<CategoryDictionary> getCategoryDictionaryList() {
        if (categoryDictionaryList == null) getTaxonomyCategoriesTraduction();
        return categoryDictionaryList;
    }
}
