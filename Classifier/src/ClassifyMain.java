import Configuration.ClassifierConfiguration;
import Configuration.CommandLineParameters;
import DatabaseMapping.Category;
import DatabaseMapping.Dcmi;
import DatabaseMapping.MysqlAccessor;
import NLPUtils.*;
import NLPUtils.SearchHit.ClassifierHit;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.commons.configuration.ConfigurationException;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 29/06/13
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
public class ClassifyMain {
    private static Logger logger = LogManager.getLogger(ClassifyMain.class.getName());

    public static void main(String[] args) {
        args = new String[6];
        args[0] = "-user";
        args[1] = "root";
        args[2] = "-pwd";
        args[3] = "";
        args[4] = "-nmb";
        args[5] = "100";
        logger.trace("Entering application");
        logger.entry(args);
        CommandLineParameters parameters = new CommandLineParameters();
        CmdLineParser cmdLineParser = new CmdLineParser(parameters);
        try {
            cmdLineParser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java -jar Classifier.jar [options... ] arguments...");
            cmdLineParser.printUsage(System.err);
            return;
        }
        try {
            MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(parameters.getPassword());
            MysqlAccessor.getMysqlAccessorSingleton().setUSER(parameters.getUser());
            ClassifierConfiguration.getClassiferConfigurationInstance().checkConfiguration();
        } catch (ConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        //  startClassifier(parameters.getNumber());
        startTest(parameters.getNumber());
        logger.exit();
    }


    public static void createLuceneIndex() {
        try {
            IndexesCreator indexesAccessor = new IndexesCreator();
            indexesAccessor.createLuceneIndexes();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static void startTest(long param) {
        long compute;
        try {
            ObjectBigArrayBigList<Dcmi> dcmifromDB = MysqlAccessor.getMysqlAccessorSingleton().getDcmifromDB();
            if (param == 0) compute = dcmifromDB.size64();
            else {
                if (param < dcmifromDB.size64()) compute = param;
                else compute = dcmifromDB.size64();
            }
            logger.trace("STARTING TEST:  {} entries to compute",param);
            for (long i = 0; i < compute; i++) {
                String text = Utils.removeDoubleSpace(dcmifromDB.get(i).getPriorityLanguageContent().toString());
                logger.debug("text to classify: {}  language: {}",text,dcmifromDB.get(i).getDefLanguage());
                ClassifierLanguage language = dcmifromDB.get(i).getPriorityLanguageContent().getClassifierLanguage();
                int tokenNumber = Utils.tokenizeString(Utils.getLightAnalyser(language), text).size();
                ArrayList<ClassifierHit> hits = IndexesSearch.getInstance().search(text, language.getLanguage());
                ArrayList<ClassifierHit> agreggateHits = null;
                if (hits != null) agreggateHits = HitsAggregate.ordinateHits(hits);
                ArrayList<Category> categories = null;
                if (agreggateHits != null)
                    categories = ModelScorer.getInstance().elaborateResults(agreggateHits, tokenNumber, language);
                if (categories != null) {
                    logger.trace("Founded {} categories",categories.size());
                    logger.debug("Winning categories");
                    for (Category cat : categories) {
                        System.out.println(cat.toString());
                    }
                    ClassifierWriter.getInstance().createTaxonomyLinks(dcmifromDB.get(i).getAxoid(), categories);
                } else System.out.println("no winner categories for this axoid!!");

            }
            ClassifierWriter.getInstance().updateTaxonomyDB();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }


    }


}



