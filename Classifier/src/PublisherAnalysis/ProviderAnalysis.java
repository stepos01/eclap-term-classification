package PublisherAnalysis;

import Configuration.ClassifierConfiguration;
import DatabaseMapping.Category;
import DatabaseMapping.Dcmi;
import DatabaseMapping.MysqlAccessor;
import NLPUtils.*;
import NLPUtils.SearchHit.ClassifierHit;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.commons.configuration.ConfigurationException;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/11/2013
 * Time: 21:25
 * To change this template use File | Settings | File Templates.
 */
public class ProviderAnalysis {
    public static void main(String[] args) {
        args = new String[6];
        args[0] = "-user";
        args[1] = "root";
        args[2] = "-pwd";
        args[3] = "";
        args[4] = "-m";
        args[5] = "1";
        CommandLineParameters4Publisher parameters = loadArgs(args);
        try {
            MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(parameters.getPassword());
            MysqlAccessor.getMysqlAccessorSingleton().setUSER(parameters.getUser());
            ClassifierConfiguration.getClassiferConfigurationInstance().checkConfiguration();
            ClassifierConfiguration.getClassiferConfigurationInstance().setAccuracytAccuracy(0);
            ClassifierConfiguration.getClassiferConfigurationInstance().setScorethreshold(0);
            if (parameters.getMode() == 0) {
                //soft analyse for publisher
                softAnalysePublisher();
            } else {
                hardAnalysePublisher();
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private static void softAnalysePublisher() {
        try {
            ArrayList<String> listPublisher = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getPublisherList());
            if(listPublisher.size()==1&&listPublisher.get(0).equals("")) {
                System.err.println("no publisher name found in configuration files... return");
                return;
            }
            for (int i = 0; i < listPublisher.size(); i++) {
                int count = 0, classified = 0;
                ArrayList<String> axoids4Publisher = MysqlAccessor.getMysqlAccessorSingleton().getAxoid4Publisher(listPublisher.get(i));
                if (axoids4Publisher.isEmpty()){
                    System.err.println("the publisher '"+listPublisher.get(i)+"' publisher doesn't seem to exist...no axoid found");
                    continue;
                }
                for (int j = 0; j < axoids4Publisher.size(); j++) {
                    int result = MysqlAccessor.getMysqlAccessorSingleton().isAxoidClassifiedeInTaxonomyLink(axoids4Publisher.get(j));
                    if (result == 1) {
                        count++;
                        classified++;
                    } else if (result == -1) {
                        count++;
                    }
                }
                System.out.println("Publisher: " + listPublisher.get(i) + " Classified: " + classified + " over " + count + " in total");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private static void hardAnalysePublisher() {
        try {
            ArrayList<String> listPublisher = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getPublisherList());
            if(listPublisher.size()==1&&listPublisher.get(0).equals("")) {
                System.err.println("no publisher name found in configuration files... return");
                return;
            }
            for (int i = 0; i < listPublisher.size(); i++) {
                float score_average = 0;
                float accuracy_average = 0;
                ObjectBigArrayBigList<Dcmi> dcmis = MysqlAccessor.getMysqlAccessorSingleton().getDcmifromDBForPublisher(listPublisher.get(i));
                if (dcmis.isEmpty()){
                    System.err.println("the publisher '"+listPublisher.get(i)+"' publisher doesn't seem to exist...no axoid related found");
                    continue;
                }
                for (int j = 0; j < dcmis.size64(); j++) {
                    float hits_score = 0;
                    float hits_accuracy = 0;
                    String text = dcmis.get(j).getPriorityLanguageContent().toString();
                    System.out.println("text:" + text + "\t" + "language:" + dcmis.get(i).getDefLanguage());
                    ClassifierLanguage language = dcmis.get(j).getPriorityLanguageContent().getClassifierLanguage();
                    int tokenNumber = Utils.tokenizeString(Utils.getLightAnalyser(language), text).size();
                    ArrayList<ClassifierHit> hits = IndexesSearch.getInstance().search(text, language.getLanguage());
                    ArrayList<ClassifierHit> agreggateHits = null;
                    if (hits != null) agreggateHits = HitsAggregate.ordinateHits(hits);
                    ArrayList<Category> categories = null;
                    if (agreggateHits != null)
                        categories = ModelScorer.getInstance().elaborateResults(agreggateHits, tokenNumber, language);
                    if (categories != null) {
                        for (Category cat : categories) {
                            hits_score = hits_score + cat.getScore();
                            hits_accuracy = hits_accuracy + cat.getAccuracy();
                        }
                        hits_score = hits_score / hits.size();
                        hits_accuracy = hits_accuracy / hits.size();
                        score_average = score_average + hits_score;
                        accuracy_average = accuracy_average + hits_accuracy;
                    } else System.out.println("no winner categories for this axoid!!");
                }
                score_average = score_average / dcmis.size64();
                accuracy_average = accuracy_average / dcmis.size64();
                System.out.println("Publisher: " + listPublisher.get(i) + "\t" + "average score: " + score_average + "\t" + "average accuracy: " + accuracy_average);
            }
        } catch (ConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private static CommandLineParameters4Publisher loadArgs(String[] args) {
        CommandLineParameters4Publisher commandLineParameters = new CommandLineParameters4Publisher();
        CmdLineParser cmdLineParser = new CmdLineParser(commandLineParameters);
        try {
            cmdLineParser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java -jar Classifier.jar [options... ] arguments...");
            cmdLineParser.printUsage(System.err);
            System.exit(-1);
        }
        return commandLineParameters;
    }
}
