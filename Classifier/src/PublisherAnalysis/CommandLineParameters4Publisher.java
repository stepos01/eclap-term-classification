package PublisherAnalysis;

import org.kohsuke.args4j.Option;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 20/09/13
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */
public class CommandLineParameters4Publisher {


    @Option(name = "-user", usage = "username for accessing Mysql")
    private String user;

    @Option(name = "-pwd", usage = "password for accessing Mysql")
    private String password;

    @Option(name = "-m", usage = "hard analyse(1) or soft one(0)")
    private int mode;

    public int getMode() {
        return mode;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "user: " + user + "\t" + "password: " + password + "\t" + "numberOfInstances: " + mode;    //To change body of overridden methods use File | Settings | File Templates.
    }
}
