import configuration.ClassifierConfiguration;
import databaseMapping.*;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 11/03/2014
 * Time: 16:35
 * To change this template use File | Settings | File Templates.
 */
public class testDB {
    public static Logger logger = LogManager.getLogger();
    public static void main(String[]args)
    {
       MysqlAccessor.getMysqlAccessorSingleton().setUSER("root");
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD("");
        testgetdefLanguage();

    }

    public static void testGetParentTid()
    {
        int parenttid = 0;
        try {
            parenttid = MysqlAccessor.getMysqlAccessorSingleton().getParentTid(190);
        } catch (Exception e) {
            logger.catching(e);
        }
        assert parenttid == 115;
    }
    public  static void testgetTopCategoryFromTid()
    {
        Category Cat = null;
        try {
            Cat = MysqlAccessor.getMysqlAccessorSingleton().getTopParentFromTid(506);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        assert Cat.getTid() == 664;
    }
    public  static void testgetCagoryFromName()
    {
        Category category = null;
        try {
            category = MysqlAccessor.getMysqlAccessorSingleton().getCategoryFromName("performer",new EclapLanguage(EclapLanguage.english));
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        assert category.getTid() == 591;
    }
    public static void testTraduction()
    {
        String result = MysqlAccessor.getMysqlAccessorSingleton().getTraductionForTerm("Modern", new EclapLanguage("en"),new EclapLanguage("it"));
        assert result == "Moderno";
    }
    public static void testgetdefLanguage()
    {
        try {
            String lang = MysqlAccessor.getMysqlAccessorSingleton().getDefLanguage("urn:axmedis:00000:obj:d37c1446-0a31-47b3-8f01-c8fdc26c37af");
                assert lang == "en";
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    public static void testPublisherStuff()
    {
            String publisher = (String) ClassifierConfiguration.getClassiferConfigurationInstance().getPublisherList().get(1);
            ObjectArrayList<String> axoid4Publisher = MysqlAccessor.getMysqlAccessorSingleton().getAxoid4Publisher(publisher);
            for (String axoid : axoid4Publisher)
            {
                ObjectBigArrayBigList<Dcmi> dcmiForAxoid = MysqlAccessor.getMysqlAccessorSingleton().getDcmiForAxoid(axoid);
                assert dcmiForAxoid.size64() == 1;
                System.out.println(dcmiForAxoid.get(0));

            }
    }
    public static void testExplorerTAbleCreation()
    {
        MysqlAccessor.getMysqlAccessorSingleton().createTaxonomy_Link_explorer();
    }
    public static void testExplorerTableWriting()
    {
        ObjectArrayList<TaxonomyLink> explorers = new ObjectArrayList<TaxonomyLink>();
        TaxonomyLinkExplorer taxonomyLinkExplorer = new TaxonomyLinkExplorer("abc",0,"antani");
        explorers.add(taxonomyLinkExplorer);
        MysqlAccessor.getMysqlAccessorSingleton().writeOnTaxonomy_link_explorer(explorers,10);
    }

}
