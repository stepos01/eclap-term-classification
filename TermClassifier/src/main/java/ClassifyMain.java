
import configuration.ClassifierConfiguration;
import configuration.CommandLineParameters;
import databaseMapping.MysqlAccessor;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import searchUtils.Classifier.Classifier;
import searchUtils.Explorer.Explorer;
import searchUtils.publisher.Publisher;

import java.io.File;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 29/06/13
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
public class ClassifyMain {
    private static Logger logger = LogManager.getLogger(ClassifyMain.class.getName());

    public static void main(String[] args) throws CmdLineException {
       /* args = new String[8];
        args[0] = "-a";
        args[1] = "ex";
        args[2] = "-u";
        args[3] = "root";
        args[4] = "-p";
        args[5] = "";
        args[6] = "-nmb";
        args[7] = "100";*/

        Configurator.initialize(null, null,new File(ClassifierConfiguration.getClassiferConfigurationInstance().getConfigPath(),"config/log4j2.xml").getAbsolutePath());
        logger.trace("Entering classifier application");
        CommandLineParameters parameters = new CommandLineParameters();
        CmdLineParser cmdLineParser = new CmdLineParser(parameters);
        try {
            cmdLineParser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java -jar TermClassifier.jar [options... ] arguments...");
            cmdLineParser.printUsage(System.err);
            return;
        }
        try {
            MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(parameters.getPassword());
            MysqlAccessor.getMysqlAccessorSingleton().setUSER(parameters.getUser());
            ClassifierConfiguration.getClassiferConfigurationInstance().checkConfiguration();
        } catch (ConfigurationException e) {
            logger.catching(e);
            return;
        }
        if (parameters.getAction().equals("cl"))
            runClassifier(parameters.getNumber());
        else if (parameters.getAction().equals("pu"))
            runPublisher();
        else if (parameters.getAction().equals("ex"))
            runExplorer(parameters.getNumber());
        else throw new CmdLineException("parameters -a should be 'cl' for classifier  'pu' for publisher or 'ex' for explorer");

        return;
    }

    private static void runPublisher() {
        Publisher publisher = new Publisher();
        publisher.analyseAllPublisher();
    }
    private static void  runClassifier(int param)
    {
        Classifier classifier = new Classifier();
        classifier.classify(param);
    }
    private static void runExplorer(int param)
    {
        Explorer explorer = new Explorer();
        explorer.explore(param);

    }



}



