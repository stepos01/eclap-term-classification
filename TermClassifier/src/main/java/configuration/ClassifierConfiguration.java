package configuration;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.configuration.reloading.InvariantReloadingStrategy;
import org.apache.commons.configuration.reloading.ManagedReloadingStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 16/07/13
 * Time: 07:59
 * To change this template use File | Settings | File Templates.
 */
public class ClassifierConfiguration {
    private static ClassifierConfiguration instance = null;
    private static Logger logger = LogManager.getLogger();
    private org.apache.commons.configuration.PropertiesConfiguration config = null;
    public static final String CONFIG_FOLDER = "TermClassifier/config/";
    public static final String CONFIG_PATH = "/eclap.classifier.properties";

    private ClassifierConfiguration() {
       File configFile = new File(getConfigPath(),"config/eclap.classifier.properties");
    //    File configFile = new File(CONFIG_FOLDER,CONFIG_PATH);
        boolean bDone = false;
        if (configFile.exists()) {
            logger.debug("Loading " + configFile + " FROM " + configFile.getPath());
            try {
                this.config = new PropertiesConfiguration(configFile);
                bDone = true;
            } catch (ConfigurationException ce) {
                ce.printStackTrace();
            }
        }
        if (!bDone) {
            try {
                throw logger.throwing(new ConfigurationException("the following configuration file  'eclap.classifier.properties is' not found"));
            } catch (ConfigurationException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public void checkConfiguration() throws ConfigurationException {
        //TODO
        if (getLuceneIndexes().isEmpty())
            throw new ConfigurationException("lucene.indexes not set in eclap.classifier.properties file");
    }

    public static ClassifierConfiguration getClassiferConfigurationInstance() {
        if (instance == null) instance = new ClassifierConfiguration();
        return instance;
    }

    public String getConfigurationParameter(String parameter) {
        if (config == null) {
            throw new AssertionError("the following configuration file  'eclap.classifier.properties is' not found");
        }
        return config.getString(parameter);
    }

    public String getDocuments() {
        return config.getString("documents");
    }

    public String getAxoidDBAxoid() {
        return config.getString("axoid.table.axoid");
    }

    public String getAxoidDBTermrefinement() {
        return config.getString("axoid.table.termrefinement");
    }

    public String getAxoidDBTermrefinement_publisher() {
        return config.getString("axoid.tabble.termRefinement.publisher");
    }

    public String getAxoidDBTermValue() {
        return config.getString("axoid.table.termvalue");

    }

    public String getAxoiDBLanguage() {
        return config.getString("axoid.table.language");
    }

    public List<String> getAxoidDBTermNameIncluded() {
        return config.getList("axoid.table.termname.included");
    }

    public String getTermHierarchyDBTid() {
        return config.getString("term.hierarchy.db.tid");
    }

    public String getTermHierarchyDBParent() {
        return config.getString("term.hierarchy.db.parent");
    }
    public String getTaxonomyLinkTable4Explorer()
    {
        return config.getString("taxonomy.link.explorer.table");
    }
    public String getTaxonomyLinkTable4Explorer_content()
    {
        return config.getString("taxonomy.link.explorer.table.content");
    }
    public String getTaxonomyLinkTableTid() {
        return config.getString("taxonomy.link.table.tid");
    }

    public String getTaxonomyLinkTableAxoid() {
        return config.getString("taxonomy.link.table.axoid");
    }

    public String getTaxonomyLinkTableTaxExtractionDate() {
        return config.getString("taxonomy.link.table.taxExtractionDate");
    }

    public String getDBUrl() {
        return config.getString("db.url");
    }

    public String getTermDatatid() {
        return config.getString("term.table.tid");
    }

    public String getTermDataName() {
        return config.getString("term.table.name");
    }

    public String getTermDataVid() {
        return config.getString("term.table.vid");
    }

    public String getAxoidDB() {
        return config.getString("axoid.db");
    }

    public String getAxoidTable() {
        return config.getString("axoid.table");
    }

    public String getTermDB() {
        return config.getString("term.db");
    }

    public String getTermTable() {
        return config.getString("term.table");
    }

    public String getTermHierarchyDB() {
        return config.getString("term.hierarchy.db");
    }

    public String getTermHierarchyTable() {
        return config.getString("term.hierarchy.table");
    }

    public String getTaxonomyLinkDB() {
        return config.getString("taxonomy.link.db");
    }

    public String getTaxonomyLinkTable() {
        return config.getString("taxonomy.link.table");
    }

    public String getLuceneIndexes() {
        return config.getString("lucene.indexes");
    }
    public void setLunceIndexes(String value)
    {
        try {
            config.setProperty("lucene.indexes",value);
            config.save();
            config.reload();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public String getStopWords() {
        return config.getString("stopwords");
    }

    public String getTaxonomyLanguageDB() {
        return config.getString("taxonomy.language.db");
    }

    public String getTaxonomyLanguageTable() {
        return config.getString("taxonomy.language.table");
    }

    public String getTaxonomyLinkUpdate() {
        return config.getString("taxonomy.link.update");
    }

    public String getLogFile() {
        return config.getString("log");
    }

    public float getTitleHitScore() {
        return config.getFloat("title.hit.score");
    }

    public float getTagHitScore() {
        return config.getFloat("tag.hit.score");
    }

    public float getScoreThreshold() {
        return config.getFloat("score.threshold");
    }

    public float getAccuracyThreshold() {
        return config.getFloat("accuracy.threshold");
    }

    public List getTaxonomyExclude() {
        return config.getList("taxonomy.exclude");
    }

    public void setScorethreshold(float value) {
        try {
            config.setProperty("score.threshold", value);
            config.save();
            config.reload();
        } catch (ConfigurationException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public void setAccuracytAccuracy(float value) {
        try {
            config.setProperty("accuracy.threshold", value);
            config.save();
            config.reload();
        } catch (ConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        }
    }

    public String getRootObjectInfoDB() {
        return config.getString("axoid.rootobjectinfo.db");
    }

    public String getRootObjectInfoTable() {
        return config.getString("axoid.rootobjectinfo.table");
    }

    public String getRootObjectInfoDefLanguage() {
        return config.getString("axoid.rootobjectinfo.deflanguage");
    }

    public String getRootObjectAxoid() {
        return config.getString("axoid.rootobjectinfo.rootobjectaxoid");
    }

    public String getOptionalFieldDB() {
        return config.getString("axoid.optionalfield.db");
    }

    public String getOptionalFieldTable() {
        return config.getString("axoid.optionalfield.table");
    }

    public String getOptionalFieldAxoid() {
        return config.getString("axoid.optionalfield.axoid");
    }

    public String getOptionalFieldFieldValue() {
        return config.getString("axoid.optionalfield.fieldvalue");
    }

    public List getOptionalFieldFieldNameIncluded() {
        return config.getList("axoid.optionalfield.fielName.included");
    }

    public String getOptionalFieldFieldName() {
        return config.getString("axoid.optionalfield.fieldname");
    }

    public List getLangagueList() {
        return config.getList("taxonomy.language.table.languages");
    }

    public List getPublisherList() {
        return config.getList("test.publisher");
    }

    public String getTestFileOutput() {
        return config.getString("test.output");
    }

    public List getStopWordExclusion() {
        return config.getList("stop.word.exclusion");
    }
    public String getConfigPath()
    {
        String decodedPath = null;

        try {
            String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();

            String jarpath = URLDecoder.decode(path, "UTF-8");
            File jarfile = new File(jarpath);
            decodedPath = jarfile.getParent();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return decodedPath;
    }

}
