package configuration;

import org.kohsuke.args4j.Option;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 20/09/13
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */
public class CommandLineParameters {


    @Option(name = "-u", usage = "username for accessing Mysql", required = true)
    private String user;

    @Option(name = "-a", usage = "action to execute: 'cl' for classifying , 'pu' for analysing publishers, 'ex' for explorer",required = true)
    private String action;

    @Option(name = "-p", usage = "password for accessing Mysql",required = true)
    private String password;

    @Option(name = "-nmb", usage = "how many instances to classify",required = true)
    private int number;

    public int getNumber() {
        return number;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
    public String getAction()
    {
        return action;
    }
    @Override
    public String toString() {
        return "action: "+action+"\t"+"user: " + user + "\t" + "password: " + password + "\t" + "numberOfInstances: " + number;    //To change body of overridden methods use File | Settings | File Templates.
    }
}
