package databaseMapping;

import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.*;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import searchUtils.Utils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 30/06/13
 * Time: 20:11
 * To change this template use File | Settings | File Templates.
 */
public class MysqlAccessor {
    private static Logger logger = LogManager.getLogger();
    private static boolean taxonomy_link_exists = false;
    private ObjectArrayList<Category> taxonomyCategoryArrayList = null;

    ///*****************////
    private ArrayList<CategoryHierarchy> categoryHierarchyArrayList = null;
    private static String DBURL = null;
    private static String USER = null;
    private static String PASSWORD = null;
    private static String AXOID_DB = null;
    private static String AXOID_TABLE = null;
    private static String TERM_DB = null;
    private static String TERM_TABLE = null;
    private static String TERM_HIERARCHY_DB = null;
    private static String TERM_HIERARCHY_TABLE = null;
    private static String TAXONOMY_LINK_DB = null;
    private static String TAXONOMY_LINK_TABLE = null;
    private static String TERM_TABLE_TID = null;
    private static String TERM_TABLE_NAME = null;
    private static String TAXONOMY_LINK_TABLE_TID = null;
    private static String TAXONOMY_LINK_TABLE_AXOID = null;
    private static String TAXONOMY_LINK_TABLE_TAXEXTRACTIONDATE = null;
    private static String TAXONOMY_LINK_EXPLORER_TABLE = null;
    private static String TAXONOMY_LINK_EXPLORER_TABLE_CONTENT = null;
    private static String AXOIDDB_AXOID = null;
    private static String AXOIDDB_TERMVALUE = null;
    private static String AXOIDDB_LANGUAGE = null;
    private static String AXOIDDB_TERMREFINEMENT = null;
    private static String TERM_HIERARCHY_DB_TID = null;
    private static String TERM_HIERARCHY_DB_PARENT = null;
    private static String TAXONOMY_LANGUAGE_DB = null;
    private static String TAXONOMY_LANGAUE_TABLE = null;
    private static String ROOT_OBJECT_INFO_DB = null;
    private static String ROOT_OBJECT_INFO_TABLE = null;
    private static String ROOT_OBJECT_INFO_AXOID = null;
    private static String ROOT_OBJECT_INFO_DEFLANGUAGE = null;
    private static String OPTIONAL_FIELD_DB = null;
    private static String OPTIONAL_FIELD_TABLE = null;
    private static String OPTIONAL_FIELD_AXOID = null;
    private static String OPTIONAL_FIELD_FIELDVALUE = null;
    private static String OPTIONAL_FIELD_FIELDNAME = null;
    private static String TERM_TABLE_VID = null;
    private static MysqlAccessor mysqlAccessor = null;

    private MysqlAccessor() {
        ClassifierConfiguration configuration = ClassifierConfiguration.getClassiferConfigurationInstance();
        AXOID_DB = configuration.getAxoidDB();
        AXOID_TABLE = configuration.getAxoidTable();
        TERM_DB = configuration.getTermDB();
        TERM_TABLE = configuration.getTermTable();
        TERM_HIERARCHY_DB = configuration.getTermHierarchyDB();
        TERM_HIERARCHY_TABLE = configuration.getTermHierarchyTable();
        TAXONOMY_LINK_DB = configuration.getTaxonomyLinkDB();
        TAXONOMY_LINK_TABLE = configuration.getTaxonomyLinkTable();
        TAXONOMY_LINK_EXPLORER_TABLE = configuration.getTaxonomyLinkTable4Explorer();
        TERM_TABLE_TID = configuration.getTermDatatid();
        TERM_TABLE_NAME = configuration.getTermDataName();
        DBURL = configuration.getDBUrl();
        TAXONOMY_LINK_TABLE_TID = configuration.getTaxonomyLinkTableTid();
        TAXONOMY_LINK_TABLE_AXOID = configuration.getTaxonomyLinkTableAxoid();
        TAXONOMY_LINK_TABLE_TAXEXTRACTIONDATE = configuration.getTaxonomyLinkTableTaxExtractionDate();
        TAXONOMY_LINK_EXPLORER_TABLE_CONTENT = configuration.getTaxonomyLinkTable4Explorer_content();
        AXOIDDB_AXOID = configuration.getAxoidDBAxoid();
        AXOIDDB_TERMVALUE = configuration.getAxoidDBTermValue();
        AXOIDDB_LANGUAGE = configuration.getAxoiDBLanguage();
        TERM_HIERARCHY_DB_TID = configuration.getTermHierarchyDBTid();
        TERM_HIERARCHY_DB_PARENT = configuration.getTermHierarchyDBParent();
        TAXONOMY_LANGUAGE_DB = configuration.getTaxonomyLanguageDB();
        TAXONOMY_LANGAUE_TABLE = configuration.getTaxonomyLanguageTable();
        ROOT_OBJECT_INFO_DB = configuration.getRootObjectInfoDB();
        ROOT_OBJECT_INFO_DEFLANGUAGE = configuration.getRootObjectInfoDefLanguage();
        ROOT_OBJECT_INFO_AXOID = configuration.getRootObjectAxoid();
        ROOT_OBJECT_INFO_TABLE = configuration.getRootObjectInfoTable();
        OPTIONAL_FIELD_AXOID = configuration.getOptionalFieldAxoid();
        OPTIONAL_FIELD_FIELDVALUE = configuration.getOptionalFieldFieldValue();
        OPTIONAL_FIELD_TABLE = configuration.getOptionalFieldTable();
        OPTIONAL_FIELD_DB = configuration.getOptionalFieldDB();
        OPTIONAL_FIELD_FIELDNAME = configuration.getOptionalFieldFieldName();
        AXOIDDB_TERMREFINEMENT = configuration.getAxoidDBTermrefinement();
        TERM_TABLE_VID = configuration.getTermDataVid();
        try {
            checkConfiguration();
        } catch (ConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        }
    }

    private static void close(ResultSet resultSet, Statement statement, Connection connect) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
        }
    }

    public void setPASSWORD(String PASSWORD) {
        MysqlAccessor.PASSWORD = PASSWORD;
    }

    public void setUSER(String USER) {
        MysqlAccessor.USER = USER;
    }

    public void checkConfiguration() throws ConfigurationException {
        verifyLanguageList();
        if (AXOID_DB.isEmpty())
            throw new ConfigurationException("axoid.db not set in eclap.classifier.properties file");
        if (AXOID_TABLE.isEmpty())
            throw new ConfigurationException("axoid.table not set in eclap.classifier.properties file");
        if (TERM_DB.isEmpty()) throw new ConfigurationException("term.db not set in eclap.classifier.properties file");
        if (TERM_TABLE.isEmpty())
            throw new ConfigurationException("term.table not set in eclap.classifier.properties file");
        if (TERM_HIERARCHY_DB.isEmpty())
            throw new ConfigurationException("term.hierarchy.db not set in eclap.classifier.properties file");
        if (TERM_HIERARCHY_TABLE.isEmpty())
            throw new ConfigurationException("term.hierarchy.table not set in eclap.classifier.properties file");
        if (TAXONOMY_LINK_DB.isEmpty())
            throw new ConfigurationException("taxonomy.link.db not set in eclap.classifier.properties file");
        if (TAXONOMY_LINK_TABLE.isEmpty())
            throw new ConfigurationException("taxonomy.link.table not set in eclap.classifier.properties file");
        if (DBURL.isEmpty()) throw new ConfigurationException("url  not set in eclap.classifier.properties file");
        if (TERM_TABLE_TID.isEmpty())
            throw new ConfigurationException("term.table.tid  not set in eclap.classifier.properties file");
        if (TERM_TABLE_NAME.isEmpty())
            throw new ConfigurationException("term.table.name  not set in eclap.classifier.properties file");
        if (TAXONOMY_LINK_TABLE_TID.isEmpty())
            throw new ConfigurationException("taxonomy.link.table.tid   not set in eclap.classifier.properties file");
        if (TAXONOMY_LINK_TABLE_AXOID.isEmpty())
            throw new ConfigurationException("taxonomy.link.table.axoid not set in eclap.classifier.properties file");
        if (TAXONOMY_LANGUAGE_DB.isEmpty())
            throw new ConfigurationException("taxonomy.language.db not set in eclap.classifier.properties file");
        if (TAXONOMY_LANGAUE_TABLE.isEmpty())
            throw new ConfigurationException("taxonomy.language.table not set in eclap.classifier.properties file");
        if (ROOT_OBJECT_INFO_TABLE.isEmpty())
            throw new ConfigurationException("axoid.rootobjectinfo.table not set in eclap.classifier.properties file");
        if (ROOT_OBJECT_INFO_DEFLANGUAGE.isEmpty())
            throw new ConfigurationException("axoid.rootobjectinfo.deflanguage not set in eclap.classifier.properties file");
        if (ROOT_OBJECT_INFO_AXOID.isEmpty())
            throw new ConfigurationException("axoid.rootobjectinfo.rootobjectaxoid not set in eclap.classifier.properties file");
        if (OPTIONAL_FIELD_AXOID.isEmpty())
            throw new ConfigurationException("axoid.optionalfield.axoid not set in eclap.classifier.properties file");
        if (OPTIONAL_FIELD_TABLE.isEmpty())
            throw new ConfigurationException("axoid.optionalfield.table not set in eclap.classifier.properties file");
        if (OPTIONAL_FIELD_FIELDVALUE.isEmpty())
            throw new ConfigurationException("axoid.optionalfield.fieldvalue not set in eclap.classifier.properties file");
        if (ROOT_OBJECT_INFO_DB.isEmpty())
            throw new ConfigurationException("axoid.rootobjectinfo.db not set in eclap.classifier.properties file");
        if (OPTIONAL_FIELD_DB.isEmpty())
            throw new ConfigurationException("axoid.optionalfield.db not set in eclap.classifier.properties file");
        if (OPTIONAL_FIELD_FIELDNAME.isEmpty())
            throw new ConfigurationException("axoid.optionalfield.fieldName not set in eclap.classifier.properties file");
        if (AXOIDDB_TERMREFINEMENT.isEmpty())
            throw new ConfigurationException("axoid.table.termrefinement not set in eclap.classifier.properties file");
        if (TERM_TABLE_VID.isEmpty())
            throw new ConfigurationException("term.table.vid not set in eclap.classifier.properties file");
    }

    public static MysqlAccessor getMysqlAccessorSingleton() {
        if (mysqlAccessor == null) mysqlAccessor = new MysqlAccessor();
        return mysqlAccessor;
    }

    public void createTaxonomy_Link_explorer()
    {
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + TAXONOMY_LINK_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement();
            statement.execute("CREATE  TABLE iF NOT EXISTS`" + TAXONOMY_LINK_DB + "`.`" + TAXONOMY_LINK_EXPLORER_TABLE + "` (" +
                    "  `id` INT NOT NULL AUTO_INCREMENT ," +
                    TAXONOMY_LINK_TABLE_TID + " VARCHAR(60) NOT NULL ," +
                    TAXONOMY_LINK_TABLE_AXOID + "  VARCHAR(60) NOT NULL ," +
                    TAXONOMY_LINK_EXPLORER_TABLE_CONTENT + " TEXT NULL ," +
                    "  PRIMARY KEY (`id`) ," +
                    "  UNIQUE INDEX `id_UNIQUE` (`id` ASC))  ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;;");
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
    }

    private  void createTaxonomy_link() {
        if(taxonomy_link_exists)return;
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + TAXONOMY_LINK_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
      //      DatabaseMetaData DBmetadata = connect.getMetaData();
      //      resultSet = DBmetadata.getTables(null, null, TAXONOMY_LINK_TABLE, null);
            statement = connect.createStatement();

            /*String query = "SELECT count(*) as count\n" +
                    "FROM information_schema.TABLES\n" +
                    "WHERE (TABLE_SCHEMA = "+Utils.formatParameter(TAXONOMY_LINK_DB)+") AND (TABLE_NAME ="+Utils.formatParameter(TAXONOMY_LINK_TABLE)+")" ;
            resultSet = statement.executeQuery(query);
            resultSet.next();
            int count = resultSet.getInt(1);
            if (count == 1)return;
            statement = connect.createStatement();*/
            statement.execute("CREATE  TABLE iF NOT EXISTS`" + TAXONOMY_LINK_DB + "`.`" + TAXONOMY_LINK_TABLE + "` (" +
                    "  `id` INT NOT NULL AUTO_INCREMENT ," +
                    TAXONOMY_LINK_TABLE_TID + " VARCHAR(60) NOT NULL ," +
                    TAXONOMY_LINK_TABLE_AXOID + "  VARCHAR(60) NOT NULL ," +
                    TAXONOMY_LINK_TABLE_TAXEXTRACTIONDATE + " DATETIME NULL ," +
                    "  PRIMARY KEY (`id`) ," +
                    "  UNIQUE INDEX `id_UNIQUE` (`id` ASC))  ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;;");
            taxonomy_link_exists = true;
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
    }

    private ObjectArrayList<Category> getTaxonomyCategories() {
        ObjectArrayList<Category> taxonomycategories = new ObjectArrayList<Category>();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + TERM_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            resultSet = statement
                    .executeQuery("SELECT " + TERM_TABLE_TID + "," + TERM_TABLE_NAME + " from " + TERM_DB + "." + TERM_TABLE + " where " + TERM_TABLE_VID + " =" + 5);
            while (resultSet.next()) {
                int tid = resultSet.getInt(1);
                String category_name = resultSet.getString(2).trim();
                taxonomycategories.add(new Category(category_name, tid));
            }
        } catch (Exception e) {
            logger.catching(e);
            System.exit(-1);

        } finally {
            close(resultSet, statement, connect);
        }
        return taxonomycategories;
    }



/*   public ObjectArrayList<Dcmi> getDcmibyPublisher(String publisher, int number) {
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<String> axoidOfPublisher = new ArrayList<String>();
        HashMap<String, Dcmi> publisherMap = new HashMap<String, Dcmi>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            resultSet = statement.executeQuery("SELECT " + AXOIDDB_AXOID + " FROM " + AXOID_DB + "." + AXOID_TABLE + " WHERE " + AXOIDDB_TERMVALUE + " = " + publisher);
            while (resultSet.next()) {
                axoidOfPublisher.add(resultSet.getString(1));
            }
            for (String axoid : axoidOfPublisher) {
                String query = "SELECT " + AXOIDDB_AXOID + "," + AXOIDDB_TERMVALUE + "," + AXOIDDB_LANGUAGE + " FROM " + AXOID_DB + "." + AXOID_TABLE + " where " + AXOIDDB_TERMVALUE + " > '' AND " + AXOIDDB_LANGUAGE + " > '' AND " + AXOIDDB_AXOID + " = " + axoid;
                ArrayList<String> parameters = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermNameIncluded());
                ArrayList<String> formatedParameters = new ArrayList<String>();
                for (String param : parameters) {
                    formatedParameters.add(Utils.formatParameter(param));
                }
                if (!parameters.isEmpty() && !parameters.get(0).isEmpty()) {
                    query = query.concat(" AND( " + AXOIDDB_TERMREFINEMENT + "=");
                    query = query.concat(StringUtils.join(formatedParameters, " OR " + AXOIDDB_TERMREFINEMENT + "="));
                    query = query.concat(")");
                }
                resultSet = statement.executeQuery(query);
                while (resultSet.next()) {
                    String termValue = resultSet.getString(AXOIDDB_TERMVALUE);
                    String lang = resultSet.getString(AXOIDDB_LANGUAGE);
                    if (publisherMap.containsKey(axoid)) {
                        publisherMap.get(axoid).addContent(lang, termValue);
                    } else {
                        String defLanguage = getDefLanguagesHashMap().get(axoid);
                        publisherMap.put(axoid, new Dcmi(axoid, defLanguage));
                        publisherMap.get(axoid).addContent(lang, termValue);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
        publisherMap = includeOptionalField(publisherMap);
        ArrayList<Dcmi> dcmisByPublisher = new ArrayList<Dcmi>();
        ArrayList<Integer> integers = new ArrayList<Integer>();
        if (publisherMap.values().size() > number) {
            for (int i = 0; i < number; i++) {
                int randomNumber = RandomUtils.nextInt(publisherMap.values().size());
                if (!integers.contains(randomNumber)) {
                    integers.add(randomNumber);
                    dcmisByPublisher.add(new ArrayList<Dcmi>(publisherMap.values()).get(randomNumber));
                }
            }
        }
        return dcmisByPublisher;

    }*/

    public ObjectArrayList<String> getAxoid4Publisher(String publisher) {
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ObjectArrayList<String> axoidListForPublisher = new ObjectArrayList<String>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            String query = "select " + AXOIDDB_AXOID + " FROM " + AXOID_DB + "." + AXOID_TABLE + " WHERE " + AXOIDDB_TERMREFINEMENT + " = " + Utils.formatParameter(ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermrefinement_publisher()) + " AND " + AXOIDDB_TERMVALUE + " = " + Utils.formatParameter(publisher);
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String axoid = resultSet.getString(AXOIDDB_AXOID);
                axoidListForPublisher.add(axoid);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }finally {
            close(resultSet,statement,connect);
        }
        return axoidListForPublisher;
    }
/*
 public ObjectBigArrayBigList<Dcmi> getDcmifromDBForPublisher(String publisher) {
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        HashMap<String, Dcmi> dcmiHashMap = new HashMap<String, Dcmi>();
        ArrayList<String> axoidListForPublisher = new ArrayList<String>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            String query = "select " + AXOIDDB_AXOID + " FROM " + AXOID_DB + "." + AXOID_TABLE + " WHERE " + AXOIDDB_TERMREFINEMENT + " = " + Utils.formatParameter(ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermrefinement_publisher()) + " AND " + AXOIDDB_TERMVALUE + " = " + Utils.formatParameter(publisher);
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String axoid = resultSet.getString(AXOIDDB_AXOID);
                axoidListForPublisher.add(axoid);
            }
            for (int i = 0; i < axoidListForPublisher.size(); i++) {
                String axoid = axoidListForPublisher.get(i);
                query = "SELECT " + AXOIDDB_AXOID + "," + AXOIDDB_TERMVALUE + "," + AXOIDDB_LANGUAGE + " FROM " + AXOID_DB + "." + AXOID_TABLE + " where " + AXOIDDB_TERMVALUE + " > '' AND " + AXOIDDB_LANGUAGE + " > '' AND " + AXOIDDB_AXOID + " = " + Utils.formatParameter(axoid);
                ArrayList<String> parameters = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermNameIncluded());
                ArrayList<String> formatedParameters = new ArrayList<String>();
                for (String param : parameters) {
                    formatedParameters.add(Utils.formatParameter(param));
                }
                if (!parameters.isEmpty() && !parameters.get(0).isEmpty()) {
                    query = query.concat(" AND( " + AXOIDDB_TERMREFINEMENT + "=");
                    query = query.concat(StringUtils.join(formatedParameters, " OR " + AXOIDDB_TERMREFINEMENT + "="));
                    query = query.concat(")");
                }
                resultSet = statement
                        .executeQuery(query);
                while (resultSet.next()) {
                    String termValue = resultSet.getString(AXOIDDB_TERMVALUE);
                    String lang = resultSet.getString(AXOIDDB_LANGUAGE);
                    if (dcmiHashMap.containsKey(axoid)) {
                        dcmiHashMap.get(axoid).addContent(lang, termValue);
                    } else {
                        String defLanguage = getDefLanguagesHashMap().get(axoid);
                        dcmiHashMap.put(axoid, new Dcmi(axoid, defLanguage));
                        dcmiHashMap.get(axoid).addContent(lang, termValue);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
        dcmiHashMap = includeOptionalField(dcmiHashMap);
        return new ObjectBigArrayBigList<Dcmi>(dcmiHashMap.values().iterator());
    }*/
    public ObjectBigArrayBigList<Dcmi> getDcmiForAxoid(String axoid)
    {
        Object2ObjectOpenHashMap<String, Dcmi> dcmiHashMap = new Object2ObjectOpenHashMap<String, Dcmi>();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            String query = "SELECT " + AXOIDDB_TERMVALUE + "," + AXOIDDB_LANGUAGE + " FROM " + AXOID_DB + "." + AXOID_TABLE + " where " + AXOIDDB_TERMVALUE + " > '' AND " + AXOIDDB_LANGUAGE + " > '' AND "+AXOIDDB_AXOID+" = "+Utils.formatParameter(axoid);
            List<String> parameters = ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermNameIncluded();
            if (parameters.isEmpty()||parameters.get(0).isEmpty())
            {
                logger.warn("axoid.table.termname.included not set in eclap.classifier.properties file, using all of them");
                parameters = getAllTermRefinement();
            }
            ArrayList<String> formatedParameters = new ArrayList<String>();
            for (String param : parameters) {
                formatedParameters.add(Utils.formatParameter(param));
            }
            query = query.concat(" AND( " + AXOIDDB_TERMREFINEMENT + "=");
            query = query.concat(StringUtils.join(formatedParameters, " OR " + AXOIDDB_TERMREFINEMENT + "="));
            query = query.concat(")");
            resultSet = statement.executeQuery(query);
            while (resultSet.next())
            {
                String termValue = resultSet.getString(AXOIDDB_TERMVALUE);
                String lang = resultSet.getString(AXOIDDB_LANGUAGE);
                if (dcmiHashMap.containsKey(axoid))
                {
                    dcmiHashMap.get(axoid).addContent(lang, termValue);
                } else {
                    String defLanguage = getDefLanguage(axoid);
                    dcmiHashMap.put(axoid, new Dcmi(axoid, defLanguage));
                    dcmiHashMap.get(axoid).addContent(lang, termValue);
                }
            }
        } catch (ClassNotFoundException e)
        {
            logger.catching(e);
            System.exit(-1);
        } catch (SQLException e) {
            logger.catching(e);
            System.exit(-1);
        } catch (ConfigurationException e) {
            logger.catching(e);
            System.exit(-1);
        } catch (Exception e) {
            logger.catching(e);
            System.exit(-1);
        }
        dcmiHashMap = includeOptionalField(dcmiHashMap);
        return new ObjectBigArrayBigList<Dcmi>(dcmiHashMap.values().iterator());

    }
    public List<String> getAllTermRefinement()
    {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ArrayList<String> values = new ArrayList<String>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connection.createStatement();
                        resultSet = statement.executeQuery(
                                "SELECT DISTINCT " + AXOIDDB_TERMREFINEMENT  + " from " + AXOID_TABLE);
            while (resultSet.next())
            {
                values.add(resultSet.getString(1));
            }
        } catch (ClassNotFoundException e) {
            logger.catching(e);  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        } catch (SQLException e) {
            logger.catching(e);  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        } finally {
            close(resultSet, statement, connection);
        }
        return values;

    }
    public ObjectBigArrayBigList<Dcmi> getDcmifromDB(int limit) {
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        createTaxonomy_link();
        Object2ObjectOpenHashMap<String, Dcmi> dcmiHashMap = new Object2ObjectOpenHashMap<String, Dcmi>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            String query = "SELECT " + AXOIDDB_AXOID + "," + AXOIDDB_TERMVALUE + "," + AXOIDDB_LANGUAGE + " FROM " + AXOID_DB + "." + AXOID_TABLE + " where " + AXOIDDB_TERMVALUE + " > '' AND " + AXOIDDB_LANGUAGE + " > '' ";
            List<String> parameters = ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermNameIncluded();
            if (parameters.isEmpty()||parameters.get(0).isEmpty())
            {
                logger.warn("axoid.table.termname.included not set in eclap.classifier.properties file, using all of them");
                parameters = getAllTermRefinement();
            }
            ArrayList<String> formatedParameters = new ArrayList<String>();
            for (String param : parameters) {
                formatedParameters.add(Utils.formatParameter(param));
            }
            query = query.concat(" AND( " + AXOIDDB_TERMREFINEMENT + "=");
            query = query.concat(StringUtils.join(formatedParameters, " OR " + AXOIDDB_TERMREFINEMENT + "="));
            query = query.concat(")");
            query = query.concat("AND "+AXOIDDB_AXOID+" NOT IN (SELECT "+TAXONOMY_LINK_TABLE+"."+TAXONOMY_LINK_TABLE_AXOID+" FROM "+TAXONOMY_LINK_DB+"."+TAXONOMY_LINK_TABLE+")");
            if (limit!=0) query = query.concat(" LIMIT 0,"+limit);
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String axoid = resultSet.getString(AXOIDDB_AXOID);
                String termValue = resultSet.getString(AXOIDDB_TERMVALUE);
                String lang = resultSet.getString(AXOIDDB_LANGUAGE);

                    if (!dcmiHashMap.containsKey(axoid))
                    {
                        dcmiHashMap.put(axoid,getDcmiForAxoid(axoid).get(0));
                    }
            }
        } catch (SQLException e) {
            logger.catching(e);
            System.exit(-1);
        } catch (ClassNotFoundException e) {
            logger.catching(e);
            System.exit(-1);
        } catch (Exception e) {
            logger.catching(e);
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
        dcmiHashMap = includeOptionalField(dcmiHashMap);
        return new ObjectBigArrayBigList<Dcmi>(dcmiHashMap.values().iterator());
    }
    public ObjectBigArrayBigList<Dcmi> getDcmifromDB4Explorer(int limit) {
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        createTaxonomy_link();
        Object2ObjectOpenHashMap<String, Dcmi> dcmiHashMap = new Object2ObjectOpenHashMap<String, Dcmi>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            String query = "SELECT " + AXOIDDB_AXOID + "," + AXOIDDB_TERMVALUE + "," + AXOIDDB_LANGUAGE + " FROM " + AXOID_DB + "." + AXOID_TABLE + " where " + AXOIDDB_TERMVALUE + " > '' AND " + AXOIDDB_LANGUAGE + " > '' ";
            List<String> parameters = ClassifierConfiguration.getClassiferConfigurationInstance().getAxoidDBTermNameIncluded();
            if (parameters.isEmpty()||parameters.get(0).isEmpty())
            {
                 logger.warn("axoid.table.termname.included not set in eclap.classifier.properties file");
                parameters = getAllTermRefinement();
            }
            ArrayList<String> formatedParameters = new ArrayList<String>();
            for (String param : parameters) {
                formatedParameters.add(Utils.formatParameter(param));
            }
            query = query.concat(" AND( " + AXOIDDB_TERMREFINEMENT + "=");
            query = query.concat(StringUtils.join(formatedParameters, " OR " + AXOIDDB_TERMREFINEMENT + "="));
            query = query.concat(")");
            query = query.concat("AND "+AXOIDDB_AXOID+" NOT IN (SELECT "+TAXONOMY_LINK_EXPLORER_TABLE+"."+TAXONOMY_LINK_TABLE_AXOID+" FROM "+TAXONOMY_LINK_DB+"."+TAXONOMY_LINK_EXPLORER_TABLE+")");
            if (limit!=0) query = query.concat(" LIMIT 0,"+limit);
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String axoid = resultSet.getString(AXOIDDB_AXOID);
                String termValue = resultSet.getString(AXOIDDB_TERMVALUE);
                String lang = resultSet.getString(AXOIDDB_LANGUAGE);

                if (dcmiHashMap.containsKey(axoid))
                {
                    dcmiHashMap.get(axoid).addContent(lang, termValue);
                } else {
                    String defLanguage = getDefLanguage(axoid);
                    dcmiHashMap.put(axoid, new Dcmi(axoid, defLanguage));
                    dcmiHashMap.get(axoid).addContent(lang, termValue);
                }
            }
        } catch (SQLException e) {
            logger.catching(e);
            System.exit(-1);
        } catch (ClassNotFoundException e) {
            logger.catching(e);
            System.exit(-1);
        } catch (ConfigurationException e) {
            logger.catching(e);
            System.exit(-1);
        } catch (Exception e) {
            logger.catching(e);
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
        dcmiHashMap = includeOptionalField(dcmiHashMap);
        return new ObjectBigArrayBigList<Dcmi>(dcmiHashMap.values().iterator());
    }
        public Object2ObjectOpenHashMap<String, Dcmi> includeOptionalField(Object2ObjectOpenHashMap<String, Dcmi> dcmiHashMap) {
        ObjectSet<String> keyset = dcmiHashMap.keySet();
        Object2ObjectOpenHashMap<String,Dcmi> copy = dcmiHashMap.clone();
        ObjectArrayList<Object> axoids = ObjectArrayList.wrap(keyset.toArray());
        Object2ObjectOpenHashMap<String,ObjectArrayList<String>> optionalFieds4Axoids = getOptionalFieds4Axoids(axoids);

        for (Map.Entry<String, Dcmi> entry : copy.entrySet()) {
            String axoid = entry.getKey();
            ObjectArrayList<String> values = optionalFieds4Axoids.get(axoid);
            if (!values.isEmpty())
            {
                String language = entry.getValue().getDefLanguage() == null ? EclapLanguage.english : entry.getValue().getDefLanguage().getLanguage();
                for (String value : values) {
                    entry.getValue().addContent(language, value);
                }
            }
        }
        return copy;
    }

    public String getDefLanguage(String targetAxoid) throws Exception {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        String defLanguage = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + ROOT_OBJECT_INFO_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connection.prepareStatement("SELECT DISTINCT " + ROOT_OBJECT_INFO_AXOID + "," + ROOT_OBJECT_INFO_DEFLANGUAGE + " from " + ROOT_OBJECT_INFO_DB + "." + ROOT_OBJECT_INFO_TABLE + " WHERE " + ROOT_OBJECT_INFO_DEFLANGUAGE + " > '' AND " + ROOT_OBJECT_INFO_AXOID + " = ?");
            statement.setString(1,targetAxoid);
            resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                defLanguage = resultSet.getString(2);
            }
        } catch (ClassNotFoundException e) {
            logger.catching(e);  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        } catch (SQLException e) {
            logger.catching(e);  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        } finally {
            close(resultSet, statement, connection);
        }
        if (defLanguage == null) logger.warn("not def language found for axoid => {} , using default",targetAxoid);
        return  defLanguage;

    }
    public String getDefLanguageAlternate(String targetaxoid) throws Exception
    {
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        ArrayList<String> defLanguages = new ArrayList<String>();
        String defLanguage = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connection.prepareStatement("SELECT " + AXOIDDB_AXOID + "," + AXOIDDB_TERMVALUE + " from " + AXOID_DB + "." + AXOID_TABLE + " WHERE " + AXOIDDB_AXOID + " = ? AND " + AXOIDDB_TERMREFINEMENT + " = ?");
            statement.setString(1,targetaxoid);
            statement.setString(2,AXOIDDB_LANGUAGE);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                defLanguages.add(resultSet.getString(2));
            }
            if (defLanguages.size()==1)
            {
                defLanguage = defLanguages.get(0);
            }if (defLanguages.size()>1){
                if (defLanguages.contains(EclapLanguage.english))defLanguage = EclapLanguage.english;
                else defLanguage = defLanguages.get(0);
            }else {
                //deflanguages is empty
                defLanguage = EclapLanguage.english;
            }
        } catch (ClassNotFoundException e) {
            logger.catching(e);  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        } catch (SQLException e) {
            logger.catching(e);  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        } finally {
            close(resultSet, statement, connection);
        }
        assert defLanguage!= null;
        return  defLanguage;
    }
    public Object2ObjectOpenHashMap<String,ObjectArrayList<String>> getOptionalFieds4Axoids(ObjectArrayList<Object> axoids)
    {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Object2ObjectOpenHashMap<String, ObjectArrayList<String>> optionalFields = new Object2ObjectOpenHashMap<String, ObjectArrayList<String>>();
        try{
            List<String> parameterList = ClassifierConfiguration.getClassiferConfigurationInstance().getOptionalFieldFieldNameIncluded();
            if (parameterList.isEmpty()||parameterList.get(0).isEmpty())
            {
                throw logger.throwing(Level.ERROR,new ConfigurationException("axoid.table.termname.included not set in eclap.classifier.properties file"));
            }
            ArrayList<String> formatedParameterList = new ArrayList<String>();
            for (String param : parameterList) {
                formatedParameterList.add(Utils.formatParameter(param));
            }
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + AXOID_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            String query = "SELECT " + OPTIONAL_FIELD_FIELDVALUE + " FROM " + OPTIONAL_FIELD_DB + "." + OPTIONAL_FIELD_TABLE;
            query = query.concat(" WHERE " + OPTIONAL_FIELD_FIELDNAME + " > ' ' AND (" + OPTIONAL_FIELD_FIELDNAME + " = ");
            query = query.concat(StringUtils.join(formatedParameterList, " or " + OPTIONAL_FIELD_FIELDNAME + " = ")+" AND "+OPTIONAL_FIELD_AXOID+" = ?");
            query = query.concat(")");
            statement = connection.prepareStatement(query);
            for (Object axoid : axoids)
            {
                statement.setString(1,(String)axoid);
                ObjectArrayList<String>values = new ObjectArrayList<String>();
                resultSet = statement.executeQuery();
                while (resultSet.next())
                {
                    values.add(resultSet.getString(1));
                }
                optionalFields.put((String)axoid,values);
            }
        } catch (ConfigurationException e) {
            logger.catching(e);
            System.exit(-1);
        } catch (ClassNotFoundException e) {
            logger.catching(e);
            System.exit(-1);        }
        catch (SQLException e) {
            logger.catching(e);
            System.exit(-1);
        }
        return optionalFields;
    }
    public void writeOnTaxonomy_link_explorer(ObjectArrayList<TaxonomyLink> linkExplorers,int limit)
    {
        createTaxonomy_Link_explorer();
        Connection connect = null;
        PreparedStatement statement = null;

        try {
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + TAXONOMY_LINK_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            String sql = "insert INTO "+TAXONOMY_LINK_EXPLORER_TABLE+" ("+TAXONOMY_LINK_TABLE_AXOID+","+TAXONOMY_LINK_TABLE_TID+","+TAXONOMY_LINK_EXPLORER_TABLE_CONTENT+") VALUES(?,?,?) ";
            statement = connect.prepareStatement(sql);
            for (TaxonomyLink linkExplorer : linkExplorers)
            {
                statement.setString(1,linkExplorer.getAxoid());
                statement.setInt(2, linkExplorer.getTid());
                statement.setString(3, ((TaxonomyLinkExplorer)linkExplorer).getText());
                statement.addBatch();
                if (linkExplorers.indexOf(linkExplorer)>limit){
                    statement.executeBatch();
                }
            }
            statement.executeBatch();
        }catch (SQLException e)
        {
            logger.catching(e);
            System.exit(-1);
        }finally {
            close(null,statement,connect);
        }
    }
    public  void writeTaxonomyLinkOnDatabase(ObjectArrayList<TaxonomyLink> taxonomyLinks,int limit)
    {
        createTaxonomy_link();
        Connection connect = null;
        PreparedStatement statement = null;

        try {
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + TAXONOMY_LINK_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            String sql = "insert INTO "+TAXONOMY_LINK_TABLE+" ("+TAXONOMY_LINK_TABLE_AXOID+","+TAXONOMY_LINK_TABLE_TID+","+TAXONOMY_LINK_TABLE_TAXEXTRACTIONDATE+") VALUES(?,?,?) ";
            statement = connect.prepareStatement(sql);
            for (TaxonomyLink taxonomyLink : taxonomyLinks)
            {
                statement.setString(1,taxonomyLink.getAxoid());
                statement.setInt(2, taxonomyLink.getTid());
                statement.setDate(3, taxonomyLink.getTaxExtractionDate());
                statement.addBatch();
                if (taxonomyLinks.indexOf(taxonomyLink)>limit){
                    statement.executeBatch();
                }
            }
            statement.executeBatch();
        }catch (SQLException e)
        {
            logger.catching(e);
            System.exit(-1);
        }finally {
            close(null,statement,connect);
        }
    }
   /* public static void writeTaxonomyLinkOnDatabaseAlternate() throws FileNotFoundException {
        createTaxonomy_link();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        File file = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getTaxonomyLinkUpdate());
        try {
            if (!file.exists())
                file.createNewFile();
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + TAXONOMY_LINK_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement();
            resultSet = statement
                    .executeQuery("load data local INFILE '" + file.getPath() + "' replace into table " + TAXONOMY_LINK_DB + "." + TAXONOMY_LINK_TABLE + "(" + TAXONOMY_LINK_TABLE_AXOID + "," + TAXONOMY_LINK_TABLE_TID + "," + TAXONOMY_LINK_TABLE_TAXEXTRACTIONDATE + ")");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        } finally {
            close(resultSet, statement, connect);
        }
    }*/
    public String getTraductionForTerm(String term,EclapLanguage from,EclapLanguage to)
    {
        Connection connect = null;
        PreparedStatement statement= null;
        ResultSet resultSet= null;
        String traducedTerm = null;
        try {
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + TAXONOMY_LANGUAGE_DB + "?useUnicode=true&characterEncoding=UTF-8&"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.prepareStatement("SELECT "+to.getLanguage()+" FROM "+TAXONOMY_LANGAUE_TABLE+" WHERE "+from.getLanguage() +" = ?");
            statement.setString(1,term);
            resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                traducedTerm = resultSet.getString(1);
            }else
            {
                System.out.println(statement.toString());
                throw new SQLException("traduction for term "+term+" not found in language "+to.getLanguage() );
            }
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            logger.catching(e);
            System.exit(-1);
        }finally {
            close(resultSet,statement,connect);
        }
        assert traducedTerm != null;
        return traducedTerm.toLowerCase();
    }
    private void verifyLanguageList()
    {
            ObjectArrayList<String> languages = new ObjectArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList());
            for (String lang : languages)
            {
                EclapLanguage.convert(lang);
            }
    }

    public Category getTopCategoryFromName(String name,EclapLanguage from) throws Exception {
        Category category = getCategoryFromName(name,from);
        return  getTopParentFromTid(category.getTid());
    }

    public Category getParentFromName(String categoryName,EclapLanguage from) throws Exception {
        Category category = getCategoryFromName(categoryName,from);
        int parent = getParentTid(category.getTid());
        return getCagoryFromTid(parent);
    }

    public Category getCagoryFromTid(int tid) throws Exception {
        Category sonCategory = null;
        for (Category category : getTaxonomyCategoryArrayList())
        {
            if (category.getTid()==tid)
            {
                sonCategory = category;
                break;

            }
        }
        if (sonCategory == null) throw new Exception("category not found for tid => "+tid);
        return sonCategory;
    }
    public Category getCategoryFromName(String fname,EclapLanguage from) throws Exception {
        Category Category = null;
        String name = getTraductionForTerm(fname,from,new EclapLanguage(EclapLanguage.english));
        for (Category category : getTaxonomyCategoryArrayList())
        {
            if (category.getName().equalsIgnoreCase(name))
            {
                Category = category;
                break;
            }
        }
        if (Category == null) throw new Exception("category not found for name => "+name);
        return Category;
    }
    public Category getTopParentFromTid(int tid) throws Exception {
        int son = tid;
        try {
            int parent = getParentTid(son);
            while (parent != 0)
            {
                son = parent;
                parent = getParentTid(son);
            }

        }catch (Exception e)
        {
            logger.catching(e);
            System.exit(-1);
        }
        return getCagoryFromTid(son);

    }
    public int getParentTid(int tid) throws Exception
    {
        Connection connect = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int parentTid = Integer.MAX_VALUE;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DBURL + "/" + TERM_HIERARCHY_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.prepareStatement("SELECT "+ TERM_HIERARCHY_DB_PARENT+" FROM " + TERM_HIERARCHY_TABLE+" WHERE "+TERM_HIERARCHY_DB_TID+" = ?");
            statement.setInt(1,tid);
            resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                parentTid = resultSet.getInt(1);
            }
        }catch (SQLException e)
        {
            logger.catching(e);
            System.exit(-1);
        } catch (ClassNotFoundException e) {
            logger.catching(e);
            System.exit(-1);
        } finally {
            close(resultSet,statement,connect);
        }
        if (parentTid ==Integer.MAX_VALUE) throw new Exception("couldn't find the parent for category with tid => "+tid);
        return parentTid;
    }

    private ObjectArrayList<Category> getTaxonomyCategoryArrayList() {
        if (taxonomyCategoryArrayList == null) taxonomyCategoryArrayList = getTaxonomyCategories();
        return taxonomyCategoryArrayList;
    }

}
