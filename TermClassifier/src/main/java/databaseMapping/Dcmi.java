package databaseMapping;

import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 14/07/13
 * Time: 13:02
 * To change this template use File | Settings | File Templates.
 */
public class Dcmi {
    private static Logger logger = org.apache.logging.log4j.LogManager.getLogger();
    private String axoid;
    private HashMap<String,ArrayList<String>> contentmap;
    private EclapLanguage defLanguage;

    public Dcmi(String axoid, String defLanguage) {
        this.axoid = axoid;
        contentmap = new HashMap<String, ArrayList<String>>();
        this.defLanguage = (defLanguage == null ? null : new EclapLanguage(defLanguage));
    }

    public void addContent(String lang, String newContent) {

        String eclaplang = EclapLanguage.convert(lang);
        if (!contentmap.containsKey(eclaplang))
        {
            contentmap.put(eclaplang,new ArrayList<String>());
        }
        contentmap.get(eclaplang).add(newContent);
    }

    public Map.Entry<String,ArrayList<String>> getPriorityLanguageContent() {
        Map.Entry<String,ArrayList<String>> priorityContent = null;
        if (defLanguage != null) {
            priorityContent = getContentsForLanguage(defLanguage.getLanguage());
        } else {
            try {
                priorityContent = getLowPriority();
            }catch (Exception e)
            {
                logger.catching(e);
                System.exit(-1);
            }
        }
        if (priorityContent == null )
        {
            try {
                priorityContent = getLowPriority();
            }catch (Exception e)
            {
                logger.catching(e);
                System.exit(-1);
            }
        }
        return priorityContent;
    }

    private Map.Entry<String,ArrayList<String>> getLowPriority() throws Exception {
        if (getContentsForLanguage(EclapLanguage.english) != null)
            return getContentsForLanguage(EclapLanguage.english);
        else if (getContentsForLanguage(EclapLanguage.italian) != null)
            return getContentsForLanguage(EclapLanguage.italian);
        else if (getContentsForLanguage(EclapLanguage.french) != null)
            return getContentsForLanguage(EclapLanguage.french);
        else if (getContentsForLanguage(EclapLanguage.catalan) != null)
            return getContentsForLanguage(EclapLanguage.catalan);
        else if (getContentsForLanguage(EclapLanguage.hungarian) != null)
            return getContentsForLanguage(EclapLanguage.hungarian);
        else if (getContentsForLanguage(EclapLanguage.polish) != null)
            return getContentsForLanguage(EclapLanguage.polish);
        else if (getContentsForLanguage(EclapLanguage.dutch) != null)
            return getContentsForLanguage(EclapLanguage.dutch);
        else if (getContentsForLanguage(EclapLanguage.spanish) != null)
            return getContentsForLanguage(EclapLanguage.spanish);
        else if (getContentsForLanguage(EclapLanguage.portuguese) != null)
            return getContentsForLanguage(EclapLanguage.portuguese);
        else if (getContentsForLanguage(EclapLanguage.greek) != null)
            return getContentsForLanguage(EclapLanguage.greek);
        else if (getContentsForLanguage(EclapLanguage.danish) != null)
            return getContentsForLanguage(EclapLanguage.danish);
        else if (getContentsForLanguage(EclapLanguage.slovene) != null)
            return getContentsForLanguage(EclapLanguage.slovene);
        else if (getContentsForLanguage(EclapLanguage.german) != null)
            return getContentsForLanguage(EclapLanguage.german);
        else throw new Exception("not language available for this axoid: " + axoid);
    }

    public String getAxoid() {
        return axoid;
    }

    public Map.Entry<String,ArrayList<String>> getContentsForLanguage(String language)
    {
        String eclaplang = EclapLanguage.convert(language);
        Map.Entry found = null;
        if (contentmap.containsKey(eclaplang))
        {
           for (Map.Entry<String,ArrayList<String>> entry: contentmap.entrySet())
            {
                if (entry.getKey().equals(eclaplang))
                    found =  entry;
            }
        }
        return found;
    }

    public EclapLanguage getDefLanguage() {
        return defLanguage;
    }

    public String toString() {
        return "axoid: " + axoid + "\t" + defLanguage.toString() + "\t" + getContentsForLanguage(defLanguage.getLanguage());


    }
}
