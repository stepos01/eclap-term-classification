package databaseMapping;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import searchUtils.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 02/07/13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */

public class Category implements Comparable<Category>{
    private String name;
    private int tid;
    private float score;
    private float accuracy;
    private int hitnumber = 0;
    private static Logger logger = LogManager.getLogger();

    public float getAccuracy() {
        return accuracy;
    }

    public void incAccuracy(float accuracy) {
        this.accuracy +=accuracy;
        hitnumber++;
    }

    public float getScore() {
        return score;
    }

    public void incScore(float score) {
        this.score += score;
    }

    public Category(String name, int tid) {
        try {
            this.tid = tid;
            if (Utils.checkNullOrEmpty(name)) this.name = name;
            else throw new Exception("try to create a category with a null or empty string name");
        }catch (Exception e)
        {
            logger.catching(e);
            System.exit(-1);
        }

    }



    public int getTid() {
        return tid;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return "name: " + getName() + "\t" + "score: " + getScore() + "\t" + "accuracy: " + getAccuracy();
    }

    @Override
    public int compareTo(Category o) {
        return  Math.round(o.getAccuracy()-Math.round(getAccuracy()));

    }
    public void setAccuracy(float accuracy)
    {
        this.accuracy = accuracy;
    }
    public void setScore(float score)
    {
        this.score = score;
    }
    public int gethitnumber(){
       return hitnumber;
    }
}
