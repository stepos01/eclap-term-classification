package databaseMapping;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 23/09/13
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */
public class CategoryDictionary {
    private ArrayList<DictionaryEntry> dictionaryEntries = null;

    public CategoryDictionary() {
        dictionaryEntries = new ArrayList<DictionaryEntry>();
    }

    public boolean contains(String name) {
        boolean found = false;
        for (DictionaryEntry entry : dictionaryEntries) {
            if (entry.getEntry().equals(name)) {
                found = true;
                break;
            }
        }
        return found;
    }

    public void addEntry(String entry, String lang) {
        dictionaryEntries.add(new DictionaryEntry(entry, lang));
    }

    public String getTraduction(EclapLanguage language) {
        String traduction = null;
        boolean found = false;
        for (DictionaryEntry entry : dictionaryEntries) {
            if (language.equals(entry.getLanguage())) {
                traduction = entry.getEntry();
                found = true;
                break;
            }
        }
        if (found == false)
            System.err.println("the " + language.getLanguage() + " traduction for " + getTraduction(new EclapLanguage(EclapLanguage.english)) + " was not found");
        return traduction;
    }

}
