package databaseMapping;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 23/03/2014
 * Time: 23:45
 * To change this template use File | Settings | File Templates.
 */
public class TaxonomyLinkExplorer extends TaxonomyLink {
    private String text;
    public TaxonomyLinkExplorer(String axoid,int tid,String text)
    {
        super(axoid,tid);
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
