package lightAnalyzer;

import configuration.ClassifierConfiguration;
import databaseMapping.EclapLanguage;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.ca.CatalanAnalyzer;
import org.apache.lucene.analysis.da.DanishAnalyzer;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.analysis.el.GreekAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.es.SpanishAnalyzer;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.analysis.hu.HungarianAnalyzer;
import org.apache.lucene.analysis.it.ItalianAnalyzer;
import org.apache.lucene.analysis.nl.DutchAnalyzer;
import org.apache.lucene.analysis.pl.PolishAnalyzer;
import org.apache.lucene.analysis.pt.PortugueseAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.io.StringReader;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 26/02/2014
 * Time: 22:12
 * To change this template use File | Settings | File Templates.
 */
public class AnalyzerUtils {
    private final static String[] SLOVENIAN_DEFAULT_STOPWORDS = {"h", "in", "je", "k", "onega", "onem", "onemu", "onim", "ono", "v", "&"};

    public static Analyzer getLightAnalyser(EclapLanguage language) {
        if (language.getLanguage().equals("fr")) return new FrenchLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("en")) return new EnglishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("it")) return new ItalianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("de")) return new GermanLightanalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("da")) return new DanishLightAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("nl")) return new DutchLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("hu")) return new HungarianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("pt")) return new PortugueseLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("es")) return new SpanishLightAnalyser(Version.LUCENE_44);
        else if (language.getLanguage().equals("ca")) return new CatalanLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("pl")) return new PolishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals("el")) return new GreekLightAnalyser(Version.LUCENE_44);
        else if (language.getLanguage().equals("sl")) return new SlovenianLightAnalyzer(Version.LUCENE_44);
        else
            throw new RuntimeException("light Analyser for the required language : " + language.getLanguage() + "  not found");
    }

    public static ObjectArrayList<String> tokenizeString(Analyzer analyzer, String string) {
        ObjectArrayList<String> result = new ObjectArrayList<String>();
        try {
            TokenStream stream = analyzer.tokenStream(null, new StringReader(string));
            stream.reset();
            while (stream.incrementToken()) {
                result.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
        } catch (IOException e) {
            // not thrown b/c we're using a string reader...
            throw new RuntimeException(e);
        }
        return result;
    }

    public static Analyzer getLuceneAnalyser(EclapLanguage language) {
        if (language.getLanguage().equals("fr")) return new FrenchAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("en")) return new EnglishAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("it")) return new ItalianAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("de")) return new GermanAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("da")) return new DanishAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("nl")) return new DutchAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("hu")) return new HungarianAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("pt")) return new PortugueseAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("es")) return new SpanishAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("ca")) return new CatalanAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("pl")) return new PolishAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("el")) return new GreekAnalyzer(Version.LUCENE_45);
        else if (language.getLanguage().equals("sl"))
            return new StandardAnalyzer(Version.LUCENE_45, new CharArraySet(Version.LUCENE_45, Arrays.asList(SLOVENIAN_DEFAULT_STOPWORDS), true));
        else
            throw new RuntimeException("Analyser for the required language : " + language.getLanguage() + "  not found");
    }

    public static String deAccentuate(String str) {
        if (str == null) {
            throw new AssertionError("accentremover String to should not be null ");
        }
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }


    public static ArrayList<String> createStopWordList(CharArraySet set) {
        ArrayList<String> stopWords = new ArrayList<String>();
        List<String> stopWordExlusion = ClassifierConfiguration.getClassiferConfigurationInstance().getStopWordExclusion();
        Iterator iter = set.iterator();
        stopWords.add("&");
        while (iter.hasNext()) {
            char[] stopWord = (char[]) iter.next();
            String word = new String(stopWord);
            if (!stopWordExlusion.contains(word)) stopWords.add(word);
        }
        return stopWords;
    }
}
