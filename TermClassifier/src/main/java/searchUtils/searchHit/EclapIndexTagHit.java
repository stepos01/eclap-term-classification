package searchUtils.searchHit;


import databaseMapping.EclapLanguage;
import searchUtils.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 14/10/13
 * Time: 11:09
 * To change this template use File | Settings | File Templates.
 */
public class EclapIndexTagHit extends EclapIndexTitleHit {
    private int count;
    public EclapIndexTagHit(String categoryName, String targetHit, int count, String htmlText, EclapLanguage language) {
        super(categoryName,targetHit,htmlText,language);
        this.count = count;

    }

    public void setCount(int count) {
        this.count = count;
    }
    @Override
    public int getCount()
    {
        return count;
    }



}
