package searchUtils.searchHit;


import databaseMapping.EclapLanguage;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import lightAnalyzer.AnalyzerUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import searchUtils.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 09/10/13
 * Time: 00:38
 * To change this template use File | Settings | File Templates.
 */
public class EclapIndexTitleHit {
    private String categoryName;
    private String hitTarget;
    private String htmlText;
    private ObjectArrayList<String> realHitTargets = new ObjectArrayList<String>();
    private ObjectArrayList<String> possibleHitTarget = new ObjectArrayList<String>();
    private static Logger logger = LogManager.getLogger();
    private EclapLanguage language;
    private float inter_score;
    private float hitWeight;
    private float accuracy;
    public EclapIndexTitleHit(String categoryName,String hitTarget,String htmlText, EclapLanguage language) {
        try {
            setCategoryName(categoryName);
            setHitTarget(hitTarget);
            setHtmlText(htmlText);
            this.language = language;
        }catch (Exception e)
        {
            logger.catching(e);
            System.exit(-1);
        }

    }
    public EclapLanguage getLanguage()
    {
        return language;
    }
    public EclapIndexTitleHit initialize()
    {
        setRealHitTarget();
        setPossibleHitTarget();
        inter_score = setScore();
        return this;
    }
    public ObjectArrayList<String> getPossibleHitTarget() {
        return possibleHitTarget;
    }

    public ObjectArrayList<String> getRealHitTargets() {
        return realHitTargets;
    }

    private void setPossibleHitTarget()
    {
       possibleHitTarget = AnalyzerUtils.tokenizeString(AnalyzerUtils.getLuceneAnalyser(language), hitTarget);

    }
    private void setRealHitTarget()
    {
              String target = htmlText;
             while (target.contains("<B>"))
             {
                 int start = target.indexOf(">")+1;
                 int end = target.indexOf("<",start);
                 realHitTargets.add(target.substring(start,end));
                 target = target.substring(end+4);
             }
    }

    public void setHitTarget(String hitTarget) throws Exception {
        if (Utils.checkNullOrEmpty(hitTarget))this.hitTarget = hitTarget;
        else throw new Exception("tried to set a category hit target null");
    }

    public void setCategoryName(String categoryName) throws Exception {
        if (Utils.checkNullOrEmpty(categoryName))this.categoryName  = categoryName;
        else throw new Exception("tried to set a category name null");
    }
    public void setHtmlText(String htmlText) throws Exception {
        if (Utils.checkNullOrEmpty(htmlText)) this.htmlText = htmlText;
        else throw new Exception("tried to set a category htmlText null");

    }

    public float getHitWeight() {
        return hitWeight;
    }

    public void setHitWeight(float hitWeight) {
        this.hitWeight = hitWeight;
    }

    public int getCount()
    {
        return 1;
    }
    public String getCategoryName() {
        return categoryName;
    }

    public String getHitTarget() {
        return hitTarget;
    }

    public String getHtmlText() {
        return htmlText;
    }
    public void setSiblings(int a)
    {}
    public boolean isSibling(EclapIndexTitleHit target)
    {
        return true;
    }

    private float setScore()
    {
        float score = 0;
        float possible = (float)possibleHitTarget.size();
        float real = (float)realHitTargets.size();
        if (possible > real)
        {
            if ((possible/2)>real)
            {
                score = real/possible;
            }else
            {
                score = real;
            }
        }else  {
           score = real * real;
        }
//        if (possible < real){
//            logger.warn("goat found: more real hits than possible hits: possible hits => {} real hits => {}",possibleHitTarget,realHitTargets);
//        }
        return score*getCount();
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy*getCount();
    }

    public float getScore()
    {return inter_score;}
    public String toString()
    {
        return "title: "+ categoryName+"\t"+"hit target: "+hitTarget+"\t"+"real hit target: ";
    }
    public float setScoreAlternate()
    {

         return (float)realHitTargets.size();
    }
}
