package searchUtils.Explorer;

import databaseMapping.Category;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/03/2014
 * Time: 00:29
 * To change this template use File | Settings | File Templates.
 */
public class CategoryExplorer extends Category {
    public String htmlParsedText;
    public CategoryExplorer(String name, int tid) {
        super(name, tid);
    }

    public String getHtmlParsedText() {
        return htmlParsedText;
    }

    public void setHtmlParsedText(String htmlParsedText) {
        this.htmlParsedText = htmlParsedText;
    }
}
