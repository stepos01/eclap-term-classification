package searchUtils.Explorer;

import databaseMapping.Category;
import databaseMapping.TaxonomyLink;
import databaseMapping.TaxonomyLinkExplorer;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import searchUtils.Classifier.ClassifierQueue;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/03/2014
 * Time: 00:35
 * To change this template use File | Settings | File Templates.
 */
public class ExplorerQueue extends ClassifierQueue {
    public ExplorerQueue(int limit) {
        super(limit);
    }

    @Override
    public void put(String axoid, ObjectArrayList<Category> categories) {
        synchronized (this)
        {
            if (!categories.isEmpty())
            {
                for (Category cat : categories)
                {
                    CategoryExplorer categoryExplorer = (CategoryExplorer)cat;
                    taxonomyLinks.add(new TaxonomyLinkExplorer(axoid,categoryExplorer.getTid(),categoryExplorer.getHtmlParsedText()));
                }
            }
            this.notify();
        }    }
}
