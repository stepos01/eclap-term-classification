package searchUtils.Explorer;

import databaseMapping.MysqlAccessor;
import databaseMapping.TaxonomyLink;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import searchUtils.Classifier.ClassifierQueue;
import searchUtils.Classifier.ClassifierWriter;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 23/03/2014
 * Time: 23:59
 * To change this template use File | Settings | File Templates.
 */
public class ExplorerWriter extends ClassifierWriter {
    public ExplorerWriter(ClassifierQueue queue) {
        super(queue);
    }

    @Override
    protected void updateTaxonomyDB(ObjectArrayList<TaxonomyLink> copy) {
        if (copy.isEmpty()) return;
        //  MysqlAccessor.writeTaxonomyLinkOnDatabaseAlternate();
        MysqlAccessor.getMysqlAccessorSingleton().writeOnTaxonomy_link_explorer(copy, getLimit());
    }
}
