package searchUtils.Explorer;

import databaseMapping.Dcmi;
import databaseMapping.MysqlAccessor;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import searchUtils.MyLatch;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 23/03/2014
 * Time: 23:12
 * To change this template use File | Settings | File Templates.
 */
public class Explorer {
    private static Logger logger = LogManager.getLogger();
    public Explorer() {
    }

    public void explore(int param)
    {

            long compute;

            logger.info("trying to load dcmi table");
            ObjectBigArrayBigList<Dcmi> dcmifromDB = MysqlAccessor.getMysqlAccessorSingleton().getDcmifromDB4Explorer(param);
            logger.debug("loaded dcmi");
            if (param == 0) compute = dcmifromDB.size64();
            else {
                if (param < dcmifromDB.size64()) compute = param;
                else compute = dcmifromDB.size64();
            }
            logger.debug("STARTING TEST:  {} entries to compute", param);
            final ExplorerQueue queue = new ExplorerQueue(100);
            final ExplorerWriter writer = new ExplorerWriter(queue);
            writer.start();
            ExecutorService pool = Executors.newFixedThreadPool(10);
            MyLatch myLatch = new MyLatch(compute);
            for (long i = 0; i < compute; i++)
            {
                pool.execute(new ExplorerThread(dcmifromDB.get(i),queue,myLatch));
            }
            pool.shutdown();
            try {
                synchronized (myLatch)
                {
                    while (!myLatch.isDone())
                    {
                        myLatch.wait();
                    }
                }
            } catch (InterruptedException e) {
                logger.catching(e);
            }
            assert myLatch.isDone();
            queue.setKeepProducing(false);
    }



}
