package searchUtils.Explorer;

import databaseMapping.Category;
import databaseMapping.Dcmi;
import databaseMapping.EclapLanguage;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.commons.lang.StringUtils;
import searchUtils.Classifier.ClassifierQueue;
import searchUtils.Classifier.EclapIndexesSearch;
import searchUtils.Classifier.EclapScorerModel;
import searchUtils.Classifier.SearchThread;
import searchUtils.MyLatch;
import searchUtils.searchHit.EclapIndexTitleHit;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/03/2014
 * Time: 01:13
 * To change this template use File | Settings | File Templates.
 */
public class ExplorerThread extends SearchThread {
    public ExplorerThread(Dcmi dcmi, ClassifierQueue queue, MyLatch myLatch) {
        super(dcmi, queue, myLatch);
    }


    @Override
    public void run() {
        try {
        String text = StringUtils.join(dcmi.getPriorityLanguageContent().getValue(), " ");
        logger.debug("working with {}",dcmi.getAxoid());
        EclapLanguage language = new EclapLanguage(dcmi.getPriorityLanguageContent().getKey());
        EclapIndexesSearch eclapIndexesSearch = new EclapIndexesSearch();
        ObjectArrayList<EclapIndexTitleHit> resultFromIndex = eclapIndexesSearch.search(text, language);
        ObjectArrayList<Category> categories = new ObjectArrayList<Category>();
        if (resultFromIndex != null)
        {
            EclapScorerModel scorerModel = EclapScorerModel.getInstance();
            ObjectArrayList<EclapIndexTitleHit> titleHits = scorerModel.ScoreHits(resultFromIndex, text, language);
            ObjectArrayList<CategoryExplorer> categoryExplorers = scorerModel.rateCategoryWithScore4Explorer(titleHits,text);
            categories.addAll(categoryExplorers);
            categories = scorerModel.getWinner(categories);
        }
        queue.put(dcmi.getAxoid(), categories);
    }finally {
        synchronized (myLatch)
        {
            myLatch.dec();
            myLatch.notifyAll();
        }
    }
    }
}
