package searchUtils;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 23/03/2014
 * Time: 23:16
 * To change this template use File | Settings | File Templates.
 */
public class MyLatch {


    private  double count;
    public MyLatch(double count)
    {
        this.count = count;
    }
    public   void dec()
    {
        synchronized (this)
        {
            count--;
        }
    }
    public double getCount()
    {

        synchronized (this)
        {
            return count;
        }
    }
    public boolean isDone()
    {
        return getCount()==0;
    }
}
