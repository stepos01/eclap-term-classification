package searchUtils; /**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/06/13
 * Time: 01:09
 * To change this template use File | Settings | File Templates.
 */

import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.lucene.analysis.util.CharArraySet;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;


public class Utils {
    public static Charset ENCODING = StandardCharsets.UTF_8;
    public static final String PARTS = "PARTS";
    public static final String TAGS = "TAGS";


    public static void writeLargerTextFile(String aFileName, List<String> aLines) {
        try {
            PrintWriter printWriter = new PrintWriter(new File(aFileName));
            printWriter.print("");
            printWriter.close();
            Path path = Paths.get(aFileName);
            BufferedWriter writer = Files.newBufferedWriter(path, ENCODING);
            writer.write("");
            for (String line : aLines) {
                writer.write(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public static ObjectBigArrayBigList<String> loadBigFile(String path) {
        ObjectBigArrayBigList<String> objectBigArrayBigList = new ObjectBigArrayBigList<String>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            while (line != null) {
                objectBigArrayBigList.add(line);
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return objectBigArrayBigList;
    }

    public static String formatParameter(String s) {
        return "'" + s + "'";
    }

    public static String deAccentuate(String str) {
        if (str == null) {
            throw new AssertionError("accentremover String to should not be null ");
        }
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }





    public static ArrayList<String> createStopWordList(CharArraySet set) {
        ArrayList<String> stopWords = new ArrayList<String>();
        List<String> stopWordExlusion = ClassifierConfiguration.getClassiferConfigurationInstance().getStopWordExclusion();
        Iterator iter = set.iterator();
        stopWords.add("&");
        while (iter.hasNext()) {
            char[] stopWord = (char[]) iter.next();
            String word = new String(stopWord);
            if (!stopWordExlusion.contains(word)) stopWords.add(word);
        }
        return stopWords;
    }
    public static boolean checkNullOrEmpty(String target)
    {
        if (target==null||target.isEmpty())
        return false;
        else return true;

    }
    public static boolean compareArraysOfString(ArrayList<String> a, ArrayList<String> b) {
        boolean equals = false;
        if (a == null && b == null) equals = true;
        else if (a != null && b != null) {
            if (a.size() == b.size()) {
                equals = true;
                for (int i = 0; i < a.size(); i++) {
                    if (!a.get(i).equals(b.get(i))) {
                        equals = false;
                        break;
                    }
                }
            }
        } else equals = false;
        return equals;

    }

    public static String removeDoubleSpace(String s) {
        return s.replaceAll(" +", " ").trim();
    }


}










