package searchUtils.Classifier;

import databaseMapping.Category;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import databaseMapping.TaxonomyLink;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 17/03/2014
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */
public class ClassifierQueue {
    protected ObjectArrayList<TaxonomyLink> taxonomyLinks = null;
    private boolean keepProducing = true;
    private int limit;

    public ClassifierQueue(int limit)
    {
        taxonomyLinks = new ObjectArrayList<TaxonomyLink>();
        this.limit = limit;
    }
    public int getLimit()
    {
        return limit;
    }
    public void  put(String axoid,ObjectArrayList<Category> categories)
    {
        synchronized (this)
        {
            if (categories.isEmpty())
            {
                TaxonomyLink link = new TaxonomyLink(axoid,0);
                taxonomyLinks.add(link);
            }else
            {
                for (Category cat : categories)
                {
                    taxonomyLinks.add(new TaxonomyLink(axoid,cat.getTid()));
                }
            }
            this.notify();
        }
    }
    public void setKeepProducing(boolean keepProducing)
    {
        synchronized (this)
        {
            this.keepProducing = keepProducing;
            this.notify();
        }
    }
    public boolean getkeepProducing()
    {
        synchronized (this)
        {
            return keepProducing;
        }
    }
    public ObjectArrayList<TaxonomyLink> getTaxonomyLinks()
    {
        synchronized (this)
        {
            if (taxonomyLinks.size()< limit&& keepProducing)
                return null;
            else {
                ObjectArrayList<TaxonomyLink> clone = taxonomyLinks.clone();
                taxonomyLinks.clear();
                return clone;
            }
        }
    }
}
