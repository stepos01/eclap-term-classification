package searchUtils.Classifier;

import databaseMapping.EclapLanguage;
import lightAnalyzer.AnalyzerUtils;

import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.*;
import searchUtils.searchHit.EclapIndexTagHit;
import searchUtils.searchHit.EclapIndexTitleHit;
import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 10/10/13
 * Time: 15:46
 * To change this template use File | Settings | File Templates.
 */
public class EclapIndexesSearch {
    private static Logger logger = LogManager.getLogger();
    public enum fieldUsed {
        tags,
        tagCount,
        tokenized_title,
        category_title

    }

    public EclapIndexesSearch() {
    }

    public ObjectArrayList<EclapIndexTitleHit> search(String target, EclapLanguage lang) {
        ObjectArrayList<String> tokenizedTarget = AnalyzerUtils.tokenizeString(AnalyzerUtils.getLuceneAnalyser(lang), target);
        ObjectArrayList<EclapIndexTitleHit> hits = new ObjectArrayList<EclapIndexTitleHit>();
        ObjectArrayList<String> lemma4Search = new ObjectArrayList<String>();

        for (int i = 0; i < tokenizedTarget.size(); i++)
        {
            lemma4Search.add(tokenizedTarget.get(i));
            if (lemma4Search.size() > (BooleanQuery.getMaxClauseCount() / 2) - 1) {
                ObjectArrayList<EclapIndexTitleHit> results = searchTitlesInIndex(lemma4Search, lang);
                if (results != null) hits.addAll(results);
                lemma4Search.clear();
            }
        }
        ObjectArrayList<EclapIndexTitleHit> results = searchTitlesInIndex(lemma4Search, lang);
        if (results != null) hits.addAll(results);

        //2nd part

        lemma4Search.clear();
        for (String lemma : tokenizedTarget)
        {
            lemma4Search.add(lemma);
            if (lemma4Search.size() > BooleanQuery.getMaxClauseCount() - 4) {
                results = searchTagsInIndex(lemma4Search, lang);
                if (results != null) hits.addAll(results);
                lemma4Search.clear();
            }
        }
        results = searchTagsInIndex(lemma4Search, lang);
        if (results != null) hits.addAll(results);

        if (hits.isEmpty()) hits = null;
        return hits;
    }

    private ObjectArrayList<EclapIndexTitleHit> searchTitlesInIndex(ObjectArrayList<String> tokens, EclapLanguage lang) {
        ObjectArrayList<EclapIndexTitleHit> classifierHits = new ObjectArrayList<EclapIndexTitleHit>();
        try {
            File luceneIndex = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getLuceneIndexes(),lang.getLanguage());
            if (!luceneIndex.exists()){
                logger.error("lucene indexes for language {} not found in {}",lang.getLanguage(),luceneIndex.getPath());
                System.exit(-1);
            }
            Directory directory = FSDirectory.open(luceneIndex);
            DirectoryReader ireader = DirectoryReader.open(directory);
            IndexSearcher isearcher = new IndexSearcher(ireader);
            BooleanQuery query = new BooleanQuery();
            for (String token : tokens) {
                query.add(new TermQuery(new Term(fieldUsed.tokenized_title.name(), token)), BooleanClause.Occur.SHOULD);
            }
            ScoreDoc[] hits = isearcher.search(query, null, 1000).scoreDocs;
            if (hits == null) return null;
            for (int i = 0; i < hits.length; i++) {
                int docId = hits[i].doc;
                Document d = isearcher.doc(docId);
                String title = d.get(fieldUsed.category_title.name());
                String tokenizedTitle = d.get(fieldUsed.tokenized_title.name());
                Highlighter highlighter = new Highlighter(new QueryScorer(query));
                highlighter.setTextFragmenter(new SimpleFragmenter());
                TokenStream tokenStream = TokenSources.getAnyTokenStream(isearcher.getIndexReader(), docId, fieldUsed.tokenized_title.name(), AnalyzerUtils.getLuceneAnalyser(lang));
                String htmlText = highlighter.getBestFragment(tokenStream, tokenizedTitle);
                classifierHits.add(new EclapIndexTitleHit(title,tokenizedTitle,htmlText,lang).initialize());

            }
            ireader.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (InvalidTokenOffsetsException e) {
            e.printStackTrace();
            System.exit(-1);        }
        return classifierHits;
    }

    private ObjectArrayList<EclapIndexTitleHit> searchTagsInIndex(ObjectArrayList<String> tokens, EclapLanguage lang) {
        ObjectArrayList<EclapIndexTitleHit> classifierHits = new ObjectArrayList<EclapIndexTitleHit>();
        try {
            File luceneIndex = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getLuceneIndexes() + lang.getLanguage());
            Directory directory = FSDirectory.open(luceneIndex);
            DirectoryReader ireader = DirectoryReader.open(directory);
            IndexSearcher isearcher = new IndexSearcher(ireader);
            BooleanQuery tagsQuery = new BooleanQuery();
            for (String token : tokens) {
                tagsQuery.add(new TermQuery(new Term(fieldUsed.tags.name(), token)), BooleanClause.Occur.SHOULD);
            }
            ScoreDoc[] hits = isearcher.search(tagsQuery, null, 1000).scoreDocs;
            if (hits == null) return null;
            for (int i = 0; i < hits.length; i++)
            {
                int docId = hits[i].doc;
                Document d = isearcher.doc(docId);
                int count = d.getField(fieldUsed.tagCount.name()).numericValue().intValue();
                String tag = d.get(fieldUsed.tags.name());
                String tag_title = d.get(fieldUsed.category_title.name());
                Highlighter highlighter = new Highlighter(new QueryScorer(tagsQuery));
                highlighter.setTextFragmenter(new SimpleFragmenter());
                TokenStream tokenStream = TokenSources.getAnyTokenStream(isearcher.getIndexReader(), docId, fieldUsed.tags.name(), AnalyzerUtils.getLuceneAnalyser(lang));
                String text = highlighter.getBestFragment(tokenStream, tag);
                classifierHits.add(new EclapIndexTagHit(tag_title,tag,count,text,lang).initialize());
            }
            ireader.close();
        } catch (InvalidTokenOffsetsException e) {
            logger.catching(e);
        } catch (IOException e) {
            logger.catching(e);
            System.exit(-1);
        return classifierHits;
    }
       return classifierHits;
    }
}