package searchUtils.Classifier;
import configuration.ClassifierConfiguration;
import databaseMapping.MysqlAccessor;
import databaseMapping.TaxonomyLink;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import searchUtils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 20/09/13
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */
public class ClassifierWriter extends Thread {
    private static Logger logger = LogManager.getLogger();
    private int limit;
    private ClassifierQueue queue;
    private boolean keepConsuming = true;

    public ClassifierWriter(ClassifierQueue queue) {
        this.limit = queue.getLimit();
        this.queue = queue;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    protected void updateTaxonomyDB(ObjectArrayList<TaxonomyLink> copy) {
        if (copy.isEmpty()) return;
          //  MysqlAccessor.writeTaxonomyLinkOnDatabaseAlternate();
            MysqlAccessor.getMysqlAccessorSingleton().writeTaxonomyLinkOnDatabase(copy, limit);
        }

    private static void writeOnTaxonomyLinkUpdate(ObjectArrayList<TaxonomyLink> taxonomyLinkArrayList) {
         File taxonomyLinkUpadate = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getTaxonomyLinkUpdate());
        List<String> lines = new ArrayList<String>();
        try {
            if (!taxonomyLinkUpadate.exists())taxonomyLinkUpadate.createNewFile();
            for (TaxonomyLink link : taxonomyLinkArrayList) {
                String line = link.toString();
                lines.add(line);
            }
            Utils.writeLargerTextFile(taxonomyLinkUpadate.getPath(), lines);
        } catch (IOException e) {
           logger.catching(e);
        }
    }


 /*   public ObjectArrayList<TaxonomyLink> getTaxonomyLink(String axoid) {
        ObjectArrayList<TaxonomyLink> links = new ObjectArrayList<TaxonomyLink>();
        synchronized (taxonomyLinks)
        {
            for (TaxonomyLink taxonomyLink : taxonomyLinks)
            {
                if (taxonomyLink.getAxoid().equals(axoid)) {
                    links.add(taxonomyLink);
                }
            }
            taxonomyLinks.notifyAll();
        }
        return links;
    }*/

    @Override
    public void run() {
        try {
            synchronized (queue)
            {
                while (keepConsuming)
                {
                    ObjectArrayList<TaxonomyLink>result;
                    while ((result = queue.getTaxonomyLinks()) == null)
                        queue.wait();
                    keepConsuming = queue.getkeepProducing();
                    updateTaxonomyDB(result);
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger.catching(e);
        }


    }
}
