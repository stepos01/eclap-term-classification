package searchUtils.Classifier;

import databaseMapping.Category;
import databaseMapping.Dcmi;
import databaseMapping.EclapLanguage;
import databaseMapping.MysqlAccessor;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import searchUtils.MyLatch;
import searchUtils.searchHit.EclapIndexTitleHit;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 23/03/2014
 * Time: 23:18
 * To change this template use File | Settings | File Templates.
 */
public class Classifier {
    private static Logger logger = LogManager.getLogger();

    public Classifier()
    {
    }
    public  void classify(int param)
    {
        long compute;

        logger.info("trying to load dcmi table");
        ObjectBigArrayBigList<Dcmi> dcmifromDB = MysqlAccessor.getMysqlAccessorSingleton().getDcmifromDB(param);
        logger.debug("loaded dcmi");
        if (param == 0) compute = dcmifromDB.size64();
        else {
            if (param < dcmifromDB.size64()) compute = param;
            else compute = dcmifromDB.size64();
        }
        logger.debug("STARTING TEST:  {} entries to compute", param);
        final ClassifierQueue classifierQueue = new ClassifierQueue(100);
        final ClassifierWriter classifierWriter = new ClassifierWriter(classifierQueue);
        classifierWriter.start();
        ExecutorService pool = Executors.newFixedThreadPool(10);
        MyLatch myLatch = new MyLatch(compute);
        for (long i = 0; i < compute; i++)
        {
            pool.execute(new SearchThread(dcmifromDB.get(i),classifierQueue,myLatch));
        }
        pool.shutdown();
        try {
            synchronized (myLatch)
            {
                while (!myLatch.isDone())
                {
                    myLatch.wait();
                }
            }
        } catch (InterruptedException e) {
            logger.catching(e);
        }
        assert myLatch.isDone();
        classifierQueue.setKeepProducing(false);
    }



}
