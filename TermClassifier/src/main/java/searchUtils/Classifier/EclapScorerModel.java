package searchUtils.Classifier;

import configuration.ClassifierConfiguration;
import databaseMapping.Category;
import databaseMapping.EclapLanguage;
import databaseMapping.MysqlAccessor;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import lightAnalyzer.AnalyzerUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import searchUtils.Explorer.CategoryExplorer;
import searchUtils.searchHit.EclapIndexTitleHit;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 11/03/2014
 * Time: 10:02
 * To change this template use File | Settings | File Templates.
 */
public class EclapScorerModel {
    private static Logger logger = LogManager.getLogger();
    private static EclapScorerModel instance;
    private EclapScorerModel(){}
    public static EclapScorerModel getInstance()
    {
        if (instance == null)instance = new EclapScorerModel();
        return instance;
    }
    public ObjectArrayList<EclapIndexTitleHit> ScoreHits(ObjectArrayList<EclapIndexTitleHit> targetHits, String text, EclapLanguage language)
    {
        ObjectArrayList<String> textList = AnalyzerUtils.tokenizeString(AnalyzerUtils.getLuceneAnalyser(language),text);
        ObjectArrayList<EclapIndexTitleHit> winners = new ObjectArrayList<EclapIndexTitleHit>();
        float accuracyThreshold = ClassifierConfiguration.getClassiferConfigurationInstance().getAccuracyThreshold();
        for (EclapIndexTitleHit hit : targetHits)
        {
            float hitWeight = 0;
            for ( String s : hit.getPossibleHitTarget())
            {

                    int count = numberOf(s,textList.clone());
                    hitWeight = hitWeight + count;

            }

            hit.setHitWeight(hitWeight);
            float accuracy = (hit.getHitWeight()/textList.size());
            hit.setAccuracy(accuracy);
            if (accuracy>=accuracyThreshold)
            {
                winners.add(hit);
            }

        }
        return winners;
    }
    private String generateHtmlTextfromOriginalTextAndHits(String text,ObjectArrayList<String> targets)
    {
        String copy = text.toLowerCase();
        for (String target : targets)
        {
            String tosearch = target;
            boolean found = true;
            while (!copy.contains(tosearch.toLowerCase()))
            {
                tosearch = tosearch.substring(0,tosearch.length()-1);
                if (tosearch.length()<4)
                {
                    found = false;
                   // logger.error("a hit tag '{}' was exactly found in corresponding text: {} ! exiting...",tosearch,text);
                    System.exit(-1);
                }
            }
            if (found)
                copy = copy.replaceAll(target.toLowerCase(),"<b>"+target.toLowerCase()+"</b>");
        }
        return copy;
    }
    public ObjectArrayList<CategoryExplorer>rateCategoryWithScore4Explorer(ObjectArrayList<EclapIndexTitleHit>targetHits,String text)
    {
        Int2ObjectOpenHashMap<CategoryExplorer> categories = new Int2ObjectOpenHashMap<CategoryExplorer>();
        try {
            for (EclapIndexTitleHit hit : targetHits)
            {
                Category category = MysqlAccessor.getMysqlAccessorSingleton().getCategoryFromName(hit.getCategoryName(),hit.getLanguage());
                if (!categories.containsKey(category.getTid()))
                {
                    categories.put(category.getTid(),new CategoryExplorer(category.getName(),category.getTid()));
                }
                categories.get(category.getTid()).incAccuracy(hit.getAccuracy());
                categories.get(category.getTid()).incScore(hit.getScore());
                categories.get(category.getTid()).setHtmlParsedText(generateHtmlTextfromOriginalTextAndHits(text,hit.getRealHitTargets()));
            }
        }catch (Exception e )
        {
            logger.catching(e);
            System.exit(-1);
        }
        CategoryExplorer[]categoriesArray = new CategoryExplorer[categories.values().size()];
        categories.values().toArray(categoriesArray);
        Arrays.sort(categoriesArray);
        for (Category category:categoriesArray)
        {
            category.setAccuracy(category.getAccuracy()/category.gethitnumber());
            category.setScore(category.getScore()/category.gethitnumber());
            //      category.setAccuracy(category.getAccuracy());
            //      category.setScore(category.getScore());
        }
        return ObjectArrayList.wrap(categoriesArray);
    }
    public ObjectArrayList<Category> rateCategoryWithScore(ObjectArrayList<EclapIndexTitleHit> targetHits)
    {
        Int2ObjectOpenHashMap<Category> categories = new Int2ObjectOpenHashMap<Category>();
        try {
            for (EclapIndexTitleHit hit : targetHits)
            {

                Category category = MysqlAccessor.getMysqlAccessorSingleton().getCategoryFromName(hit.getCategoryName(),hit.getLanguage());
                if (!categories.containsKey(category.getTid()))
                {
                   categories.put(category.getTid(),category);
                }
                categories.get(category.getTid()).incAccuracy(hit.getAccuracy());
                categories.get(category.getTid()).incScore(hit.getScore());
            }
        }catch (Exception e )
        {
            logger.catching(e);
            System.exit(-1);
        }
        Category[]categoriesArray = new Category[categories.values().size()];
        categories.values().toArray(categoriesArray);
        Arrays.sort(categoriesArray);
        for (Category category:categoriesArray)
        {
            category.setAccuracy(category.getAccuracy()/category.gethitnumber());
            category.setScore(category.getScore()/category.gethitnumber());
      //      category.setAccuracy(category.getAccuracy());
      //      category.setScore(category.getScore());
        }
        return ObjectArrayList.wrap(categoriesArray);
    }
    public ObjectArrayList<Category> rateTopCategoryWithScore(ObjectArrayList<EclapIndexTitleHit> targetHits)
    {
        Int2ObjectOpenHashMap<Category> topCategories = new Int2ObjectOpenHashMap<Category>();
        try {
            ObjectArrayList<Category> categories = rateCategoryWithScore(targetHits);
            for (Category son : categories)
            {
                Category parent = MysqlAccessor.getMysqlAccessorSingleton().getTopParentFromTid(son.getTid());
                if (!topCategories.containsKey(parent.getTid()))
                {
                    topCategories.put(parent.getTid(),parent);
                }
                topCategories.get(parent.getTid()).incAccuracy(son.getAccuracy());
                topCategories.get(parent.getTid()).incScore(son.getScore());
            }
        }catch (Exception e)
        {
            logger.catching(e);
            System.exit(-1);
        }
        Category[]topcategoriesArray = new Category[topCategories.values().size()];
        topCategories.values().toArray(topcategoriesArray);
        Arrays.sort(topcategoriesArray);
        return ObjectArrayList.wrap(topcategoriesArray);
    }

    public ObjectArrayList<Category> getWinner(ObjectArrayList<Category> targets)
    {
       /* ObjectArrayList<Category>winners = new ObjectArrayList<Category>();
        float accuracyThreshold = ClassifierConfiguration.getClassiferConfigurationInstance().getAccuracyThreshold();
        for (Category hit : targets)
        {

            if (hit.getAccuracy()>=accuracyThreshold)
            {
                winners.add(hit);
            }
        }
        return winners;*/
        return targets;
    }

    private int numberOf(String target,ObjectArrayList<String> list)
    {
        int count = 0;
        while (list.contains(target))
        {
            count++;
            list.remove(target);
        }
        return count;
    }

}
