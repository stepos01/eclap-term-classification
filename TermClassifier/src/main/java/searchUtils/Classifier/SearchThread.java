package searchUtils.Classifier;

import databaseMapping.Category;
import databaseMapping.Dcmi;
import databaseMapping.EclapLanguage;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import searchUtils.MyLatch;
import searchUtils.searchHit.EclapIndexTitleHit;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/03/2014
 * Time: 01:00
 * To change this template use File | Settings | File Templates.
 */
public class SearchThread implements Runnable {
    protected static Logger logger = LogManager.getLogger();
    protected final Dcmi dcmi;
    protected final MyLatch myLatch;
    protected final ClassifierQueue  queue;
    public SearchThread(final Dcmi dcmi,final ClassifierQueue queue, final MyLatch myLatch)
    {
        this.dcmi = dcmi;
        this.myLatch = myLatch;
        this.queue = queue;
    }
    public void run() {
        try {
            String text = StringUtils.join(dcmi.getPriorityLanguageContent().getValue()," ");
            logger.debug("working with {}",dcmi.getAxoid());
            EclapLanguage language = new EclapLanguage(dcmi.getPriorityLanguageContent().getKey());
            EclapIndexesSearch eclapIndexesSearch = new EclapIndexesSearch();
            ObjectArrayList<EclapIndexTitleHit> resultFromIndex = eclapIndexesSearch.search(text, language);
            ObjectArrayList<Category> categories = new ObjectArrayList<Category>();
            if (resultFromIndex != null)
            {
                EclapScorerModel scorerModel = EclapScorerModel.getInstance();
                ObjectArrayList<EclapIndexTitleHit> titleHits = scorerModel.ScoreHits(resultFromIndex, text, language);
                categories = scorerModel.rateCategoryWithScore(titleHits);
                categories = scorerModel.getWinner(categories);
            }
            queue.put(dcmi.getAxoid(), categories);
        }finally {
            synchronized (myLatch)
            {
                myLatch.dec();
                myLatch.notifyAll();
            }
        }
    }
}
