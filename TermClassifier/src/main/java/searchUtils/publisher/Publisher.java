package searchUtils.publisher; /**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/06/13
 * Time: 01:09
 * To change this template use File | Settings | File Templates.
 */

import configuration.ClassifierConfiguration;
import databaseMapping.Category;
import databaseMapping.Dcmi;
import databaseMapping.EclapLanguage;
import databaseMapping.MysqlAccessor;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import searchUtils.Classifier.EclapIndexesSearch;
import searchUtils.Classifier.EclapScorerModel;
import searchUtils.MyLatch;
import searchUtils.searchHit.EclapIndexTitleHit;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Publisher {
    private static Logger logger = LogManager.getLogger();
    private Object2ObjectOpenHashMap<String,ObjectArrayList<Category>> resultFromPublisher = new Object2ObjectOpenHashMap<String, ObjectArrayList<Category>>();
    private ExecutorService pool = Executors.newFixedThreadPool(10);

    public Publisher()
    {

    }
    public  synchronized void add2Result(String publisher,ObjectArrayList<Category> categories)
    {
        resultFromPublisher.put(publisher,categories);
    }
      private void analysePublisher(String publisher)
      {
          ObjectArrayList<String> axoid4Publisher = MysqlAccessor.getMysqlAccessorSingleton().getAxoid4Publisher(publisher);
          MyLatch myLatch = new MyLatch(axoid4Publisher.size());
          for (String axoid : axoid4Publisher)
          {
              ObjectBigArrayBigList<Dcmi> dcmiForAxoid = MysqlAccessor.getMysqlAccessorSingleton().getDcmiForAxoid(axoid);
              assert dcmiForAxoid.size64()== 1;
              Dcmi dcmi = dcmiForAxoid.get(0);
              pool.execute(generateSearchThread(dcmi,myLatch,publisher));
          }

          try {
              synchronized (myLatch)
              {
                  while (!myLatch.isDone())
                  {
                      myLatch.wait();
                  }
              }
          } catch (InterruptedException e) {
              logger.catching(e);
              Thread.currentThread().interrupt();
          }
          assert myLatch.isDone();

      }
    public void analyseAllPublisher()
    {
        List<String> publisherList = ClassifierConfiguration.getClassiferConfigurationInstance().getPublisherList();
        for (String publisher: publisherList)
        {
            analysePublisher(publisher);
        }
        pool.shutdown();
        for (Map.Entry<String, ObjectArrayList<Category>> entry : resultFromPublisher.entrySet())
        {
            String publishername = entry.getKey();
            float accuracy = 0;
            for (Category cat : entry.getValue())
            {
                accuracy+= cat.getAccuracy();
            }
            accuracy = accuracy/entry.getValue().size();
            logger.debug("publisher name : {} \t accuracy: {}",publishername,accuracy);

        }
    }
    private  Runnable generateSearchThread(final Dcmi dcmi, final MyLatch myLatch,final String publisher) {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    String text = StringUtils.join(dcmi.getPriorityLanguageContent().getValue()," ");
                    logger.debug("working with {}",dcmi.getAxoid());
                    EclapLanguage language = new EclapLanguage(dcmi.getPriorityLanguageContent().getKey());
                    EclapIndexesSearch eclapIndexesSearch = new EclapIndexesSearch();
                    ObjectArrayList<EclapIndexTitleHit> resultFromIndex = eclapIndexesSearch.search(text, language);
                    ObjectArrayList<Category> categories = new ObjectArrayList<Category>();
                    if (resultFromIndex != null)
                    {
                        EclapScorerModel scorerModel = EclapScorerModel.getInstance();

                        ObjectArrayList<EclapIndexTitleHit> titleHits = scorerModel.ScoreHits(resultFromIndex, text, language);

                        categories = scorerModel.rateCategoryWithScore(titleHits);

                        categories = scorerModel.getWinner(categories);
                    }
                    add2Result(publisher, categories);
                }finally {
                    synchronized (myLatch)
                    {
                        myLatch.dec();
                        myLatch.notifyAll();
                    }
                }
            }
        };
    }

}










