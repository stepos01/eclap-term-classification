import Configuration.ClassifierConfiguration;
import Configuration.CommandLineParameters;
import Translation.MicrosoftTranslateApiMethod;
import databaseMapping.Category;
import databaseMapping.CategoryDictionary;
import databaseMapping.ClassifierLanguage;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 27/09/13
 * Time: 15:00
 * To change this template use File | Settings | File Templates.
 */
public class Translator {
    public static void main(String[] args) {
        args = new String[4];
        args[0]= "-user";
        args[1]= "root";
        args[2]= "-pwd";
        args[3]= "";
        CommandLineParameters parameters = new CommandLineParameters();
        CmdLineParser cmdLineParser = new CmdLineParser(parameters);
        try {
            cmdLineParser.parseArgument(args);
            ClassifierConfiguration.getClassiferConfigurationInstance().checkConfiguration();
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java -jar Classifier.jar [options... ] arguments...");
            cmdLineParser.printUsage(System.err);
            return;
        } catch (org.apache.commons.configuration.ConfigurationException e) {
            e.printStackTrace();
            return;//To change body of catch statement use File | Settings | File Templates.
        }
        ArrayList<CategoryDictionary> categoryDictionaryArrayList = new ArrayList<CategoryDictionary>();
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(parameters.getPassword());
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(parameters.getUser());

        MicrosoftTranslateApiMethod apiMethod =  MicrosoftTranslateApiMethod.getInstance();
        ArrayList<Category> taxonomyCategoryArrayList = MysqlAccessor.getMysqlAccessorSingleton().getTaxonomyCategoryArrayList();
        ArrayList<String> languageList = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList());

        for (Category category :taxonomyCategoryArrayList)
        {
            CategoryDictionary categoryDictionary = new CategoryDictionary(category.getTid());
            for (String language : languageList) {
                try {
                    ClassifierLanguage classifierLanguage = new ClassifierLanguage(language);
                    String translate = apiMethod.translate(category.getName(), ClassifierLanguage.english, classifierLanguage.getLanguage());
                    categoryDictionary.addEntry(translate, classifierLanguage.getLanguage());
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
            System.out.println("Traduced totally: " + category.getName());
            categoryDictionaryArrayList.add(categoryDictionary);
        }
        System.out.println("Done!");
        MysqlAccessor.getMysqlAccessorSingleton().updateTaxonomyLanguage(categoryDictionaryArrayList);

    }
}
