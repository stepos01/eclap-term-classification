import configuration.ClassifierConfiguration;
import configuration.CommandLineParameters;
import databaseEntity.*;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import translationApi.MicrosoftTranslateApiMethod;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 27/09/13
 * Time: 15:00
 * To change this template use File | Settings | File Templates.
 */
public class EclapTranslator {
    private static int count = 0;
    private static Logger logger = LogManager.getLogger();
    private static ReentrantLock lock = new ReentrantLock();
    private static ArrayList<Tax_Category> taxonomyTaxCategoryArrayList;
    private static ArrayList<String> languageList = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList());
    private static ExecutorService pool = Executors.newFixedThreadPool(10);
    private static ArrayList<CategoryDictionary> categoryDictionaryArrayList = new ArrayList<CategoryDictionary>();
    private static ObjectArrayList<Txt_Category> categoriesResult = new ObjectArrayList<Txt_Category>();
    public static void main(String[] args) {
        Configurator.initialize(null, null, ClassifierConfiguration.getClassiferConfigurationInstance().getConfigPath() + "/config/log4j2.xml");
//        args = new String[6];
//        args[0] = "-u";
//        args[1] = "root";
//        args[2] = "-p";
//        args[3] = "";
//        args[4] = "-src";
//        args[5] = "txt";
        CommandLineParameters parameters = new CommandLineParameters();
        CmdLineParser cmdLineParser = new CmdLineParser(parameters);
        try {
            cmdLineParser.parseArgument(args);
            ClassifierConfiguration.getClassiferConfigurationInstance().checkConfiguration();
        } catch (CmdLineException e) {
            logger.catching(e);
            cmdLineParser.printUsage(System.err);
            return;
        } catch (org.apache.commons.configuration.ConfigurationException e) {
            e.printStackTrace();
            logger.catching(e);
            return;//To change body of catch statement use File | Settings | File Templates.
        }
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(parameters.getUser());
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(parameters.getPassword());
        if (languageList.contains("en"))
            languageList.remove("en");
        if (parameters.getSource().equals("db"))startDB(parameters);
        else if (parameters.getSource().equals("txt"))startTxt(parameters);
        else logger.error("incorrect parameter '-src' can only be 'txt' or 'db'");
        System.exit(-1);

    }
    private static void startTxt(CommandLineParameters parameters)
    {

        for (String lang : languageList)
        {
            categoriesResult.clear();
            File dest = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getConfigPath(),ClassifierConfiguration.getClassiferConfigurationInstance().getDocumentDST());
            dest.mkdirs();
            try {
                ObjectArrayList<Txt_Category> categoriesFromText = DocumentMapper.loadDocument4Traduction(EclapLanguage.english);
                CountDownLatch countDownLatch = new CountDownLatch(categoriesFromText.size());
                for (Txt_Category txtCategory : categoriesFromText)
                {
                    pool.execute(generateTask_textFile(txtCategory,countDownLatch,lang));
                }
                countDownLatch.await();
                ObjectArrayList<String> lines = new ObjectArrayList<String>();
                for (Txt_Category cat : categoriesResult)
                {
                    lines.add(cat.toString());
                }
                DocumentMapper.writeLargerTextFile(dest.getPath(),lang,lines);
                logger.debug("writing to file: {}",dest.getPath());
            } catch (IOException e) {
                logger.catching(Level.ERROR,e);
                System.exit(-1);
            } catch (InterruptedException e) {
                logger.catching(Level.ERROR,e);
                System.exit(-1);
            }
        }
        pool.shutdown();

    }
    private static void startDB(CommandLineParameters parameters) {
        MysqlAccessor.getMysqlAccessorSingleton().setPASSWORD(parameters.getPassword());
        MysqlAccessor.getMysqlAccessorSingleton().setUSER(parameters.getUser());
        taxonomyTaxCategoryArrayList = MysqlAccessor.getMysqlAccessorSingleton().getTaxonomyCategoryArrayList();
        CountDownLatch latch = new CountDownLatch(taxonomyTaxCategoryArrayList.size());

        for (int i = 0; i< taxonomyTaxCategoryArrayList.size();i++)
        {
            pool.execute(generateteTask_Mysql(latch));
        }
        pool.shutdown();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            logger.catching(e);
        }
        logger.trace("Translation Done! \n Now writing to DB...");
        MysqlAccessor.getMysqlAccessorSingleton().updateTaxonomyLanguage(categoryDictionaryArrayList);
    }

    private static Runnable generateTask_textFile(final Txt_Category txtCategory,final CountDownLatch countDownLatch,final String lang){
        return new Runnable() {
            @Override
            public void run() {
                try {
                    CategoryTags categoryTags = null;
                    if (!txtCategory.isEmpty()) {
                        categoryTags = new CategoryTags(new EclapLanguage(lang));
                        for (String tag: txtCategory.getCategoryTags().getTags().keySet())
                        {
                            String translate = MicrosoftTranslateApiMethod.getInstance().translate(tag, EclapLanguage.english, lang);
                            int count = txtCategory.getCategoryTags().getTags().getInt(tag);
                            for (int i = 0; i<count;i++)
                            {
                                categoryTags.addTag(translate);
                            }
                        }
                    }
                    //String name = MicrosoftTranslateApiMethod.getInstance().translate(txtCategory.getName(), EclapLanguage.english, lang);
                    String name = MysqlAccessor.getMysqlAccessorSingleton().getTraductionForTerm(txtCategory.getName(),lang);
                    Txt_Category result = new Txt_Category(name);
                    if (categoryTags!=null)result.setCategoryTags(categoryTags);
                    add2ResultCatefory(result);

                }finally {
                    countDownLatch.countDown();
                }


            }
        };
    }

    private static void add2ResultCatefory(Txt_Category txtCategory)
    {
        lock.lock();
        try {
            categoriesResult.add(txtCategory);
        }finally {
            lock.unlock();
        }
    }
    private static Runnable generateteTask_Mysql(final CountDownLatch latch)
    {
        return new Runnable() {
            @Override
           public void run(){
                try {
                    Tax_Category taxCategory = popTaxonomyCategoryArrayList();
                    if (taxCategory == null) {
                        return;
                    }
                    CategoryDictionary categoryDictionary = new CategoryDictionary(taxCategory.getTid());
                    categoryDictionary.addEntry(taxCategory.getName(),EclapLanguage.english);
                    for (String language : languageList) {
                        EclapLanguage eclapLanguage = new EclapLanguage(language);
                        String translate = MicrosoftTranslateApiMethod.getInstance().translate(taxCategory.getName(), EclapLanguage.english, eclapLanguage.getLanguage());
                        categoryDictionary.addEntry(translate, eclapLanguage.getLanguage());
                        logger.debug("traduced {} => {}", taxCategory.getName(),translate );
                    }
                    pushcategoryDictionaryArrayList(categoryDictionary);
                    logger.trace("Traduced totally: " + taxCategory.getName());
                }finally {
                    latch.countDown();
                }

                
           }
       };
    }
    private static Tax_Category popTaxonomyCategoryArrayList()
    {
        Tax_Category taxCategory;
        lock.lock();
        try {
            taxCategory = taxonomyTaxCategoryArrayList.get(count);
            count++;
        }catch (IndexOutOfBoundsException e)
        {
            assert count >= taxonomyTaxCategoryArrayList.size();
            return null;
        }
        finally {
            lock.unlock();
        }
        return taxCategory;

    }
    private static void pushcategoryDictionaryArrayList(CategoryDictionary categoryDictionary)
    {
        lock.lock();
        try {
            categoryDictionaryArrayList.add(categoryDictionary);
        }finally {
            lock.unlock();
        }
    }

}
