package translationApi;

import configuration.ClassifierConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 26/05/13
 * Time: 20:56
 * To change this template use File | Settings | File Templates.
 */
public class MicrosoftTranslateApiMethod {
    private String authToken;
    private ArrayList<String> languagesCode = null;
    private static MicrosoftTranslateAuthentication auth = null;
    private AccessToken token = null;
    private static MicrosoftTranslateApiMethod instance = null;
    private static Logger logger = LogManager.getLogger();
    private String charset;
    private String language4TranslateEndPoint;
    private String translateEndPoint;
    private ReentrantLock lock = new ReentrantLock();
    private MicrosoftTranslateApiMethod() {
        charset = ClassifierConfiguration.getClassiferConfigurationInstance().getMicrosoftTranslatorCharset();
        language4TranslateEndPoint = ClassifierConfiguration.getClassiferConfigurationInstance().getMicrosoftTranslatorLanguageForTranslateEndPoint();
        translateEndPoint = ClassifierConfiguration.getClassiferConfigurationInstance().getMicrosoftTranslatorTranslateEndPoint();
        authToken = "Bearer" + " " + getAccessToken().getAccess_token();
    }

    public boolean canTranslateThislanguage(String languageCode) {
        //call the microsoft service to obtain the list of available languages for translation
        if (getLanguagesCode().contains(languageCode)) {
            return true;
        } else {
            return false;
        }
    }

    public static MicrosoftTranslateApiMethod getInstance() {
        if (instance == null) instance = new MicrosoftTranslateApiMethod();
        return instance;
    }

    private void getLanguageSupportedCode() {
        URLConnection connection;
        try {
            connection = new URL(language4TranslateEndPoint).openConnection();
            connection.setRequestProperty("Authorization", authToken);
            InputStream response = connection.getInputStream();
            HttpURLConnection new_connection = (HttpURLConnection) connection;
            int status = new_connection.getResponseCode();
            if (status == 200) {
                String result = IOUtils.toString(response);
                IOUtils.closeQuietly(response);
                languagesCode = new ArrayList<String>(parseXml(result, "string"));
            }
        } catch (IOException e) {
            logger.catching(e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String translate(String textToTranslate, String languageSrc, String languageDest) {
        getAccessToken();
        String result = null;
        String params = null;
        if (canTranslateThislanguage(languageSrc) && canTranslateThislanguage(languageDest)) {
            try {
                params = "text=" + URLEncoder.encode(textToTranslate, charset) + "&" + "from=" + languageSrc + "&" + "to=" + languageDest;
            } catch (UnsupportedEncodingException e) {
                logger.catching(e);
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            String URLString = translateEndPoint + params;
            URLConnection connection = null;
            try {
                connection = new URL(URLString).openConnection();
                connection.setRequestProperty("Authorization", authToken);
                connection.setConnectTimeout(20000);
                InputStream response = connection.getInputStream();
                HttpURLConnection new_connection = (HttpURLConnection) connection;
                int status = new_connection.getResponseCode();
                if (status == 200) {
                    result = IOUtils.toString(response);
                    IOUtils.closeQuietly(response);
                    result = parseXml(result, "string").get(0);
                }else {
                    logger.warn("Translation","{} from {} => {} went wrong \n response code:{}",textToTranslate,languageSrc,languageDest,String.valueOf(status));
                }
            } catch (IOException e) {
                logger.catching(e);
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                translate(textToTranslate, languageSrc, languageDest);
            }
        } else {
            logger.warn("couldn't translate" + textToTranslate + "from" + languageSrc + "to" + languageDest);
        }
        if (result != null)
            logger.debug("Translation","{} from {} => {} in ",textToTranslate,languageSrc,languageDest,result);
        else logger.warn("Translation","{} from {} => {} in NULL",textToTranslate,languageSrc,languageDest);
        return result!=null?result.toLowerCase():null;

    }

    private ArrayList<String> parseXml(String xmlToParse, String tagName) {
        DocumentBuilder parser;
        Document document = null;
        try {
            parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = parser.parse(new InputSource(new StringReader(xmlToParse)));
        } catch (ParserConfigurationException e) {
            logger.catching(e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SAXException e) {
            logger.catching(e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            logger.catching(e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        NodeList nodelist = document.getElementsByTagName(tagName);
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < nodelist.getLength(); i++) {
            Element element = (Element) nodelist.item(i);
            list.add(element.getTextContent());
        }
        return list;
    }

    public ArrayList<String> getLanguagesCode() {
        if (languagesCode == null) getLanguageSupportedCode();
        return languagesCode;
    }

    public AccessToken getAccessToken() {
        lock.lock();
        try {
            if (token != null && token.isTokenValid()) return token;
            else {
                getAuth().postRequest();
                while (token== null)
                {
                    token = getAuth().getAccessToken();
                }
                authToken = "Bearer" + " " + token.getAccess_token();
                return token;
            }
        }finally {
            lock.unlock();
        }

    }

    public static MicrosoftTranslateAuthentication getAuth() {
        String userId = ClassifierConfiguration.getClassiferConfigurationInstance().getMicrosoftTranslatorUserId();
        String userSecret = ClassifierConfiguration.getClassiferConfigurationInstance().getMicrosoftTranslatorUserSecret();
        if (auth == null) auth = new MicrosoftTranslateAuthentication(userId, userSecret);
        return auth;
    }
}
