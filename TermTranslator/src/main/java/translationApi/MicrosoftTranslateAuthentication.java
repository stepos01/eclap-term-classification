package translationApi; /**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 25/05/13
 * Time: 16:01
 * To change this template use File | Settings | File Templates.
 */

import com.google.gson.Gson;
import configuration.ClassifierConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

public class MicrosoftTranslateAuthentication {
    private AccessToken accessToken;
    private String response;
    private String request_parameters;
    private Logger logger = LogManager.getLogger();
    private String charset;
    private String scope   ;
    private String grantType;
    private String urlEndPoint;
    public MicrosoftTranslateAuthentication(String client_id, String client_secret)
    {
         charset = ClassifierConfiguration.getClassiferConfigurationInstance().getMicrosoftTranslatorCharset();
         scope = ClassifierConfiguration.getClassiferConfigurationInstance().getMicrosoftTranslatorScope();
         grantType = ClassifierConfiguration.getClassiferConfigurationInstance().getMicrosoftTranslatorGrantType();
         urlEndPoint = ClassifierConfiguration.getClassiferConfigurationInstance().getMicrosoftTranslatorUrlEndPoint();
        try {
            request_parameters = "client_id=" + URLEncoder.encode(client_id,charset) + "&" +
                    "client_secret=" + URLEncoder.encode(client_secret, charset) + "&" +
                    "scope=" + scope + "&" +
                    "grant_type=" + grantType;
        } catch (UnsupportedEncodingException e) {
            logger.catching(e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void postRequest() {
        URLConnection connection = null;
        try {
            connection = new URL(urlEndPoint).openConnection();
        } catch (IOException e) {
            logger.catching(e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        connection.setDoOutput(true); // Triggers POST.
        connection.setRequestProperty("Accept-Charset", charset);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
        OutputStream output = null;
        try {
            output = connection.getOutputStream();
            output.write(request_parameters.getBytes(charset));
        } catch (UnsupportedEncodingException e) {
            logger.catching(e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            logger.catching(e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            if (output != null)
                try {
                    output.close();
                } catch (IOException logOrIgnore) {
                }
        }
        HttpURLConnection new_connection = (HttpURLConnection) connection;
        try {
            int status = new_connection.getResponseCode();
            if (status == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                response = sb.toString();
                processResponse();
            }
        } catch (IOException e) {
            logger.catching(e);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void processResponse() {
        accessToken = new Gson().fromJson(response, AccessToken.class);
        accessToken.setLimitTime();
    }

    private void setUpMyTimer() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                postRequest();
            }
        }, 9 * 60 * 1000, 9 * 60 * 1000);
    }

    public AccessToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(AccessToken accessToken) {
        this.accessToken = accessToken;
    }
}
