package databaseEntity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 23/09/13
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */
public class CategoryDictionary {
    private static Logger logger = LogManager.getLogger();
    private ArrayList<DictionaryEntry> dictionaryEntries = null;
    private int tid;

    public CategoryDictionary(int tid) {
        this.tid = tid;
        dictionaryEntries = new ArrayList<DictionaryEntry>();
    }

    public boolean contains(String name) {
        boolean found = false;
        for (DictionaryEntry entry : dictionaryEntries) {
            if (entry.getEntry().equals(name)) found = true;
        }
        return found;
    }

    public void addEntry(String entry, String lang) {
        dictionaryEntries.add(new DictionaryEntry(entry, lang));
    }

    public String getTraduction(String language) {
        String traduction = null;
        boolean found = false;
        EclapLanguage eclapLanguage = new EclapLanguage(language);
        for (DictionaryEntry entry : dictionaryEntries) {
            if (eclapLanguage.getLanguage().equals(entry.getLanguage().getLanguage())){
                traduction = entry.getEntry();
                found = true;
            }
        }
        if (found == false)
            logger.warn("the " + eclapLanguage.getLanguage() + " traduction for " + getTraduction(EclapLanguage.english) + " was not found");
        return traduction;
    }

    public int getTid() {
        return tid;
    }
}
