package databaseEntity;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 02/07/13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */

public class Tax_Category {
    private String name;
    private int tid;

    public Tax_Category(String name, int tid) {
        this.name = name;
        this.tid = tid;
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return "category: " + name + "\t tid: " + tid;
    }


}
