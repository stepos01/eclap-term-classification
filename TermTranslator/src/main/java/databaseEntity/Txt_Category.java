package databaseEntity;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 02/07/13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */

public class Txt_Category {
    private String name;
    private CategoryTags categoryTags;

    public Txt_Category(String name, CategoryTags categoryTags) {
        this.name = name;
        this.categoryTags = categoryTags;

    }
    public Txt_Category(String name)
    {
        this.name = name;
    }
    public void setCategoryTags(CategoryTags categoryTags) {
        this.categoryTags = categoryTags;
    }


    public String getName() {
        return name;
    }

    public CategoryTags getCategoryTags() {
        return categoryTags;
    }
    public boolean isEmpty()
    {
        return categoryTags==null||categoryTags.isEmpty();
    }
    public String toString() {
        if (isEmpty())
            return getName();
        else
            return getName()+"\t" + categoryTags.toString();
    }

}
