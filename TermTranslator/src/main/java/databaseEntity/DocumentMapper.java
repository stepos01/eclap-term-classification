package databaseEntity;

import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/02/2014
 * Time: 13:53
 * To change this template use File | Settings | File Templates.
 */

public class DocumentMapper {

    public static Charset ENCODING = StandardCharsets.UTF_8;
    public static final String TAGS = "TAGS";
    private static Logger logger = LogManager.getLogger();
    public static ObjectArrayList<Txt_Category> loadDocument4Traduction(String lang) throws IOException
    {
        File src = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocumentSRC()+lang);
        ObjectArrayList<Txt_Category> categories = new ObjectArrayList<Txt_Category>();
        if (!src.exists()){
            throw logger.throwing(Level.ERROR,new IOException("error using file: "+src.getName()));
        }
        else {
            ObjectArrayList<String> list = loadBigFile(src.getPath());
            for (String line : list)
            {
                categories.add(parseTextLine(line));
            }
            return categories;
        }
    }
    private static Txt_Category parseTextLine(String line)
    {
        CategoryTags categoryTags = new CategoryTags(new EclapLanguage(EclapLanguage.english));
        String[] block = line.split("\t");
        String name = block[0];
        if (block.length == 1) return new Txt_Category(name);
        String[] tags = block[1].split("\\|");
        for (String tag :tags)
        {
            String tagPart1 = tag.split(",")[0];
            tagPart1 = tagPart1.replace("{","").trim();
            String tagPart2  = tag.split(",")[1];
            tagPart2 = tagPart2.replace("}","").trim();
            Integer count = Integer.valueOf(tagPart2);
            for (int i = 0; i <count;i++)
            {
                categoryTags.addTag(tagPart1);
            }
        }
        return new Txt_Category(name,categoryTags);
    }


    public static void writeLargerTextFile(String folder,String filename,List<String> aLines) {
        try {
            File f = new File(folder,filename);
            f.createNewFile();
            Path path = Paths.get(f.getPath());
            BufferedWriter writer = Files.newBufferedWriter(path, ENCODING);
            writer.write("");
            for (String line : aLines) {
                writer.write(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static ObjectArrayList<String> loadBigFile(String path) {
        ObjectArrayList<String> objectArrayList = new ObjectArrayList<String>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            while (line != null) {
                objectArrayList.add(line);
                line = br.readLine();
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
        return objectArrayList;
    }

}
