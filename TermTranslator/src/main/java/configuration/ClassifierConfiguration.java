package configuration;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 16/07/13
 * Time: 07:59
 * To change this template use File | Settings | File Templates.
 */
public class ClassifierConfiguration {
    private static final ClassifierConfiguration instance = new ClassifierConfiguration();
    private PropertiesConfiguration config = null;
    public static final String CONFIG_FOLDER = "/config";
    public static final String CONFIG_PATH = "/eclap.translator.properties";
    private static Logger logger ;
    private ClassifierConfiguration() {
        logger = LogManager.getLogger();
        File configFile = new File(getConfigPath()+CONFIG_FOLDER,"eclap.translator.properties");
        System.out.println("lamorte: "+configFile.getAbsolutePath());
        boolean bDone = false;
        if (configFile.isFile()) {
            logger.debug("Loading " + configFile + " FROM " + configFile.getAbsolutePath());
            try {
                this.config = new PropertiesConfiguration(configFile);
                bDone = true;
            } catch (ConfigurationException ce) {
                ce.printStackTrace();
            }
        }
        if (!bDone) {
            try {
                throw new ConfigurationException("the following configuration file  'eclap.translator.properties is' not found");
            } catch (ConfigurationException e) {
                logger.catching(e);
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public String getDocumentSRC() {
        return config.getString("document.src");
    }
    public String getDocumentDST() {
        return config.getString("document.dst");
    }
    public void checkConfiguration() throws ConfigurationException {
        if (getMicrosoftTranslatorUserId().isEmpty() || getMicrosoftTranslatorUserSecret().isEmpty())
        {
            logger.error("The microsoft settings used for translation are not set in 'eclap.translator.properties");
            throw new ConfigurationException("The microsoft settings used for translation are not set in 'eclap.translator.properties");
        }
    }

    public static ClassifierConfiguration getClassiferConfigurationInstance() {
        return instance;
    }

    public String getConfigurationParameter(String parameter) {
        if (config == null) {
            throw new AssertionError("the following configuration file  'eclap.translator.properties is' not found");
        }
        return config.getString(parameter);
    }

    public String getUrl() {
        return config.getString("db.url");
    }

    public String getTermDatatid() {
        return config.getString("term.table.tid");
    }

    ;

    public String getTermDataName() {
        return config.getString("term.table.name");
    }

    public String getTermDataVid() {
        return config.getString("term.table.vid");
    }

    public String getTermDB() {
        return config.getString("term.db");
    }

    public String getTermTable() {
        return config.getString("term.table");
    }

    public String getTaxonomyLanguageDB() {
        return config.getString("taxonomy.language.db");
    }

    public String getTaxonomyLanguageTable() {
        return config.getString("taxonomy.language.table");
    }

    public List getLangagueList() {
        return config.getList("taxonomy.language.table.languages");
    }

    public String getMicrosoftTranslatorUserId() {
        return config.getString("microsot.client.id");
    }

    public String getMicrosoftTranslatorUserSecret() {
        return config.getString("microsoft.client.secret");
    }
    public String getMicrosoftTranslatorScope()
    {
        return config.getString("microsoft.scope");
    }
    public String getMicrosoftTranslatorGrantType()
    {
       return config.getString("microsoft.granttype");
    }
    public String getMicrosoftTranslatorUrlEndPoint()
    {
        return config.getString("microsoft.urlendpoint");
    }
    public String getMicrosoftTranslatorCharset()
    {
       return config.getString("microsoft.charset");
    }
    public String getMicrosoftTranslatorTranslateEndPoint()
    {
        return config.getString("microsoft.translatedendpoint");
    }
    public String getMicrosoftTranslatorLanguageForTranslateEndPoint()
    {
        return config.getString("microsoft.languagefortranslatedendpoint");
    }
    public String getConfigPath()
    {
            String decodedPath = null;

            try {
                String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();

                String jarpath = URLDecoder.decode(path, "UTF-8");
                File jarfile = new File(jarpath);
                decodedPath = jarfile.getParent();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return decodedPath;
    }

}
