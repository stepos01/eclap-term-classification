package configuration;

import org.kohsuke.args4j.Option;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 20/09/13
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */
public class CommandLineParameters {


    @Option(name = "-u", usage = "username for accessing Mysql")
    private String user;

    @Option(name = "-p", usage = "password for accessing Mysql")
    private String password;

    public String getUser() {
        return user;
    }
    @Option(name = "-src", usage = "source file: has to be db or txt")
    private String source;

    public String getPassword() {
        return password;
    }

    public String getSource() {
        return source;
    }

    @Override
    public String toString() {
        return "user: " + user + "\t" + "password: " + password + "\t" + "source: "+source;    //To change body of overridden methods use File | Settings | File Templates.
    }
}
