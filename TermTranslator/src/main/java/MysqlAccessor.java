import configuration.ClassifierConfiguration;
import databaseEntity.CategoryDictionary;
import databaseEntity.EclapLanguage;
import databaseEntity.Tax_Category;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 */
public class MysqlAccessor {
    private ArrayList<Tax_Category> taxonomyTaxCategoryArrayList = null;
    private static String DB_URL;
    private static String USER = null;
    private static String PASSWORD = null;
    private static String TERM_DB = null;
    private static String TERM_TABLE = null;
    private static String TERM_TABLE_TID = null;
    private static String TERM_TABLE_NAME = null;
    private static String TAXONOMY_LANGUAGE_DB = null;
    private static String TAXONOMY_LANGAUE_TABLE = null;
    private static String TERM_TABLE_VID = null;
    private static MysqlAccessor mysqlAccessor = null;
    private static Logger logger = LogManager.getLogger();
    private MysqlAccessor() {
        ClassifierConfiguration configuration = ClassifierConfiguration.getClassiferConfigurationInstance();
        logger.info("Loading  database configuration FROM " + ClassifierConfiguration.CONFIG_PATH);
        TERM_DB = configuration.getTermDB();
        TERM_TABLE = configuration.getTermTable();
        TERM_TABLE_TID = configuration.getTermDatatid();
        TERM_TABLE_NAME = configuration.getTermDataName();
        DB_URL = configuration.getUrl();
        TAXONOMY_LANGUAGE_DB = configuration.getTaxonomyLanguageDB();
        TAXONOMY_LANGAUE_TABLE = configuration.getTaxonomyLanguageTable();
        TERM_TABLE_VID = configuration.getTermDataVid();
        try {
            checkConfiguration();
        } catch (ConfigurationException e) {

            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.exit(-1);
        }
    }

    public void setPASSWORD(String PASSWORD) {
        MysqlAccessor.PASSWORD = PASSWORD;
    }

    public void setUSER(String USER) {
        MysqlAccessor.USER = USER;
    }

    public void checkConfiguration() throws ConfigurationException {
        if (TERM_DB.isEmpty())
            throw logger.throwing(new ConfigurationException("term.db not set in eclap.termexpansion.properties file"));
        if (TERM_TABLE.isEmpty())
            throw logger.throwing(new ConfigurationException("term.table not set in eclap.termexpansion.properties file"));
        if (DB_URL.isEmpty())
            throw logger.throwing(new ConfigurationException("db.url  not set in eclap.termexpansion.properties file"));
        if (TERM_TABLE_TID.isEmpty())
            throw logger.throwing(new ConfigurationException("term.table.tid  not set in eclap.termexpansion.properties file"));
        if (TERM_TABLE_NAME.isEmpty())
            throw logger.throwing(new ConfigurationException("term.table.name  not set in eclap.termexpansion.properties file"));
        if (TAXONOMY_LANGUAGE_DB.isEmpty())
            throw logger.throwing(new ConfigurationException("taxonomy.language.db not set in eclap.termexpansion.properties file"));
        if (TAXONOMY_LANGAUE_TABLE.isEmpty())
            throw logger.throwing(new ConfigurationException("taxonomy.language.table not set in eclap.termexpansion.properties file"));
        if (TERM_TABLE_VID.isEmpty())
            throw logger.throwing(new ConfigurationException("term.table.vid not set in eclap.termexpansion.properties file"));
    }

    public static MysqlAccessor getMysqlAccessorSingleton() {
        if (mysqlAccessor == null) mysqlAccessor = new MysqlAccessor();
        return mysqlAccessor;
    }

    private static void close(ResultSet resultSet, Statement statement, Connection connect) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
        }
    }

    private void getTaxonomyCategories() {
        ArrayList<Tax_Category> taxonomycategories = new ArrayList<Tax_Category>();
        Connection connect = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DB_URL + "/" + TERM_DB + "?"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            statement.setFetchSize(Integer.MIN_VALUE);
            resultSet = statement
                    .executeQuery("SELECT " + TERM_TABLE_TID + "," + TERM_TABLE_NAME + " from " + TERM_DB + "." + TERM_TABLE + " where " + TERM_TABLE_VID + " =" + 5);
            while (resultSet.next()) {
                int tid = resultSet.getInt(1);
                String category_name = resultSet.getString(2).toLowerCase().trim();
                taxonomycategories.add(new Tax_Category(category_name, tid));
            }
        } catch (Exception e) {
            logger.catching(e);
            e.printStackTrace();
            System.exit(-1);

        } finally {
            close(resultSet, statement, connect);
        }
        taxonomyTaxCategoryArrayList = taxonomycategories;
    }

    public ArrayList<Tax_Category> getTaxonomyCategoryArrayList() {
        if (taxonomyTaxCategoryArrayList == null) getTaxonomyCategories();
        return taxonomyTaxCategoryArrayList;
    }
    public void createTaxonomyLanguageTable() throws Exception
    {
        Connection connect;
        Statement statement;
        ArrayList<String> listLanguage = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList());
        String query = "";
        for (String l : listLanguage)
        {
            query = query.concat("`"+l+"` text CHARACTER SET utf8,");
        }
        connect = DriverManager
                .getConnection("jdbc:mysql://" + DB_URL + "?useUnicode=true&characterEncoding=UTF-8&"
                        + "user=" + USER + "&password=" + PASSWORD);
        statement = connect.createStatement();
        statement.execute("CREATE DATABASE IF NOT EXISTS  "+TAXONOMY_LANGUAGE_DB+" DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci");
        statement.execute("USE "+TAXONOMY_LANGUAGE_DB);
        statement.execute("CREATE TABLE IF NOT EXISTS `"+TAXONOMY_LANGAUE_TABLE+"` (\n" +
               query +
                "  `tid` int(10) unsigned NOT NULL DEFAULT '0',\n" +
                "  PRIMARY KEY (`tid`) USING BTREE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;\n");

    }
    public String getTraductionForTerm(String term,String language)
    {
        Connection connect = null;
        ResultSet resultSet= null;
        PreparedStatement statement;
        String traducedTerm = null;
        try {
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DB_URL + "/" + TAXONOMY_LANGUAGE_DB + "?useUnicode=true&characterEncoding=UTF-8&"
                            + "user=" + USER + "&password=" + PASSWORD);
            statement = connect.prepareStatement("SELECT "+language+" FROM "+TAXONOMY_LANGAUE_TABLE+" WHERE "+EclapLanguage.english+" = ?");
            statement.setString(1,term);
            resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                traducedTerm = resultSet.getString(1);
            }else
            {
                throw new SQLException("traduction for term "+term+" not found in language "+language );
            }
            resultSet.close();
            connect.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            logger.catching(e);
            System.exit(-1);
        }
        assert traducedTerm != null;
        return traducedTerm.toLowerCase();
    }

    public void updateTaxonomyLanguage(ArrayList<CategoryDictionary> dictionaries) {
        Connection connect = null;
        PreparedStatement preparedStatement = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            createTaxonomyLanguageTable();
            connect = DriverManager
                    .getConnection("jdbc:mysql://" + DB_URL + "/" + TAXONOMY_LANGUAGE_DB + "?useUnicode=true&characterEncoding=UTF-8&"
                            + "user=" + USER + "&password=" + PASSWORD);
            ArrayList<String> listLanguage = new ArrayList<String>(ClassifierConfiguration.getClassiferConfigurationInstance().getLangagueList());
            listLanguage.add("tid");
            ArrayList<String> query = new ArrayList<String>();
            query.add("INSERT INTO " + TAXONOMY_LANGAUE_TABLE);
            query.add("(");
            query.add(StringUtils.join(listLanguage, ","));
            query.add(")");
            query.add("VALUES");
            query.add("(");
            ArrayList<String> args = new ArrayList<String>();
            for (int i = 0;i< listLanguage.size();i++) args.add("?");
            query.add(StringUtils.join(args, ","));
            query.add(")");
            statement = connect.createStatement();
            statement.execute("TRUNCATE TABLE " + TAXONOMY_LANGAUE_TABLE);
            preparedStatement = connect.prepareStatement(StringUtils.join(query, " "));
            for (CategoryDictionary categoryDictionary : dictionaries) {
                for (int i = 0; i < listLanguage.size() - 1; i++) {
                        EclapLanguage eclapLanguage = new EclapLanguage(listLanguage.get(i));
                        String traduction = categoryDictionary.getTraduction(eclapLanguage.getLanguage());
                        preparedStatement.setString(i + 1, traduction);
                }
                preparedStatement.setInt(listLanguage.size(), categoryDictionary.getTid());
                preparedStatement.addBatch();
                if ((dictionaries.indexOf(categoryDictionary) + 1) % 1000 == 0) {
                    preparedStatement.executeBatch(); // Execute every 1000 items.
                }
            }
            preparedStatement.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
            logger.catching(e);
            System.exit(-1);
        } finally {
            close(resultSet, preparedStatement, connect);
            if (statement != null) try {
                statement.close();
            } catch (SQLException e) {
                logger.catching(e);
                e.printStackTrace();
                System.exit(-1);
            }
        }
        logger.debug("Taxonomy language DB updated");
    }
}

