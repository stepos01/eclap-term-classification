package Translation;

import Configuration.ClassifierConfiguration;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 26/05/13
 * Time: 20:56
 * To change this template use File | Settings | File Templates.
 */
public class MicrosoftTranslateApiMethod {
    private  String authToken;
    private ArrayList<String> languagesCode = null;
    private final static String TRANSLATE_END_POINT = "http://api.microsofttranslator.com/v2/Http.svc/Translate?";
    private final static String LANGUAGES_4_TRANSLATE_END_POINT = "http://api.microsofttranslator.com/V2/Http.svc/GetLanguagesForTranslate";
    private static MicrosoftTranslateAuthentication auth = null;
    private AccessToken token = null;
    private static final String charset = "UTF-8";
    private static MicrosoftTranslateApiMethod instance = null;
    private MicrosoftTranslateApiMethod() {
        authToken = "Bearer" + " " + getAccessToken().getAccess_token();
    }

    public boolean canTranslateThislanguage(String languageCode) {
        //call the microsoft service to obtain the list of available languages for translation
        if (getLanguagesCode().contains(languageCode)) {
            return true;
        } else {
            return false;
        }
    }

    public static MicrosoftTranslateApiMethod getInstance() {
        if (instance == null) instance = new MicrosoftTranslateApiMethod();
        return instance;
    }

    private void getLanguageSupportedCode() {
        URLConnection connection = null;
        try {
            connection = new URL(LANGUAGES_4_TRANSLATE_END_POINT).openConnection();
            connection.setRequestProperty("Authorization", authToken);
            InputStream response = connection.getInputStream();
            HttpURLConnection new_connection = (HttpURLConnection) connection;
            int status = new_connection.getResponseCode();
            if (status == 200) {
                String result = IOUtils.toString(response);
                IOUtils.closeQuietly(response);
                languagesCode = new ArrayList<String>(parseXml(result, "string"));
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String translate(String textToTranslate, String languageSrc, String languageDest) {
        getAccessToken();
        String result = null;
        String params = null;
        if (canTranslateThislanguage(languageSrc) && canTranslateThislanguage(languageDest)) {
            try {
                params = "text=" + URLEncoder.encode(textToTranslate, charset) + "&" + "from=" + languageSrc + "&" + "to=" + languageDest;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            String URLString = TRANSLATE_END_POINT + params;
            URLConnection connection = null;
            try {
                connection = new URL(URLString).openConnection();
                connection.setRequestProperty("Authorization", authToken);
                InputStream response = connection.getInputStream();
                HttpURLConnection new_connection = (HttpURLConnection) connection;
                int status = new_connection.getResponseCode();
                if (status == 200) {
                    result = IOUtils.toString(response);
                    IOUtils.closeQuietly(response);
                    result = parseXml(result, "string").get(0);
                }
            } catch (IOException e) {

                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } else {
            System.err.println("couldn't translate" + textToTranslate + "from" + languageSrc + "to" + languageDest);
        }
        return result;

    }

    private ArrayList<String> parseXml(String xmlToParse, String tagName) {
        DocumentBuilder parser = null;
        Document document = null;
        try {
            parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = parser.parse(new InputSource(new StringReader(xmlToParse)));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SAXException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        NodeList nodelist = document.getElementsByTagName(tagName);
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < nodelist.getLength(); i++) {
            Element element = (Element) nodelist.item(i);
            list.add(element.getTextContent());
        }
        return list;
    }

    public ArrayList<String> getLanguagesCode() {
        if (languagesCode == null) getLanguageSupportedCode();
        return languagesCode;
    }

    public AccessToken getAccessToken()
    {
        if (token!=null&&token.isTokenValid()) return token;
        else {
           getAuth().postRequest();
            token = getAuth().getAccessToken();
           authToken = "Bearer" + " " + token.getAccess_token() ;
            return token;
        }
    }

    public static MicrosoftTranslateAuthentication getAuth() {
        String userId = ClassifierConfiguration.getClassiferConfigurationInstance().getMicrosoftTranslatorUserId();
        String userSecret = ClassifierConfiguration.getClassiferConfigurationInstance().getMicrosoftTranslatorUserSecret();
        if (auth== null) auth = new MicrosoftTranslateAuthentication(userId, userSecret);
        return auth;
    }
}
