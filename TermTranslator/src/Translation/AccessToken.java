package Translation;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 25/05/13
 * Time: 15:50
 * To change this template use File | Settings | File Templates.
 */
public class AccessToken {
    private String access_token;
    private String expires_in;
    private String scope;
    private String token_type;
    private Date limitTime;

    public String getAccess_token() {
        return access_token;
    }
    public void setLimitTime()
    {
        long limit = new Date().getTime()+(Long.valueOf(expires_in)-2)*1000;
        limitTime = new Date(limit);
    }
    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getExpire_in() {
        return expires_in;
    }
    public boolean isTokenValid()
    {
        if (new Date().after(limitTime))return false;
        else return true;
    }
    public void setExpire_in(String expire_in) {
        this.expires_in = expire_in;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }


}
