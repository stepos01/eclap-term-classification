package databaseMapping;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 23/09/13
 * Time: 17:09
 * To change this template use File | Settings | File Templates.
 */
public class CategoryDictionary {
    private ArrayList<DictionaryEntry> dictionaryEntries = null;
    private int tid;
    public CategoryDictionary(int tid) {
        this.tid = tid;
        dictionaryEntries = new ArrayList<DictionaryEntry>();
    }

    public boolean contains(String name) {
        boolean found = false;
        for (DictionaryEntry entry : dictionaryEntries) {
            if (entry.getEntry().equals(name)) found = true;
        }
        return found;
    }

    public void addEntry(String entry, String lang) {
        dictionaryEntries.add(new DictionaryEntry(entry, lang));
    }

    public String getTraduction(String language) {
        String traduction = null;
        boolean found = false;
        ClassifierLanguage classifierLanguage = null;
        try {
            classifierLanguage = new ClassifierLanguage(language);
        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        for (DictionaryEntry entry : dictionaryEntries) {
            if (classifierLanguage.getLanguage().equals(entry.getLanguage().getLanguage()))
                traduction = entry.getEntry();
            found = true;
        }
        if (found == false)
            System.err.println("the " + classifierLanguage.getLanguage() + " traduction for " + getTraduction(ClassifierLanguage.english) + " was not found");
        return traduction;
    }

    public int getTid() {
        return tid;
    }
}
