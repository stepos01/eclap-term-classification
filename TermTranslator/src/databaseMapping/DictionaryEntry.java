package databaseMapping;

import java.text.ParseException;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 23/09/13
 * Time: 17:56
 * To change this template use File | Settings | File Templates.
 */
public class DictionaryEntry {
    private String entry;
    private ClassifierLanguage language;

    public DictionaryEntry(String entry, String lang) {
        try {
            language = new ClassifierLanguage(lang);
        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        this.entry = entry;
    }

    public String getEntry() {
        return entry;
    }

    public ClassifierLanguage getLanguage() {
        return language;
    }
}
