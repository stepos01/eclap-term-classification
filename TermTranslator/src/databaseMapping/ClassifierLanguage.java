package databaseMapping;

import java.text.ParseException;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 14/09/13
 * Time: 22:22
 * To change this template use File | Settings | File Templates.
 */
public class ClassifierLanguage {
    public final static int LANGUAGE_COUNT = 13;
    public final static String italian = "it";
    public final static String italian_alternative = "ita";
    public final static String french = "fr";
    public final static String catalan = "ca";
    public final static String english = "en";
    public final static String hungarian = "hu";
    public final static String polish = "pl";
    public final static String dutch = "nl";
    public final static String spanish = "es";
    public final static String portuguese = "pt";
    public final static String greek = "el";
    public final static String danish = "da";
    public final static String german = "de";
    public final static String slovene = "sl";
    public final static String slovene_alternative = "slo";
    private String language;

    public ClassifierLanguage(String language) throws ParseException {
            this.language = convert(language);
    }

    public static String convert(String language) throws ParseException {
        if (language.equals(italian)) return italian;
        if (language.equals(italian_alternative)) return italian;
        if (language.equals(french)) return french;
        if (language.equals(catalan)) return catalan;
        if (language.equals(english)) return english;
        if (language.equals(hungarian)) return hungarian;
        if (language.equals(polish)) return polish;
        if (language.equals(dutch)) return dutch;
        if (language.equals(spanish)) return spanish;
        if (language.equals(portuguese)) return portuguese;
        if (language.equals(greek)) return greek;
        if (language.equals(danish)) return danish;
        if (language.equals(german)) return german;
        if (language.equals(slovene)) return slovene;
        if (language.equals(slovene_alternative)) return slovene;
        else throw new ParseException("language not found: " + language,0);
    }

    public String getLanguage() {
        return language;
    }

    public String toString() {
        return "defLanguage: " + getLanguage();
    }

    public boolean equals(ClassifierLanguage classifierLanguage) {
        if (this.language.equals(classifierLanguage.language)) return true;
        else return false;
    }


}
