import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.File;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 16/07/13
 * Time: 07:59
 * To change this template use File | Settings | File Templates.
 */
public class ClassifierConfiguration {
    private static ClassifierConfiguration instance = null;
    private PropertiesConfiguration config = null;
    private static final String CONFIG_FOLDER = "BBnet/config/";
    public static final String CONFIG_PATH = "eclap.properties";

    private ClassifierConfiguration() {
        File configFile = new File(CONFIG_FOLDER, CONFIG_PATH);
        boolean bDone = false;
        if (configFile.exists()) {
            System.out.println("Loading " + configFile + " FROM " + configFile.getPath());
            try {
                this.config = new PropertiesConfiguration(configFile);
                bDone = true;
            } catch (ConfigurationException ce) {
                ce.printStackTrace();
            }
        }
        if (!bDone) {
            try {
                throw new ConfigurationException("the following configuration file  'eclap.classifier.properties is' not found");
            } catch (ConfigurationException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public void checkConfiguration() throws ConfigurationException {
        //TODO
        if (getLuceneIndexes().isEmpty())
            throw new ConfigurationException("lucene.indexes not set in eclap.classifier.properties file");
        if (getStopWords().isEmpty())
            throw new ConfigurationException("stopwords not set in eclap.classifier.properties file");
    }

    public static ClassifierConfiguration getClassiferConfigurationInstance() {
        if (instance == null) instance = new ClassifierConfiguration();
        return instance;
    }

    public String getConfigurationParameter(String parameter) {
        if (config == null) {
            throw new AssertionError("the following configuration file  'eclap.classifier.properties is' not found");
        }
        return config.getString(parameter);
    }

    public String getDocuments() {
        return config.getString("documents");
    }

    public String getAxoidDBAxoid() {
        return config.getString("axoid.table.axoid");
    }

    public String getAxoidDBTermrefinement() {
        return config.getString("axoid.table.termrefinement");
    }

    public String getAxoidDBTermrefinement_publisher() {
        return config.getString("axoid.tabble.termRefinement.publisher");
    }

    public String getAxoidDBTermValue() {
        return config.getString("axoid.table.termvalue");
    }

    public String getAxoiDBLanguage() {
        return config.getString("axoid.table.language");
    }

    public List<String> getAxoidDBTermNameIncluded() {
        return config.getList("axoid.table.termname.included");
    }

    public String getTermHierarchyDBTid() {
        return config.getString("term.hierarchy.db.tid");
    }

    public String getTermHierarchyDBParent() {
        return config.getString("term.hierarchy.db.parent");
    }

    public String getTaxonomyLinkTableTid() {
        return config.getString("taxonomy.link.table.tid");
    }

    public String getTaxonomyLinkTableAxoid() {
        return config.getString("taxonomy.link.table.axoid");
    }

    public String getTaxonomyLinkTableTaxExtractionDate() {
        return config.getString("taxonomy.link.table.taxExtractionDate");
    }

    public String getDBUrl() {
        return config.getString("db.url");
    }

    public String getTermDatatid() {
        return config.getString("term.table.tid");
    }

    public String getTermDataName() {
        return config.getString("term.table.name");
    }

    public String getTermDataVid() {
        return config.getString("term.table.vid");
    }

    public String getAxoidDB() {
        return config.getString("axoid.db");
    }

    public String getAxoidTable() {
        return config.getString("axoid.table");
    }

    public String getTermDB() {
        return config.getString("term.db");
    }

    public String getTermTable() {
        return config.getString("term.table");
    }

    public String getTermHierarchyDB() {
        return config.getString("term.hierarchy.db");
    }

    public String getTermHierarchyTable() {
        return config.getString("term.hierarchy.table");
    }

    public String getTaxonomyLinkDB() {
        return config.getString("taxonomy.link.db");
    }

    public String getTaxonomyLinkTable() {
        return config.getString("taxonomy.link.table");
    }

    public String getLuceneIndexes() {
        return config.getString("lucene.indexes");
    }

    public String getStopWords() {
        return config.getString("stopwords");
    }

    public String getTaxonomyLanguageDB() {
        return config.getString("taxonomy.language.db");
    }

    public String getTaxonomyLanguageTable() {
        return config.getString("taxonomy.language.table");
    }

    public String getTaxonomyLinkUpdate() {
        return config.getString("taxonomy.link.update");
    }

    public String getLogFile() {
        return config.getString("log");
    }

    public float getTitleHitScore() {
        return config.getFloat("title.hit.score");
    }

    public float getTagHitScore() {
        return config.getFloat("tag.hit.score");
    }

    public float getScoreThreshold() {
        return config.getFloat("score.threshold");
    }

    public float getAccuracyThreshold() {
        return config.getFloat("accuracy.threshold");
    }

    public List getTaxonomyExclude() {
        return config.getList("taxonomy.exclude");
    }

    public void setScorethreshold(float value) {
        config.setProperty("score.threshold", value);
    }

    public void setAccuracytAccuracy(float value) {
        config.setProperty("accuracy.threshold", value);
    }

    public String getRootObjectInfoDB() {
        return config.getString("axoid.rootobjectinfo.db");
    }

    public String getRootObjectInfoTable() {
        return config.getString("axoid.rootobjectinfo.table");
    }

    public String getRootObjectInfoDefLanguage() {
        return config.getString("axoid.rootobjectinfo.deflanguage");
    }

    public String getRootObjectAxoid() {
        return config.getString("axoid.rootobjectinfo.rootobjectaxoid");
    }

    public String getOptionalFieldDB() {
        return config.getString("axoid.optionalfield.db");
    }

    public String getOptionalFieldTable() {
        return config.getString("axoid.optionalfield.table");
    }

    public String getOptionalFieldAxoid() {
        return config.getString("axoid.optionalfield.axoid");
    }

    public String getOptionalFieldFieldValue() {
        return config.getString("axoid.optionalfield.fieldvalue");
    }

    public List getOptionalFieldFieldNameIncluded() {
        return config.getList("axoid.optionalfield.fielName.included");
    }

    public String getOptionalFieldFieldName() {
        return config.getString("axoid.optionalfield.fieldname");
    }

    public List getLangagueList() {
        return config.getList("eclap.languages");
    }

    public List getPublisherList() {
        return config.getList("test.publisher");
    }

    public String getTestFileOutput() {
        return config.getString("test.output");
    }

    public List getStopWordExclusion() {
        return config.getList("stop.word.exclusion");
    }

}
