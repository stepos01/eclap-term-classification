import Configuration.ClassifierConfiguration;
import DocumentsMapping.Category;
import DocumentsMapping.CategoryTags;
import DocumentsMapping.ClassifierLanguage;
import DocumentsMapping.SubCategory;
import NLPUtils.Tag;
import NLPUtils.Utils;
import NLPUtils.WordNetAccessor;
import Translation.MicrosoftTranslateApiMethod;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;

import java.io.File;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 30/09/13
 * Time: 17:20
 * To change this template use File | Settings | File Templates.
 */
public class TermExpand {
    public static void main(String[] args) {
        File docs = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocuments());
        assert docs.exists() && docs.isDirectory();
        // generateEnglishDocument();
        //for (File documents : docs.listFiles()) {
        File documents = docs.listFiles()[12];
        if (!documents.isHidden() && !documents.getName().equals(ClassifierLanguage.english)) {
            assert documents.isDirectory();
            try {
                generateLangDocument(documents.getName(), documents.getPath());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        //   }
    }

    public static void generateLangDocument(String language, String path) throws Exception {
        File documents = new File(path);
        System.out.println(path);
        for (File document : documents.listFiles()) {
            if (document.canRead() && !document.isHidden()) {
                ObjectBigArrayBigList<String> lines = Utils.loadBigFile(document.getPath());
                ArrayList<Category> categories = new ArrayList<Category>();
                for (int i = 0; i < lines.size64(); i++) {
                    String line = lines.get(i);
                    String name = line.split("\t")[0];
                    String partName = null;
                    if (document.getName().equals(Utils.PARTS)) partName = line.split("\t")[1];
                    ArrayList<Tag> tags;
                    ArrayList<Tag> translatedTag = new ArrayList<Tag>();
                    if (document.getName().equals(Utils.PARTS) && partName != null)
                        tags = WordNetAccessor.getInstance().start(MicrosoftTranslateApiMethod.getInstance().translate(partName, language, ClassifierLanguage.english));
                    else
                        tags = WordNetAccessor.getInstance().start(MicrosoftTranslateApiMethod.getInstance().translate(name, language, ClassifierLanguage.english));
                    for (Tag tag : tags) {
                        String translatedLemma = MicrosoftTranslateApiMethod.getInstance().translate(tag.getLemma(), ClassifierLanguage.english, language);
                        translatedTag.add(new Tag(tag.getCount(), translatedLemma));
                    }
                    ArrayList<String> reordered = new ArrayList<String>();
                    for (Tag t : translatedTag) {
                        for (int c = 0; c < t.getCount(); c++) {
                            reordered.add(t.getLemma());
                        }
                    }
                    translatedTag = Utils.orderTags(reordered);
                    if (document.getName().equals(Utils.PARTS)) {
                        categories.add(new SubCategory(name, partName, new CategoryTags(new ClassifierLanguage(language), translatedTag)));
                    } else {
                        categories.add(new Category(name, new CategoryTags(new ClassifierLanguage(language), translatedTag)));
                    }
                }
                writeCategories(categories, document.getPath());

            }
        }
    }

    public static void writeCategories(ArrayList<Category> categories, String path) {
        ArrayList<String> lines = new ArrayList<String>();
        for (Category category : categories) {
            if (category instanceof SubCategory) lines.add(((SubCategory) category).toString());
            else lines.add(category.toString());
        }
        Utils.createDocument(path);
        Utils.writeLargerTextFile(path, lines);
        System.out.println("Created doc " + path);
    }

    public static void generateEnglishDocument() {
        File file = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocuments() + "/" + ClassifierLanguage.english);
        if (file.exists() && file.isDirectory()) {
            for (File doc : file.listFiles()) {
                if (!doc.isHidden() && doc.canRead()) {
                    ArrayList<Category> categories = new ArrayList<Category>();
                    ObjectBigArrayBigList<String> documents = Utils.loadBigFile(doc.getPath());
                    for (String categoryName : documents) {
                        String name;
                        if (categoryName.split("\t").length == 1) name = categoryName;
                        else name = categoryName.split("\t")[1];
                        try {
                            WordNetAccessor wordNetAccessor = WordNetAccessor.getInstance();
                            ArrayList<Tag> tags = wordNetAccessor.start(name);
                            Category category = null;
                            if (categoryName.equals(name)) {
                                assert doc.getName().equals(Utils.TAGS);
                                category = new Category(name, new CategoryTags(new ClassifierLanguage(ClassifierLanguage.english), tags));
                            } else {
                                assert doc.getName().equals(Utils.PARTS);
                                category = new SubCategory(categoryName.split("\t")[0], name, new CategoryTags(new ClassifierLanguage(ClassifierLanguage.english), tags));
                            }
                            // we get the list used in english
                            categories.add(category);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                    writeCategories(categories, doc.getPath());
                }

            }

        }
    }

}


