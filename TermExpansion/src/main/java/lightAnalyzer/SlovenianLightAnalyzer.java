package lightAnalyzer;

import NLPUtils.Utils;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.StopwordAnalyzerBase;
import org.apache.lucene.util.Version;

import java.io.Reader;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 30/09/13
 * Time: 14:12
 * To change this template use File | Settings | File Templates.
 */
public class SlovenianLightAnalyzer extends StopwordAnalyzerBase {

    private static String[] DEFAULT_STOPWORDS = {"h", "in", "je", "k", "onega", "onem", "onemu", "onim", "ono", "v"};

    public static CharArraySet getDefaultStopSet() {
        CharArraySet set = new CharArraySet(Version.LUCENE_44, Arrays.asList(DEFAULT_STOPWORDS), true);
        return new CharArraySet(Version.LUCENE_CURRENT, LightAnalyserUtils.createStopWordList(set), true);

    }

    public SlovenianLightAnalyzer(Version matchVersion) {
        super(matchVersion, getDefaultStopSet());
    }


    /**
     * Creates a
     * {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
     * which tokenizes all the text in the provided {@link java.io.Reader}.
     *
     * @return A
     *         {@link org.apache.lucene.analysis.Analyzer.TokenStreamComponents}
     *         built from an {@link org.apache.lucene.analysis.standard.StandardTokenizer} filtered with
     *         {@link org.apache.lucene.analysis.standard.StandardFilter}, {@link org.apache.lucene.analysis.core.LowerCaseFilter}, {@link org.apache.lucene.analysis.core.StopFilter}
     *         , {@link org.apache.lucene.analysis.miscellaneous.SetKeywordMarkerFilter} if a stem exclusion set is
     *         provided and {@link org.apache.lucene.analysis.es.SpanishLightStemFilter}.
     */
    @Override
    protected TokenStreamComponents createComponents(String fieldName,
                                                     Reader reader) {
        final Tokenizer source = new WhitespaceTokenizer(matchVersion, reader);
        TokenStream result = new StandardFilter(matchVersion, source);
        result = new LowerCaseFilter(matchVersion, result);
        result = new StopFilter(matchVersion, result, getDefaultStopSet());
        return new TokenStreamComponents(source, result);
    }
}
