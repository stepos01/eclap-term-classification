package lightAnalyzer; /**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/06/13
 * Time: 01:09
 * To change this template use File | Settings | File Templates.
 */

import DocumentsMapping.EclapLanguage;
import configuration.ClassifierConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;


public class LightAnalyserUtils {
    public static Charset ENCODING = StandardCharsets.UTF_8;
    public static final String TAGS = "TAGS";
    public static Logger logger = LogManager.getLogger();


    public static ArrayList<String> tokenizeString(Analyzer analyzer, String tobeTokenize) {
        logger.debug("tokenizing {}",tobeTokenize);
        ArrayList<String> result = new ArrayList<String>();
        try {
            TokenStream stream = analyzer.tokenStream(null, new StringReader(tobeTokenize));
            stream.reset();
            while (stream.incrementToken()) {
                result.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
        } catch (IOException e) {
            // not thrown b/c we're using a string reader...
            throw new RuntimeException(e);
        }
        logger.debug("{} => ({})",tobeTokenize, StringUtils.join(result,'-'));
        return result;
    }


    public static ArrayList<String> createStopWordList(CharArraySet set) {
        ArrayList<String> stopWords = new ArrayList<String>();
        List<String> stopWordExlusion = ClassifierConfiguration.getClassiferConfigurationInstance().getStopWordExclusion();
        Iterator iter = set.iterator();
        stopWords.add("&");
        while (iter.hasNext()) {
            char[] stopWord = (char[]) iter.next();
            String word = new String(stopWord);
            if (!stopWordExlusion.contains(word)) stopWords.add(word);
        }
        return stopWords;
    }

    public static Analyzer getCustomAnalyser4Language(EclapLanguage language) {
        Analyzer analyzer = null;
        if (language.getLanguage().equals(EclapLanguage.french))
            analyzer = new FrenchLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.english))
            analyzer = new EnglishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.italian))
            analyzer = new ItalianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.portuguese))
            analyzer = new PortugueseLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.polish))
            analyzer = new PolishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.dutch))
            analyzer = new DutchLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.spanish))
            analyzer = new SpanishLightAnalyser(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.hungarian))
            analyzer = new HungarianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.german))
            analyzer = new GermanLightanalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.danish))
            analyzer = new DanishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.catalan))
            analyzer = new CatalanLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.slovene))
            analyzer = new SlovenianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(EclapLanguage.greek))
            analyzer = new GreekLightAnalyser(Version.LUCENE_44);

        return analyzer;
    }

    public static String deAccentuate(String str) {

        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }



}










