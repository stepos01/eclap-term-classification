package lightAnalyzer;

import NLPUtils.Utils;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.StopwordAnalyzerBase;
import org.apache.lucene.util.Version;

import java.io.Reader;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 26/09/13
 * Time: 00:42
 * To change this template use File | Settings | File Templates.
 */


public class EnglishLightAnalyzer extends StopwordAnalyzerBase {
    private static CharArraySet getDefaultStopSet() {
        return new CharArraySet(Version.LUCENE_CURRENT, LightAnalyserUtils.createStopWordList(StandardAnalyzer.STOP_WORDS_SET), true);

    }

    /**
     * Builds an analyzer with the default stop words: {@link #getDefaultStopSet}.
     */
    public EnglishLightAnalyzer(Version matchVersion) {
        super(matchVersion, getDefaultStopSet());
    }

    @Override
    protected TokenStreamComponents createComponents(String fieldName,
                                                     Reader reader) {
        final Tokenizer source = new WhitespaceTokenizer(matchVersion, reader);
        TokenStream result = new StandardFilter(matchVersion, source);
        // prior to this we get the classic behavior, standardfilter does it for us.
        result = new LowerCaseFilter(matchVersion, result);
        result = new StopFilter(matchVersion, result, getDefaultStopSet());
        return new TokenStreamComponents(source, result);
    }
}

