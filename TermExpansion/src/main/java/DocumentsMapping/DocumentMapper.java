package DocumentsMapping;

import NLPUtils.Utils;
import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 05/10/13
 * Time: 15:09
 * To change this template use File | Settings | File Templates.
 */
public class DocumentMapper {

    private static DocumentMapper ourInstance = null;
    private static Logger logger = LogManager.getLogger();
    public static DocumentMapper getInstance() {
        if (ourInstance == null) ourInstance = new DocumentMapper();
        return ourInstance;
    }

    private DocumentMapper() {
    }

    /*
    private void loadEnglishDocuments() {
        File file = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocuments() + "/" +EclapLanguage.english.toUpperCase());
        if (file.exists() && file.isDirectory()) {
            for (File doc : file.listFiles()) {
                if (doc.canRead() && !doc.isHidden()) {
                    ObjectArrayList<String> documents = Utils.loadBigFile(doc.getPath());
                    for (String line : documents) {
                        Category category = Utils.parseEnglishDocument(line, file.getName(), doc.getName());
                        if (category instanceof SubCategory) subcategories.add(category);
                        else categories.add(category);
                    }
                }
            }
        }
    }
    */

    public ObjectArrayList<String> loadDocument(String lang)
    {
        ObjectArrayList<String> resultList = new ObjectArrayList<String>();
        try {
            resultList = loadDocuments4Expansion(lang);
        } catch (IOException e) {
            logger.catching(Level.ERROR,e);
        }
        return resultList.isEmpty()?null:resultList;
    }
    private ObjectArrayList<String> loadDocuments4Expansion(String lang) throws IOException
    {

        File file = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocumentSRC() +lang);
        //File file = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocuments() + "/" +EclapLanguage.english.toUpperCase());
        if (file.exists())
        {
            ObjectArrayList<String> tags = Utils.loadBigFile(file.getPath());
            return tags;
        }else {
            throw new  IOException("error using file: "+file.getName());
        }
    }
}
