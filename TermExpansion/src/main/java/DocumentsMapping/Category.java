package DocumentsMapping;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 02/07/13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */

public class Category {
    private String name;
    private EclapLanguage language;
    private CategoryTags categoryTags;

    public Category(String name, CategoryTags categoryTags) {
        this.name = name;
        this.categoryTags = categoryTags;

    }

    public EclapLanguage getLanguage() {
        return language;
    }

    public void setLanguage(EclapLanguage language) {
        this.language = language;
    }

    public Category(String name)
    {
        this.name = name;
    }
    public void setCategoryTags(CategoryTags categoryTags) {
        this.categoryTags = categoryTags;
    }


    public String getName() {
        return name;
    }

    public CategoryTags getCategoryTags() {
        return categoryTags;
    }
    public boolean isCategoryTagEmpty()
    {
        return categoryTags==null||categoryTags.isEmpty();
    }
    public String toString() {
        if (isCategoryTagEmpty())
            return getName();
        else
            return getName()+"\t"+categoryTags.toString();
    }


}
