package filter;

import DocumentsMapping.Category;
import DocumentsMapping.CategoryTags;
import DocumentsMapping.EclapLanguage;
import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.StringReader;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 22/02/2014
 * Time: 13:02
 * To change this template use File | Settings | File Templates.
 */
public abstract class ResultFilter {
    private static Logger logger = LogManager.getLogger();
    private static ObjectArrayList<FilterScore> filterScores = new ObjectArrayList<FilterScore>();
    public Category filter(Category category)
    {
        filterScores.clear();
        ExecutorService pool = Executors.newFixedThreadPool(5);
        CountDownLatch countDownLatch = new CountDownLatch(category.getCategoryTags().getTags().size());
        float rootScore = computeScore(category.getName(), null);
        for (String key : category.getCategoryTags().getTags().keySet())
        {
            pool.execute(generateCallable(category.getName(),key,countDownLatch));
        }
        pool.shutdown();
        try{
            countDownLatch.await();
        } catch (InterruptedException e) {
            logger.catching(Level.ERROR,e);
            return null;
        }
        float confidence = ClassifierConfiguration.getClassiferConfigurationInstance().getWikiPediaAprioriConfidence();
        CategoryTags categoryTags = new CategoryTags(new EclapLanguage(EclapLanguage.english));
        for (FilterScore filterScore : filterScores)
        {
            float result = (float)filterScore.getScore()/rootScore;
            if (result >= confidence)
            {
                logger.debug("root: {}",category.getName());
                logger.debug("{} passed with a score of {} - final Tag: {}",filterScore.getOriginalTag(),result*100,filterScore.getFinalTag());
                int count = category.getCategoryTags().getTags().getInt(filterScore.getOriginalTag());
                for (int i = 0;i < count; i++)
                {
                    categoryTags.addTag(filterScore.getFinalTag());
                }
            }
        }
        Category resultCategory = new Category(category.getName());
        resultCategory.setCategoryTags(categoryTags);
        return resultCategory;
    }

    protected abstract int computeScore(String root,ObjectArrayList<String> target);
    protected abstract ObjectArrayList<String> parse(String target);
    private Runnable generateCallable(final String root,final String originTags,final CountDownLatch countDownLatch)
    {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    ObjectArrayList<String> finalTags = parse(originTags);
                    if (finalTags == null || finalTags.isEmpty()){
                        logger.warn("Obtained null when parsing '{}' having category name: '{}'",originTags,root);
                        return;
                    }
                    int score = computeScore(root, finalTags);
                    add2Score(originTags, StringUtils.join(finalTags," "),score);
                }finally {
                    countDownLatch.countDown();
                }
            }
        };
    }
    private  void add2Score(String originTag,String finalTag,int score)
    {
        synchronized (this)
        {
            FilterScore filterScore = new FilterScore();
            filterScore.setFinalTag(finalTag).setOriginalTag(originTag).setScore(score);
            filterScores.add(filterScore);
        }
    }
    private class FilterScore
    {
        String originalTag;
        String finalTag;
        int score;

        private FilterScore() {
        }

        private String getFinalTag() {
            return finalTag;
        }

        private FilterScore setFinalTag(String finalTag) {
            this.finalTag = finalTag;
            return this;
        }

        private String getOriginalTag() {
            return originalTag;
        }

        private FilterScore setOriginalTag(String originalTag) {
            this.originalTag = originalTag;
            return this;
        }

        private int getScore() {
            return score;
        }

        private FilterScore setScore(int score) {
            this.score = score;
            return this;
        }
    }
    protected ObjectArrayList<String> tokenizeString(Analyzer analyzer, String string) {
        ObjectArrayList<String> result = new ObjectArrayList<String>();
        try {
            TokenStream stream = analyzer.tokenStream(null, new StringReader(string));
            stream.reset();
            while (stream.incrementToken()) {
                result.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
        } catch (IOException e) {
            // not thrown b/c we're using a string reader...
            throw new RuntimeException(e);
        }
        return result;
    }



}


