package filter;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.util.Version;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 22/02/2014
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
public class WikipediaFilter extends ResultFilter{
    private static Logger logger = LogManager.getLogger();
    private WikipediaSearch wiki = WikipediaSearch.getInstance();
    private static WikipediaFilter instance = null;
    @Override
    public int computeScore(String root,ObjectArrayList<String> targets) {
        String query;
        if (targets == null)
            query = root;
        else
            query = root +" AND "+ StringUtils.join(targets," AND ");
        int response = wiki.search(query);
        logger.debug("query {} obtained score {}",query,response);
        return response;
    }

    @Override
    protected ObjectArrayList<String> parse(String target) {
        return tokenizeString(new EnglishAnalyzer4Wikipedia(Version.LUCENE_46),target);
    }



    public static WikipediaFilter getInstance() {
        if (instance== null)instance = new WikipediaFilter();
        return instance;
    }

    private WikipediaFilter(){}
}
