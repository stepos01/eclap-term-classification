package filter;


import configuration.ClassifierConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 */
public class WikipediaSearch  {
    private static Logger logger = LogManager.getLogger();
    private static ClassifierConfiguration classifierConfiguration = ClassifierConfiguration.getClassiferConfigurationInstance();
    private static WikipediaSearch instance = null;
    private static final String TEXT = "text";   //?action=query&list=search&srprop=timestamp&srwhat=text
    private static final String TIMESTAMP = "timestamp";
    private static final String SEARCH = "search";
    private static final String QUERY = "query";
    private static final String USER_AGENT = "stephane kamga, www.eclap.eu";
    private WikipediaSearch(){}
    public static WikipediaSearch getInstance() {
        if (instance == null) instance = new WikipediaSearch();
        return instance;
    }

    public String getMessage() {
        return null;
    }

    public boolean getStatus() {
        return true;
    }


    public int search(String target)
    {
        String json = sendGet(target);
        int count = parseJson(json);
        logger.debug("Obtained {} results searching with word {}",count,target);
        return count;
    }
    private int parseJson(String value)
    {
        JSONObject level1 = (JSONObject) JSONValue.parse(value);
        JSONObject level2 = (JSONObject) level1.get("query");
        JSONObject level3 = (JSONObject) level2.get("searchinfo");
        Number totalhits = (Number) level3.get("totalhits");
        return totalhits.intValue();
    }
    private String sendGet(String world)  {
        StringBuffer response = null;
        HttpURLConnection connection = null;
        try {
            URL serverAddress = new URL(classifierConfiguration.getWikipediaEndPoint() +
                    "?action="+ URLEncoder.encode(QUERY, "UTF-8")+"&list="+SEARCH+"&srprop="+TIMESTAMP+"&srwhat="
                    +URLEncoder.encode(TEXT, "UTF-8")+"&srsearch="+URLEncoder.encode(world, "UTF-8")+"&format=json");
            connection = (HttpURLConnection)serverAddress.openConnection();
            connection.setRequestProperty("User-Agent", USER_AGENT);
            connection.connect();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

        }catch (MalformedURLException e)
        {
            logger.catching(e);
            System.exit(-1);
        }catch (IOException e)
        {
            logger.catching(e);
            return null;
        }finally {
            connection.disconnect();
        }
        return response.toString();
    }
}
