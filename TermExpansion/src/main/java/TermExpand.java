import DocumentsMapping.Category;
import DocumentsMapping.CategoryTags;
import DocumentsMapping.DocumentMapper;
import DocumentsMapping.EclapLanguage;
import NLPUtils.Utils;
import configuration.ClassifierConfiguration;
import filter.WikipediaFilter;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import queryExpansion.QueryExpansionProvider;
import queryExpansion.Stand4;
import queryExpansion.ThesaurusAltervista;
import queryExpansion.WordNetExpansion;

import java.io.File;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 30/09/13
 * Time: 17:20
 * To change this template use File | Settings | File Templates.
 */
public class TermExpand {
    private static Logger logger = LogManager.getLogger();
    private static ExecutorService pool = Executors.newFixedThreadPool(5);
    private static ArrayList<QueryExpansionProvider> providers = new ArrayList<QueryExpansionProvider>();
    public static void main(String[] args) {
        Configurator.initialize(null, null, ClassifierConfiguration.CONFIG_FOLDER + "log4j2.xml");
        File docs = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocumentSRC());
        String language = "en";
        if (!docs.isDirectory()) {
            logger.error(docs.getAbsolutePath() +"is not a directory");
            return;
        }
        if (!Arrays.asList(docs.list()).contains(language))
        {
            logger.error("{} file is not found in {} directory",language,docs.getPath());
            return;
        }
        ObjectArrayList<String> listTerm = DocumentMapper.getInstance().loadDocument(language);
        logger.trace("start expanding in {}",language);
        Set<Callable<Category>> callables = new HashSet<Callable<Category>>();
        CountDownLatch countDownLatch = new CountDownLatch(listTerm.size());
        addProviders(WordNetExpansion.getInstance());
        addProviders(Stand4.getInstance());
        addProviders(ThesaurusAltervista.getInstance());
        for (String s : listTerm)
        {
            callables.add(generateCallable(s,countDownLatch));
        }
        ObjectArrayList<Category> categoryList = new ObjectArrayList<Category>();
        try {
            List<Future<Category>> futures = pool.invokeAll(callables);
            pool.shutdown();
            countDownLatch.await();
            for (Future<Category> future : futures)
            {
                    Category category = future.get();
                    if (category != null)
                        categoryList.add(category);
                    else
                        logger.warn("i obtain a strange result list from expanding... possibly term: {}",listTerm.get(futures.indexOf(future)));
            }
        }catch (InterruptedException e)
        {
            logger.catching(Level.ERROR,e);
        } catch (ExecutionException e) {
            logger.catching(Level.ERROR,e);
        }finally {
        }
        WikipediaFilter wikipediaFilter = WikipediaFilter.getInstance();
        ObjectArrayList<Category> filteredCategory = new ObjectArrayList<Category>();
        for (Category category : categoryList)
        {

            if (!category.isCategoryTagEmpty()){
                Category filter = wikipediaFilter.filter(category);
                filteredCategory.add(filter);
            }else
                filteredCategory.add(category);
        }
        File dest = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getConfigPath(),ClassifierConfiguration.getClassiferConfigurationInstance().getDocumentsDEST());
        dest.mkdirs();
        logger.debug("writing to {}",dest.getPath());
        writeCategories(categoryList, new File(dest.getPath(),language).getPath());

    }
    public static void addProviders(QueryExpansionProvider provider )
    {
        providers.add(provider);
        logger.trace("Add provider {}",provider.getClass().getName());
    }


    public static void writeCategories(ObjectArrayList<Category> categories, String path) {
        ObjectArrayList<String> lines = new ObjectArrayList<String>();
        for (Category category : categories) {
            lines.add(category.toString());
        }
        Utils.writeLargerTextFile(path, lines);
        logger.trace("Created doc " + path);
    }
    private static Callable<Category> generateCallable(final String name,final CountDownLatch countDownLatch )
    {
        return new Callable<Category>() {
            @Override
            public Category call() throws Exception {
                Category category = new Category(name);
                CategoryTags categoryTags = new CategoryTags(new EclapLanguage(EclapLanguage.english));
                ExecutorService innerPool = Executors.newFixedThreadPool(providers.size());
                try {
                    ArrayList<Callable<ObjectArrayList<String>>> threads = new ArrayList<Callable<ObjectArrayList<String>>>();
                    CountDownLatch innnercountDownLatch = new CountDownLatch(providers.size());
                    for (int i = 0; i< providers.size();i++)
                    {
                        threads.add(generateInnerCallback(i,name,innnercountDownLatch));
                    }
                    List<Future<ObjectArrayList<String>>> futures;
                    futures = innerPool.invokeAll(threads);
                    innerPool.shutdown();
                    try {

                        innnercountDownLatch.await();
                    } catch (InterruptedException e) {
                        logger.catching(Level.ERROR, e);
                    }
                    for (Future<ObjectArrayList<String>> future : futures)
                    {
                        ObjectArrayList<String> list = future.get();
                        if (list!=null&&!list.isEmpty())
                        {
                            for (String syn : list)
                            {
                                if (!syn.isEmpty())
                                    categoryTags.addTag(syn);
                            }
                        }

//                        else {
//                            logger.warn("Java future return Null when expanding {} using provider {}",name,providers.get(futures.indexOf(future)).getClass().getName());
//                        }
                    }
                    if (!categoryTags.getTags().isEmpty())
                        category.setCategoryTags(categoryTags);



                return category;
                }finally
                {
                    countDownLatch.countDown();
                }
            }
        };
    }
    private static Callable<ObjectArrayList<String>> generateInnerCallback(final int i,final String name,final CountDownLatch innnercountDownLatch)
    {
        return new Callable<ObjectArrayList<String>>() {
            @Override
            public ObjectArrayList<String> call() throws Exception {
                try {
                    ObjectArrayList<String> list = providers.get(i).expandQuery(name, new EclapLanguage(EclapLanguage.english));
                    return list;
                }finally {
                    innnercountDownLatch.countDown();
                }

            }
        };
    }



}


