package queryExpansion;

import DocumentsMapping.EclapLanguage;
import configuration.ClassifierConfiguration;
import edu.cmu.lti.jawjaw.JAWJAW;
import edu.cmu.lti.jawjaw.pobj.POS;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 20/02/2014
 * Time: 04:54
 * To change this template use File | Settings | File Templates.
 */
public class WordNetExpansion extends QueryExpansionProvider {
    private static Logger logger = LogManager.getLogger();
    private static WordNetExpansion instance = null;

    public static WordNetExpansion getInstance() {
        if (instance == null) instance = new WordNetExpansion();
        return instance;
    }

    private WordNetExpansion() {

    }

    @Override
    public ObjectArrayList<String> getSimilar(String target,EclapLanguage eclapLanguage) {
   //     Set<String> hypernyms = JAWJAW.findHypernyms(target, POS.n);
   //     Set<String> hyponyms = JAWJAW.findHyponyms(target, POS.n);
        Set<String> synonyms = JAWJAW.findSynonyms(target, POS.n);
        Set<String> similarTo = JAWJAW.findSimilarTo(target, POS.n);
        ObjectArrayList<String> list = new ObjectArrayList<String>();
 //       if (!hypernyms.isEmpty())list.addAll(hypernyms);
 //       if (!hyponyms.isEmpty())list.addAll(hyponyms);
        if (!synonyms.isEmpty())list.addAll(synonyms);
        if (!similarTo.isEmpty())list.addAll(similarTo);
        return list.isEmpty()?null:cleanWordnetResult(list);
    }

    @Override
    public ObjectArrayList<EclapLanguage> getSupportedLanguages() {
          ObjectArrayList<EclapLanguage> languages = new ObjectArrayList<EclapLanguage>();
        languages.add(new EclapLanguage(EclapLanguage.english));
        return languages;
    }

    @Override
    public boolean getStatus() {
        return true;
    }
    public String getMessage() {
        return null;
    }
    private ObjectArrayList<String> cleanWordnetResult(ObjectArrayList<String> target)
    {
        ObjectArrayList<String> result = new ObjectArrayList<String>();
        for (String s : target)
        {
            result.add(s.replace("_"," ").trim().toLowerCase());
        }
        return result;
    }
}
