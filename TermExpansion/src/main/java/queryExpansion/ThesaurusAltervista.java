package queryExpansion;

import DocumentsMapping.EclapLanguage;
import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 20/02/2014
 * Time: 23:40
 * To change this template use File | Settings | File Templates.
 */
public class ThesaurusAltervista extends QueryExpansionProvider{

    private enum ThesaurusLanguages{
        it_IT,
        fr_FR,
        de_DE,
        en_US,
        el_GR,
        es_ES,
        pt_PT
    };
    private static Logger logger = LogManager.getLogger();
    private boolean status = true;
    private String statusMessage;
    private static ThesaurusAltervista instance = null;
    private ClassifierConfiguration classifierConfiguration = ClassifierConfiguration.getClassiferConfigurationInstance();
    private ThesaurusAltervista() {
    }

    public boolean getStatus() {
        boolean b;
        synchronized (this)
        {
            b = this.status;
        }
        return b;
    }

    public void setStatus(boolean status,String message) {
        synchronized (this)
        {
            this.status = status;
            this.statusMessage = message;
        }
    }
    public String getMessage() {
        return statusMessage;
    }
    public static ThesaurusAltervista getInstance() {
        if (instance == null) instance = new ThesaurusAltervista();
        return instance;
    }
    private ObjectArrayList<String> parseJson(String target)
    {
        logger.debug(target);
        ObjectArrayList<String> objectArrayList = new ObjectArrayList<String>();
        JSONObject obj = (JSONObject) JSONValue.parse(target);
        JSONArray array = (JSONArray)obj.get("response");
        for (int i=0; i < array.size(); i++) {
            JSONObject list = (JSONObject) ((JSONObject)array.get(i)).get("list");
            String unparsed = list.get("synonyms").toString();
            for (String s : unparsed.split("\\|"))
            {
                if (!s.contains("antonym"))objectArrayList.add(s);
            }
        }
        return objectArrayList;
    }

    private String sendRequest(String word,EclapLanguage eclapLanguage) {
        String key = classifierConfiguration.getThesaurusAltervistaApiKey();
        String endPoint = classifierConfiguration.getThesaurusAltervistaApiEndPoint();
        String language = convert2ThesaurusLanguage(eclapLanguage);
        String outputType = "json";
        String responseMessage = null;
        HttpURLConnection connection = null;
        try {
            URL serverAddress = new URL(endPoint +
                    "?word="+ URLEncoder.encode(word, "UTF-8")+"&language="+language+"&key="+key+"&output="+outputType);
             connection = (HttpURLConnection)serverAddress.openConnection();
            connection.connect();
            int rc = connection.getResponseCode();
            if (rc == 200) {
                String line;
                logger.debug("retrieved syn for {}",word);
                BufferedReader br = new BufferedReader(new java.io.InputStreamReader(connection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null)
                    sb.append(line + '\n');
                responseMessage = sb.toString();
            } else if (rc == 403)
            {
                String message = "\"Usage Exceeded: Usage limits have been exceeded\"";
                setStatus(false,message);
                logger.warn(message);
                return null;
            }else if(rc == 404)
            {
                String message = "No match found for given word: "+word;
                setStatus(false,message);
                logger.warn(message);
                return null;
            }

        } catch (java.net.MalformedURLException e) {
            logger.catching(e);
            System.exit(-1);        }
        catch (java.net.ProtocolException e) {
            logger.catching(e);
            System.exit(-1);        }
        catch (java.io.IOException e) {
            logger.catching(e);
            return null;
        }finally {
            connection.disconnect();
        }
        return responseMessage;
    }


    @Override
    public ObjectArrayList<String> getSimilar(String target, EclapLanguage language) {
        String result = sendRequest(target,language);
        if (result != null)
        {
            ObjectArrayList<String> list = parseJson(result);
            if (list.isEmpty())return null;
            else return list;
        }else {
            logger.warn(statusMessage);
            return null;
        }
    }
    private String convert2ThesaurusLanguage(EclapLanguage language)
    {
        if (language.getLanguage().equals(EclapLanguage.italian)) return ThesaurusLanguages.it_IT.name();
        else if (language.getLanguage().equals(EclapLanguage.english)) return ThesaurusLanguages.en_US.name();
        else if (language.getLanguage().equals(EclapLanguage.german)) return ThesaurusLanguages.de_DE.name();
        else if (language.getLanguage().equals(EclapLanguage.greek)) return ThesaurusLanguages.el_GR.name();
        else if (language.getLanguage().equals(EclapLanguage.spanish)) return ThesaurusLanguages.es_ES.name();
        else if (language.getLanguage().equals(EclapLanguage.portuguese)) return ThesaurusLanguages.pt_PT.name();
        else return ThesaurusLanguages.fr_FR.name();
    }
    @Override
    public ObjectArrayList<EclapLanguage> getSupportedLanguages() {
        ObjectArrayList<EclapLanguage> languages = new ObjectArrayList<EclapLanguage>();
        languages.add(new EclapLanguage("it"));
        languages.add(new EclapLanguage("fr"));
        languages.add(new EclapLanguage("de"));
        languages.add(new EclapLanguage("en"));
        languages.add(new EclapLanguage("el"));
        languages.add(new EclapLanguage("es"));
        languages.add(new EclapLanguage("pt"));
        return languages;
    }
}


