package queryExpansion;

import DocumentsMapping.EclapLanguage;
import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/02/2014
 * Time: 10:58
 * To change this template use File | Settings | File Templates.
 */
public class Stand4 extends QueryExpansionProvider {
    private static Logger logger = LogManager.getLogger();
    private ClassifierConfiguration classifierConfiguration = ClassifierConfiguration.getClassiferConfigurationInstance();
    private static Stand4 instance = null;
    private boolean status = true;
    private String message;

    private Stand4() {
    }

    public static Stand4 getInstance() {
        if (instance == null)instance = new Stand4();
        return instance;
    }

    public String getMessage() {
        return message;
    }

    public boolean getStatus() {
        boolean b;
        synchronized (this)
        {
            b = status;
        }
        return b;
    }

    public void setStatus(boolean status,String message) {
        synchronized (this)
        {
            this.status = status;
            this.message = message;
        }
    }
    @Override
    public ObjectArrayList<EclapLanguage> getSupportedLanguages() {
        ObjectArrayList<EclapLanguage> languages = new ObjectArrayList<EclapLanguage>();
        languages.add(new EclapLanguage(EclapLanguage.english));
        return languages;    }

    @Override
    public ObjectArrayList<String> getSimilar(String target, EclapLanguage language) {
        String result = sendGet(target);
        if (result!= null)
        {
            ObjectArrayList<String> list = parseXml(result);
            if (list.isEmpty()) return null;
            else return list;
        }else {
            logger.warn(message);
            return null;
        }
    }
    private String sendGet(String target)
    {
        StringBuffer response = null;
        String endPoint = classifierConfiguration.getStand4ApiEndPoint();
        String uid = classifierConfiguration.getStand4ApiUserId();
        String tokenId = classifierConfiguration.getStand4ApiToken();
        HttpURLConnection connection = null;
        http://www.stands4.com/services/v2/syno.php
        try {
            URL serverAddress = new URL(endPoint +
                    "?uid="+uid+"&tokenid="+tokenId+"&word="+URLEncoder.encode(target, "UTF-8"));
            connection = (HttpURLConnection)serverAddress.openConnection();
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode == 200)
            {
                //good to go
                logger.debug("retrieved syn for {}",target);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

            }else {
                    String message = "unknow error";
                    logger.warn(message);
                    return null;
            }
        } catch (MalformedURLException e) {
            logger.catching(e);
            System.exit(-1);
        } catch (UnsupportedEncodingException e) {
            logger.catching(e);
            System.exit(-1);
        } catch (IOException e) {
            logger.catching(e);
            return null;
        }finally {
            connection.disconnect();
        }
        return response.toString();
    }
    public ObjectArrayList<String> parseXml(String target)
    {
        ObjectArrayList<String> list = new ObjectArrayList<String>();
        SAXBuilder builder = new SAXBuilder();
        try {
            InputSource inputSource = new InputSource(new StringReader(target));
            Document document = builder.build(inputSource);
            Element rootNode = document.getRootElement();
            List<Element> results = rootNode.getChildren("result");
            if (results== null)return list;
            for (Element result : results)
            {
                if (result == null) break;
                String term = result.getChildText("term");
                if (term!=null)
                {
                    for (String t : term.split(","))
                        list.add(t);
                }
                String pos = result.getChildText("partofspeech");
                if (pos != "noun") break;
                String syns = result.getChildText("synonyms");
                if (syns==null||syns.isEmpty())break;
                for (String syn : syns.split(","))
                {
                    list.add(syn.trim());
                }
            }

        } catch (JDOMException e) {
           logger.catching(Level.ERROR,e);
            System.exit(-1);
        } catch (IOException e) {
            logger.catching(Level.ERROR,e);
            return null;
        }
        return list;
    }
}
