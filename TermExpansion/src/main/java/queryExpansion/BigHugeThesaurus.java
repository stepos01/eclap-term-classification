package queryExpansion;

import DocumentsMapping.EclapLanguage;
import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 20/02/2014
 * Time: 16:00
 * To change this template use File | Settings | File Templates.
 */

public class BigHugeThesaurus extends QueryExpansionProvider {
//    private final String USER_AGENT = "Mozilla/5.0";
    private static Logger logger = LogManager.getLogger();
    private ClassifierConfiguration classifierConfiguration = ClassifierConfiguration.getClassiferConfigurationInstance();
    private static BigHugeThesaurus instance = null;
    private Boolean status = true;
    private String statusMessage = null;
    @Override
    public ObjectArrayList<String> getSimilar(String target, EclapLanguage language) {
            String result = sendGet(target);
            if (result!=null)
            {
                ObjectArrayList<String> list = parseJson(result);
                if (!list.isEmpty())return list;
                else return null;
            }else{
                logger.warn(statusMessage);
                return null;
            }
    }

    @Override
    public ObjectArrayList<EclapLanguage> getSupportedLanguages() {
        ObjectArrayList<EclapLanguage> languages = new ObjectArrayList<EclapLanguage>();
        languages.add(new EclapLanguage(EclapLanguage.english));
        return languages;
    }

    private ObjectArrayList<String> parseJson(String target)
    {
        logger.debug(target);
        ObjectArrayList<String> list = new ObjectArrayList<String>();
        JSONObject obj = (JSONObject) JSONValue.parse(target);
        if (!obj.isEmpty()&&obj.containsKey("noun")) {
            JSONObject noun = ((JSONObject) obj.get("noun"));
            if (!noun.isEmpty()&&noun.containsKey("syn"))
            {
                JSONArray  syn = (JSONArray)noun.get("syn");
                for (int i = 0; i<syn.size();i++)
                {
                    list.add(syn.get(i).toString());
                }
            }

        }
   /*     if (!obj.isEmpty()&&obj.containsKey("verb"))
        {
            JSONObject verb = (JSONObject)obj.get("verb");
            if (!verb.isEmpty()&&verb.containsKey("syn"))
            {
                JSONArray syn = (JSONArray)verb.get("syn");
                for (int i = 0; i<syn.size();i++)
                {
                    list.add(syn.get(i).toString());
                }
            }

        }*/
        return list;
      /*  ObjectArrayList<String> list = new ObjectArrayList<String>();
        try {
            BigHugeThesaurusResponse response = null;
            ObjectMapper mapper = new ObjectMapper();
            response = mapper.readValue(target,BigHugeThesaurusResponse.class);
            if (response.getNoun().getSim() != null) list.addAll(response.getNoun().getSim());
            if (response.getNoun().getSyn() != null) list.addAll(response.getNoun().getSyn());
        }catch (IOException e)
        {
            logger.catching(e);
            System.exit(-1);
        }
        return list;*/
    }
    private BigHugeThesaurus() {
    }

    public static BigHugeThesaurus getInstance() {
        if (instance == null) instance = new BigHugeThesaurus();
        return instance;
    }

    public boolean getStatus() {
        boolean b;
        synchronized (this)
        {
            b = status;
        }
        return b;
    }
    private void setStatus(boolean b,String message)
    {
        synchronized (this)
        {
            status = b;
            statusMessage = message;
        }
    }

    public String getMessage() {
        return statusMessage;
    }

    private String sendGet(String world)  {
        URL obj;
        HttpURLConnection con = null;
        StringBuffer response = null;
        try {
            String url = classifierConfiguration.getBigHugeThesaurusApiUrl()+classifierConfiguration.getBigHugeThesaurusApiVersion()+
                    "/"+classifierConfiguration.getBigHugeThesaurusApikey()+"/"+
                    world+"/"+"json";

            obj = new URL(url);
            con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");
            //add request header
         //   con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();

            logger.debug("Response Code : " + responseCode);
            if (responseCode == 200)
            {
                //it's ok
            }else if(responseCode == 330){
                String alternate = con.getResponseMessage();
                logger.debug("String : \"{}\" not found! searching with alternate \" {}\"",world,alternate);
                return sendGet(alternate);
            }

            else if(responseCode == 404)
            {
                logger.warn("no data could be found using '{}'",world);
                return null;
            }else if(responseCode == 500 && con.getResponseMessage()=="Usage Exceeded")
            {
                String message = "Usage Exceeded: Usage limits have been exceeded";
                logger.warn(message);
                setStatus(false,message);
                return null;
            }else if(responseCode == 500 && con.getResponseMessage()=="Inactive key")
            {
                String message = "Incorrect api key";
                logger.warn(message);
                setStatus(false,message);
                return null;
            }
            else {
                String message = "unknow error";
                logger.warn(message);
                return null;
            }
            if (status)
            {
                logger.debug("retrieved syn for {}",world);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            }

        }catch (MalformedURLException e)
        {
            logger.catching(e);
            System.exit(-1);
        }catch (IOException e)
        {
            logger.catching(e);
            return null;
        }finally {
            con.disconnect();
        }
        return response.toString();
    }
}
