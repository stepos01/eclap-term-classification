package queryExpansion;

import DocumentsMapping.EclapLanguage;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import lightAnalyzer.LightAnalyserUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 20/02/2014
 * Time: 13:56
 * To change this template use File | Settings | File Templates.
 */
public abstract class QueryExpansionProvider {
    private static Logger logger = LogManager.getLogger();
    public ObjectArrayList<String> expandQuery(String target,EclapLanguage language)
    {
        if (!getSupportedLanguages().contains(language))
        {
            logger.warn("language {} not supported by {}",language.getLanguage(),this.getClass().toString());
            return null;
        }
        else if (!getStatus())
        {
            logger.warn(getMessage());
            return null;
        }
        //we first parse the target as it could be a multiple words query
        ObjectArrayList<String> list = new ObjectArrayList<String>();
        //we do  retokenize the target as it may not have been already tokenized
        ArrayList<String> split = LightAnalyserUtils.tokenizeString(LightAnalyserUtils.getCustomAnalyser4Language(language), target);
        logger.debug("{} => '{}' starting expansion",target, StringUtils.join(split,","));
        for (String s : split)
        {
            ObjectArrayList<String> similars = getSimilar(s, language);
            if (similars!= null)list.addAll(similars);
        }
        return list.isEmpty() ?null : list;

    }
    protected abstract ObjectArrayList<EclapLanguage> getSupportedLanguages();
    protected abstract ObjectArrayList<String> getSimilar(String target,EclapLanguage language);
    public abstract boolean getStatus();
    public abstract String getMessage();
}
