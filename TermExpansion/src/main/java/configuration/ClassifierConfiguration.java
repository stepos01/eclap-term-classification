package configuration;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 16/07/13
 * Time: 07:59
 * To change this template use File | Settings | File Templates.
 */
public class ClassifierConfiguration {
    private static ClassifierConfiguration instance = null;
    private PropertiesConfiguration config = null;
    public static final String CONFIG_FOLDER = "TermExpansion/config";
    public static final String CONFIG_PATH = "/eclap.expansion.properties";
    private static Logger logger = LogManager.getLogger();
    private ClassifierConfiguration() {
        //File configFile = new File(CONFIG_FOLDER,CONFIG_PATH);
        File configFile = new File(getConfigPath(),"config/eclap.expansion.properties");
        logger.debug("loading config file: {}",configFile.getPath());

        boolean bDone = false;
        if (configFile.exists()) {
            logger.debug("Loading " + configFile + " FROM " + configFile.getAbsolutePath());
            try {
                this.config = new PropertiesConfiguration(configFile);
                bDone = true;
            } catch (ConfigurationException ce) {
                ce.printStackTrace();
            }
        }
        if (!bDone) {
            try {
                throw new ConfigurationException("the following configuration file  'eclap.expansion.properties is' not found");
            } catch (ConfigurationException e) {
                logger.catching(e);
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }
    public File loadConfigFile(String path)
    {
        return new File(getClass().getResource(path).getPath());
    }

    public void checkConfiguration() throws ConfigurationException {
        if (getStopWords().isEmpty())
            throw new ConfigurationException("stopwords not set in eclap.expansion.properties file");
    }

    public static ClassifierConfiguration getClassiferConfigurationInstance() {
        if (instance == null)
            instance = new ClassifierConfiguration();
        return instance;
    }

    public String getDocumentSRC() {
        return config.getString("documents.src");
    }
    public String getDocumentsDEST()
    {
        return config.getString("documents.dst");
    }
    public List getStopWordExclusion()
    {
        return config.getList("stop.word.exclusion");
    }
    public String getStopWords() {
        return config.getString("stopwords");
    }

    public String getBigHugeThesaurusApikey()
    {
        return config.getString("big.huge.thesaurus.api.key");
    }
    public String getBigHugeThesaurusApiUrl()
    {
        return config.getString("big.huge.thesaurus.api.url");
    }
    public String getBigHugeThesaurusApiVersion()
    {
        return config.getString("big.huge.thesaurus.api.version");
    }
    public String getThesaurusAltervistaApiKey()
    {
        return config.getString("thesaurus.altervista.api.key");
    }
    public String getThesaurusAltervistaApiEndPoint()
    {
        return config.getString("thesaurus.altervista.api.endpoint");
    }
    public String getStand4ApiUserId()
    {
        return config.getString("stand4.api.user.id");
    }
    public String getStand4ApiEndPoint()
    {
        return config.getString("stand4.api.endpoint");
    }
    public String getStand4ApiToken()
    {
        return config.getString("stand4.api.token");
    }
    public String getWikipediaEndPoint()
    {
        return config.getString("wikipedia.end.point");
    }
    public float getWikiPediaAprioriConfidence()
    {
        return config.getFloat("wikipedia.apriori.confidence");
    }
    public void setWikipediaAprioriConfidence(float value)
    {
        config.setProperty("wikipedia.apriori.confidence",value);
        try {
            config.save();
            config.reload();
        } catch (ConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    public String getConfigPath()
    {
        String decodedPath = null;

        try {
            String path = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();

            String jarpath = URLDecoder.decode(path, "UTF-8");
            File jarfile = new File(jarpath);
            decodedPath = jarfile.getParent();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return decodedPath;
    }


}
