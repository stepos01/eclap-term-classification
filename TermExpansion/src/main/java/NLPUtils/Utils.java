package NLPUtils; /**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/06/13
 * Time: 01:09
 * To change this template use File | Settings | File Templates.
 */

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


public class Utils {
    public static Charset ENCODING = StandardCharsets.UTF_8;
    public static final String TAGS = "TAGS";


    public static void writeLargerTextFile(String aFileName, List<String> aLines) {
        try {
            File f = new File(aFileName);
            f.createNewFile();
            Path path = Paths.get(aFileName);
            BufferedWriter writer = Files.newBufferedWriter(path, ENCODING);
            writer.write("");
            for (String line : aLines) {
                writer.write(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static ObjectArrayList<String> loadBigFile(String path) {
        ObjectArrayList<String> objectArrayList = new ObjectArrayList<String>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            while (line != null) {
                objectArrayList.add(line);
                line = br.readLine();
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
        return objectArrayList;
    }

}











