import DocumentsMapping.EclapLanguage;
import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.core.config.Configurator;
import queryExpansion.BigHugeThesaurus;


/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 20/02/2014
 * Time: 18:06
 * To change this template use File | Settings | File Templates.
 */
public class TestBigHugeThesaurus {
    public static void main(String[]args)
    {
        Configurator.initialize(null, null, ClassifierConfiguration.CONFIG_FOLDER + "log4j2.xml");
        ObjectArrayList<String> stringArrayList = BigHugeThesaurus.getInstance().getSimilar("morning", new EclapLanguage(EclapLanguage.english));
        System.out.println(StringUtils.join(stringArrayList,"|"));
    }
}
