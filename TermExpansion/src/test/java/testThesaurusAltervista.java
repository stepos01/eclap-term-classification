import DocumentsMapping.EclapLanguage;
import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.core.config.Configurator;
import queryExpansion.ThesaurusAltervista;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/02/2014
 * Time: 23:38
 * To change this template use File | Settings | File Templates.
 */
public class testThesaurusAltervista {
    public static void main(String[]args)
    {
        Configurator.initialize(null, null, ClassifierConfiguration.CONFIG_FOLDER + "log4j2.xml");
        ObjectArrayList<String> stringArrayList = ThesaurusAltervista.getInstance().getSimilar("actor", new EclapLanguage(EclapLanguage.english));
        System.out.println(StringUtils.join(stringArrayList, "|"));
    }
}
