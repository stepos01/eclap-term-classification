import DocumentsMapping.EclapLanguage;
import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.core.config.Configurator;
import queryExpansion.WordNetExpansion;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/02/2014
 * Time: 23:16
 * To change this template use File | Settings | File Templates.
 */
public class TestWordnet {
    public static void main(String[]args)
    {
        Configurator.initialize(null, null, ClassifierConfiguration.CONFIG_FOLDER + "log4j2.xml");
        ObjectArrayList<String> stringArrayList = WordNetExpansion.getInstance().getSimilar("actor", new EclapLanguage(EclapLanguage.english));
        System.out.println(StringUtils.join(stringArrayList, "|"));
    }
}
