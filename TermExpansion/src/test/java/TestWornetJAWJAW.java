import edu.cmu.lti.jawjaw.db.SenseDAO;
import edu.cmu.lti.jawjaw.db.SynsetDAO;
import edu.cmu.lti.jawjaw.db.WordDAO;
import edu.cmu.lti.jawjaw.pobj.POS;
import edu.cmu.lti.jawjaw.pobj.Sense;
import edu.cmu.lti.jawjaw.pobj.Word;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.Lesk;
import edu.cmu.lti.ws4j.impl.Lin;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;
import edu.cmu.lti.ws4j.util.WordSimilarityCalculator;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 27/02/2014
 * Time: 11:17
 * To change this template use File | Settings | File Templates.
 */
public class TestWornetJAWJAW {
    public static void main(String[]args)
    {
        ObjectArrayList<String> dog = getsense("dog");
        ObjectArrayList<String> house = getsense("house");
        System.out.println(dog);
        System.out.println(house);
        calculateRelatedNess("car#n#1", "bus#n#2");
    }
    public static ObjectArrayList<String> getsense(String target)
    {
        ObjectArrayList<String> senses = new ObjectArrayList<String>();
        List<Word> lemmas = WordDAO.findWordsByLemma(target);
        for (Word w : lemmas)
        {
              senses.addAll(getSensefromword(w));
        }
        return senses;
    }
    public static ObjectArrayList<String> getSensefromword(Word w)
    {
        ObjectArrayList<String> sense4Pos = new ObjectArrayList<String>();
        List<Sense> sensesByWordid = SenseDAO.findSensesByWordid(w.getWordid());
        for (Sense s: sensesByWordid)
        {
            String sense = w.getLemma().concat("#").concat(w.getPos().name()).concat("#").concat(String.valueOf(sensesByWordid.indexOf(s)));
            sense4Pos.add(sense);
        }
        return sense4Pos;

    }
    public static void calculateRelatedNess(String a,String b)
    {
        NictWordNet db = new NictWordNet();
        WS4JConfiguration.getInstance().setMFS(false);
        WordSimilarityCalculator wordSimilarityCalculator = new WordSimilarityCalculator();
        RelatednessCalculator lesk = new Lin(db);
        double v = lesk.calcRelatednessOfWords("theater", "absurd");
        System.out.println(v);
    }
}
