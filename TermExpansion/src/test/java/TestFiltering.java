import DocumentsMapping.*;
import NLPUtils.Utils;
import configuration.ClassifierConfiguration;
import filter.WikipediaFilter;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import queryExpansion.QueryExpansionProvider;
import queryExpansion.Stand4;
import queryExpansion.ThesaurusAltervista;
import queryExpansion.WordNetExpansion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 05/06/2014
 * Time: 23:22
 * To change this template use File | Settings | File Templates.
 */
public class TestFiltering {
    private static Logger logger = LogManager.getLogger();
    private static ExecutorService pool = Executors.newFixedThreadPool(5);
    private static ArrayList<QueryExpansionProvider> providers = new ArrayList<QueryExpansionProvider>();

    public static void main(String[] args) {

        countTags();

    }
    public static void filter()
    {
        try {
            ObjectArrayList<Category> en = loadDocument4Traduction("en", String.valueOf(0));
            float soglia = 5f;
            ClassifierConfiguration.getClassiferConfigurationInstance().setWikipediaAprioriConfidence(soglia/10);
            start(en,String.valueOf(soglia/10));

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    public static void countTags ()
    {
        for (int i = 0;i<6;i= i+1)
        {
            try {
                ObjectArrayList<Category> en = loadDocument4Traduction("en", String.valueOf(i));
                float size = 0;
                for (Category category : en)
                {
                    size= size+ (category.getCategoryTags()==null?0:category.getCategoryTags().getTags().size());
                }
                size = size/en.size();
                System.out.println("Soglia: "+i+"\t tags count: "+size);

            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }
    }
    public static void start(ObjectArrayList<Category>categoryList,String soglia)
    {
        WikipediaFilter wikipediaFilter = WikipediaFilter.getInstance();
        ObjectArrayList<Category> filteredCategory = new ObjectArrayList<Category>();
        System.out.println("running using soglia for wiki equals: "+ClassifierConfiguration.getClassiferConfigurationInstance().getWikiPediaAprioriConfidence());
        for (Category category : categoryList)
        {

            if (!category.isCategoryTagEmpty()){
                Category filter = wikipediaFilter.filter(category);
                filteredCategory.add(filter);
            }else
                filteredCategory.add(category);
        }
        File dest = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocumentsDEST());
        dest.mkdirs();
        logger.debug("writing to {}",dest.getPath());
        writeCategories(filteredCategory, new File(dest.getPath(),"en_expanded_filtered_soglia_"+soglia.replace(".","_")).getPath());

    }
    public static void addProviders(QueryExpansionProvider provider )
    {
        providers.add(provider);
        logger.trace("Add provider {}",provider.getClass().getName());
    }


    public static void writeCategories(ObjectArrayList<Category> categories, String path) {
        ObjectArrayList<String> lines = new ObjectArrayList<String>();
        for (Category category : categories) {
            lines.add(category.toString());
        }
        Utils.writeLargerTextFile(path, lines);
        logger.trace("Created doc " + path);
    }


    public static ObjectArrayList<Category> loadDocument4Traduction(String lang,String soglia) throws IOException {
        File dest = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocumentsDEST(),lang + "_expanded_filtered_soglia_0_"+soglia);
        ObjectArrayList<Category> categories = new ObjectArrayList<Category>();
        if (!dest.exists()) {
            throw logger.throwing(Level.ERROR, new IOException("error using file: " + dest.getPath()));
        } else {
            ObjectArrayList<String> list = loadBigFile(dest.getPath());
            for (String line : list) {
                categories.add(parseTextLine(line, lang));
            }
            return categories;
        }
    }
    private static Category parseTextLine(String line, String lang) {
        CategoryTags categoryTags = new CategoryTags(new EclapLanguage(lang));
        String[] block = line.split("\t");
        String name = block[0];
        if (block.length == 1) return new Category(name);
        String[] tags = block[1].split("\\|");
        for (String tag : tags) {
            String tagPart1 = tag.split(",")[0];
            tagPart1 = tagPart1.replace("{", "").trim();
            String tagPart2 = tag.split(",")[1];
            tagPart2 = tagPart2.replace("}", "").trim();
            Integer count = Integer.valueOf(tagPart2);
            for (int i = 0; i < count; i++) {
                categoryTags.addTag(tagPart1);
            }
        }
        return new Category(name, categoryTags);
    }

    private static ObjectArrayList<String> loadBigFile(String path) {
        ObjectArrayList<String> objectArrayList = new ObjectArrayList<String>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            while (line != null) {
                objectArrayList.add(line);
                line = br.readLine();
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
        return objectArrayList;
    }


}
