import DocumentsMapping.EclapLanguage;
import configuration.ClassifierConfiguration;
import it.unimi.dsi.fastutil.Hash;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.core.config.Configurator;
import queryExpansion.BigHugeThesaurus;
import queryExpansion.Stand4;
import queryExpansion.ThesaurusAltervista;
import queryExpansion.WordNetExpansion;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/02/2014
 * Time: 23:33
 * To change this template use File | Settings | File Templates.
 */
public class TestStand4 {
    public static void main(String[]args)
    {
        Configurator.initialize(null, null, ClassifierConfiguration.CONFIG_FOLDER + "log4j2.xml");
        ObjectArrayList<String> stringArrayList = Stand4.getInstance().getSimilar("theater", new EclapLanguage(EclapLanguage.english));
        stringArrayList.addAll(ThesaurusAltervista.getInstance().getSimilar("theater", new EclapLanguage(EclapLanguage.english)));
        stringArrayList.addAll(BigHugeThesaurus.getInstance().getSimilar("theater", new EclapLanguage(EclapLanguage.english)));
        stringArrayList.addAll(WordNetExpansion.getInstance().getSimilar("theater", new EclapLanguage(EclapLanguage.english)));
        HashMap<String,Integer> result = new HashMap<String, Integer>();
        for (String s : stringArrayList)
        {
            int i = 0;
            if (result.containsKey(s))
            {
                i = result.remove(s);
            }
            result.put(s,i+1);
        }
        System.out.println(result);

    }
}
