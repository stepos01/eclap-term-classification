import configuration.ClassifierConfiguration;
import filter.WikipediaFilter;
import filter.WikipediaSearch;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.logging.log4j.core.config.Configurator;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/02/2014
 * Time: 23:59
 * To change this template use File | Settings | File Templates.
 */
public class TestWikipediaSearch {
    private static String tag = "morn|morning time|forenoon|time period|period of time|period|good morning|greeting|salutation|farewell|word of farewell|dawn|dawning|aurora|first light|daybreak|break of day|break of the day|dayspring|sunrise|sunup|cockcrow|hour|time of day|dawn|start";
    public static void main(String[]args)
    {
        Configurator.initialize(null, null, ClassifierConfiguration.CONFIG_FOLDER + "log4j2.xml");
        int score = WikipediaSearch.getInstance().search("acrobat");
        System.out.println("score: "+score);
        ObjectArrayList<String> tags = new ObjectArrayList<String>(tag.split("\\|"));
        for (String s : tags)
        {
            int tagscore = WikipediaFilter.getInstance().computeScore("morning", new ObjectArrayList<String>(s.split(" ")));
            float confidence = ClassifierConfiguration.getClassiferConfigurationInstance().getWikiPediaAprioriConfidence();
            float result = (float)tagscore/score;
            if (result >= confidence)
                System.err.println(s+" passed with "+result*100);
            else System.err.println(s+" failed with "+result*100);

        }

    }
}
