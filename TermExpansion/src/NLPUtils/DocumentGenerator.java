/*package NLPUtils; *//**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 23/06/13
 * Time: 23:47
 * To change this template use File | Settings | File Templates.
 */
/*

import ClassifierLanguage;
import edu.mit.jwi.item.POS;
import it.uniroma1.lcl.babelnet.*;
import it.uniroma1.lcl.jlt.util.Language;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class DocumentGenerator {
    private BabelNet bable = null;
    private WordNetAccessor wordNetAccessor;

    public DocumentGenerator() {
        bable = BabelNet.getInstance();
        wordNetAccessor = WordNetAccessor.getInstance();
    }

    private ArrayList<String> exploreWIKI(BabelSynset synset, ClassifierLanguage lang, boolean allowMultiplesWords) throws IOException {
        ArrayList<String> tags = new ArrayList<String>();
        List<BabelSense> senses = synset.getSenses(Language.valueOf(lang.getLanguage().toUpperCase()));
        for (BabelSense sense : senses)
        {
            if (sense.getLanguage().equals(Language.valueOf(lang.getLanguage().toUpperCase())))
            {
                ArrayList<String> lemmas = Utils.tokenizeString(Utils.getCustomAnalyser4Language(lang), sense.getSimpleLemma());
                if (allowMultiplesWords) {
                    tags.add(StringUtils.join(lemmas," "));
                } else if (lemmas.size()==1&&!lemmas.get(0).isEmpty()) {
                    tags.add(lemmas.get(0));
                }
            }
        }
        return tags;
    }

    private ArrayList<String> exploreWN(BabelSynset synset, String lemma) throws Exception {
        ArrayList<String> glosses = new ArrayList<String>();
        for (String offset : synset.getWordNetOffsets()) {
            BableNetMap bableNetMap = new BableNetMap(lemma, synset.getPOS(), offset);
            ArrayList<Tag> tags = wordNetAccessor.start(lemma);
            for (Tag tag : tags)
            {
                ArrayList<String> lemmas = Utils.tokenizeString(Utils.getCustomAnalyser4Language(new ClassifierLanguage(ClassifierLanguage.english)), tag.getLemma());
                if (lemmas.size() == 1&&!lemmas.get(0).isEmpty())
                {
                    glosses.add(tag.getLemma());
                }
            }
        }
        return glosses;
    }

    public ArrayList<String> expandTerm(String lemma, POS pos, ClassifierLanguage lang) throws Exception {
        ArrayList<String> tags = new ArrayList<String>();
        ArrayList<SynsetMap> synsetMapArrayList = new ArrayList<SynsetMap>();
        List<BabelSynset> synsets = bable.getSynsets(Language.valueOf(lang.getLanguage().toUpperCase()), lemma, pos,false,BabelSenseSource.WN,BabelSenseSource.WIKI);
        if (!synsets.isEmpty()) {
            for (BabelSynset synset : synsets)
            {
                SynsetMap synsetMap = new SynsetMap(lemma);
                if (synset.getSynsetSource().equals(BabelSynsetSource.WIKI))
                {
                    synsetMap.setSource(BabelSynsetSource.WIKI);
                    synsetMap.addAllWordForms(exploreWIKI(synset, lang, lemma.replace("_","").split(" ").length > 1));
                } else if (synset.getSynsetSource().equals(BabelSynsetSource.WN))
                {
                    if (lang.equals(new ClassifierLanguage(ClassifierLanguage.english)))
                    {
                        synsetMap.setSource(BabelSynsetSource.WN);
                        synsetMap.addAllWordForms(exploreWN(synset, lemma));
                    }
                } else
                {
                    synsetMap.setSource(BabelSynsetSource.WIKIWN);
                    synsetMap.addAllWordForms(exploreWIKI(synset, lang, lemma.replace("_","").split(" ").length > 1));
                    if (lang.equals(new ClassifierLanguage(ClassifierLanguage.english))) synsetMap.addAllWordForms(exploreWN(synset, lemma));
                }
                synsetMapArrayList.add(synsetMap);
            }
        }
        for (SynsetMap synsetMap : synsetMapArrayList)
        {
            tags.addAll(synsetMap.getWordForm());
        }
        return tags;
    }
    public ArrayList<String> fastExpandTerm(String lemma,ClassifierLanguage lang)
    {
        ArrayList<String> tags = new ArrayList<String>();
        bable = BabelNet.getInstance();
        try {
            List<BabelSynset> synsets = bable.getSynsets(Language.valueOf(lang.getLanguage().toUpperCase()), lemma);
            List<BabelSynset> concept_synset = new ArrayList<BabelSynset>();
            for (BabelSynset synset : synsets)
            {
                if (synset.getSynsetType().equals(BabelSynsetType.CONCEPT))concept_synset.add(synset);
            }
            for (BabelSynset synset : concept_synset)
            {
                List<BabelSense> wiki = new ArrayList<BabelSense>();
                wiki.addAll(synset.getSenses(BabelSenseSource.WIKI));
                wiki.addAll(synset.getSenses(BabelSenseSource.WIKIRED));
                wiki.addAll(synset.getSenses(BabelSenseSource.WN));
                for (BabelSense sense : wiki)
                {
                    if (sense.getLanguage().equals(Language.valueOf(lang.getLanguage().toUpperCase())))tags.add(sense.getSimpleLemma());
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
       return tags;
    }*/
    /*private void generateTopCategory(ClassifierLanguage language){
        ArrayList<Category> categories = MysqlAccessor.getMysqlAccessorSingleton().getTaxonomyCategoryArrayList();
        for (Category category : categories)
        {
            try {
                Category parent = MysqlAccessor.getMysqlAccessorSingleton().getParentFromId(category.getTid());
                if (parent!=null)
                {
                    String parentName = MysqlAccessor.getMysqlAccessorSingleton().getTopLevelCategory(category);
                    TopCategory currentTopcategory = null;
                    Iterator<TopCategory> it = topCategories.iterator();
                    while (it.hasNext()){
                        TopCategory topCategory = it.next();
                        if (topCategory.getName().equals(parentName)){
                            currentTopcategory = topCategory;
                            it.remove();
                            break;
                        }
                    }
                    if (currentTopcategory == null) currentTopcategory = new TopCategory(parentName);
                    String traducedName = MysqlAccessor.getMysqlAccessorSingleton().getTraductionForTaxonomyTerm(category.getName(),language);
                    System.out.println("Loading tags for: "+category.getName() +"... ");
                    ArrayList<String> tags = loadTerms(category.getName());
                    if (!tags.isEmpty()) currentTopcategory.addCategories(new Category(traducedName,category.getTid()));




                    for (TopCategory topCategory : topCategories){
                        if (topCategory.getType().toString().replaceAll("_"," ").equals(parentName)){
                            foundParent = true;
                            try {
                                System.out.println("Loading tags 4: "+taxonomyCategory.getCategoryName() +"... ");
                                ArrayList<String> tags = loadTags(taxonomyCategory.getCategoryName());
                                if (!tags.isEmpty())topCategory.addCategories(taxonomyCategory.getCategoryName(),tags);
                                else {
                                    System.err.println("no tags found for "+taxonomyCategory.getCategoryName());
                                    NLPUtils.write2LogFile("no tags found for "+taxonomyCategory.getCategoryName()+"...won't be inserted into the training set");
                                }

                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }finally {
                            }
                        }
                    }
                    if (!foundParent)throw new AssertionError("parent were not found 4 category: "+taxonomyCategory.getCategoryName());
                }
            }catch (Exception e)
            {
            e.printStackTrace();
            }
        }

        }
    private ArrayList<String> splitCategoryTerm(String term)
    {
        ArrayList<String> names = new ArrayList<String>();
        boolean done = false;
        if (term.contains(" & "))
        {
            names.add(Utils.encodeCategoryName(term));
            String[] splits = term.split("&");
            for (String s : splits)
            {
                names.add(Utils.encodeCategoryName(s.trim()));
            }
            done = true;
        }
        if (done==false && term.contains(" and "))
        {
            names.add(Utils.encodeCategoryName(term));
            String[] splits = term.split("and");
            for (String s : splits){
                names.add(Utils.encodeCategoryName(s.trim()));
                done = true;
            }
        }
        if (done==false && term.split(" ").length==1)
        {
            names.add(term);
            done = true;
        }
        if (done==false && term.split(" ").length>1)
        {
            names.add(Utils.encodeCategoryName(term));
            String[] parts = Utils.removeDoubleSpace(term).split(" ");
            ArrayList<String> startWithStopWords = bable.start(newcategoryName, POS.NOUN);
            if (startWithStopWords.isEmpty()){
                newcategoryName =  opennlp.removeStopWord(categoryName);
                splits = newcategoryName.split(" ");
                for (int i = 0;i <splits.length;i++){
                    splits[i] = splits[i].trim();
                }
                newcategoryName = StringUtils.join(splits, "_");
                tags.addAll(bable.start(newcategoryName, POS.NOUN));
            }else {
                tags.addAll(startWithStopWords);
            }

        }
        return names;
    }
    private ArrayList<String> generateTags(String categoryName) throws Exception {
        ArrayList<String> tags = new ArrayList<String>();
        boolean done = false;
        if (categoryName.contains("&")){
            String[] splits = categoryName.split("&");
            for (String s : splits){
                tags.addAll(expandTerm(s.trim(), POS.NOUN));
                done = true;
            }
        }
        if (done==false&&categoryName.contains("and")){
            String[] splits = categoryName.split("and");
            for (String s : splits){
                tags.addAll(bable.start(s.trim(), POS.NOUN));
                done = true;
            }
        }
        if (done==false&&categoryName.split(" ").length==1){
            tags.addAll(bable.start(categoryName, POS.ADJECTIVE));
            tags.addAll(bable.start(categoryName, POS.NOUN));
            tags.addAll(bable.start(categoryName, POS.VERB));
            done = true;
        }
        if (done==false&&categoryName.split(" ").length>1){
            String[] splits = categoryName.split(" ");
            for (int i = 0;i <splits.length;i++){
                splits[i] = splits[i].trim();
            }
            String newcategoryName = StringUtils.join(splits, "_");
            ArrayList<String> startWithStopWords = bable.start(newcategoryName, POS.NOUN);
            if (startWithStopWords.isEmpty()){
                newcategoryName =  opennlp.removeStopWord(categoryName);
                splits = newcategoryName.split(" ");
                for (int i = 0;i <splits.length;i++){
                    splits[i] = splits[i].trim();
                }
                newcategoryName = StringUtils.join(splits, "_");
                tags.addAll(bable.start(newcategoryName, POS.NOUN));
            }else {
                tags.addAll(startWithStopWords);
            }

        }
        return tags;
    }


}*/
