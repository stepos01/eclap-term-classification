package NLPUtils;

import Configuration.ClassifierConfiguration;
import DocumentsMapping.ClassifierLanguage;
import edu.smu.tspell.wordnet.*;
import edu.smu.tspell.wordnet.Synset;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 03/10/13
 * Time: 16:40
 * To change this template use File | Settings | File Templates.
 */
public class WordNetAccessor {
    static private String CONFIG_DIR = "TermExpansion/config/";
    static public String CONFIG_FILE = "jawsLibrary.properties";
    private WordNetDatabase database = null;
    private static int SEARCH_DEPTH;
    public final static String language = ClassifierLanguage.english;
    private static WordNetAccessor instance = null;

    private WordNetAccessor() {
        try {
            SEARCH_DEPTH = ClassifierConfiguration.getClassiferConfigurationInstance().getTermExpansionDepth();
            setConfiJaws();
            database = WordNetDatabase.getFileInstance();
        } catch (ConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static WordNetAccessor getInstance() {
        if (instance == null) instance = new WordNetAccessor();
        return instance;
    }

    private void setConfiJaws() throws ConfigurationException {
        File configFile = new File(CONFIG_DIR, CONFIG_FILE);
        PropertiesConfiguration config = null;
        boolean bDone = false;
        if (configFile.exists()) {
            System.out.println("Loading " + CONFIG_FILE + " FROM " + configFile.getAbsolutePath());
            try {
                config = new PropertiesConfiguration(configFile);
                bDone = true;
                String wordNetDir = config.getString("jaws.wordnetdictdir");
                System.setProperty("wordnet.database.dir", wordNetDir);
            } catch (ConfigurationException ce) {
                ce.printStackTrace();
            }
        }
        if (!bDone) {
            throw new ConfigurationException("jaws configuration files is not set");
        }
    }


    public ArrayList<Tag> start(String lemma) throws Exception {
        ArrayList<String> tags = new ArrayList<String>();
        Synset[] synsets = database.getSynsets(lemma);
        for (Synset synset : synsets) {
            ArrayList<String> list = exploreSynset(synset);
            tags.addAll(list);
        }
        return Utils.orderTags(tags);
    }

    private ArrayList<String> exploreSynset(Synset synset) {
        ArrayList<String> tags = new ArrayList<String>();
        int depth = 0;
        ArrayList<Synset> list2Explore = new ArrayList<Synset>();
        list2Explore.add(synset);
        while (!list2Explore.isEmpty() && depth < SEARCH_DEPTH) {
            ArrayList<Synset> tempList = new ArrayList<Synset>(list2Explore);
            for (Synset currentSynset : tempList) {
                SynsetType type = synset.getType();
                if (type.equals(SynsetType.ADJECTIVE)) {
                    String[] wordform = currentSynset.getWordForms();
                    ArrayList<String> related = getRelatedAdjectives(((AdjectiveSynset) currentSynset));
                    System.out.println("wordform: " + Arrays.asList(wordform) + "\t" + "relatedForms: " + related);
                    tags.addAll(related);
                    tags.addAll(Arrays.asList(wordform));
                    list2Explore.addAll(new ArrayList<Synset>(Arrays.asList(((AdjectiveSynset) currentSynset).getRelated())));
                    list2Explore.addAll(new ArrayList<Synset>(Arrays.asList(((AdjectiveSynset) currentSynset).getSimilar())));
                } else if (type.equals(SynsetType.VERB)) {
                    String[] wordform = currentSynset.getWordForms();
                    ArrayList<String> related = getRelated(currentSynset);
                    System.out.println("wordform: " + Arrays.asList(wordform) + "\t" + "relatedForms: " + related);
                    tags.addAll(related);
                    tags.addAll(Arrays.asList(wordform));
                } else if (type.equals(SynsetType.ADVERB)) {
                    String[] wordform = currentSynset.getWordForms();
                    ArrayList<String> related = getRelatedAdverb(((AdverbSynset) currentSynset));
                    System.out.println("wordform: " + Arrays.asList(wordform) + "\t" + "relatedForms: " + related);
                    tags.addAll(related);
                    tags.addAll(Arrays.asList(wordform));
                } else if (type.equals(SynsetType.NOUN)) {
                    String[] wordform = currentSynset.getWordForms();
                    ArrayList<String> related = getRelated(currentSynset);
                    System.out.println("wordform: " + Arrays.asList(wordform) + "\t" + "relatedForms: " + related);
                    tags.addAll(related);
                    tags.addAll(Arrays.asList(wordform));

                }
                //we delete the synset we just explore and go on to the next one XD
                boolean found = false;
                Iterator<Synset> it = list2Explore.iterator();
                while (it.hasNext()) {
                    if (it.next().equals(currentSynset)) {
                        found = true;
                        it.remove();
                        break;
                    }
                }
                assert found == true;
            }
            depth++;
        }
        return tags;
    }


    private ArrayList<String> getRelatedAdverb(AdverbSynset synset) {
        ArrayList<String> list = new ArrayList<String>();
        for (String s : new ArrayList<String>(Arrays.asList(synset.getWordForms()))) {
            WordSense[] senses = synset.getPertainyms(s);
            ArrayList<String> sense = new ArrayList<String>();
            for (int i = 0; i < senses.length; i++) {
                sense.add(senses[i].getWordForm());
            }
            list.addAll(sense);
        }
        return list;
    }

    private ArrayList<String> getRelated(Synset synset) {
        ArrayList<String> list = new ArrayList<String>();
        for (String s : new ArrayList<String>(Arrays.asList(synset.getWordForms()))) {
            WordSense[] derivationallyRelatedForms = synset.getDerivationallyRelatedForms(s);
            ArrayList<String> sense = new ArrayList<String>();
            for (int i = 0; i < derivationallyRelatedForms.length; i++) {
                sense.add(derivationallyRelatedForms[i].getWordForm());
            }
            list.addAll(sense);
        }
        return list;
    }

    private ArrayList<String> getRelatedAdjectives(AdjectiveSynset synset) {
        ArrayList<String> list = new ArrayList<String>();
        for (String s : new ArrayList<String>(Arrays.asList(synset.getWordForms()))) {
            WordSense[] senses = synset.getPertainyms(s);
            ArrayList<String> sense = new ArrayList<String>();
            for (int i = 0; i < senses.length; i++) {
                sense.add(senses[i].getWordForm());
            }
            list.addAll(sense);
        }
        return list;
    }

}
