package NLPUtils; /**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 21/06/13
 * Time: 01:09
 * To change this template use File | Settings | File Templates.
 */

import Configuration.ClassifierConfiguration;
import DocumentsMapping.Category;
import DocumentsMapping.CategoryTags;
import DocumentsMapping.ClassifierLanguage;
import DocumentsMapping.SubCategory;
import LightAnalyzer.*;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;


public class Utils {
    public static Charset ENCODING = StandardCharsets.UTF_8;
    public static final String PARTS = "PARTS";
    public static final String TAGS = "TAGS";


    public static void writeLargerTextFile(String aFileName, List<String> aLines) {
        try {
            Path path = Paths.get(aFileName);
            BufferedWriter writer = Files.newBufferedWriter(path, ENCODING);
            writer.write("");
            for (String line : aLines) {
                writer.write(line);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> tokenizeString(Analyzer analyzer, String string) {
        ArrayList<String> result = new ArrayList<String>();
        try {
            TokenStream stream = analyzer.tokenStream(null, new StringReader(string));
            stream.reset();
            while (stream.incrementToken()) {
                result.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
        } catch (IOException e) {
            // not thrown b/c we're using a string reader...
            throw new RuntimeException(e);
        }
        return result;
    }


    public static ObjectBigArrayBigList<String> loadBigFile(String path) {
        ObjectBigArrayBigList<String> objectBigArrayBigList = new ObjectBigArrayBigList<String>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            while (line != null) {
                objectBigArrayBigList.add(line);
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        return objectBigArrayBigList;
    }

    public static String formatParameter(String s) {
        return "'" + s + "'";
    }


    public static String removeDoubleSpace(String str) {
        String cleaned = str.replaceAll("\\s+", " ");
        return cleaned;
    }


    public static String deAccentuate(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

    public static String createDocument(String path) {
        File file = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocuments(), path);
        if (!file.exists()) file.mkdir();
       /* File parts = new File(file,PARTS);
        File tags = new File(file,TAGS);
        try {
            if (!parts.exists())parts.createNewFile();
            if (!tags.exists())tags.createNewFile();
        }catch (IOException e)
        {
            e.printStackTrace();
        }*/
        return file.getPath();
    }

    public static Analyzer getCustomAnalyser4Language(ClassifierLanguage language) {
        Analyzer analyzer = null;
        if (language.getLanguage().equals(ClassifierLanguage.french))
            analyzer = new FrenchLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.english))
            analyzer = new EnglishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.italian))
            analyzer = new ItalianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.portuguese))
            analyzer = new PortugueseLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.polish))
            analyzer = new PolishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.dutch))
            analyzer = new DutchLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.spanish))
            analyzer = new SpanishLightAnalyser(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.hungarian))
            analyzer = new HungarianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.german))
            analyzer = new GermanLightanalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.danish))
            analyzer = new DanishLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.catalan))
            analyzer = new CatalanLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.slovene))
            analyzer = new SlovenianLightAnalyzer(Version.LUCENE_44);
        else if (language.getLanguage().equals(ClassifierLanguage.greek))
            analyzer = new GreekLightAnalyser(Version.LUCENE_44);
        return analyzer;
    }

    public static Category parseEnglishDocument(String line, String language, String fileName) {
        ArrayList<Tag> tags = new ArrayList<Tag>();
        String[] blocks = line.split("\t");
        Category category = null;
        if (fileName.equals(TAGS)) {
            String name = blocks[0];
            String[] tagsString = null;
            if (blocks.length > 1) {
                tagsString = blocks[1].split(";");
                for (String t : tagsString) {
                    t = t.replace("(", "");
                    t = t.replace(")", "");
                    Tag tag = new Tag(Integer.valueOf(t.split(",")[1]), t.split(",")[0]);
                    tags.add(tag);
                }
            }
            category = new Category(name, new CategoryTags(new ClassifierLanguage(language), tags));
        } else {
            String name = blocks[0];
            String partName = blocks[1];
            String[] tagsString = null;
            if (blocks.length > 2) {
                tagsString = blocks[2].split(";");
                for (String t : tagsString) {
                    t = t.replace("(", "");
                    t = t.replace(")", "");
                    Tag tag = new Tag(Integer.valueOf(t.split(",")[1]), t.split(",")[0]);
                    tags.add(tag);
                }
            }
            category = new SubCategory(name, partName, new CategoryTags(new ClassifierLanguage(language), tags));
        }
        return category;
    }

    public static ArrayList<Tag> orderTags(ArrayList<String> tags) {
        ArrayList<Tag> orderedTags = new ArrayList<Tag>();
        for (String t : tags) {
            int i = 0;
            for (String s : tags) {
                if (cleanString(t).equals(cleanString(s))) {
                    i++;
                }
            }
            boolean exist = false;
            for (Tag tag : orderedTags) {
                if (tag.getLemma().equals(t)) {
                    exist = true;
                    break;
                }
            }
            if (exist == false) orderedTags.add(new Tag(i, t));
        }
        //verification
        Set<String> set = new TreeSet<String>(tags);
        assert set.size() == tags.size();
        return orderedTags;
    }

    private static String cleanString(String t) {
        return t.trim().toLowerCase().replace(";","");
    }

}










