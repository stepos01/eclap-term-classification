package translationApi; /**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 25/05/13
 * Time: 16:01
 * To change this template use File | Settings | File Templates.
 */

import com.google.gson.Gson;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

public class MicrosoftTranslateAuthentication {
    private static final String SCOPE = "http://api.microsofttranslator.com ";
    private static final String GRANT_TYPE = "client_credentials";
    private AccessToken accessToken;
    private static final String charset = "UTF-8";
    private String response;
    private final static String URL_ENDPOINT = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13";
    private String request_parameters;

    public MicrosoftTranslateAuthentication(String client_id, String client_secret) {
        try {
            request_parameters = "client_id=" + URLEncoder.encode(client_id, charset) + "&" +
                    "client_secret=" + URLEncoder.encode(client_secret, charset) + "&" +
                    "scope=" + SCOPE + "&" + "grant_type=" + GRANT_TYPE;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void postRequest() {
        URLConnection connection = null;
        try {
            connection = new URL(URL_ENDPOINT).openConnection();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        connection.setDoOutput(true); // Triggers POST.
        connection.setRequestProperty("Accept-Charset", charset);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
        OutputStream output = null;
        try {
            output = connection.getOutputStream();
            output.write(request_parameters.getBytes(charset));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            if (output != null)
                try {
                    output.close();
                } catch (IOException logOrIgnore) {
                }
        }
        HttpURLConnection new_connection = (HttpURLConnection) connection;
        try {
            int status = new_connection.getResponseCode();
            if (status == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                response = sb.toString();
                processResponse();

            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void processResponse() {
        accessToken = new Gson().fromJson(response, AccessToken.class);
        accessToken.setLimitTime();
    }

    private void setUpMyTimer() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                postRequest();
            }
        }, 9 * 60 * 1000, 9 * 60 * 1000);
    }

    public AccessToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(AccessToken accessToken) {
        this.accessToken = accessToken;
    }
}
