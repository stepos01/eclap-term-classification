package DocumentsMapping;

import NLPUtils.Tag;
import NLPUtils.Utils;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 24/09/13
 * Time: 17:04
 * To change this template use File | Settings | File Templates.
 */
public class CategoryTags {
    private ClassifierLanguage language;
    private ArrayList<Tag> tags;

    public CategoryTags(ClassifierLanguage language, ArrayList<Tag> tags) {
        this.language = language;
        this.tags = new ArrayList<Tag>(tags);
    }

    public ClassifierLanguage getLanguage() {
        return language;
    }

    public void setLanguage(ClassifierLanguage language) {
        this.language = language;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public String cleanTags(String str) {
        String s = Utils.deAccentuate(str);
        s = s.replace("_", " ");
        //s = s.replaceAll("\\s+", " ");
        return s.trim();
    }

    public String toString() {
        ArrayList<String> r = new ArrayList<String>();
        for (Tag t : tags) {
            r.add(t.toString());
        }
        return StringUtils.join(r, ";");
    }


}
