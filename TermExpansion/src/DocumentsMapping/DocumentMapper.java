package DocumentsMapping;

import Configuration.ClassifierConfiguration;
import NLPUtils.Utils;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;

import java.io.File;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 05/10/13
 * Time: 15:09
 * To change this template use File | Settings | File Templates.
 */
public class DocumentMapper {
    private ArrayList<Category> categories;
    private ArrayList<Category> subcategories;
    private static DocumentMapper ourInstance = null;

    public static DocumentMapper getInstance() {
        if (ourInstance == null) ourInstance = new DocumentMapper();
        return ourInstance;
    }

    private DocumentMapper() {
        categories = new ArrayList<Category>();
        subcategories = new ArrayList<Category>();
        loadEnglishDocuments();

    }

    private void loadEnglishDocuments() {
        File file = new File(ClassifierConfiguration.getClassiferConfigurationInstance().getDocuments() + "/" + ClassifierLanguage.english);
        if (file.exists() && file.isDirectory()) {
            for (File doc : file.listFiles()) {
                if (doc.canRead() && !doc.isHidden()) {
                    ObjectBigArrayBigList<String> documents = Utils.loadBigFile(doc.getPath());
                    for (String line : documents) {
                        Category category = Utils.parseEnglishDocument(line, file.getName(), doc.getName());
                        if (category instanceof SubCategory) subcategories.add(category);
                        else categories.add(category);
                    }
                }
            }
        }
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public ArrayList<Category> getSubcategories() {
        return subcategories;
    }
}
