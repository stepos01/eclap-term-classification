package DocumentsMapping;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 25/09/13
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */
public class SubCategory extends Category {
    String subName;

    public SubCategory(String name, String subName, CategoryTags categoryTags) {
        super(name, categoryTags);
        this.subName = subName;
    }

    public String getSubName() {
        return subName;
    }

    public String toString() {
        return getName() + "\t" + subName + "\t" + getCategoryTags().toString();
    }
}
