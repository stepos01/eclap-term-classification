package Configuration;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.File;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: stephanekamga
 * Date: 16/07/13
 * Time: 07:59
 * To change this template use File | Settings | File Templates.
 */
public class ClassifierConfiguration {
    private static final ClassifierConfiguration instance = new ClassifierConfiguration();
    private org.apache.commons.configuration.PropertiesConfiguration config = null;
    private static final String CONFIG_FOLDER = "TermExpansion/config/";
    public static final String CONFIG_PATH = "eclap.classifier.properties";

    private ClassifierConfiguration() {
        File configFile = new File(CONFIG_FOLDER, CONFIG_PATH);
        System.out.println(CONFIG_FOLDER);

        boolean bDone = false;
        if (configFile.exists()) {
            System.out.println("Loading " + configFile + " FROM " + configFile.getAbsolutePath());
            try {
                this.config = new PropertiesConfiguration(configFile);
                bDone = true;
            } catch (ConfigurationException ce) {
                ce.printStackTrace();
            }
        }
        if (!bDone) {
            try {
                throw new ConfigurationException("the following configuration file  'eclap.classifier.properties is' not found");
            } catch (ConfigurationException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void checkConfiguration() throws ConfigurationException {
        if (getStopWords().isEmpty())
            throw new ConfigurationException("stopwords not set in eclap.classifier.properties file");
    }

    public static ClassifierConfiguration getClassiferConfigurationInstance() {
        return instance;
    }

    public String getConfigurationParameter(String parameter) {
        if (config == null) {
            throw new AssertionError("the following configuration file  'eclap.classifier.properties is' not found");
        }
        return config.getString(parameter);
    }

    public int getTermExpansionDepth() {
        return config.getInt("term.expansion.depth");
    }

    public String getDocuments() {
        return config.getString("documents");
    }

    public String getTermHierarchyDBTid() {
        return config.getString("term.hierarchy.db.tid");
    }

    public String getTermHierarchyDBParent() {
        return config.getString("term.hierarchy.db.parent");
    }

    public String getUrl() {
        return config.getString("url");
    }

    public String getTermDatatid() {
        return config.getString("term.table.tid");
    }

    ;

    public String getTermDataName() {
        return config.getString("term.table.name");
    }

    public String getTermDataVid() {
        return config.getString("term.table.vid");
    }

    public String getTermDB() {
        return config.getString("term.db");
    }

    public String getTermTable() {
        return config.getString("term.table");
    }

    public String getTermHierarchyDB() {
        return config.getString("term.hierarchy.db");
    }

    public String getTermHierarchyTable() {
        return config.getString("term.hierarchy.table");
    }

    public String getStopWords() {
        return config.getString("stopwords");
    }

    public String getTaxonomyLanguageDB() {
        return config.getString("taxonomy.language.db");
    }

    public String getTaxonomyLanguageTable() {
        return config.getString("taxonomy.language.table");
    }

    public String getLogFile() {
        return config.getString("log");
    }

    public List getLangagueList() {
        return config.getList("taxonomy.language.table.languages");
    }

    public String getMicrosoftTranslatorUserId() {
        return config.getString("microsot.client.id");
    }

    public String getMicrosoftTranslatorUserSecret() {
        return config.getString("microsoft.client.secret");
    }
}
